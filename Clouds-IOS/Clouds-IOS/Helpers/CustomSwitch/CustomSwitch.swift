//
//  CustomSwitch.swift
//  CustomSwitch
//
//  Created by Ivan Kovacevic on 15/12/2016.
//  Copyright © 2016 Ivan Kovacevic. All rights reserved.
//

import UIKit

public class CustomSwitch: UIControl {
    
    // MARK: Public properties
    public var animationDelay: Double = 0
    public var animationSpriteWithDamping = CGFloat(0.7)
    public var initialSpringVelocity = CGFloat(0.5)
    public var animationOptions: UIView.AnimationOptions = [UIView.AnimationOptions.curveEaseOut, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.allowUserInteraction]
    
    public var isOn:Bool = false
    
    public var animationDuration: Double = 0.5
    
    public var padding: CGFloat = 0 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    public var onTintColor: UIColor = UIColor(red: 144/255, green: 202/255, blue: 119/255, alpha: 1) {
        didSet {
            self.setupUI()
        }
    }
    
    public var offTintColor: UIColor = UIColor.black {
        didSet {
            self.setupUI()
        }
    }
    
    public var cornerRadiuss: CGFloat {
        
        get {
            return self.privateCornerRadius
        }
        set {
            if newValue > 0.5 || newValue < 0.0 {
                privateCornerRadius = 0.5
            } else {
                privateCornerRadius = newValue
            }
        }
        
    }
    
    public var borderWidthh: CGFloat = 0 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    public var borderColorr: CGColor = UIColor.black.cgColor {
        didSet {
            self.setupUI()
        }
    }
    
    private var privateCornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    // thumb properties
    public var thumbTintColor: UIColor = UIColor.white {
        didSet {
            self.thumbView.backgroundColor = self.thumbTintColor
        }
    }
    
    public var thumbCornerRadius: CGFloat {
        get {
            return self.privateThumbCornerRadius
        }
        set {
            if newValue > 0.5 || newValue < 0.0 {
                privateThumbCornerRadius = 0.5
            } else {
                privateThumbCornerRadius = newValue
            }
        }
        
    }
    
    private var privateThumbCornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
            
        }
    }
    
    public var thumbSize: CGSize = CGSize.zero {
        didSet {
            self.layoutSubviews()
        }
    }
    
    public var thumbImage:UIImage? = nil {
        didSet {
            guard let image = thumbImage else {
                return
            }
            thumbView.thumbImageView.image = image
        }
    }
    
    public var onThumbImage:UIImage? {
        didSet {
            self.onThumbImageView.image = onThumbImage?.withRenderingMode(.alwaysTemplate)
            onThumbImageView.tintColor = UIColor.black.withAlphaComponent(0.87)
            self.layoutSubviews()
        }
    }
    
    public var offThumbImage:UIImage? {
        didSet {
            self.offThumbImageView.image = offThumbImage?.withRenderingMode(.alwaysTemplate)
            offThumbImageView.tintColor = UIColor.black.withAlphaComponent(0.87)
            self.layoutSubviews()
        }
    }
    
    fileprivate(set) var selfImageView = UIImageView(frame: CGRect.zero)
    
    public var selfImage:UIImage? = nil {
        didSet {
            guard let image = selfImage else {
                return
            }
            self.selfImageView.image = image
        }
    }
    
    public var onSelfImage:UIImage? {
        didSet {
            onSelfImageView.image = onSelfImage?.withRenderingMode(.alwaysTemplate)
            onSelfImageView.tintColor = UIColor.black.withAlphaComponent(0.38)
            self.layoutSubviews()
        }
    }
    
    public var offSelfImage:UIImage? {
        didSet {
            offSelfImageView.image = offSelfImage?.withRenderingMode(.alwaysTemplate)
            offSelfImageView.tintColor = UIColor.black.withAlphaComponent(0.38)
            self.layoutSubviews()
        }
    }
    
    
    // dodati kasnije
    public var thumbShadowColor: UIColor = UIColor.black {
        didSet {
            self.thumbView.layer.shadowColor = self.thumbShadowColor.cgColor
        }
    }
    
    public var thumbShadowOffset: CGSize = CGSize(width: 0.0, height: 0) {
        didSet {
            self.thumbView.layer.shadowOffset = self.thumbShadowOffset
        }
    }
    
    public var thumbShaddowRadius: CGFloat = 0 {
        didSet {
            self.thumbView.layer.shadowRadius = self.thumbShaddowRadius
        }
    }
    
    public var thumbShaddowOppacity: Float = 0.0 {
        didSet {
            self.thumbView.layer.shadowOpacity = self.thumbShaddowOppacity
        }
    }
    
    // labels
    
    public var labelOff:UILabel = UILabel()
    public var labelOn:UILabel = UILabel()
    
    public var areLabelsShown: Bool = false {
        didSet {
            self.setupUI()
        }
    }
    
    public var thumbView = CustomThumbView(frame: CGRect.zero)
    public var onThumbImageView = UIImageView(frame: CGRect.zero)
    public var onSelfImageView = UIImageView(frame: CGRect.zero)
    public var offThumbImageView = UIImageView(frame: CGRect.zero)
    public var offSelfImageView = UIImageView(frame: CGRect.zero)
    public var onPoint = CGPoint.zero
    public var offPoint = CGPoint.zero
    public var isAnimating = false
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
}

// MARK: Private methods
extension CustomSwitch {
    fileprivate func setupUI() {
        // clear self before configuration
        self.clear()
        
        self.clipsToBounds = false
        
        // configure thumb view
        self.thumbView.backgroundColor = self.thumbTintColor
        self.thumbView.isUserInteractionEnabled = false
        
        // dodati kasnije
        self.thumbView.layer.shadowColor = self.thumbShadowColor.cgColor
        self.thumbView.layer.shadowRadius = self.thumbShaddowRadius
        self.thumbView.layer.shadowOpacity = self.thumbShaddowOppacity
        self.thumbView.layer.shadowOffset = self.thumbShadowOffset
        
        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        
        self.layer.borderColor = self.borderColorr
        
        self.addSubview(self.thumbView)
        self.addSubview(self.onThumbImageView)
        self.addSubview(self.offThumbImageView)
        self.addSubview(self.onSelfImageView)
        self.addSubview(self.offSelfImageView)
        
        self.setupLabels()
    }
    
    
    private func clear() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        self.animate()
        return true
    }
    
    func setOn(on:Bool, animated:Bool) {
        
        switch animated {
        case true:
            self.animate(on: on)
        case false:
            self.isOn = on
            self.setupViewsOnAction()
            self.completeAction()
        }
    }
    
    fileprivate func animate(on:Bool? = nil) {
        self.isOn = on ?? !self.isOn
        
        self.isAnimating = true
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [UIView.AnimationOptions.curveEaseIn, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.allowUserInteraction], animations: {
            self.setupViewsOnAction()
            
        }, completion: { _ in
            self.completeAction()
        })
    }
    
    private func setupViewsOnAction() {
        self.thumbView.frame.origin.x = self.isOn ? self.onPoint.x : self.offPoint.x
        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        self.setOnOffImageFrame()
    }

    private func completeAction() {
        self.isAnimating = false
        self.sendActions(for: UIControl.Event.valueChanged)
    }
    
}

// Mark: Public methods
extension CustomSwitch {
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if !self.isAnimating {
            self.layer.cornerRadius = self.bounds.size.height * self.cornerRadiuss
            self.layer.borderWidth = self.borderWidthh
            self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
            
            // thumb managment
            // get thumb size, if none set, use one from bounds
            let thumbSize = self.thumbSize != CGSize.zero ? self.thumbSize : CGSize(width: self.bounds.size.height, height: self.bounds.height)
            let yPostition = (self.bounds.size.height - thumbSize.height) / 2
            
            self.onPoint = CGPoint(x: self.bounds.size.width - thumbSize.width - self.padding, y: yPostition)
            self.offPoint = CGPoint(x: self.padding, y: yPostition)
            
            self.thumbView.frame = CGRect(origin: self.isOn ? self.onPoint : self.offPoint, size: thumbSize)
            self.thumbView.layer.cornerRadius = thumbSize.height * self.thumbCornerRadius
            
            
            //label frame
            if self.areLabelsShown {
                let labelWidth = self.bounds.width / 2 - self.padding * 2
                self.labelOn.frame = CGRect(x: 0, y: 0, width: labelWidth, height: self.frame.height)
                self.labelOff.frame = CGRect(x: self.frame.width - labelWidth, y: 0, width: labelWidth, height: self.frame.height)
            }
            
            
            
            self.selfImageView.frame = CGRect(x: 0, y: 0, width: self.thumbView.thumbImageView.frame.width, height: self.frame.height)
            self.selfImageView.layer.cornerRadius = self.layer.cornerRadius
            self.selfImageView.clipsToBounds = self.clipsToBounds
            // on/off images
            //set to preserve aspect ratio of image in thumbView
            
            guard onThumbImage != nil && offThumbImage != nil else {
                return
            }
            
            guard onSelfImage != nil && offSelfImage != nil else {
                return
            }
            
            let frameSize = thumbSize.width > thumbSize.height ? thumbSize.height * 0.7 : thumbSize.width * 0.7
            
            let onOffImageSize = CGSize(width: frameSize, height: frameSize)
            
            //zz
            self.onThumbImageView.frame.size = onOffImageSize
            self.offThumbImageView.frame.size = onOffImageSize
            
            self.onSelfImageView.frame.size = onOffImageSize
            self.offSelfImageView.frame.size = onOffImageSize
            
            self.onThumbImageView.center = CGPoint(x: self.onPoint.x + self.thumbView.frame.size.width / 2, y: self.thumbView.center.y)
            self.offThumbImageView.center = CGPoint(x: self.offPoint.x + self.thumbView.frame.size.width / 2, y: self.thumbView.center.y)
            
            self.onSelfImageView.center = CGPoint(x: self.onPoint.x - self.thumbView.frame.size.width / 2, y: self.thumbView.center.y)
            self.offSelfImageView.center = CGPoint(x: self.offPoint.x + self.thumbView.frame.size.width * 1.5, y: self.thumbView.center.y)
            
            self.onThumbImageView.alpha = self.isOn ? 1.0 : 0.0
            self.offThumbImageView.alpha = self.isOn ? 0.0 : 1.0
            
            self.onSelfImageView.alpha = self.isOn ? 1.0 : 0.0
            self.offSelfImageView.alpha = self.isOn ? 0.0 : 1.0
            
        }
    }
}

//Mark: Labels frame
extension CustomSwitch {
    
    fileprivate func setupLabels() {
        guard self.areLabelsShown else {
            self.labelOff.alpha = 0
            self.labelOn.alpha = 0
            return
            
        }
        
        self.labelOff.alpha = 1
        self.labelOn.alpha = 1
        
        let labelWidth = self.bounds.width / 2 - self.padding * 2
        self.labelOn.frame = CGRect(x: 0, y: 0, width: labelWidth, height: self.frame.height)
        self.labelOff.frame = CGRect(x: self.frame.width - labelWidth, y: 0, width: labelWidth, height: self.frame.height)
        self.labelOn.font = UIFont.boldSystemFont(ofSize: 12)
        self.labelOff.font = UIFont.boldSystemFont(ofSize: 12)
        self.labelOn.textColor = UIColor.white
        self.labelOff.textColor = UIColor.white
        
        self.labelOff.sizeToFit()
        self.labelOff.text = "Off"
        self.labelOn.text = "On"
        self.labelOff.textAlignment = .center
        self.labelOn.textAlignment = .center
        
        self.insertSubview(self.labelOff, belowSubview: self.thumbView)
        self.insertSubview(self.labelOn, belowSubview: self.thumbView)
        
    }
    
}

//Mark: Animating on/off images
extension CustomSwitch {
    
    fileprivate func setOnOffImageFrame() {
        guard onThumbImage != nil && offThumbImage != nil else {
            return
        }
        
        guard onSelfImage != nil && offSelfImage != nil else {
            return
        }
        //zzz
        self.onThumbImageView.center.x = self.isOn ? self.onPoint.x + self.thumbView.frame.size.width / 2 : self.frame.width
        self.offThumbImageView.center.x = !self.isOn ? self.offPoint.x + self.thumbView.frame.size.width / 2 : 0
        self.onThumbImageView.alpha = self.isOn ? 1.0 : 0.0
        self.offThumbImageView.alpha = self.isOn ? 0.0 : 1.0
        
        self.onSelfImageView.center.x = self.isOn ? self.onPoint.x - self.thumbView.frame.size.width / 2 : self.frame.width
        self.offSelfImageView.center.x = !self.isOn ? self.offPoint.x + self.thumbView.frame.size.width * 1.5 : 0
        self.onSelfImageView.alpha = self.isOn ? 1.0 : 0.0
        self.offSelfImageView.alpha = self.isOn ? 0.0 : 1.0
    }
}
