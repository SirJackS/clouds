//
//  Font.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import UIKit

extension UIFont {
    
    enum Font: String {
        case SFProDisplay = "SFProDisplay"
    }

    enum FontWeight: String {
        case black = "-Black"
        case bold = "-Bold"
        case semibold = "-Semibold"
        case light = "-Light"
        case ultralight = "-Ultralight"
        case medium = "-Medium"
        case heavy = "-Heavy"
        case thin = "-Thin"
        case regular = "-Regular"
    }

    convenience init?(ofSize fontSize: CGFloat) {
        self.init(name: .SFProDisplay, weight: .regular, size: fontSize)
    }
    
    convenience init?(name: Font = .SFProDisplay, weight: FontWeight, size: CGFloat) {
        self.init(name: "\(name.rawValue)\(weight.rawValue)", size: size)
    }
   
    convenience init?(_ weight: FontWeight, size: CGFloat) {
        self.init(name: .SFProDisplay, weight: weight, size: size)
    }
}

//extension UIFont {
//
//    enum Font: String {
//        case system = "System"
//        case avenir = "Avenir"
//    }
//
//    enum FontWeight: String {
//        case black = "Black"
//        case bold = "Bold"
//        case book = "Book"
//        case italic = "Italic"
//        case light = "Light"
//        case medium = "Medium"
//        case heavy = "Heavy"
//        case regular = ""
//    }
//
//    convenience init?(ofSize fontSize: CGFloat) {
//        self.init(name: .system, weight: .regular, size: fontSize)
//    }
//
//    convenience init?(name: Font = .system, weight: FontWeight, size: CGFloat) {
//        self.init(name: "\(name)-\(weight)", size: size)
//    }
//
//    convenience init?(_ weight: FontWeight, size: CGFloat) {
//        self.init(name: .system, weight: weight, size: size)
//    }
//}

