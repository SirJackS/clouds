//
//  UINavigationBar.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 06.01.21.
//

import UIKit

extension UINavigationBar {
    func toggle() {
        if self.layer.zPosition == -1 {
            self.layer.zPosition = 0
            self.isUserInteractionEnabled = true
        } else {
            self.layer.zPosition = -1
            self.isUserInteractionEnabled = false
        }
    }
}
