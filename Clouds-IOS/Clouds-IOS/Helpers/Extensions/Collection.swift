//
//  Collection.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

extension Collection {
    func chunk(by predicate: @escaping (Iterator.Element, Iterator.Element) -> Bool) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var k: Index
        while i != endIndex {
            k = endIndex
            var j = index(after: i)
            while j != endIndex {
                if !predicate(self[i], self[j]) {
                    k = j
                    break
                }
                formIndex(after: &j)
            }
            res.append(self[i..<k])
            i = k
        }
        return res
    }
}
