//
//  Sequence.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

extension Sequence  {
    func sum<T: AdditiveArithmetic>(_ keyPath: KeyPath<Element, T>) -> T { reduce(.zero) { $0 + $1[keyPath: keyPath] } }
}
