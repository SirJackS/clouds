//
//  UIView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//


import UIKit

extension UIView {
    
    convenience public init(backgroundColor: UIColor? = .clear) {
        self.init()
        self.backgroundColor = backgroundColor
    }
    
    open func setupShadow(opacity: Float = 0, radius: CGFloat = 0, offset: CGSize = .zero, color: UIColor = .black) {
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
    }
    
    open func setupBorder(width: CGFloat = 1, color: UIColor = .black) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
    
    func roundedView(size: Int) {
        self.frame.size = CGSize(width: size, height: size)
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = true
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        
        if #available(iOS 11, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
        
    }
    
    func addDashedBorder(color: UIColor = UIColor.black.withAlphaComponent(0.38), corner: CGFloat = 0.0, height: CGFloat = 56.0, width: CGFloat = UIScreen.main.bounds.size.width - 32) {
          let color = color.cgColor

          let shapeLayer:CAShapeLayer = CAShapeLayer()
        
          //let frameSize = self.frame.size
          let shapeRect = CGRect(x: 0, y: 0, width: width, height: height)

          shapeLayer.bounds = shapeRect
          shapeLayer.position = CGPoint(x: width / 2, y: height/2)
          shapeLayer.fillColor = UIColor.clear.cgColor
          shapeLayer.strokeColor = color
          shapeLayer.lineWidth = 1
          shapeLayer.lineJoin = CAShapeLayerLineJoin.round
          shapeLayer.lineDashPattern = [6,3]
          shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: corner).cgPath

          self.layer.addSublayer(shapeLayer)
    }
    
    func addDashedRoundedBorder(color: String = "009688", corner: CGFloat = 48.0) {
        let rect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 96, height: 96))
        let layer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 48)
        layer.path = path.cgPath
        let shapeRect = CGRect(x: 0, y: 0, width: 96, height: 96)
        layer.bounds = shapeRect
        layer.strokeColor = UIColor(hexString: color).cgColor
        layer.backgroundColor = UIColor.clear.cgColor
        layer.fillColor = UIColor.clear.cgColor
        layer.position = CGPoint(x: 48, y: 48)
        layer.lineWidth = 2
        layer.lineJoin = CAShapeLayerLineJoin.round
        layer.lineDashPattern = [6,6]
        self.layer.addSublayer(layer)
    }
    
    func addTopBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addBottomBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        border.frame = CGRect(x: 0, y: frame.size.height - borderWidth, width: frame.size.width, height: borderWidth)
        addSubview(border)
    }

    func addLeftBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.frame = CGRect(x: 0, y: 0, width: borderWidth, height: frame.size.height)
        border.autoresizingMask = [.flexibleHeight, .flexibleRightMargin]
        addSubview(border)
    }

    func addRightBorder(with color: UIColor?, andWidth borderWidth: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.autoresizingMask = [.flexibleHeight, .flexibleLeftMargin]
        border.frame = CGRect(x: frame.size.width - borderWidth, y: 0, width: borderWidth, height: frame.size.height)
        addSubview(border)
    }
    
    func setUpTriangle(height: CGFloat, width: CGFloat) {
        let path = CGMutablePath()

        path.move(to: CGPoint(x: 0, y: height))
        path.addLine(to: CGPoint(x:width/2, y: height/2))
        path.addLine(to: CGPoint(x:width, y: height))
        path.addLine(to: CGPoint(x:0, y: height))

        let shape = CAShapeLayer()
        shape.path = path
        shape.fillColor = UIColor.white.cgColor

        self.layer.insertSublayer(shape, at: 0)
    }
}
