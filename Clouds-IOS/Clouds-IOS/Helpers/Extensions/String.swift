//
//  String.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//


import Foundation
import UIKit

extension String {
    
    var length: Int {
        return count
    }
    
//    subscript (i: Int) -> String {
//        return self[i ..< i + 1]
//    }
    
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
    
//    subscript (r: Range<Int>) -> String {
//        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
//                                            upper: min(length, max(0, r.upperBound))))
//        let start = index(startIndex, offsetBy: range.lowerBound)
//        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
//        return String(self[start ..< end])
//    }
    
    func trim() -> String {
       return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    
    func removeWhites() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
    func strikeThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(
            NSAttributedString.Key.strikethroughStyle,
               value: NSUnderlineStyle.single.rawValue,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
    
    public func getAcronyms(separator: String = "") -> String
        {
            let acronyms = self.components(separatedBy: " ").map({ String($0.first!) }).joined(separator: separator);
            return acronyms;
        }
    
    func removeStrike() -> NSAttributedString {
        let attributeString = NSMutableAttributedString(string: self)
        attributeString.removeAttribute(
            NSAttributedString.Key.strikethroughStyle,
                   range:NSMakeRange(0,attributeString.length))
        return attributeString
    }
    
    func strikeRedThrough() -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range: NSMakeRange(0, attributeString.length))
        attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: UIColor(hexString: "C70D3A"), range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
//    func getFormattedDate() -> String {
//        print(self)
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
//        dateFormatterGet.locale = Locale(identifier: "UTC")
//
//        let dateObj: Date? = dateFormatterGet.date(from: self)
//
//        let formatString: String = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
//        let hasAMPM = formatString.contains("a")
//
//        if hasAMPM {
//            dateFormatterGet.amSymbol = "AM"
//            dateFormatterGet.pmSymbol = "PM"
//            dateFormatterGet.dateFormat = "dd.MM.yyyy h:mm a"
//        } else {
//            dateFormatterGet.dateFormat = "dd.MM.yyyy HH:mm"
//        }
//        return dateFormatterGet.string(from: dateObj!)
//    }
    
    func getFormattedDate() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        dateFormatterGet.locale = Locale(identifier: "UTC")

        let dateObj: Date? = dateFormatterGet.date(from: self)
        dateFormatterGet.dateFormat = "dd.MM.yyyy"
        return dateFormatterGet.string(from: dateObj!)
    }
    
    func getFormattedDateForDayCell(fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", toFormat: String = "yyyy-MM-dd") -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = fromFormat
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        dateFormatterGet.locale = Locale(identifier: "UTC")

        let dateObj: Date? = dateFormatterGet.date(from: self)
        dateFormatterGet.dateFormat = toFormat
        let loc = Locale(identifier: L102Language.currentAppleLanguage())
        dateFormatterGet.locale = loc
        return dateFormatterGet.string(from: dateObj!)
    }
    
    func getFormattedDateForDayCellWithLocale(fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", toFormat: String = "yyyy-MM-dd") -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = fromFormat
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        let loc = Locale(identifier: L102Language.currentAppleLanguage())
        dateFormatterGet.locale = loc

        let dateObj: Date? = dateFormatterGet.date(from: self)
        dateFormatterGet.dateFormat = toFormat
        
        dateFormatterGet.locale = loc
        return dateFormatterGet.string(from: dateObj!)
    }
    
    func getFormattedTime() -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatterGet.timeZone = TimeZone(identifier: "UTC")
        dateFormatterGet.locale = Locale(identifier: "UTC")

        let dateObj: Date? = dateFormatterGet.date(from: self)
        
        let formatString: String = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
        let hasAMPM = formatString.contains("a")
        
        if hasAMPM {
            dateFormatterGet.amSymbol = "AM"
            dateFormatterGet.pmSymbol = "PM"
            dateFormatterGet.dateFormat = "h:mm a"
        } else {
            dateFormatterGet.dateFormat = "HH:mm"
        }
        return dateFormatterGet.string(from: dateObj!)
    }
}
