//
//  UIButton.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit

extension UIButton {
    func colorStrings(
        string1: String = "", color1: UIColor = UIColor.black.withAlphaComponent(0.6),
        string2: String = "", color2: UIColor = UIColor.black.withAlphaComponent(0.87)) {
        let att = NSMutableAttributedString(string: "\(string1)\(string2)");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: color1 , range: NSRange(location: 0, length: string1.count))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: color2, range: NSRange(location: string1.count, length: string2.count))
        att.addAttributes([NSAttributedString.Key.font : UIFont(weight: .medium, size: 16)!], range: NSRange(location: string1.count, length: string2.count))
        self.setAttributedTitle(att, for: .normal)
    }
}
