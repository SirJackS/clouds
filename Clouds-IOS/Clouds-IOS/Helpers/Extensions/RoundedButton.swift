//
//  RoundedButton.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//


import UIKit

@IBDesignable
class RoundedButton: UIButton {
    //IBInspectable olduqda main storyboardda sag terefdeki menyudan deyismey olur
    @IBInspectable var cornerRadiuss: CGFloat = 6.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderColorr: CGColor = #colorLiteral(red: 0.8666666667, green: 0.8823529412, blue: 0.8862745098, alpha: 1) {
        didSet {
            self.layer.borderColor = borderColorr
        }
    }
    
    @IBInspectable var borderWidthh: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    override func awakeFromNib() {
        self.setupView()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.setupView()
    }
    
    
    func setupView() {
        self.layer.cornerRadius = cornerRadiuss
        self.layer.borderColor = borderColorr
        self.layer.borderWidth = borderWidthh
    }
}
