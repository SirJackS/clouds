//
//  Date.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.01.21.
//

import Foundation

extension Date {
    func getDateOf(year: Int) -> Date {
        let currentDate = Date()
        let calendar = Calendar(identifier: .gregorian)
        var components = DateComponents()
        components.calendar = calendar
        components.year = -(year)
        let minDate = calendar.date(byAdding: components, to: currentDate)!
        return minDate
    }
}
