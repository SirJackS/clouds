//
//  UITabbarController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import UIKit

//extension UITabBarController {
//    
//    private struct AssociatedKeys {
//        // Declare a global var to produce a unique address as the assoc object handle
//        static var orgConstraintConstant: UInt8 = 0
//        static var orgTabBarAlpha       : UInt8 = 1
//    }
//    
//    var orgConstraintConstant: CGFloat? {
//        get { return objc_getAssociatedObject(self, &AssociatedKeys.orgConstraintConstant) as? CGFloat }
//        set { objc_setAssociatedObject(self, &AssociatedKeys.orgConstraintConstant, newValue, .OBJC_ASSOCIATION_COPY) }
//    }
//    
//    var orgTabBarAlpha: CGFloat? {
//        get { return objc_getAssociatedObject(self, &AssociatedKeys.orgTabBarAlpha) as? CGFloat }
//        set { objc_setAssociatedObject(self, &AssociatedKeys.orgTabBarAlpha, newValue, .OBJC_ASSOCIATION_COPY) }
//    }
//    
//    func setTabBarVisible(visible:Bool, animated:Bool, bottomConstraint: NSLayoutConstraint) {
//        // bail if the current state matches the desired state
//        if (tabBarIsVisible() == visible) { return }
//        //define segment animation duration (note we have two segments so total animation time = times 2x)
//        let segmentAnimationDuration = animated ? 0.15 : 0.0
//        //we should show it
//        if visible {
//            //animate moving up
//            UIView.animate(withDuration:  segmentAnimationDuration,
//                           delay: 0,
//                           options: [],
//                           animations: {
//                            [weak self] in
//                            guard let self = self else { return }
//                            bottomConstraint.constant = self.orgConstraintConstant ?? 0
//                            self.view.layoutIfNeeded()
//                           },
//                           completion: {
//                            (_) in
//                            //animate tabbar fade in
//                            UIView.animate(withDuration: segmentAnimationDuration) {
//                                [weak self] in
//                                guard let self = self else { return }
//                                self.tabBar.alpha = self.orgTabBarAlpha ?? 0
//                                self.view.layoutIfNeeded()
//                            }
//                           })
//            //reset our values
//            self.orgConstraintConstant = nil
//        }
//        //we should hide it
//        else {
//            //save our previous values
//            self.orgConstraintConstant = bottomConstraint.constant
//            self.orgTabBarAlpha = tabBar.alpha
//            //animate fade bar out
//            UIView.animate(withDuration:  segmentAnimationDuration,
//                           delay: 0,
//                           options: [],
//                           animations: {
//                            [weak self] in
//                            guard let self = self else { return }
//                            self.tabBar.alpha = 0.0
//                            self.view.layoutIfNeeded()
//                           },
//                           completion: {
//                            (_) in
//                            //then animate moving down
//                            UIView.animate(withDuration: segmentAnimationDuration) {
//                                [weak self] in
//                                guard let self = self else { return }
//                                bottomConstraint.constant = bottomConstraint.constant - self.tabBar.frame.height + 4 // + 4 looks nicer on no-home button devices
//                                //self.tabBar.alpha = 0.0
//                                self.view.layoutIfNeeded()
//                            }
//                           })
//        }
//    }
//    
//    func tabBarIsVisible() ->Bool {
//        return orgConstraintConstant == nil
//    }
//}
