//
//  Constants.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

let termAndConditions = "Help protect your website and its users with clear and fair website terms and conditions. These terms and conditions for a website set out key issues such as acceptable use, privacy, cookies, registration and passwords, intellectual property, links to other sites, termination and disclaimers of responsibility. Terms and conditions are used and necessary to protect a website owner from liability of a user relying on the information or the goods provided from the site then suffering a loss."

//User Defaults
let TOKEN_KEY = "token"
let OTP_TOKEN_KEY = "otpToken"
let LOGGED_IN_KEY = "loggedIn"
let FULLNAME = "fullname"
let KEYWORDS = "keywords"
let GENDER_KEY = "genderKey"
let AGE_KEY = "ageKey"
let UD_KEY_LANG = "language"
//let LANGUAGE = "i18n_language"

