//
//  FilterManager.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 05.02.21.
//

import Foundation

class FilterManager {
    
    static let instance = FilterManager()
    
    var categoriesName: String = ""
    var categoriesIndexs: [IndexPath] = []
    var categories: [DetailFilter] = []
    var productTypesName: String = ""
    var productTypesIndexs: [IndexPath] = []
    var productTypes: [DetailFilter] = []
    var brandsName: String = ""
    var brandsIndexs: [IndexPath] = []
    var brands: [DetailFilter] = []
    var colorsName: String = ""
    var colorsIndexs: [IndexPath] = []
    var colors: [DetailFilter] = []
    var sizesName: String = ""
    var sizesIndexs: [IndexPath] = []
    var sizes: [DetailFilter] = []
    var price: String = ""
    var minPrice: Int = 0
    var maxPrice: Int = 10000

    func getNames(_ data: [DetailFilter]) -> String {
        var array: [String] = []
        for item in data {
            array.append(item.name)
        }
        return array.joined(separator: ",")
    }
    
    func removeFilterData() {
        categoriesName = ""
        categoriesIndexs.removeAll()
        categories.removeAll()
        productTypesName = ""
        productTypesIndexs.removeAll()
        productTypes.removeAll()
        brandsName = ""
        brandsIndexs.removeAll()
        brands.removeAll()
        colorsName = ""
        colorsIndexs.removeAll()
        colors.removeAll()
        sizesName = ""
        sizesIndexs.removeAll()
        sizes.removeAll()
        self.removePrice()
    }
    
    func removePrice() {
        price = ""
        minPrice = 0
        maxPrice = 10000
    }
}
