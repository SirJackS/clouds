//
//  MainNavigation.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import UIKit

final class MainNavigation: UINavigationController {
    
    var barTintColor: UIColor? {
        didSet {
            if barTintColor == .clear {
                self.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationBar.shadowImage = UIImage()
                self.isTranslucent = true
                self.view.backgroundColor = .clear
            }
            else {
                self.navigationBar.barTintColor = barTintColor
            }
        }
    }
    
    var isTranslucent: Bool? {
        didSet {
            self.navigationBar.isTranslucent = isTranslucent ?? false
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .darkContent
        } else {
            return .default
        }
    }
    
    
    // MARK: - Private
    
    private func setupUI() {
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = .white
        self.navigationBar.tintColor = .black
        self.navigationBar.setValue(true, forKey: "hidesShadow")
        
        self.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black,
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold)
        ]
    }
}
