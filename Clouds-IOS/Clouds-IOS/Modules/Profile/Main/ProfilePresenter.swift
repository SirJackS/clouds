//
//  ProfilePresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import Foundation

final class ProfilePresenter: ProfilePresenterProtocol {
    
    private unowned let view: ProfileViewProtocol
    private var service: ServiceProtocol
    
    init(view: ProfileViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getMe()
        self.getProfilModels()
    }
    
    private func getProfilModels() {
        let models: [ProfileModel] = [
            ProfileModel(icon: "myorders-icon", title: "My orders".localized()),
            ProfileModel(icon: "mydetails-icon", title: "My details".localized()),
            ProfileModel(icon: "myrewards-icon", title: "My rewards".localized()),
            ProfileModel(icon: "myaddresses-icon", title: "My addresses".localized()),
            ProfileModel(icon: "notifications-icon", title: "Notifications".localized()),
            ProfileModel(icon: "giftcards-icon", title: "Gift cards".localized()),
            ProfileModel(icon: "language-icon", title: "Language".localized()),
            ProfileModel(icon: "about-icon", title: "About".localized()),
            ProfileModel(icon: "lock-icon", title: "Change password".localized()),
            ProfileModel(icon: "signout-icon", title: "Sign out".localized()),
        ]
        self.view.handleOutput(.showList(models))
    }
    
    func logout() {
        self.view.handleOutput(.showActivity(true))
        service.logout { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print("success: ", response)
                UserDefaults.standard.removeObject(forKey: TOKEN_KEY)
                UserDefaults.standard.removeObject(forKey: LOGGED_IN_KEY)
                UserDefaults.standard.removeObject(forKey: KEYWORDS)
                self.view.handleOutput(.logout)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.logout))
                
            default: break
            }
        }
    }
    
    func getMe() {
        self.view.handleOutput(.showActivity(true))
        service.me { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showFullname("\(data.firstname ?? "") \(data.lastname ?? "")"))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.me))
                
            default: break
            }
        }
    }
}
