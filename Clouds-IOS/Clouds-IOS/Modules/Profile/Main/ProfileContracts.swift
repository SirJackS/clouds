//
//  ProfileContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import Foundation

// MARK: - Presenter

protocol ProfilePresenterProtocol: class {
    func load()
    func logout()
    func getMe()
}

enum ProfilePresenterOutput {
    case showActivity(Bool)
    case showMessage(String)
    case showList([ProfileModel])
    case showFullname(String)
    case logout
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol ProfileViewProtocol: class {
    func handleOutput(_ output: ProfilePresenterOutput)
}
