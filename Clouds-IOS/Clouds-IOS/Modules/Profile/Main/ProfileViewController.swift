//
//  ProfileViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import UIKit
import SDWebImage

final class ProfileViewController: UIViewController, ProfileViewProtocol {

    var presenter: ProfilePresenterProtocol!
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(ProfileCell.self, forCellReuseIdentifier: ProfileCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.isScrollEnabled = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        return tableView
    }()
    
    private lazy var profileView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        let height = UIScreen.main.bounds.width * 0.234
        view.layer.cornerRadius = height/2
        view.clipsToBounds = true
        view.anchor(.size(width: height, height: height))
//        view.isHidden = true
        return view
    }()
    
    private lazy var profileTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont(weight: .semibold, size: 24)
        label.numberOfLines = 1
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.anchor(.height(32))
        return label
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .semibold, size: 24)
        label.numberOfLines = 1
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.anchor(.height(32))
//        label.isHidden = true
        return label
    }()
    
    private var data: [ProfileModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    //reappears navigation bar on next page
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.fillSuperview())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        profileView.anchor(.top(24), .centerX())
        profileTitleLabel.anchor( .leading(), .trailing(), .centerY())
        
        titleLabel.anchor( .top(profileView.bottomAnchor, constant: 4), .leading(16), .trailing(-16))
        
        tableView.anchor(.top(titleLabel.bottomAnchor, constant: 16), .leading(16), .trailing(-16), .bottom(-87), .height(CGFloat(data.count*48)))
        
    }

    func handleOutput(_ output: ProfilePresenterOutput) {
        switch output {
        case .showFullname(let fullname):
            self.setupData(fullname)
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
        case .logout:
            App.router.start()
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .me:
                        self.presenter.load()
                    case .logout:
                        self.presenter.logout()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(profileView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(tableView)
        
        profileView.addSubview(profileTitleLabel)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
    }
    
    private func setupData(_ fullname: String) {
        titleLabel.text = fullname.capitalized
        profileTitleLabel.text = fullname.getAcronyms(separator: " ").uppercased()
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleModalDismissed),
        name: NSNotification.Name(rawValue: "modalIsDimissed1"),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.refreshMe),
        name: NSNotification.Name(rawValue: "myDetailUpdated"),
        object: nil)
    }
    
    // MARK: - Action
    
    @objc func handleModalDismissed() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func refreshMe() {
        self.presenter.getMe()
    }
}


extension ProfileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let count = data[indexPath.section].foods?.count ?? 0
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCell.ID, for: indexPath) as? ProfileCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
//        cell.isLast = (indexPath.row == count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let viewController = OrderMainBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 1:
            let viewController = MyDetailBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 2:
            let viewController = RewardMainBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 3:
            let viewController = AddressMainBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 4:
            let viewController = NotificationsBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 5:
            let viewController = GiftCardMainBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 6:
            let viewController = LangDropUpBuilder.make()
            viewController.modalTransitionStyle = .coverVertical
            viewController.modalPresentationStyle = .overCurrentContext
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.present(viewController, animated: true, completion: nil)
        case 7:
            let viewController = AboutBuilder.make()
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        case 8:
            let viewController = ChangePasswordBuilder.make()
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        case 9:
            tableView.isUserInteractionEnabled = false
            self.presenter.logout()
            tableView.isUserInteractionEnabled = true
        default: break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
