//
//  ProfileBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import Foundation

final class ProfileBuilder {
    
    static func make() -> ProfileViewController {
        let view = ProfileViewController()
        
        let presenter = ProfilePresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
