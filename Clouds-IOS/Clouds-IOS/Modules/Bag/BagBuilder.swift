//
//  BagBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

final class BagBuilder {
    
    static func make() -> BagViewController {
        let view = BagViewController()
        
        let presenter = BagPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
