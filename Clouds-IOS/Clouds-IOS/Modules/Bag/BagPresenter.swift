//
//  BagPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

final class BagPresenter: BagPresenterProtocol {
    
    private unowned let view: BagViewProtocol
    private var service: ServiceProtocol
    
    init(view: BagViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getCartItems()
    }
    
    private func getCartItems() {
        self.view.handleOutput(.showActivity(true))
        service.getCart { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.show(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)

            default: break
            }

        }
    }
    
    func removeBy(id: Int, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.deletBagItem(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.remove(indexPath))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                
            default: break
            }
        }
    }
    
    func addWishlistBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        let request = AddToWishlistRequest(productId: id)
        service.addToWishlist(request){ [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addToWishlist))
                
            default: break
            }
            
        }
    }

    
    func updateQuantityInCart(itemId: Int, quantity: Int) {
        self.view.handleOutput(.showActivity(true))
        let request = UpdateQuantityInBagRequest(quantity: quantity)
        service.updateQuantityInBag(itemId, request: request) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.load()

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func updateOptionsInCart(itemId: Int, productId: Int, color: String, size: String) {
        self.view.handleOutput(.showActivity(true))
        let request = UpdateOptionsInBagRequest(productId: productId, size: size, color: color)
        service.updateOptionsInBag(itemId, request: request) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.load()

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func getColorsWithSize(id: Int, size: String, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.getColorsWithSize(id, size) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showColorList(data,indexPath))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func getSizesWithColor(id: Int, color: String, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.getSizesWithColor(id, color) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showSizeList(data,indexPath))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
}
