//
//  BagViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit
import SDWebImage

//protocol BagViewControllerDelegate: class {
//    func countBagItems()
//}

final class BagViewController: UIViewController, BagViewProtocol {
 
    var presenter: BagPresenterProtocol!
    
//    weak var delegate: BagViewControllerDelegate?
    
    private lazy var navBarView: BagNavbarView = {
        let view = BagNavbarView(title: "My Bag".localized())
        return view
    }()
    
    private lazy var blackView: UIView = {
        let view = UIView()
        view.isHidden = true
        view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        return view
    }()
    
    private lazy var blackViewTapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickButtonTouchUp))
        tap.delegate = self
        return tap
    }()
    
    private lazy var mainTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(BagCell.self, forCellReuseIdentifier: BagCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 148
        tableView.estimatedRowHeight = 148
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        return tableView
    }()
    
    private lazy var totalDetailTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(TotalDetailCell.self, forCellReuseIdentifier: TotalDetailCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 20
        tableView.estimatedRowHeight = 20
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    private lazy var totalDetailView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.white
        //view.isHidden = true
        return view
    }()
    
    private lazy var checkoutView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var totalView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var checkoutButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("checkout".uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
//        button.contentHorizontalAlignment = .center
        
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(checkoutButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var totalLabel: UILabel = {
        let label = UILabel()
        label.text = "Total"
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.text = "0.0 AZN"
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var downArrowView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "yellow-down-arrow-icon")
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()

    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(clickButtonTouchUp))
        return tap
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refreshBag), for: .valueChanged)
        return refresh
    }()
    
    private var totalDetailViewHidden = true
    private var totalPrice: Float?
    private var data: [BagItem] = []
    private var totalDetailData: [TotalDetailModel] = [
        TotalDetailModel(title: "Order Total".localized(), total: 278.60),
        TotalDetailModel(title: "Shipping".localized(), total: 6.60),
    ]
    private var removedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
        
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainTableView.anchor(.top(), .leading(), .trailing(), .bottom(checkoutView.topAnchor))
        
        checkoutView.anchor(.bottom(), .leading(), .trailing())
        
        checkoutButton.anchor(.top(16), .trailing(-16), .bottom(-26), .widthGreater((self.view.frame.width-32)/2))
        
        totalView.anchor(.top(16), .leading(16),
            .trailing(checkoutButton.leadingAnchor,constant: -24))
        
        totalLabel.anchor(.top(2), .leading(2), .height(20), .width(34), .trailing(downArrowView.leadingAnchor, constant: -10))
        
        downArrowView.anchor(.top(), .trailingLess())
        
        priceLabel.anchor(
            .top(downArrowView.bottomAnchor),
            .trailing(), .leading(), .height(26), .bottom())
        
        totalDetailView.anchor(.leading(), .trailing(), .top(checkoutView.topAnchor))
        
        blackView.anchor(.leading(), .trailing(),.height(UIScreen.main.bounds.height), .bottom(totalDetailView.topAnchor))
        
        totalDetailTableView.anchor(.top(6), .leading(16), .trailing(-16), .bottom(-6), .height(CGFloat(totalDetailData.count*20)))
        
        lineView.anchor(.leading(), .trailing(), .bottom(), .height(0.3))
        
    }
    

    func handleOutput(_ output: BagPresenterOutput) {
        switch output {
        case .show(let data):
            guard let items = data.items else { return }
            self.navBarView.titleLabel.text = String(format: "My Bag (* items)".localized(), items.count)
            self.setupData(data.total)
            self.data.removeAll()
            self.data = items
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "bagUpdated"), object: nil, userInfo: ["count": self.data.count])
            self.mainTableView.reloadData()
        case .remove(let indexPath):
            self.data.remove(at: indexPath.row)
            self.mainTableView.deleteRows(at: [indexPath], with: .automatic)
        case .refreshWishList:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshWishlist"), object: nil)
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getCartItems:
                        self.presenter.load()
                    case .deletBagItem:
                        guard let indexPath = self.removedIndexPath else { return }
                        self.presenter.removeBy(id: self.data[indexPath.row].id, indexPath: indexPath)
                    default:
                        return
                    }
                }
            }
        case .showColorList(let colors, let indexPath):
            self.openColorPicker(colors: colors, indexPath: indexPath)
        case .showSizeList(let sizes, let indexPath):
            self.openSizePicker(sizes: sizes, indexPath: indexPath)
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainTableView)
        self.view.addSubview(blackView)
        self.view.addSubview(totalDetailView)
        self.view.addSubview(checkoutView)
        
        
        self.checkoutView.addSubview(checkoutButton)
        self.checkoutView.addSubview(totalView)
        
        self.totalView.addSubview(totalLabel)
        self.totalView.addSubview(priceLabel)
        self.totalView.addSubview(downArrowView)
        
        self.totalDetailView.addSubview(totalDetailTableView)
        self.totalDetailView.addSubview(lineView)
        
        self.totalView.addGestureRecognizer(tapGesture)
        self.blackView.addGestureRecognizer(blackViewTapGesture)
        
        self.mainTableView.addSubview(refreshControl)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.mainTableView.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setupData(_ total: Float) {
        self.totalPrice = total
        self.priceLabel.text = "\(self.totalPrice ?? 0.0) AZN"
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.refreshBag),
        name: NSNotification.Name(rawValue: "refreshBag"),
        object: nil)
    }
    
    // MARK: - Action
    
    @objc func refreshBag() {
        self.refreshControl.beginRefreshing()
        self.presenter.load()
        self.refreshControl.endRefreshing()
    }
    
    private func openColorPicker(colors: [Color], indexPath: IndexPath) {
        let alert = UIAlertController(title: "Colors".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedColor: Color?
        if colors.count > 0 {
            selectedColor = colors[0]
        }
        let frameSizes: [CGFloat] = (0..<colors.count).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [colors.map { $0.name ?? "null"}]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if colors.count > 0 {
                selectedColor = colors[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                guard let color = selectedColor?.name else {return}
                guard let size = self.data[indexPath.row].size  else {return}
//                data[indexPath.row].id
                self.presenter.updateOptionsInCart(itemId: self.data[indexPath.row].id, productId: self.data[indexPath.row].productId, color: color, size: size)
            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openSizePicker(sizes: [Size], indexPath: IndexPath) {
        let alert = UIAlertController(title: "Sizes".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedSize: Size?
        if sizes.count > 0 {
            selectedSize = sizes[0]
        }
        let frameSizes: [CGFloat] = (0..<sizes.count).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [sizes.map { $0.name ?? "null"}]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if sizes.count > 0 {
                selectedSize = sizes[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                guard let size = selectedSize?.name else {return}
                guard let color = self.data[indexPath.row].color  else {return}
//                data[indexPath.row].id
                self.presenter.updateOptionsInCart(itemId: self.data[indexPath.row].id, productId: self.data[indexPath.row].productId, color: color, size: size)            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func clickButtonTouchUp() {
        if totalDetailViewHidden {
            UIView.animate(withDuration: 0.2) {
                self.downArrowView.transform = CGAffineTransform(rotationAngle: .pi)
                self.totalDetailView.center.y -= self.totalDetailView.frame.height
                self.navigationController?.navigationBar.toggle()
                self.blackView.isHidden = false
            }
            
            //self.totalDetailView.isHidden = false
            totalDetailViewHidden = false
        } else {
            UIView.animate(withDuration: 0.2) {
                self.downArrowView.transform = CGAffineTransform.identity
                self.totalDetailView.center.y += self.totalDetailView.frame.height
                self.navigationController?.navigationBar.toggle()
                self.blackView.isHidden = true
            }
            
//            self.totalDetailView.isHidden = true
            totalDetailViewHidden = true
        }
    }
    
    @objc private func checkoutButtonTouchUp() {
        if totalDetailViewHidden == false {
            clickButtonTouchUp()
        }
        if data.count > 0 {
            let viewController = CheckoutBuilder.make()
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}


extension BagViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case mainTableView:
            return data.count
        case totalDetailTableView:
            return totalDetailData.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case mainTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: BagCell.ID, for: indexPath) as? BagCell else { return UITableViewCell() }
            cell.data = data[indexPath.row]
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        case totalDetailTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalDetailCell.ID, for: indexPath) as? TotalDetailCell else { return UITableViewCell() }
            cell.data = totalDetailData[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete".localized()) { (action, view, completion) in
            // Perform your action here
            self.removedIndexPath = indexPath
            self.presenter.removeBy(id: self.data[indexPath.row].id, indexPath: indexPath)
            completion(true)
        }

        let addAction = UIContextualAction(style: .normal, title: "Add to".localized()) { (action, view, completion) in
        // Perform your action here
            self.presenter.addWishlistBy(id: self.data[indexPath.row].productId)
//            tableView.reloadRows(at: [indexPath], with: .automatic)
            completion(true)
        }

        deleteAction.image = UIImage(named: "swipe-x-icon")
        deleteAction.backgroundColor = UIColor(hexString: "C70D3A")
        addAction.image = UIImage(named: "swipe-heart-icon")
        addAction.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        return UISwipeActionsConfiguration(actions: [deleteAction, addAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        switch tableView {
//        case mainTableView:
//            let viewController = ProductDetailBuilder.make()
//            self.navigationController?.pushViewController(viewController, animated: true)
//        default: break
//        }
    }

}

extension BagViewController: BagCellDelegate {
    func pieceButtonClick(id: Int, selectedQuantity: Int) {
        let alert = UIAlertController(title: "Quantity".localized(), message: nil, preferredStyle: .actionSheet)
        var selected: Int?
        let frameSizes: [Int] = (1..<1000).map { Int($0) }
        let pickerViewValues: [[String]] = [frameSizes.map { Int($0).description }]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: selectedQuantity-1)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if frameSizes.count > 0 {
                selected = frameSizes[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                self.presenter.updateQuantityInCart(itemId: id, quantity: selected ?? selectedQuantity)           }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func colorPressed(id: Int, size: String, indexPath: IndexPath) {
        self.presenter.getColorsWithSize(id: id, size: size, indexPath: indexPath)
    }
    
    func sizePressed(id: Int, color: String, indexPath: IndexPath) {
        self.presenter.getSizesWithColor(id: id, color: color, indexPath: indexPath)
    }
}

extension BagViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
