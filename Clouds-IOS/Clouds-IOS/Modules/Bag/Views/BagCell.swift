//
//  BagCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit
import SDWebImage

protocol BagCellDelegate: class {
    func pieceButtonClick(id: Int, selectedQuantity: Int)
    func sizePressed(id: Int, color: String, indexPath: IndexPath)
    func colorPressed(id: Int, size: String, indexPath: IndexPath)
}

final class BagCell: UITableViewCell {

    static let ID: String = "BagCell"
    
    weak var delegate: BagCellDelegate?
    
    var data: BagItem! {
        didSet {
            self.configure()
        }
    }
    var indexPath: IndexPath!
    
//    var isLast: Bool = false {
//        didSet {
//            lineView.isHidden = isLast
//        }
//    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 112, height: 116))
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.isUserInteractionEnabled = true
        return label
    }()
    
    private lazy var realPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    private lazy var colorView: UIView = {
        let view = UIView(backgroundColor: UIColor.clear)
        view.anchor(.size(width: 12, height: 12))
        return view
    }()
    
    private lazy var colorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var colorStack: UIView = {
        let view = UIView(backgroundColor: UIColor.clear)
        return view
    }()
    
    private lazy var pieceButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.withAlphaComponent(0.87).cgColor
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(30))
//        button.contentHorizontalAlignment = .center
        
        //change image and text place
        button.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(pieceButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var sizeTapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(sizePressed))
        tap.delegate = self
        return tap
    }()
    
    private lazy var colorTapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(colorPressed))
        tap.delegate = self
        return tap
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.bodyView.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 16, trailing: -16))
        
        photoView.anchor(.top(16), .bottom(-16), .leading())
        
        titleLabel.anchor(.top(16),
            .leading(photoView.trailingAnchor, constant: 16),
            .trailing(-16))
        
        infoLabel.anchor(.top(titleLabel.bottomAnchor, constant: 8),
            .leading(photoView.trailingAnchor, constant: 16),
            .trailing(-16))
        
        realPriceLabel.anchor(.trailing(-10), .bottom(-16))
        
        colorStack.anchor(.top(infoLabel.bottomAnchor, constant: 8),
           .leading(photoView.trailingAnchor, constant: 16),
           .trailingLess(bodyView.trailingAnchor, constant: -16))
        
        colorLabel.anchor(.top(), .leading(), .bottom())
        
        colorView.anchor(.top(4), .trailing(), .bottom(-4),
                         .leading(colorLabel.trailingAnchor, constant: 7))
//        stackView.anchor(
//            .top(), .bottom(), .trailing(),
//            .leading(photoView.trailingAnchor, constant: 16))
        
        pieceButton.anchor(.bottom(-16), .leading(photoView.trailingAnchor, constant: 16), .trailingLess(realPriceLabel.leadingAnchor, constant: -8), .widthGreater(94))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
        self.infoLabel.text = ""
        self.realPriceLabel.text = ""
        self.colorView.backgroundColor = .clear
        self.colorLabel.text = ""
//        self.pieceButton.setTitle("", for: .normal)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(infoLabel)
        bodyView.addSubview(photoView)
        bodyView.addSubview(lineView)
        bodyView.addSubview(realPriceLabel)
        bodyView.addSubview(colorStack)
        bodyView.addSubview(pieceButton)
        
        colorStack.addSubview(colorView)
        colorStack.addSubview(colorLabel)
        
        colorStack.addGestureRecognizer(colorTapGesture)
        infoLabel.addGestureRecognizer(sizeTapGesture)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        self.photoView.backgroundColor = .clear
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }

        self.titleLabel.text = data.name
        self.infoLabel.text = String(format: "Size: ***".localized(), data.size ?? "")
        self.realPriceLabel.text = "\(data.variantPrice ?? 0.0) AZN"
       
        if let hex = data.colorCode {
            self.colorView.backgroundColor = UIColor(hexString: "\(hex)")
        }
        self.colorLabel.text = data.color ?? ""
        self.pieceButton.setTitle(String(format: "* pieces".localized(), data.quantity).uppercased(), for: .normal)
        
    }
    
    // MARK: - Action
    
    @objc private func pieceButtonTouchUp() {
        self.delegate?.pieceButtonClick(id: data.id, selectedQuantity: data.quantity)
    }
    
    @objc private func colorPressed() {
        guard let size = data.size else {return}
        self.delegate?.colorPressed(id: data.productId, size: size, indexPath: indexPath)
    }
    
    @objc private func sizePressed() {
        guard let color = data.color else {return}
        self.delegate?.sizePressed(id: data.productId, color: color, indexPath: indexPath)
    }
}

extension BagCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
