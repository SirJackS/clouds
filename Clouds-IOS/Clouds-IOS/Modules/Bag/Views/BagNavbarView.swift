//
//  BagNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit

final class BagNavbarView: UIView {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()

    init(title: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        self.titleLabel.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        titleLabel.anchor(.centerX(), .centerY())
        
        navbarLineView.anchor( .leading(), .trailing(), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(navbarLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

}
