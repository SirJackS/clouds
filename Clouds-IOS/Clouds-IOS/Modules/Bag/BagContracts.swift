//
//  BagContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

// MARK: - Presenter

protocol BagPresenterProtocol: class {
    func load()
    func removeBy(id: Int, indexPath: IndexPath)
    func addWishlistBy(id: Int)
    func getSizesWithColor(id: Int, color: String, indexPath: IndexPath)
    func getColorsWithSize(id: Int, size: String, indexPath: IndexPath)
    func updateOptionsInCart(itemId: Int, productId: Int, color: String, size: String)
    func updateQuantityInCart(itemId: Int, quantity: Int)
}

enum BagPresenterOutput {
    case remove(IndexPath)
    case showMessage(String)
    case showActivity(Bool)
    case refreshWishList
    case show(Bag)
    case showColorList([Color],IndexPath)
    case showSizeList([Size],IndexPath)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol BagViewProtocol: class {
    func handleOutput(_ output: BagPresenterOutput)
}
