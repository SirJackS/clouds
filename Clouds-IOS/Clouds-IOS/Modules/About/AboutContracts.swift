//
//  AboutContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.01.21.
//

import Foundation

// MARK: - Presenter

protocol AboutPresenterProtocol: class {
    func load()
}

enum AboutPresenterOutput {
    case showMessage(String)
    case showList([String])
}


// MARK: - View

protocol AboutViewProtocol: class {
    func handleOutput(_ output: AboutPresenterOutput)
}
