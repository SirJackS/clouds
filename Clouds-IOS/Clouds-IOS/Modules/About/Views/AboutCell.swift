//
//  AboutCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.01.21.
//

import UIKit

final class AboutCell: UITableViewCell {
    
    static let ID: String = "AboutCell"
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var rightIconView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "right-arrow-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(18))
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.5 : 1.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        rightIconView.anchor(.trailing(-16), .centerY())
        
        titleLabel.anchor(
            .leading(32),
            .trailing(rightIconView.leadingAnchor, constant: -16),
            .centerY(rightIconView.centerYAnchor, constant: -4))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(rightIconView)
        self.contentView.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        self.backgroundColor = .clear

    }
    
    private func configure() {
        self.titleLabel.text = data.localized()
    }
}
