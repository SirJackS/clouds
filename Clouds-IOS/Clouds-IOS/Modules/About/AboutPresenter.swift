//
//  AboutPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.01.21.
//

import Foundation

final class AboutPresenter: AboutPresenterProtocol {
    
    private unowned let view: AboutViewProtocol
    private var service: ServiceProtocol
    
    init(view: AboutViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getFields()
    }
    
//    func search() {
//        let keywords = self.restaurantData?.keywords ?? []
//        self.view.handleOutput(.pushSearch(restaurant, keywords))
//    }
    
    private func getFields() {
        let fields: [String] = [
            "FAQ",
            "Terms and Conditions",
            "About Us"
        ]
        self.view.handleOutput(.showList(fields))
    }
}
