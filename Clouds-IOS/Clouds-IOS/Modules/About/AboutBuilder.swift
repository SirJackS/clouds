//
//  AboutBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.01.21.
//

import Foundation

final class AboutBuilder {
    
    static func make() -> AboutViewController {
        let view = AboutViewController()
        
        let presenter = AboutPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
