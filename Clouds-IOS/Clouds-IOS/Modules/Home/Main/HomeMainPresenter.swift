//
//  HomeMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import Foundation

final class HomeMainPresenter: HomeMainPresenterProtocol {
    
    private unowned let view: HomeMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: HomeMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getHome()
    }
    
    func getHome() {
        self.view.handleOutput(.showActivity(true))
        service.getHome { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                if let banners = data.banners {
                    self.view.handleOutput(.showBanner(banners))
                }
                if let campaigns = data.campaigns {
                    self.view.handleOutput(.showCampaign(campaigns))
                }
                if let categories = data.categories {
                    self.view.handleOutput(.showCategory(categories))
                }
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.home))
            
            default: break
            }
        }
    }
}
