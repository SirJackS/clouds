//
//  HomeMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import Foundation

// MARK: - Presenter

protocol HomeMainPresenterProtocol: class {
    func load()
}

enum HomeMainPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case showCategory([Category])
    case showBanner([BannerCampaign])
    case showCampaign([BannerCampaign])
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol HomeMainViewProtocol: class {
    func handleOutput(_ output: HomeMainPresenterOutput)
}
