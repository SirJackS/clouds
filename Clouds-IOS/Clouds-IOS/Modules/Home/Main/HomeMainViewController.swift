//
//  HomeMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import UIKit
import SDWebImage

final class HomeMainViewController: UIViewController, HomeMainViewProtocol {

    var presenter: HomeMainPresenterProtocol!
    
    
    private lazy var navBarView: HomeMainNavbarView = {
        let view = HomeMainNavbarView()
        view.delegate = self
        return view
    }()
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 11
        layout.scrollDirection = .vertical
        //layout.headerReferenceSize = CGSize(width: 0, height: 100);
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.clipsToBounds = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 16, left: 16, bottom: 8, right: 16)
        collectionView.register(HomeBannerFirstCell.self, forCellWithReuseIdentifier: HomeBannerFirstCell.ID)
        collectionView.register(HomeBannerSecondCell.self, forCellWithReuseIdentifier: HomeBannerSecondCell.ID)
        collectionView.register(HomeCampaignSecondCell.self, forCellWithReuseIdentifier: HomeCampaignSecondCell.ID)
        collectionView.register(HomeCampaignFirstCell.self, forCellWithReuseIdentifier: HomeCampaignFirstCell.ID)
        collectionView.register(HomeCategoryCell.self, forCellWithReuseIdentifier: HomeCategoryCell.ID)
        collectionView.register(EmptyCell.self, forCellWithReuseIdentifier: EmptyCell.ID)
//        collectionView.register(HomeMainHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeMainHeaderView.ID)
        return collectionView
    }()
    
    private var categories: [Category] = []
    private var banners: [BannerCampaign] = []
    private var campaigns: [BannerCampaign] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        collectionView.anchor(.fillSuperview())
    }

    func handleOutput(_ output: HomeMainPresenterOutput) {
        switch output {
        case .showCategory(let categories):
            self.categories = categories
            self.collectionView.reloadData()
        case .showBanner(let banners):
            self.banners = banners
            self.collectionView.reloadData()
        case .showCampaign(let campaigns):
            self.campaigns = campaigns
            self.collectionView.reloadData()
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .home:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(collectionView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.clear
        self.navigationItem.titleView = navBarView
        
        if !Defaults.instance.isLoggedIn {
            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
                let viewController = NewsBuilder.make()
                viewController.modalTransitionStyle = .crossDissolve
                viewController.modalPresentationStyle = .overFullScreen
                self.navigationController?.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleModalDismissedBySignup),
        name: NSNotification.Name(rawValue: "modalIsDimissed3"),
        object: nil)
    }
    
    // MARK: - Action
    
    @objc func handleModalDismissedBySignup() {
        let viewController =  CreateAccountBuilder.make()
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}


// MARK: - UICollectionView datasource and delegate

extension HomeMainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let total = banners.count+(categories.count>1 ? 2:0)+campaigns.count
        return total
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let emptycell = collectionView.dequeueReusableCell(withReuseIdentifier: EmptyCell.ID, for: indexPath) as? EmptyCell else { return UICollectionViewCell() }
        if campaigns.count > indexPath.row {
            if indexPath.row==0 {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCampaignFirstCell.ID, for: indexPath) as? HomeCampaignFirstCell else { return emptycell }
                cell.data = campaigns[0]
                return cell
            } else {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCampaignSecondCell.ID, for: indexPath) as? HomeCampaignSecondCell else { return emptycell }
                cell.data = campaigns[indexPath.row]
                return cell
            }
            
        } else if banners.count > 0 && banners.count+(categories.count>1 ? 2:0) > (indexPath.row-campaigns.count)  {
            if (indexPath.row-campaigns.count == 0) {
                if banners[0].description != nil {
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeBannerFirstCell.ID, for: indexPath) as? HomeBannerFirstCell else { return emptycell }
                    cell.data = banners[0]
                    return cell
                } else {
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeBannerSecondCell.ID, for: indexPath) as? HomeBannerSecondCell else { return UICollectionViewCell() }
                    cell.data = banners[0]
                    return cell
                }
            } else {
                if categories.count > 1 && (indexPath.row-campaigns.count-1<=1) {
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCategoryCell.ID, for: indexPath) as? HomeCategoryCell else { return emptycell }
                    cell.data = categories[indexPath.row-campaigns.count-1]
                    return cell
                } else {
                    if banners[indexPath.row-campaigns.count-(categories.count>1 ? 2:0)].description != nil {
                        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeBannerFirstCell.ID, for: indexPath) as? HomeBannerFirstCell else { return emptycell }
                        cell.data = banners[indexPath.row-campaigns.count-(categories.count>1 ? 2:0)]
                        return cell
                    } else {
                        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeBannerSecondCell.ID, for: indexPath) as? HomeBannerSecondCell else { return UICollectionViewCell() }
                        cell.data = banners[indexPath.row-campaigns.count-(categories.count>1 ? 2:0)]
                        return cell
                    }
                }
            }
            
        } else if banners.count == 0 && categories.count > 1 {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCategoryCell.ID, for: indexPath) as? HomeCategoryCell else { return emptycell }
            cell.data = categories[indexPath.row-campaigns.count]
            return cell
        }
        
        return emptycell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if campaigns.count > indexPath.row {
            if indexPath.row==0 {
                return CGSize(width: UIScreen.main.bounds.width - 32, height: 44)
            } else {
                return CGSize(width: UIScreen.main.bounds.width - 32, height: 178)
            }
            
        } else if banners.count > 0 && banners.count+(categories.count>1 ? 2:0) > (indexPath.row-campaigns.count)  {
            if (indexPath.row-campaigns.count == 0) {
                return CGSize(width: UIScreen.main.bounds.width - 32, height: 212)
            } else {
                if categories.count > 1 && (indexPath.row-campaigns.count-1<=1) {
                    return CGSize(width: (UIScreen.main.bounds.width - 43)/2, height: 243)
                } else {
                    return CGSize(width: UIScreen.main.bounds.width - 32, height: 212)
                }
            }
            
        } else if banners.count == 0 && categories.count > 1 {
            return CGSize(width: (UIScreen.main.bounds.width - 43)/2, height: 243)
        }

        
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if campaigns.count > indexPath.row {
            if indexPath.row==0 {
                let viewController = ProductListBuilder.make(.banner(campaigns[0].id))
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let viewController = ProductListBuilder.make(.banner(campaigns[indexPath.row].id))
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        } else if banners.count > 0 && banners.count+(categories.count>1 ? 2:0) > (indexPath.row-campaigns.count)  {
            if (indexPath.row-campaigns.count == 0) {
                let viewController = ProductListBuilder.make(.banner(banners[0].id))
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                if categories.count > 1 && (indexPath.row-campaigns.count-1<=1) {
                    let viewController = ProductListBuilder.make(.subcategory(categories[indexPath.row-campaigns.count-1].id, Defaults.instance.getGender(), Defaults.instance.getAge()))
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else {
                    let viewController = ProductListBuilder.make(.banner(banners[indexPath.row-campaigns.count-(categories.count>1 ? 2:0)].id))
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
        } else if banners.count == 0 && categories.count > 1 {
            let viewController = ProductListBuilder.make(.subcategory(categories[indexPath.row-campaigns.count].id, Defaults.instance.getGender(), Defaults.instance.getAge()))
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
        collectionView.deselectItem(at: indexPath, animated: false)
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeMainHeaderView.ID, for: indexPath) as! HomeMainHeaderView
//        headerView.adText = adText
//        headerView.bannerImage = bannerImage
//        return headerView
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        
//        let indexPath = IndexPath(row: 0, section: section)
//        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
//        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
//    }
}


extension HomeMainViewController: HomeMainNavbarViewDelegate {
    func notificationButtonClick() {
        guard Defaults.instance.isLoggedIn else {return}
        let viewController = NotificationsBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func searchButtonClick() {
        let viewController = SearchBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
