//
//  HomeMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import Foundation

final class HomeMainBuilder {
    
    static func make() -> HomeMainViewController {
        let view = HomeMainViewController()
        
        let presenter = HomeMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
