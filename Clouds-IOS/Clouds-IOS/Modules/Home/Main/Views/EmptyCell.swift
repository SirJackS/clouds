//
//  EmptyCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.03.21.
//

import UIKit
import SDWebImage

final class EmptyCell: UICollectionViewCell {
    
    static let ID: String = "EmptyCell"
    
    private lazy var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.38)
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        coverView.anchor(.fillSuperview(bottom:-16))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(coverView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
