//
//  HomeCampaignSecondCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.03.21.
//

import UIKit

final class HomeCampaignSecondCell: UICollectionViewCell {
    
    static let ID: String = "HomeCampaignSecondCell"
    
    var data: BannerCampaign! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
       
        bodyView.anchor(
            .top(),.bottom(-16),.leading(),.trailing())
        
        photoView.anchor(.fillSuperview(),.height(162))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        bodyView.addSubview(photoView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.cover ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
