//
//  HomeBannerSecondCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.01.21.
//

import UIKit
import SDWebImage

final class HomeBannerSecondCell: UICollectionViewCell {
    
    static let ID: String = "HomeBannerSecondCell"
    
    var data: BannerCampaign! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.38)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.contentMode = .center
        label.font = UIFont(weight: .bold, size: 24)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var viewLabel: UILabel = {
        let label = UILabel()
        label.text = "VIEW"
        label.textColor = .white
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        anchor(.width(UIScreen.main.bounds.width - 32), .height(212))
        
        coverView.anchor(.fillSuperview(bottom:-16))
        
        photoView.anchor(.fillSuperview(bottom:-16))
        
        titleLabel.anchor(
            .topGreater(self.topAnchor, constant: 32),
//            .top(10),
            .leading(16) ,.trailing(-16),
            .bottom(viewLabel.topAnchor, constant: -16),
            .height(64))
        
        viewLabel.anchor(
            .bottom(-40), .centerX(),
            .height(36), .width(90))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(photoView)
        self.addSubview(coverView)
        self.addSubview(titleLabel)
        self.addSubview(viewLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.cover ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
        self.titleLabel.text = data.name
        self.titleLabel.setLineSpacing(lineSpacing: 4.0)
    }
}

