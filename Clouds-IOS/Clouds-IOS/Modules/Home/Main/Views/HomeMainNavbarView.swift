//
//  HomeMainNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import UIKit

protocol HomeMainNavbarViewDelegate: class {
    func searchButtonClick()
    func notificationButtonClick()
}

final class HomeMainNavbarView: UIView {

    weak var delegate: HomeMainNavbarViewDelegate?
    
    lazy var logoView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo-navbar"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "search-icon"), for: .normal)
        button.tintColor = .primaryColor
        button.anchor(.width(24))
        button.anchor(.height(24))
        button.addTarget(self, action: #selector(searchButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var notificationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "notification-icon"), for: .normal)
        button.tintColor = .primaryColor
        button.anchor(.width(24))
        button.anchor(.height(24))
        button.addTarget(self, action: #selector(notificationButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    private lazy var redViewOnNotif: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "C70D3A")
        view.layer.cornerRadius = 5
        view.clipsToBounds  = true
        view.anchor(.size(width: 10, height: 10))
        return view
    }()

    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        logoView.anchor(.leading(16), .centerY(), .height(36), .width(82))
        
        searchButton.anchor(.trailing(-16), .centerY())
        
        notificationButton.anchor(.trailing(searchButton.leadingAnchor,constant: -16), .centerY())
        
        navbarLineView.anchor( .leading(), .trailing(), .bottom(), .height(0.3))
        
        redViewOnNotif.anchor(.trailing(notificationButton.trailingAnchor), .top(notificationButton.topAnchor, constant: -2))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(logoView)
        addSubview(searchButton)
        addSubview(notificationButton)
        addSubview(navbarLineView)
        addSubview(redViewOnNotif)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }

    
    // MARK: - Action
    
    @objc private func searchButtonTouchUp() {
        self.delegate?.searchButtonClick()
    }
    
    @objc private func notificationButtonTouchUp() {
        self.delegate?.notificationButtonClick()
    }
}
