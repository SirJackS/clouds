//
//  HomeBannerFirstCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.12.20.
//

import UIKit
import SDWebImage

final class HomeBannerFirstCell: UICollectionViewCell {
    
    static let ID: String = "HomeBannerFirstCell"
    
    var data: BannerCampaign! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    private lazy var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.38)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 16
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(weight: .bold, size: 24)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var viewLabel: UILabel = {
        let label = UILabel()
        label.text = "VIEW"
        label.textColor = .white
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        coverView.anchor(.fillSuperview(bottom:-16))
        
        photoView.anchor(.fillSuperview(bottom:-16))
        
        stackView.anchor(.top(constant: 32), .bottom(-40), .leading(16) ,.trailing(-16))
        
        viewLabel.anchor(.height(36), .width(90))
        
        titleLabel.anchor(.height(32))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
        self.descLabel.text = ""
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(photoView)
        self.addSubview(coverView)
        self.addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descLabel)
        stackView.addArrangedSubview(viewLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.cover ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
        self.titleLabel.text = data.name
        self.descLabel.text = data.description
    }
}
