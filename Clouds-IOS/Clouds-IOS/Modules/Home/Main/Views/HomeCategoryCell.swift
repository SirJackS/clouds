//
//  HomeCategoryCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 17.12.20.
//

import UIKit
import SDWebImage

final class HomeCategoryCell: UICollectionViewCell {
    
    static let ID: String = "HomeCategoryCell"
    
    var data: Category! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var coverView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.38)
        return view
    }()
    
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .equalCentering
        stackView.spacing = 16
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.anchor(.leading(), .trailing(), .bottom(-15), .height(24))
        
        coverView.anchor(.top(), .leading(), .trailing(), .bottom(titleLabel.topAnchor,constant: -8))
        
        photoView.anchor(.top(), .leading(), .trailing(), .bottom(titleLabel.topAnchor,constant: -8))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(photoView)
        self.addSubview(coverView)
        self.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.clipsToBounds = true
    }
    
    private func configure() {
        self.titleLabel.text = data.name
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
