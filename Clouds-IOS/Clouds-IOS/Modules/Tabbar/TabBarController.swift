//
//  TabBarController.swift
//  Clouds
//
//  Created by apple on 10/9/20.
//

import UIKit

final class TabBarController: UITabBarController, TabBarViewProtocol {
    
    var presenter: TabBarPresenterProtocol!
    
    var isSelected: Bool = false
    
    private lazy var tabBarItem1: UITabBarItem = {
        let tabBarItem1: UITabBarItem = UITabBarItem(
            title: "Home".localized(),
            image: UIImage(named:"tabbar1")?.withRenderingMode(.alwaysOriginal),
            selectedImage: UIImage(named:"tabbar1S")?.withRenderingMode(.alwaysOriginal))
        tabBarItem1.tag = 0
        tabBarItem1.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        tabBarItem1.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        return tabBarItem1
    }()
    
    private lazy var tabBarItem2: UITabBarItem = {
        let tabBarItem2: UITabBarItem = UITabBarItem(
            title: "Categories".localized(),
            image: UIImage(named:"tabbar2")?.withRenderingMode(.alwaysOriginal),
            selectedImage: UIImage(named:"tabbar2S")?.withRenderingMode(.alwaysOriginal))
        tabBarItem2.tag = 1
        tabBarItem2.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        tabBarItem2.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        return tabBarItem2
    }()
    
    private lazy var raisedButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "tabbar3"), for: .normal)
        button.setImage(UIImage(named: "tabbar3"), for: .highlighted)
        button.backgroundColor = .secondaryColor
        button.tintColor = UIColor.white
        button.layer.cornerRadius = 28
        button.clipsToBounds = true
        //button.layer.removeAllAnimations()
        button.anchor(.size(width: 56, height: 56))
        button.addTarget(self, action: #selector(raisedButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var countLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.backgroundColor = .black
        label.textAlignment = .center
        label.layer.cornerRadius = 9
        label.clipsToBounds = true
        label.text = "0"
        label.adjustsFontSizeToFitWidth = true
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        label.anchor(.size(width: 18, height: 18))
        label.isHidden = true
        return label
    }()
    
    private lazy var tabBarItem3: UITabBarItem = {
        let tabBarItem3: UITabBarItem = UITabBarItem()
        tabBarItem3.tag = 2
        return tabBarItem3
    }()
    
    private lazy var tabBarItem4: UITabBarItem = {
        let tabBarItem4: UITabBarItem = UITabBarItem(
            title: "Wish List".localized(),
            image: UIImage(named:"tabbar4")?.withRenderingMode(.alwaysOriginal),
            selectedImage: UIImage(named:"tabbar4S")?.withRenderingMode(.alwaysOriginal))
        tabBarItem4.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        tabBarItem4.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        tabBarItem4.tag = 3
        return tabBarItem4
    }()
    
    private lazy var tabBarItem5: UITabBarItem = {
        let tabBarItem5: UITabBarItem = UITabBarItem(
            title: "Profile".localized(),
            image: UIImage(named:"tabbar5")?.withRenderingMode(.alwaysOriginal),
            selectedImage: UIImage(named:"tabbar5S")?.withRenderingMode(.alwaysOriginal))
        tabBarItem5.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        tabBarItem5.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .selected)
        tabBarItem5.tag = 4
        return tabBarItem5
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.selectedIndex = 0
    }
    
    override func updateViewConstraints() {
        raisedButton.anchor(.centerX(), .top(-15))
        
        countLabel.anchor(
            .top(raisedButton.topAnchor, constant: 10),
            .trailing(raisedButton.trailingAnchor, constant: -10))
        
        super.updateViewConstraints()
        
    }

    func handleOutput(_ output: TabBarPresenterOutput) {
        switch output {
        case .show(let data):
            countLabel.text = "\(data.items?.count ?? 0)"
            countLabel.isHidden = false
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getCartItems:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }

    }
    
    // MARK: - Private
    
    private func addSubviews() {
        
        let FirstVC = HomeMainBuilder.make()
        FirstVC.tabBarItem = tabBarItem1
        
        let SecondVC = CategoryMainBuilder.make()
        SecondVC.tabBarItem = tabBarItem2
        
        var ThirdVC = UIViewController()
        var FourthVC = UIViewController()
        var FifthVC = UIViewController()
        
        if Defaults.instance.isLoggedIn {
            ThirdVC = BagBuilder.make()
            FourthVC = WishListBuilder.make()
            FifthVC = ProfileBuilder.make()
        } else {
            ThirdVC = NoAuthBuilder.make(navTitle: "My Bag".localized(), descTitle: "My Bag".localized())
            FourthVC = NoAuthBuilder.make(navTitle: "Saved Items".localized(), descTitle: "Wish List".localized())
            FifthVC = NoAuthBuilder.make(navTitle: "Profile".localized(), descTitle: "Profile".localized())
        }
        ThirdVC.tabBarItem = tabBarItem3
        FourthVC.tabBarItem = tabBarItem4
        FifthVC.tabBarItem = tabBarItem5
        
        let viewControllerList = [FirstVC,SecondVC,ThirdVC,FourthVC,FifthVC]
        viewControllers = viewControllerList.map {
            MainNavigation(rootViewController: $0)
        }
        
        self.tabBar.addSubview(raisedButton)
        self.tabBar.addSubview(countLabel)
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = UIColor.black
        self.delegate = self
        self.tabBar.isTranslucent = false
        self.tabBar.backgroundColor = .white
        self.view.backgroundColor = .white
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.bagUpdated(_:)), name: NSNotification.Name(rawValue: "bagUpdated"), object: nil)
        
    }
    
    @objc func bagUpdated(_ notification: NSNotification) {
        if let count = notification.userInfo?["count"] as? Int {
            countLabel.text = "\(count)"
            countLabel.isHidden = false
        }
     }
    
    // MARK: - Action
    
    @objc private func raisedButtonTouchUp() {
        isSelected = true
        raisedButton.backgroundColor = UIColor.black
        countLabel.backgroundColor = UIColor.secondaryColor
        countLabel.textColor = UIColor.black
        self.selectedIndex = 2
    }
}

extension TabBarController: UITabBarControllerDelegate {
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("item", item.tag)
        if item.tag != 2 {
            isSelected = false
            raisedButton.backgroundColor = UIColor.secondaryColor
            countLabel.backgroundColor = .black
            countLabel.textColor = .white
        } else {
            self.selectedIndex = 2
        }
    }
}
    
    
//    private func setup() {
//        tabBarItem1.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -30)
//        tabBarItem2.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -30)
//        tabBarItem3.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -30)
//        tabBarItem4.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -30)
//        tabBarItem5.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -30)
//        tabBarItem1.imageInsets = UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 0)
//        tabBarItem2.imageInsets = UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 0)
//        tabBarItem3.imageInsets = UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 0)
//        tabBarItem4.imageInsets = UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 0)
//        tabBarItem5.imageInsets = UIEdgeInsets(top: -8, left: 0, bottom: 8, right: 0)
//    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//
//        var safeAreaBottomInset: CGFloat = 0.0
//
//        if #available(iOS 11.0, *) {
//            safeAreaBottomInset = view.safeAreaInsets.bottom
//        }
//        var tabFrame = self.tabBar.frame
//        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
//        tabFrame.size.height = 98 + safeAreaBottomInset
//        tabFrame.origin.y = self.view.frame.size.height - 98 - safeAreaBottomInset
//        self.tabBar.frame = tabFrame
//        self.tabBar.invalidateIntrinsicContentSize()
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        tabBar.invalidateIntrinsicContentSize()
//        tabBar.superview?.setNeedsLayout()
//        tabBar.superview?.layoutSubviews()
//    }
//    override func updateViewConstraints() {
//        super.updateViewConstraints()
//        self.tabBar.anchor(.bottom(view.safeAreaLayoutGuide.bottomAnchor, constant: 1))
//    }
//}

//class CustomTabBar : UITabBar {
//
//    @IBInspectable var height: CGFloat = 65.0
//
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        guard let window = UIApplication.shared.keyWindow else {
//            return super.sizeThatFits(size)
//        }
//        var sizeThatFits = super.sizeThatFits(size)
//        if height > 0.0 {
//
//            if #available(iOS 11.0, *) {
//                sizeThatFits.height = height + window.safeAreaInsets.bottom
//            } else {
//                sizeThatFits.height = height
//            }
//        }
//        return sizeThatFits
//    }
//}
