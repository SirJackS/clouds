//
//  TabBarPresenter.swift
//  Clouds
//
//  Created by apple on 10/9/20.
//

import Foundation

final class TabBarPresenter: TabBarPresenterProtocol {
    
    private unowned let view: TabBarViewProtocol
    private var service: ServiceProtocol
    
    init(view: TabBarViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getCartItems()
    }
    
    private func getCartItems() {
        self.view.handleOutput(.showActivity(true))
        service.getCart { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.show(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)

            default: break
            }

        }
    }
}

