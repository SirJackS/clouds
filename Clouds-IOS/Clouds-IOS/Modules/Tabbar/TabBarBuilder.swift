//
//  TabBarBuilder.swift
//  Clouds
//
//  Created by apple on 10/9/20.
//

import UIKit

final class TabBarBuilder {
    
    static func make() -> TabBarController {
        let view = TabBarController()
    
        let presenter = TabBarPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
