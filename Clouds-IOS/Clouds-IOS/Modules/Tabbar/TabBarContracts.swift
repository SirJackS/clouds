//
//  TabBarContracts.swift
//  Clouds
//
//  Created by apple on 10/9/20.
//

import Foundation

// MARK: - Presenter

protocol TabBarPresenterProtocol: class {
    func load()
}

enum TabBarPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case show(Bag)
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol TabBarViewProtocol: class {
    func handleOutput(_ output: TabBarPresenterOutput)
}
