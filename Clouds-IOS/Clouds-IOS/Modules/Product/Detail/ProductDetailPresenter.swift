//
//  ProductDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

final class ProductDetailPresenter: ProductDetailPresenterProtocol {
    
    private unowned let view: ProductDetailViewProtocol
    private var service: ServiceProtocol
    private var id: Int
    
    init(view: ProductDetailViewProtocol,
         service: ServiceProtocol,
         id: Int) {
        self.view = view
        self.service = service
        self.id = id
    }
    
    func load() {
        self.getProductBy(id: self.id)
    }
    
    func getProductBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.getProductBy(id) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showProduct(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getProductBy))
                
            default: break
            }
        }
    }
    
    func addWishlistBy(id: Int, isMain: Bool) {
        self.view.handleOutput(.showActivity(true))
        let request = AddToWishlistRequest(productId: id)
        service.addToWishlist(request){ [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.refreshWishList(isMain))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addToWishlist))
                
            default: break
            }
            
        }
    }
    
    func removeWishlistBy(id: Int, isMain: Bool) {
        self.view.handleOutput(.showActivity(true))
        service.removeFromWishlist(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.refreshWishList(isMain))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.removeFromWishlist))
                
            default: break
            }
        }
    }
    
    func addBagBy(id: Int, quantity: Int, size: String, color: String) {
        self.view.handleOutput(.showActivity(true))
        let request = AddBagItemRequest(productId: id, quantity: quantity, size: size, color: color)
        service.addBagItem(request){ [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.refreshBag)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addToWishlist))
                
            default: break
            }
            
        }
    }
    
    func removeFromBagBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.deletBagItem(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.refreshBag)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                
            default: break
            }
        }
    }
    
    func getSizesWithColor(id: Int, color: String) {
        self.view.handleOutput(.showActivity(true))
        service.getSizesWithColor(id, color) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showSizeList(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
}
