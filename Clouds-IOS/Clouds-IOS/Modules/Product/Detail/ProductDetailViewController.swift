//
//  ProductDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit

final class ProductDetailViewController: UIViewController, ProductDetailViewProtocol {

    var presenter: ProductDetailPresenterProtocol!
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.isHidden = true
        return view
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back-arrow-icon"), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 48, height: 24))
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(backButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var sliderCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize.init(width: UIScreen.main.bounds.width, height: self.sliderHeight)
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.isPagingEnabled = true
        collectionView.clipsToBounds = false
        collectionView.bounces = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(SliderCell.self, forCellWithReuseIdentifier: SliderCell.ID)
        return collectionView
    }()
    
    private lazy var colorCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize.init(width: 24, height: 24)
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
//        layout.flipsHorizontallyInOppositeLayoutDirection = true
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.isPagingEnabled = false
        collectionView.clipsToBounds = false
//        collectionView.semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        collectionView.bounces = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.transform = CGAffineTransform.init(scaleX: -1, y: 1)
        collectionView.register(ColorCell.self, forCellWithReuseIdentifier: ColorCell.ID)
        return collectionView
    }()
    
    private var sliderHeight: CGFloat {
        let height = UIScreen.main.bounds.height
        return (480 / 812) * height
    }
    
    private lazy var productCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 15
        layout.scrollDirection = .horizontal
        layout.itemSize = .init(width: (UIScreen.main.bounds.width-48)/2, height: productCollectionHeight)
        //layout.headerReferenceSize = CGSize(width: 0, height: 100);
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.clipsToBounds = false
        collectionView.isPagingEnabled = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        collectionView.register(ProductListCell.self, forCellWithReuseIdentifier: ProductListCell.ID)
        return collectionView
    }()
    
    private lazy var similarProductsView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var similarLabel: UILabel = {
        let label = UILabel()
        label.text = "Similar Products".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        label.anchor(.height(24))
        return label
    }()
    
    private var productCollectionHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (252 / 375) * width
    }
    
    private lazy var addToBagButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("add to bag".localized().uppercased(), for: .normal)
        button.setTitle("added to bag".localized().uppercased(), for: .selected)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.addTarget(self, action: #selector(addBagButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "heart-icon"), for: .normal)
        button.setImage(UIImage(named: "filled-heart-icon"), for: .selected)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.anchor(.size(width: 24, height: 24))
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var realPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var discountPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "C70D3A")
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        label.isHidden = true
        return label
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.14)
        pageControl.currentPageIndicatorTintColor = UIColor.black.withAlphaComponent(0.6)
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    private lazy var itemsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [sizeItemButton, productDetailsItemButton])
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        return stackView
    }()
    
    private lazy var sizeItemButton: DetailInfoButton = {
        let button = DetailInfoButton(name: "Size".localized())
        button.addTarget(self, action: #selector(infoItemButtonTouchUp(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var productDetailsItemButton: DetailInfoButton = {
        let button = DetailInfoButton(name: "Product Details".localized())
        button.addTarget(self, action: #selector(infoItemButtonTouchUp(_:)), for: .touchUpInside)
        return button
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    // MARK: - Data
    private var selectedColor: Int? {
        didSet {
            self.sizeList = []
            self.selectedSize = nil
            self.sizeItemButton.value = "Size".localized()
            self.addToBagButton.isSelected = false
            self.getSizesWithColor()
        }
    }
    private var selectedSize: Size? {
        didSet {
            self.addToBagButton.isSelected = false
        }
    }
    private var data: ProductDetail?
    private var colorList: [Color] = []
    private var sizeList: [Size] = []
    private var productList: [ProductList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    //reappears navigation bar on next page
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.top(), .leading(), .trailing(), .bottom())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        sliderCollection.anchor(
            .top(), .leading(), .trailing(), .height(self.sliderHeight))
        
        colorCollection.anchor(
            .top(titleLabel.bottomAnchor,constant: 15), .trailing(-16), .height(24))
        
        pageControl.anchor(
            .bottom(sliderCollection.bottomAnchor,constant: -32),
            .centerX())
        
        realPriceLabel.anchor(
            .leading(16), .top(titleLabel.bottomAnchor, constant: 10),
            .trailing(discountPriceLabel.leadingAnchor, constant: -7))
        
        discountPriceLabel.anchor(
            .trailing(colorCollection.leadingAnchor, constant:-16),
            .top(titleLabel.bottomAnchor, constant: 10))
        
        titleLabel.anchor(
            .leading(16), .trailing(-16),
            .top(sliderCollection.bottomAnchor, constant: 24), .height(20))
        
        lineView.anchor(.leading(16), .trailing(-16), .bottom(itemsStack.topAnchor), .height(0.3))
        
        itemsStack.anchor(
            .top(titleLabel.bottomAnchor, constant: 60),
            .leading(16), .trailing(-16), .height(80))
        
        addToBagButton.anchor(
            .leading(16),
            .top(itemsStack.bottomAnchor, constant: 24))
        
        saveButton.anchor(
            .trailing(-16),
            .leading(addToBagButton.trailingAnchor, constant: 24),
            .centerY(addToBagButton.centerYAnchor))
        
        similarProductsView.anchor(
            .top(addToBagButton.bottomAnchor, constant: 64),
            .leading(), .trailing(), .bottom(-24))
        
        similarLabel.anchor(.top(),.leading(16),.trailing())
        
        productCollection.anchor(
            .top(similarLabel.bottomAnchor, constant: 24),
            .leading(), .trailing(), .bottom(), .height(productCollectionHeight))
        
        backButton.anchor(.top(view.layoutMarginsGuide.topAnchor, constant:16), .leading(16))
    }

    func handleOutput(_ output: ProductDetailPresenterOutput) {
        switch output {
        case .showProduct(let data):
            self.data = data
            self.sliderCollection.reloadData()
            self.pageControl.numberOfPages = data.product.images.count
            self.productList = data.similarProducts ?? []
            self.productCollection.reloadData()
            self.colorList = data.colors ?? []
            self.colorCollection.reloadData()
            self.setupData(data)
            self.contentView.isHidden = false
        case .refreshWishList(let isMain):
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshWishlist"), object: nil)
            if isMain {
                self.saveButton.isSelected = !self.saveButton.isSelected
            }
        case .refreshBag:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBag"), object: nil)
            UINotificationFeedbackGenerator().notificationOccurred(.success)
            self.addToBagButton.isSelected = !self.addToBagButton.isSelected
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getProductBy:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        case .showSizeList(let sizes):
            self.sizeList = sizes
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        self.view.addSubview(backButton)
        
        mainScrollView.addSubview(contentView)
        contentView.addSubview(sliderCollection)
        contentView.addSubview(colorCollection)
        contentView.addSubview(pageControl)
        contentView.addSubview(titleLabel)
        contentView.addSubview(realPriceLabel)
        contentView.addSubview(discountPriceLabel)
        contentView.addSubview(addToBagButton)
        contentView.addSubview(saveButton)
        contentView.addSubview(similarProductsView)
        contentView.addSubview(itemsStack)
        contentView.addSubview(lineView)
        
        similarProductsView.addSubview(productCollection)
        similarProductsView.addSubview(similarLabel)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .white
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    private func setupData(_ data: ProductDetail) {
        self.titleLabel.text = data.product.title
        if let discount = data.product.salePrice {
            self.discountPriceLabel.text = "\(discount) AZN"
            self.discountPriceLabel.isHidden = false
            self.realPriceLabel.attributedText = "\(data.product.price ?? "") AZN".strikeThrough()
        } else {
            self.realPriceLabel.text = "\(data.product.price ?? "") AZN"
            self.realPriceLabel.textColor = UIColor.black.withAlphaComponent(0.87)
            self.realPriceLabel.font = UIFont(weight: .medium, size: 16)
        }
        if data.product.wishlist == true {
            self.saveButton.isSelected = true
        }
        if self.colorList.count > 0 {
            colorCollection.selectItem(at: IndexPath(item: 0, section: 0), animated: false, scrollPosition: .right)
            selectedColor = 0
        }
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleModalDismissed),
        name: NSNotification.Name(rawValue: "modalIsDimissed"),
        object: nil)
    }
    
    // MARK: - Action
    @objc func saveButtonTouchUp() {
        guard Defaults.instance.isLoggedIn else {return}
        guard let id = data?.product.id else { return }
        if self.saveButton.isSelected == true {
            self.saveButton.isEnabled = false
            self.presenter.removeWishlistBy(id: id, isMain: true)
            self.saveButton.isEnabled = true
        } else {
            self.saveButton.isEnabled = false
            self.presenter.addWishlistBy(id: id, isMain: true)
            self.saveButton.isEnabled = true
        }
    }
    
    @objc func addBagButtonTouchUp() {
        guard Defaults.instance.isLoggedIn else {return}
        guard let id = data?.product.id else { return }
        if self.addToBagButton.isSelected == true {
            self.addToBagButton.isEnabled = false
            self.presenter.removeFromBagBy(id: id)
            self.addToBagButton.isEnabled = true
        } else {
            guard let id = data?.product.id else { return }
            guard let colorIndex = selectedColor else { return self.showMessage("Choose color".localized())}
            guard let color = colorList[colorIndex].name else { return self.showMessage("Error in color".localized())}
            guard let size = selectedSize?.name else { return self.showMessage("Choose size".localized())}
            self.addToBagButton.isEnabled = false
            self.presenter.addBagBy(id: id, quantity: 1, size: size, color: color)
            self.addToBagButton.isEnabled = true
        }
    }
    
    @objc func backButtonTouchUp() {
        self.popVC()
    }
    
    @objc func handleModalDismissed() {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc private func infoItemButtonTouchUp(_ button: DetailInfoButton) {
        switch button {
        case self.sizeItemButton:
            guard sizeList.count != 0 else { return self.showMessage("There is no size".localized())}
            let viewController = ProductDetailPickerBuilder.make(data: sizeList)
            viewController.delegate = self
            viewController.modalTransitionStyle = .coverVertical
            viewController.modalPresentationStyle = .overCurrentContext
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.present(viewController, animated: true, completion: nil)

        case self.productDetailsItemButton:
            let viewController = ProductDetailInfoBuilder.make()
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)

        default: break
        }
    }
    
    func getSizesWithColor() {
        guard let colorIndex = selectedColor else { return self.showMessage("Choose color".localized())}
        guard let color = colorList[colorIndex].name else { return self.showMessage("Error in size".localized())}
        guard let id = data?.product.id else { return }
        self.presenter.getSizesWithColor(id: id, color: color)
    }
}

// MARK: - UICollectionView datasource and delegate

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case sliderCollection:
            return data?.product.images.count ?? 0
        case colorCollection:
            return colorList.count
        case productCollection:
            return productList.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case sliderCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderCell.ID, for: indexPath) as! SliderCell
            cell.data = data?.product.images[indexPath.row]
            return cell
        case colorCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ColorCell.ID, for: indexPath) as! ColorCell
            cell.data = colorList[indexPath.row]
            return cell
        case productCollection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListCell.ID, for: indexPath) as! ProductListCell
            cell.data = productList[indexPath.row]
            cell.delegate = self
            return cell
        default:
            return UICollectionViewCell.init()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case colorCollection:
            collectionView.allowsMultipleSelection = false
            guard let cell = collectionView.cellForItem(at: indexPath) as? ColorCell else { return }

            if cell.isSelected {
//                if indexPath.row == self.selectedColor {
//                    collectionView.deselectItem(at: indexPath, animated: false)
//                    self.selectedColor = nil
//                } else {
//                    self.selectedColor = indexPath.row
//                }
                if indexPath.row != self.selectedColor {
                    self.selectedColor = indexPath.row
                }
            } else {
                self.selectedColor = nil
            }
        case productCollection:
            let viewController = ProductDetailBuilder.make(id: productList[indexPath.row].id)
            self.navigationController?.pushViewController(viewController, animated: true)
        default:
            break
        }
        //collectionView.deselectItem(at: indexPath, animated: false)
    }
}

// MARK: - Picker view delegate

extension ProductDetailViewController: ProductDetailPickerViewDelegate {
    func selected(data: Size) {
        self.sizeItemButton.value = data.name?.capitalized ?? "Size".localized()
        self.selectedSize = data
    }
}

// MARK: - Scroll view delegate

extension ProductDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let collectionView = scrollView as? UICollectionView {
            switch collectionView {
            case sliderCollection:
                let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
                self.pageControl.currentPage = Int(pageNumber)
                
            default: break
            }
        }
        
        switch scrollView {
        case mainScrollView:
            if mainScrollView.contentOffset.y > 50 &&  self.backButton.isHidden == false {
                UIView.transition(
                    with: self.backButton, duration: 0.5,
                    options: .curveEaseOut,
                    animations: {
                        self.backButton.isHidden = true
                    })
            }
            
            if mainScrollView.contentOffset.y < 50 &&  self.backButton.isHidden == true {
                UIView.transition(
                    with: self.backButton, duration: 0.5,
                    options: .curveEaseOut,
                    animations: {
                        self.backButton.isHidden = false
                    })
            }
        default: break
        }
    }
}


// MARK: - ProductListCellDelegate

extension ProductDetailViewController: ProductListCellDelegate {
    func addWishlistBy(id: Int) {
        self.presenter.addWishlistBy(id: id, isMain: false)
        self.presenter.load()
    }
    
    func removeWishlistBy(id: Int) {
        self.presenter.removeWishlistBy(id: id, isMain: false)
        self.presenter.load()
    }
    
}
