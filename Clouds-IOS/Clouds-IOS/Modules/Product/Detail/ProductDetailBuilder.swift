//
//  ProductDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

final class ProductDetailBuilder {
    
    static func make(id: Int) -> ProductDetailViewController {
        let view = ProductDetailViewController()
        
        let presenter = ProductDetailPresenter(view: view, service: App.service, id: id)
        view.presenter = presenter
        
        return view
    }
}
