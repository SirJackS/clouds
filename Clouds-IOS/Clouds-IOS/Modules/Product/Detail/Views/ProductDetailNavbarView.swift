//
//  ProductDetailNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit

protocol ProductDetailNavbarViewDelegate: class {
    func backButtonClick()
}

final class ProductDetailNavbarView: UIView {

    weak var delegate: ProductDetailNavbarViewDelegate?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back-arrow-icon"), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 48, height: 24))
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(backButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()

    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        backButton.anchor(.leading(), .centerY())
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width-32, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(backButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func backButtonTouchUp() {
        self.delegate?.backButtonClick()
    }
}
