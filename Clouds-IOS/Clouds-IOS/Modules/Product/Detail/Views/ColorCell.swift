//
//  ColorCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation


import UIKit

final class ColorCell: UICollectionViewCell {
    
    static let ID: String = "ColorCell"
    
    var data: Color! {
        didSet {
            self.configure()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.setSelect()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.clear.cgColor
        return view
    }()
    
    private lazy var colorView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview())
        
        colorView.anchor(.fillSuperviewPadding(4), .width(16), .height(16))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.colorView.backgroundColor = .clear
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(colorView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        if let color = data.code {
            self.colorView.backgroundColor = UIColor(hexString: color)
        }
    }
    
    private func setSelect() {
        if isSelected {
            bodyView.layer.borderColor = UIColor.black.withAlphaComponent(0.38).cgColor
        } else {
            bodyView.layer.borderColor = UIColor.clear.cgColor
        }
    }
}
