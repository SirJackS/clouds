//
//  SliderCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import UIKit

final class SliderCell: UICollectionViewCell {
    
    static let ID: String = "SliderCell"
    
    var data: ProductImage! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill//.scaleAspectFill
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview())
        
        photoView.anchor(.fillSuperview())
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
    
    private func configure() {
        if let imageUrl = URL(string: data.path ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
