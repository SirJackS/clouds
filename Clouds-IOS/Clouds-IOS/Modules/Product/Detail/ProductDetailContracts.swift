//
//  ProductDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

// MARK: - Presenter

protocol ProductDetailPresenterProtocol: class {
    func load()
    func addWishlistBy(id: Int, isMain: Bool)
    func removeWishlistBy(id: Int, isMain: Bool)
    func getSizesWithColor(id: Int, color: String)
    func addBagBy(id: Int, quantity: Int, size: String, color: String)
    func removeFromBagBy(id: Int)
    func getProductBy(id: Int)
}

enum ProductDetailPresenterOutput {
    case showMessage(String)
    case showProduct(ProductDetail)
    case showSizeList([Size])
    case showActivity(Bool)
    case refreshWishList(Bool)
    case refreshBag
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol ProductDetailViewProtocol: class {
    func handleOutput(_ output: ProductDetailPresenterOutput)
}
