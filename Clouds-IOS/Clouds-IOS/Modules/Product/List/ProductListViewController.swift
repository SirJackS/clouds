//
//  ProductListViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import UIKit
import SDWebImage

final class ProductListViewController: UIViewController, ProductListViewProtocol {

    var presenter: ProductListPresenterProtocol!
    
    
    private lazy var navBarView: ProductListNavbarView = {
        let view = ProductListNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private var productCollectionHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (252 / 375) * width
    }
    
    private lazy var productCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 15
        layout.scrollDirection = .vertical
        layout.itemSize = .init(width: (UIScreen.main.bounds.width-48)/2, height: productCollectionHeight)
        //layout.headerReferenceSize = CGSize(width: 0, height: 100);
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .white
        collectionView.clipsToBounds = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 16, right: 16)
        collectionView.register(ProductListCell.self, forCellWithReuseIdentifier: ProductListCell.ID)
        collectionView.register(ProductListHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProductListHeaderView.ID)
        collectionView.isHidden = true
        return collectionView
    }()
    
//    private var noScroll: Bool = false
    
    private var data: [ProductList] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        productCollection.anchor(.fillSuperview())
    }

    func handleOutput(_ output: ProductListPresenterOutput) {
        switch output {
        case .setNavbarTitle(let title):
            if title.count < 20 {
                self.navBarView.titleLabel.text = title
            } else {
                self.navBarView.titleLabel.text = "\(title.substring(toIndex: 20))..."
            }
        case .dismiss:
            self.popVC()
        case .showList(let data):
            self.data.removeAll()
            self.data = data
            self.productCollection.reloadData()
            self.productCollection.isHidden = false
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getSubcategoryProductsBy:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(productCollection)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
        
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleModalDismissed),
        name: NSNotification.Name(rawValue: "sortDimissed"),
        object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.filterPressed),
        name: NSNotification.Name(rawValue: "filterViewPressed"),
        object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.sortUpdated(_:)), name: NSNotification.Name(rawValue: "sortUpdated"), object: nil)
        
    }
    
    @objc func sortUpdated(_ notification: NSNotification) {
        if let id = notification.userInfo?["id"] as? Int {
            self.presenter.sortId = id
            self.filterPressed()
        }
    }
    
    // MARK: - Action
    
    
    @objc func handleModalDismissed() {
        //self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc func filterPressed() {
        self.presenter.getFilterProductsBy(
            categoryIds: FilterManager.instance.categories.map({($0.id ?? 0)}),
            productTypeIds: FilterManager.instance.productTypes.map({($0.id ?? 0)}),
            brandIds: FilterManager.instance.brands.map({($0.id ?? 0)}),
            colors: FilterManager.instance.colors.map({($0.name)}),
            sizes: FilterManager.instance.sizes.map({($0.name)}),
            priceMin: FilterManager.instance.minPrice,
            priceMax: FilterManager.instance.maxPrice)
    }
}


// MARK: - UICollectionView datasource and delegate

extension ProductListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case productCollection:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductListCell.ID, for: indexPath) as? ProductListCell else { return UICollectionViewCell() }
            cell.data = data[indexPath.row]
            cell.delegate = self
            return cell
        default:
            return UICollectionViewCell.init()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = ProductDetailBuilder.make(id: data[indexPath.row].id)
        self.navigationController?.pushViewController(viewController, animated: true)
        
        collectionView.deselectItem(at: indexPath, animated: false)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ProductListHeaderView.ID, for: indexPath) as! ProductListHeaderView
        headerView.delegate = self
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let indexPath = IndexPath(row: 0, section: section)
        let headerView = self.collectionView(collectionView, viewForSupplementaryElementOfKind: UICollectionView.elementKindSectionHeader, at: indexPath)
        return headerView.systemLayoutSizeFitting(CGSize(width: collectionView.frame.width, height: UIView.layoutFittingExpandedSize.height),withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
    }
}

extension ProductListViewController: ProductListNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

extension ProductListViewController: ProductListHeaderDelegate {
    
    func filterButtonTouchUp() {
        let viewController = MainNavigation(rootViewController: FilterMainBuilder.make())
        viewController.modalPresentationStyle = .fullScreen
        viewController.modalTransitionStyle = .coverVertical
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func sortButtonTouchUp() {
        let viewController = SortDropDownBuilder.make(selected: self.presenter.sortId)
        viewController.modalTransitionStyle = .crossDissolve
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true, completion: nil)
    }
}

// MARK: - ProductListCellDelegate

extension ProductListViewController: ProductListCellDelegate {
    func addWishlistBy(id: Int) {
        self.presenter.addWishlistBy(id: id)
        self.presenter.load()
    }
    
    func removeWishlistBy(id: Int) {
        self.presenter.removeWishlistBy(id: id)
        self.presenter.load()
    }
    
}
