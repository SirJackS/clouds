//
//  ProductListHeaderView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.01.21.
//

import UIKit

protocol ProductListHeaderDelegate: class {
    func filterButtonTouchUp()
    func sortButtonTouchUp()
}

final class ProductListHeaderView: UICollectionReusableView {
    
    static let ID: String = "ProductListHeaderView"

    weak var delegate: ProductListHeaderDelegate?
    
    private lazy var buttonView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [sortButton, filterButton])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        return stackView
    }()
    
    private lazy var sortButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Sort", for: .normal)
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.contentHorizontalAlignment = .center
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        //change image and text place
        button.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(sortButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var filterButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.setTitle("Filter", for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.contentHorizontalAlignment = .center
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        //change image and text place
        button.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(filterButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var verticalLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    private lazy var horizontalLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        horizontalLineView.anchor(
            .top(4), .centerX(), .width(0.5),
            .bottom(verticalLineView.topAnchor, constant: -4))
        
        verticalLineView.anchor(.leading(), .bottom(-16), .trailing(), .height(0.3))
        
        buttonView.anchor(
            .top(), .leading(16), .trailing(-16), .height(40),
            .bottom(verticalLineView.topAnchor))
        
        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(buttonView)
        self.addSubview(horizontalLineView)
        self.addSubview(verticalLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    // MARK: - Action
    
    @objc func filterButtonTouchUp() {
        self.delegate?.filterButtonTouchUp()
    }
    
    @objc func sortButtonTouchUp() {
        self.delegate?.sortButtonTouchUp()
    }
}
