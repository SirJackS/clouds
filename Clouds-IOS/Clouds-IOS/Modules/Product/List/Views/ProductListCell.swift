//
//  ProductListCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import UIKit
import SDWebImage

protocol ProductListCellDelegate: class {
    func addWishlistBy(id: Int)
    func removeWishlistBy(id: Int)
}


final class ProductListCell: UICollectionViewCell {
    
    static let ID: String = "ProductListCell"
    
    weak var delegate: ProductListCellDelegate?
    
    var data: ProductList! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var realPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var discountPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "C70D3A")
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.isHidden = true
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "heart-icon"), for: .normal)
        button.setImage(UIImage(named: "filled-heart-icon"), for: .selected)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 24, height: 24))
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var discountView: UIView = {
        let view = UIView(backgroundColor: UIColor.white)
        view.anchor(.height(20))
        view.isHidden = true
        return view
    }()
    
    private lazy var discountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "C70D3A")
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //self.setNeedsDisplay()
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.realPriceLabel.font = UIFont(weight: .medium, size: 14)
        self.realPriceLabel.textColor = UIColor.black.withAlphaComponent(0.6)
        self.realPriceLabel.attributedText = "alma".removeStrike()
        self.discountPriceLabel.isHidden = true
        self.discountView.isHidden = true
        self.discountPriceLabel.text = ""
        self.photoView.image = nil
        self.saveButton.isSelected = false
    }
    
//    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
//        self.bodyView.alpha = highlighted ? 0.5 : 1.0
//        //super.setHighlighted(highlighted, animated: animated)
//    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.anchor(.leading(), .trailing(), .bottom(discountPriceLabel.topAnchor), .height(20))
        
        discountPriceLabel.anchor(.trailing(), .bottom(-8))
        realPriceLabel.anchor(.leading(), .trailing(discountPriceLabel.leadingAnchor, constant: -7), .bottom(-8))
        
        photoView.anchor(.top(), .leading(), .trailing(), .bottom(titleLabel.topAnchor,constant: -8))
        
        saveButton.anchor(.top(12), .trailing(-12))
        
        discountView.anchor(.leading(), .top(12))
        
        discountLabel.anchor(.fillSuperviewPadding(2))
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(titleLabel)
        self.addSubview(realPriceLabel)
        self.addSubview(discountPriceLabel)
        self.addSubview(photoView)
        self.addSubview(saveButton)
        self.addSubview(discountView)
        
        discountView.addSubview(discountLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.clipsToBounds = true
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
        self.titleLabel.text = data.name
        
        if let discount = data.salePrice {
            self.discountPriceLabel.text = "\(discount) AZN"
            self.discountPriceLabel.isHidden = false
            self.realPriceLabel.attributedText = "\(data.price ?? "") AZN".strikeThrough()
            self.realPriceLabel.textColor = UIColor.black.withAlphaComponent(0.60)
            self.realPriceLabel.font = UIFont(weight: .medium, size: 14)
        } else {
            self.realPriceLabel.isHidden = (data.price == nil)
            self.realPriceLabel.text = "\(data.price ?? "") AZN"
            self.realPriceLabel.textColor = UIColor.black.withAlphaComponent(0.87)
            self.realPriceLabel.font = UIFont(weight: .medium, size: 16)
        }
        if data.wishlist == true {
            self.saveButton.isSelected = true
        }
//        if let discount = data.discount {
//            self.discountLabel.text = "-\(discount)%"
//            self.discountView.isHidden = false
//        }
        
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
    }
    
    // MARK: - Action
    
    @objc private func saveButtonTouchUp() {
        guard Defaults.instance.isLoggedIn else {return}
        if self.saveButton.isSelected == true {
            self.saveButton.isEnabled = false
            self.delegate?.removeWishlistBy(id: data.id)
            self.saveButton.isSelected = false
            self.saveButton.isEnabled = true
        } else {
            self.saveButton.isEnabled = false
            self.delegate?.addWishlistBy(id: data.id)
            self.saveButton.isSelected = true
            self.saveButton.isEnabled = true
        }
    }
}
