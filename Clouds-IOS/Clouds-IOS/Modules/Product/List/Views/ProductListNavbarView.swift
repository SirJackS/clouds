//
//  ProductListNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.01.21.
//

import UIKit

protocol ProductListNavbarViewDelegate: class {
    func backButtonClick()
}

final class ProductListNavbarView: UIView {

    weak var delegate: ProductListNavbarViewDelegate?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
//        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 48, height: 24))
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(backButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()

    init(title: String, backIcon: String = "back-arrow-icon") {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
        backButton.setImage(UIImage(named: backIcon), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        backButton.anchor(.leading(8), .centerY())
        
        titleLabel.anchor(.centerX(), .centerY())
        
        navbarLineView.anchor( .leading(8), .trailing(-8), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(navbarLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func backButtonTouchUp() {
        self.delegate?.backButtonClick()
    }
}
