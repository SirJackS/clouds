//
//  ProductListBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

final class ProductListBuilder {
    
    static func make(_ type: ProductParentEnum) -> ProductListViewController {
        let view = ProductListViewController()
        
        let presenter = ProductListPresenter(view: view, service: App.service, parentEnum: type)
        view.presenter = presenter
        
        return view
    }
}
