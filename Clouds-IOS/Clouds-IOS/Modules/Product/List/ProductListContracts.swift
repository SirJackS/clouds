//
//  ProductListContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

// MARK: - Presenter

protocol ProductListPresenterProtocol: class {
    var sortId:Int { get set}
    
    func load()
    func addWishlistBy(id: Int)
    func removeWishlistBy(id: Int)
    func getSubcategoryProductsBy(categoryId: Int, genderType: Int, ageType: Int)
    func getBannerProductsBy(bannerId: Int)
    func getFilterProductsBy(categoryIds: [Int], productTypeIds: [Int], brandIds: [Int], colors: [String], sizes: [String], priceMin: Int, priceMax: Int)
    func search(_ term: String)
}

enum ProductListPresenterOutput {
    case dismiss
    case setNavbarTitle(String)
    case showMessage(String)
    case showList([ProductList])
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol ProductListViewProtocol: class {
    func handleOutput(_ output: ProductListPresenterOutput)
}
