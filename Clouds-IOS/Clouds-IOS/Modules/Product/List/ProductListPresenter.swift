//
//  ProductListPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

enum ProductParentEnum {
    case banner(_ bannerId: Int)
    case subcategory(_ categoryId: Int,_ genderType: Int,_ ageType: Int)
    case search(_ term: String)
}

final class ProductListPresenter: ProductListPresenterProtocol {
    
    private unowned let view: ProductListViewProtocol
    private var service: ServiceProtocol
    private var parentEnum: ProductParentEnum
    private var categoryId: Int?
    private var searchTerm: String?
    private var bannerId: Int?
    var sortId: Int = 0
    
    init(view: ProductListViewProtocol,
         service: ServiceProtocol,
         parentEnum: ProductParentEnum) {
        self.view = view
        self.service = service
        self.parentEnum = parentEnum
    }
    
    func load() {
        switch parentEnum {
        case let .subcategory(categoryId,genderType,ageType):
            self.getSubcategoryProductsBy(categoryId: categoryId, genderType: genderType, ageType: ageType)
            self.categoryId = categoryId
        case let .search(term):
            self.search(term)
            self.searchTerm = term
        case let .banner(bannerId):
            self.bannerId = bannerId
            if let id = self.bannerId {
                self.getBannerProductsBy(bannerId: id)
            } else {
                self.view.handleOutput(.dismiss)
            }
        }
    }
    
    func getSubcategoryProductsBy(categoryId: Int, genderType: Int, ageType: Int) {
        self.view.handleOutput(.showActivity(true))
        let request = ProductSubcategoryRequest(categoryId: categoryId, genderTypeId: genderType, ageTypeId: ageType)
        service.getSubcategoryProductsBy(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getSubcategoryProductsBy))
                
            default: break
            }
        }
    }
    
    func getFilterProductsBy(categoryIds: [Int], productTypeIds: [Int], brandIds: [Int], colors: [String], sizes: [String], priceMin: Int, priceMax: Int) {
        var request = FilterRequest(search: nil, categoryId: categoryIds, productTypeId: productTypeIds, brandId: brandIds, color: colors, size: sizes, priceMin: priceMin, priceMax: priceMax, sortBy: sortId, bannerId: nil)
        switch parentEnum {
        case .subcategory(_, _, _):
            if let id = self.categoryId {
                var categories = categoryIds
                categories.append(id)
                request.categoryId = categories
            }
        case .search(_):
            request.search = self.searchTerm
        case .banner(_):
            request.bannerId = self.bannerId
        }
        self.view.handleOutput(.showActivity(true))
        service.getFilterProducts(request: request){ [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getSubcategoryProductsBy))
                
            default: break
            }
        }
    }
    
    func getBannerProductsBy(bannerId: Int) {
        let request = FilterRequest(categoryId: nil, productTypeId: nil, brandId: nil, color: nil, size: nil, priceMin: 0, priceMax: 10000, sortBy: self.sortId, bannerId: bannerId)
        self.view.handleOutput(.showActivity(true))
        service.getFilterProducts(request: request){ [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                
            default: break
            }
        }
    }
    
    func getJoined<T>(_ data: [T]) -> String {
        var array: [String] = []
        for item in data {
            array.append("\(item)")
        }
        return array.joined(separator: ",")
    }
    
    func addWishlistBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        let request = AddToWishlistRequest(productId: id)
        service.addToWishlist(request){ [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addToWishlist))
                
            default: break
            }
            
        }
    }
    
    func removeWishlistBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.removeFromWishlist(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.removeFromWishlist))
                
            default: break
            }
        }
    }
    
    func search(_ term: String) {
        self.view.handleOutput(.showActivity(true))
        
        var keywords = Defaults.instance.searchKeywords
        keywords.append(term)
        Defaults.instance.searchKeywords = keywords
        
        let request = SearchRequest(term: term)
        service.searchProductsBy(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.setNavbarTitle(term))
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.searchProductsBy))

            default: break
            }
        }
    }
}
