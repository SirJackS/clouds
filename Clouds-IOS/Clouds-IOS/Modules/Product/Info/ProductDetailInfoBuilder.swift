//
//  ProductDetailInfoBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.01.21.
//

import Foundation

final class ProductDetailInfoBuilder {
    
    static func make() -> ProductDetailInfoViewController {
        let view = ProductDetailInfoViewController()
        
        let presenter = ProductDetailInfoPresenter(view: view)
        view.presenter = presenter
        
        return view
    }
}
