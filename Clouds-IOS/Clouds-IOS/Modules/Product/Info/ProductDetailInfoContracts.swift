//
//  ProductDetailInfoContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.01.21.
//

import Foundation

// MARK: - Presenter

protocol ProductDetailInfoPresenterProtocol: class {
    func load()
}

enum ProductDetailInfoPresenterOutput {
    case showMessage(String)
    case showData(String)
}


// MARK: - View

protocol ProductDetailInfoViewProtocol: class {
    func handleOutput(_ output: ProductDetailInfoPresenterOutput)
}
