//
//  ProductDetailInfoPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.01.21.
//

import Foundation

final class ProductDetailInfoPresenter: ProductDetailInfoPresenterProtocol {
    
    private unowned let view: ProductDetailInfoViewProtocol
    
    init(view: ProductDetailInfoViewProtocol) {
        self.view = view
    }
    
    func load() {
        self.getData()
    }
    
    private func getData() {
        let data: String = "<html><body><p>Hello!</p></body></html>"
        self.view.handleOutput(.showData(data))
    }
}
