//
//  ProductDetailInfoViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.01.21.
//

import UIKit
import WebKit

final class ProductDetailInfoViewController: UIViewController, ProductDetailInfoViewProtocol {

    var presenter: ProductDetailInfoPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Product Details")
        view.delegate = self
        return view
    }()
    
    private lazy var webView: WKWebView = {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        return webView
    }()
    
    // MARK: - Data
    private var data: String = ""
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        webView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: ProductDetailInfoPresenterOutput) {
        switch output {
        case .showData(let data):
            self.data = data
//            webView.loadHTMLString(data, baseURL: nil)
            webView.load(URLRequest(url: URL(string: "https://m.asos.com/reclaimed-vintage/reclaimed-vintage-inspired-the-94-classic-fit-jeans-in-ecru/prd/20504436?colourwayid=60059322&SearchQuery=&cid=13517")!))
        default: break
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(webView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
}

extension ProductDetailInfoViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        print("Back Pressed")
        self.popVC()
    }
}
