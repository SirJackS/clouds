//
//  CategoryDetailHeaderView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit

final class CategoryDetailHeaderView: UIView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Shop by Product".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .semibold, size: 24)
        label.numberOfLines = 1
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        titleLabel.anchor(
            .leading(16), .trailing(-16),
            .top(32), .bottom(-24),.height(32))

        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
