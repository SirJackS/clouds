//
//  CategoryDetailCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit
import SDWebImage

final class CategoryDetailCell: UITableViewCell {

    static let ID: String = "CategoryDetailCell"
    
    var data: Category! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        let height = UIScreen.main.bounds.width * (64 / 375)
        imageView.layer.cornerRadius = height/2
        imageView.anchor(.size(width: height, height: height))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.bodyView.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        photoView.image = nil
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 16, trailing: -16))
        
        photoView.anchor(.top(8), .bottom(-8), .leading(8))
        
        titleLabel.anchor(
            .leadingLess(photoView.trailingAnchor, constant: 24),
            .trailing(-16), .centerY())
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(photoView)
        bodyView.addSubview(lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data.name
        
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
