//
//  CategoryDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

// MARK: - Presenter

protocol CategoryDetailPresenterProtocol: class {
    var genderType: Int { get }
    var ageType: Int { get }
    
    func load()
    func getSubCategoriesBy(id: Int)
}

enum CategoryDetailPresenterOutput {
    case showMessage(String)
    case setNavbarTitle(String)
    case showList([Category])
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol CategoryDetailViewProtocol: class {
    func handleOutput(_ output: CategoryDetailPresenterOutput)
}
