//
//  CategoryDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit
import SDWebImage

final class CategoryDetailViewController: UIViewController, CategoryDetailViewProtocol {

    var presenter: CategoryDetailPresenterProtocol!
    
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var headerView: CategoryDetailHeaderView = {
        let view = CategoryDetailHeaderView()
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(CategoryDetailCell.self, forCellReuseIdentifier: CategoryDetailCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.estimatedRowHeight = 80
        tableView.isHidden = true
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        return tableView
    }()
    
    
    // MARK: - Data
    private var data: [Category] = []
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: CategoryDetailPresenterOutput) {
        switch output {
        case .setNavbarTitle(let title):
            if title.count < 20 {
                self.navBarView.titleText = title
            } else {
                self.navBarView.titleText = "\(title.substring(toIndex: 20))..."
            }
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
            self.tableView.isHidden = false
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getSubCategory:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
        
        self.tableView.setTableHeaderView(headerView: headerView)
        self.tableView.updateHeaderViewFrame()
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
}


extension CategoryDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let count = data[indexPath.section].foods?.count ?? 0
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryDetailCell.ID, for: indexPath) as? CategoryDetailCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
//        cell.isLast = (indexPath.row == count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = ProductListBuilder.make(.subcategory(data[indexPath.row].id, self.presenter.genderType, self.presenter.ageType))
        self.navigationController?.pushViewController(viewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CategoryDetailViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}
