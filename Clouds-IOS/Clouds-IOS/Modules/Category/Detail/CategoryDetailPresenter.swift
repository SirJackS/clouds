//
//  CategoryDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

final class CategoryDetailPresenter: CategoryDetailPresenterProtocol {
    
    private unowned let view: CategoryDetailViewProtocol
    private var service: ServiceProtocol
    private var category: Category
    var genderType: Int
    var ageType: Int
    
    init(view: CategoryDetailViewProtocol,
         service: ServiceProtocol,
         category: Category,
         genderType: Int,
         ageType: Int) {
        self.view = view
        self.service = service
        self.category = category
        self.genderType = genderType
        self.ageType = ageType
    }
    
    func load() {
        self.view.handleOutput(.setNavbarTitle(category.name ?? ""))
        self.getSubCategoriesBy(id: self.category.id)
    }
    
    func getSubCategoriesBy(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.getSubCategory(id) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getSubCategory))
                
            default: break
            }
        }
    }
}
