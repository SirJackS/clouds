//
//  CategoryDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

final class CategoryDetailBuilder {
    
    static func make(_ category: Category,genderType: Int,ageType: Int) -> CategoryDetailViewController {
        let view = CategoryDetailViewController()
        
        let presenter = CategoryDetailPresenter(view: view, service: App.service, category: category, genderType: genderType, ageType: ageType)
        view.presenter = presenter
        
        return view
    }
}
