//
//  CategoryMainCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit
import SDWebImage

final class CategoryMainCell: UITableViewCell {

    static let ID: String = "CategoryMainCell"
    
    var data: Category! {
        didSet {
            self.configure()
        }
    }
    
//    var isLast: Bool = false {
//        didSet {
//            lineView.isHidden = isLast
//        }
//    }
    
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF")
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 2
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.bodyView.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        photoView.image = nil
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(), .bottom(-16), .trailing(), .leading())
        
        photoView.anchor(.top(), .bottom(), .trailing(), .height(140), .width(146))
        
        titleLabel.anchor(.leading(24), .trailing(photoView.leadingAnchor, constant: -24), .centerY())
        
//        stackView.anchor(
//            .top(), .bottom(), .trailing(),
//            .leading(photoView.trailingAnchor, constant: 16))
//
//        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(photoView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data.name
        
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
