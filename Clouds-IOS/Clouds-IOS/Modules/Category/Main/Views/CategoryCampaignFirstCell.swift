//
//  CategoryCampaignFirstCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.04.21.
//

import UIKit
import SDWebImage

final class CategoryCampaignFirstCell: UITableViewCell {

    static let ID: String = "CategoryCampaignFirstCell"
    
    var data: BannerCampaign! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(
            .top(),.bottom(-16),.leading(),.trailing())
        
        titleLabel.anchor(.fillSuperview(top: 4, leading: 8, bottom: -4, trailing: -8),.height(20))
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        bodyView.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data.name
    }
}
