//
//  CategoryMainNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit

protocol CategoryMainNavbarViewDelegate: class {
    func searchButtonClick()
    func notificationButtonClick()
    func switchValueChange(value: Int)
}

final class CategoryMainNavbarView: UIView {

    weak var delegate: CategoryMainNavbarViewDelegate?
    
    private lazy var logoView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "logo-navbar"))
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var genderSwitch: CustomSwitch = {
        let genderSwitch = CustomSwitch(frame: .zero)
        genderSwitch.borderWidth = 1
        genderSwitch.borderColorr = UIColor.black.withAlphaComponent(0.25).cgColor
        genderSwitch.thumbView.layer.borderWidth = 2
        genderSwitch.thumbView.layer.borderColor = UIColor(hexString: "EBD857").cgColor
        genderSwitch.onThumbImage = UIImage(named: "switch-boy-icon")
        genderSwitch.offThumbImage = UIImage(named: "switch-girl-icon")
        genderSwitch.onSelfImage = UIImage(named: "switch-girl-icon")
        genderSwitch.offSelfImage = UIImage(named: "switch-boy-icon")
        genderSwitch.onTintColor = UIColor(hexString: "EFEFEF")
        genderSwitch.offTintColor = UIColor(hexString: "EFEFEF")
        genderSwitch.thumbView.backgroundColor = .white
        genderSwitch.thumbTintColor = .white
        genderSwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        return genderSwitch
    }()
    
    private lazy var searchButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "search-icon"), for: .normal)
        button.tintColor = .primaryColor
        button.anchor(.size(width: 24, height: 24))
        button.addTarget(self, action: #selector(searchButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var notificationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "notification-icon"), for: .normal)
        button.tintColor = .primaryColor
        button.anchor(.size(width: 24, height: 24))
        button.addTarget(self, action: #selector(notificationButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    private lazy var redViewOnNotif: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "C70D3A")
        view.layer.cornerRadius = 5
        view.clipsToBounds  = true
        view.anchor(.size(width: 10, height: 10))
        return view
    }()

    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        logoView.anchor(.leading(16), .centerY(), .height(36), .width(82))
        
        genderSwitch.anchor(.centerX(), .centerY(), .height(32), .width(64))
        
        searchButton.anchor(.trailing(-16), .centerY())
        
        notificationButton.anchor(.trailing(searchButton.leadingAnchor,constant: -16), .centerY())
        
        redViewOnNotif.anchor(.trailing(notificationButton.trailingAnchor), .top(notificationButton.topAnchor, constant: -2))
        
        navbarLineView.anchor( .leading(), .trailing(), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(logoView)
        addSubview(genderSwitch)
        addSubview(searchButton)
        addSubview(notificationButton)
        addSubview(navbarLineView)
        addSubview(redViewOnNotif)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }

    
    // MARK: - Action
    
    @objc private func searchButtonTouchUp() {
        self.delegate?.searchButtonClick()
    }
    
    @objc private func notificationButtonTouchUp() {
        self.delegate?.notificationButtonClick()
    }
    
    @objc func switchValueDidChange(_ sender: CustomSwitch) {
        if sender.isOn {
            self.delegate?.switchValueChange(value: 1)
        } else {
            self.delegate?.switchValueChange(value: 2)
        }
     }
}
