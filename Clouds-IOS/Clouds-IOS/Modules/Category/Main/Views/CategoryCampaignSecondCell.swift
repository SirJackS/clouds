//
//  CategoryCampaignSecondCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.04.21.
//

import UIKit
import SDWebImage

final class CategoryCampaignSecondCell: UITableViewCell {

    static let ID: String = "CategoryCampaignSecondCell"
    
    var data: BannerCampaign! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        photoView.image = nil
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(
            .top(),.bottom(-16),.leading(),.trailing())
        
        photoView.anchor(.fillSuperview(),.height(140))
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        bodyView.addSubview(photoView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.cover ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
