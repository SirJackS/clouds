//
//  CategoryMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import Foundation

// MARK: - Presenter

protocol CategoryMainPresenterProtocol: class {
    func load()
    func getCategoriesBy(genderType: Int, ageType: Int)
    func getAgeTypes()
    func getBanners()
}

enum CategoryMainPresenterOutput {
    case showMessage(String)
    case showList([Category])
    case showAgeList([CategoryType])
    case showActivity(Bool)
    case showCampaigns([BannerCampaign])
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol CategoryMainViewProtocol: class {
    func handleOutput(_ output: CategoryMainPresenterOutput)
}
