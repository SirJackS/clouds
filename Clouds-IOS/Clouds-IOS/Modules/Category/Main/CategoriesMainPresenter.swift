//
//  CategoryMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import Foundation

final class CategoryMainPresenter: CategoryMainPresenterProtocol {
    
    private unowned let view: CategoryMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: CategoryMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getAgeTypes()
        self.getBanners()
        self.getCategoriesBy(genderType: Defaults.instance.getGender(), ageType: Defaults.instance.getAge())
    }
    
    func getCategoriesBy(genderType: Int, ageType: Int) {
        self.view.handleOutput(.showActivity(true))
        let request = CategoryTypeRequest(genderType: genderType, ageType: ageType)
        service.getCategoriesByType(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCategoriesByType))
                
            default: break
            }
        }
    }
    
    func getAgeTypes() {
        self.view.handleOutput(.showActivity(true))
        service.getCategoryAges { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showAgeList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCategoryAges))
                
            default: break
            }
        }
    }
    
    func getBanners() {
        self.view.handleOutput(.showActivity(true))
        service.getAllCampaigns { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showCampaigns(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getAllCampaigns))
            
            default: break
            }
        }
    }
}
