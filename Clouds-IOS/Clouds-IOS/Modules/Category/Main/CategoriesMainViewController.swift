//
//  CategoryMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import UIKit
import SDWebImage

final class CategoryMainViewController: UIViewController, CategoryMainViewProtocol {

    var presenter: CategoryMainPresenterProtocol!
    
    private lazy var navBarView: CategoryMainNavbarView = {
        let view = CategoryMainNavbarView()
        view.delegate = self
        return view
    }()
    
    private lazy var ageCategorySegmented: CustomSegmentedControl = {
        let segmentControl = CustomSegmentedControl(frame: .zero, buttonTitle: [CategoryType(id: 0, name: "error")]) //
        segmentControl.textColor = UIColor.black.withAlphaComponent(0.38)
        segmentControl.selectorTextColor = UIColor.black.withAlphaComponent(0.6)
        segmentControl.selectorViewColor = UIColor.black.withAlphaComponent(0.6)
        //segmentControl.setButtonTitles(buttonTitles: ["BABY","TODDLER","JUNIOR"])
        segmentControl.backgroundColor = .clear
        segmentControl.delegate = self
        segmentControl.isHidden = true
        return segmentControl
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(CategoryMainCell.self, forCellReuseIdentifier: CategoryMainCell.ID)
        tableView.register(CategoryCampaignFirstCell.self, forCellReuseIdentifier: CategoryCampaignFirstCell.ID)
        tableView.register(CategoryCampaignSecondCell.self, forCellReuseIdentifier: CategoryCampaignSecondCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 156
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 8, right: 0)
        tableView.isHidden = true
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        return refresh
    }()
    
//    private var noScroll: Bool = false
    
    private var genderType: Int = Defaults.instance.getGender()
    private var ageType: Int = Defaults.instance.getAge()
    private var data: [Category] = []
    private var campaigns: [BannerCampaign] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.prepareSwitch(gender:  Defaults.instance.getGender(), age: Defaults.instance.getAge())
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        ageCategorySegmented.anchor(.top(), .leading(), .trailing(), .height(48))
        
        tableView.anchor(.top(ageCategorySegmented.bottomAnchor), .leading(16), .trailing(-16), .bottom(0))
        
    }

    func handleOutput(_ output: CategoryMainPresenterOutput) {
        switch output {
        case .showCampaigns(let campaigns):
            self.campaigns = campaigns
            self.tableView.reloadData()
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
            self.tableView.isHidden = false
        case .showAgeList(let data):
            self.setupSegmentControl(data)
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getAllCampaigns:
                        self.presenter.getBanners()
                    case .getCategoriesByType:
                        self.presenter.getCategoriesBy(genderType: self.genderType, ageType: self.ageType)
                    case .getCategoryAges:
                        self.presenter.getAgeTypes()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(ageCategorySegmented)
        self.view.addSubview(tableView)
        
        tableView.addSubview(refreshControl)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.tableView.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setupSegmentControl(_ data: [CategoryType]) {
        ageCategorySegmented.setButtonTitles(buttonTitles: data)
        ageCategorySegmented.setIndex(index: self.ageType-1)
        ageCategorySegmented.isHidden = false
        self.presenter.getCategoriesBy(genderType: genderType, ageType: ageType)
    }
    
    // MARK: - Action
    
    @objc func refresh() {
        self.tableView.isHidden = true
        self.presenter.getCategoriesBy(genderType: genderType, ageType: ageType)
        self.refreshControl.endRefreshing()
    }
    
    func prepareSwitch(gender: Int, age: Int) {
        self.genderType = gender
        self.ageType = age
        
        switch gender {
        case 1:
            self.navBarView.genderSwitch.setOn(on: true, animated: false)
        case 2:
            self.navBarView.genderSwitch.setOn(on: false, animated: false)
        default:
            self.navBarView.genderSwitch.setOn(on: false, animated: false)
        }
    }
}


extension CategoryMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let total = data.count + campaigns.count
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if campaigns.count > indexPath.row {
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCampaignFirstCell.ID, for: indexPath) as? CategoryCampaignFirstCell else { return UITableViewCell() }
                cell.data = campaigns[0]
                return cell
            }
            
            if campaigns.count > 1 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCampaignSecondCell.ID, for: indexPath) as? CategoryCampaignSecondCell else { return UITableViewCell() }
                cell.data = campaigns[indexPath.row]
                return cell
            }
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryMainCell.ID, for: indexPath) as? CategoryMainCell else { return UITableViewCell() }
            cell.data = data[indexPath.row-campaigns.count]
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if campaigns.count > indexPath.row {
            let viewController = ProductListBuilder.make(.banner(self.campaigns[indexPath.row].id))
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let viewController = CategoryDetailBuilder.make(self.data[indexPath.row-campaigns.count],genderType: self.genderType, ageType: self.ageType)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension CategoryMainViewController: CategoryMainNavbarViewDelegate {
    func notificationButtonClick() {
        guard Defaults.instance.isLoggedIn else {return}
        let viewController = NotificationsBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func switchValueChange(value: Int) {
        self.genderType = value
        Defaults.instance.genderType = self.genderType
        self.refresh()
    }
    
    func searchButtonClick() {
        let viewController = SearchBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension CategoryMainViewController: CustomSegmentedControlDelegate {
    func change(to index: Int) {
        self.ageType = index+1
        Defaults.instance.ageType = self.ageType
        self.refresh()
    }
}
