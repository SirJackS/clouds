//
//  CategoryMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import Foundation

final class CategoryMainBuilder {
    
    static func make() -> CategoryMainViewController {
        let view = CategoryMainViewController()
        
        let presenter = CategoryMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
