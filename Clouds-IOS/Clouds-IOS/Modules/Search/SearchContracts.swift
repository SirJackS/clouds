//
//  SearchContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

// MARK: - Presenter

protocol SearchPresenterProtocol: class {
    func load()
}

enum SearchPresenterOutput {
    case showList([String])
    case showMessage(String)
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol SearchViewProtocol: class {
    func handleOutput(_ output: SearchPresenterOutput)
}
