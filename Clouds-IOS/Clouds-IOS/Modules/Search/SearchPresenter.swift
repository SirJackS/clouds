//
//  SearchPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

final class SearchPresenter: SearchPresenterProtocol {
    
    private unowned let view: SearchViewProtocol
    private var service: ServiceProtocol
    
    init(view: SearchViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getKeywords()
    }
    
    private func getKeywords() {
        let keywords = Defaults.instance.searchKeywords
        self.view.handleOutput(.showList(keywords))
    }
}
