//
//  SearchBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import Foundation

final class SearchBuilder {
    
    static func make() -> SearchViewController {
        let view = SearchViewController()
        
        let presenter = SearchPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
