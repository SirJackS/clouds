//
//  SearchNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit

protocol SearchNavbarViewDelegate: class {
    func backButtonClick()
    func searchBtnPressed(_ term: String)
}

final class SearchNavbarView: UIView {

    weak var delegate: SearchNavbarViewDelegate?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
//        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 24, height: 24))
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(backButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
     lazy var searchText: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.87)
        textField.attributedPlaceholder = NSAttributedString(string: "Search".localized(), attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.38)])
        textField.textAlignment = .left
        textField.font = UIFont(weight: .medium, size: 20)
        textField.delegate = self
        return textField
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()

    init(backIcon: String = "back-arrow-icon") {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        backButton.setImage(UIImage(named: backIcon), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        backButton.anchor(.leading(8), .centerY())
        
        searchText.anchor(
            .leading(backButton.trailingAnchor, constant: 16),
            .trailing(-16), .height(24), .centerY())
        
        navbarLineView.anchor( .leading(), .trailing(), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(backButton)
        addSubview(searchText)
        addSubview(navbarLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func backButtonTouchUp() {
        self.delegate?.backButtonClick()
    }
}

extension SearchNavbarView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let term = self.searchText.text?.trim() else { return false }
        self.delegate?.searchBtnPressed(term)
        return true
    }
}
