//
//  SearchCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit
import SDWebImage

final class SearchCell: UITableViewCell {

    static let ID: String = "SearchCell"
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 16, trailing: -16))
        
        titleLabel.anchor(.leading(3),.trailing(-16), .centerY(), .height(26))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        //self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data
    }
}
