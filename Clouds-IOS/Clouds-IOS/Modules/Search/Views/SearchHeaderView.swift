//
//  SearchHeaderView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit

final class SearchHeaderView: UIView {
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "recent-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Recent".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.38)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    init() {
        super.init(frame: .zero)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        bodyView.anchor(.fillSuperview(leading: 16, trailing: -16), .height(48))
        
        photoView.anchor(.leading(8), .centerY())
        
        titleLabel.anchor(
            .leading(photoView.trailingAnchor, constant: 16),
            .trailing(), .centerY(), .height(26))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))

        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
