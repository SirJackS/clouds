//
//  SearchEmptyView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.03.21.
//

import UIKit

final class SearchEmptyView: UIView {
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [photoView,titleLabel])
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 8
        return stack
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "empty-search-icon")
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "You have no recent Search".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.38)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        label.anchor(.height(24))
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        stackView.anchor(.fillSuperview())
        
        photoView.anchor(.size(width: 32, height: 32))

        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(stackView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
