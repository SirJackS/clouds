//
//  SearchViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.12.20.
//

import UIKit
import SDWebImage

final class SearchViewController: UIViewController, SearchViewProtocol {

    var presenter: SearchPresenterProtocol!
    
    private lazy var navBarView: SearchNavbarView = {
        let view = SearchNavbarView()
        view.delegate = self
        return view
    }()
    
    private lazy var headerView: SearchHeaderView = {
        let view = SearchHeaderView()
        return view
    }()
    
    private lazy var emptyView: SearchEmptyView = {
        let view = SearchEmptyView()
        view.isHidden = true
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(SearchCell.self, forCellReuseIdentifier: SearchCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        tableView.isHidden = true
        return tableView
    }()
    
    
    // MARK: - Data
    private var data: [String] = []
    private var searchTerm: String = ""
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
        emptyView.anchor(.top(126), .leading(16), .trailing(-16))
        
    }

    func handleOutput(_ output: SearchPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
            if data.count == 0 {
                self.emptyView.isHidden = false
                self.tableView.isHidden = true
            } else {
                self.emptyView.isHidden = true
                self.tableView.isHidden = false
            }
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .searchProductsBy:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        self.view.addSubview(emptyView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
        
        self.tableView.setTableHeaderView(headerView: headerView)
        self.tableView.updateHeaderViewFrame()
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
//    private func setupData(_ data: [String] = ["GET FREE SHIPPING OVER 100 AZN","test-banner"]) {
//
//        self.headerView.adText = data[0]
//        self.headerView.bannerImage = data[1]
//    }
}


extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let count = data[indexPath.section].foods?.count ?? 0
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchCell.ID, for: indexPath) as? SearchCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
//        cell.isLast = (indexPath.row == count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = self.tableView.cellForRow(at: indexPath) as? SearchCell else { return }
        let viewController = ProductListBuilder.make(.search(cell.titleLabel.text ?? ""))
        self.navigationController?.pushViewController(viewController, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension SearchViewController: SearchNavbarViewDelegate {
    
    func searchBtnPressed(_ term: String) {
        let viewController = ProductListBuilder.make(.search(term))
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func backButtonClick() {
        self.popVC()
    }
}
