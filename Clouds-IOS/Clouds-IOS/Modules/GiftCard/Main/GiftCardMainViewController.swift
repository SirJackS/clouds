//
//  GiftCardMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit

final class GiftCardMainViewController: UIViewController, GiftCardMainViewProtocol {

    var presenter: GiftCardMainPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Gift Card".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private var sliderHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (200 / 375) * width
    }
    
    private lazy var sliderCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize.init(width: UIScreen.main.bounds.width - 44, height: self.sliderHeight)
        layout.scrollDirection = .horizontal
        layout.sectionInset = .init(top: 0, left: 8, bottom: 0, right: 8)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.alwaysBounceVertical = false
        collectionView.alwaysBounceHorizontal = false
        collectionView.isPagingEnabled = false
        collectionView.clipsToBounds = false
        collectionView.bounces = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(GiftCardCell.self, forCellWithReuseIdentifier: GiftCardCell.ID)
        collectionView.register(GiftCardAddCell.self, forCellWithReuseIdentifier: GiftCardAddCell.ID)
        return collectionView
    }()
    
    private lazy var startButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("start shopping".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.addTarget(self, action: #selector(startButtonTouchUp), for: .touchUpInside)
        button.anchor(.height(46))
        return button
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Terms and Conditions\n to use Gift Card".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 2
        label.anchor(.height(48))
        return label
    }()
    
    private lazy var textView: UITextView = {
        let view = UITextView()
        view.text = termAndConditions//termandConditions
        view.backgroundColor = .red
        view.textAlignment = .left
        view.textColor = UIColor.black.withAlphaComponent(0.87)
        view.font = UIFont(weight: .regular, size: 16)
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = true
        view.sizeToFit()
        view.isScrollEnabled = false
        return view
    }()
    
    // MARK: - Data
    private var giftCardList: [GiftCard] = []
    
    private var indexOfCellBeforeDragging = 0
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        self.addObservers()
        
        self.presenter.load()
    }
    
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.top(), .leading(), .trailing(), .bottom())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        sliderCollection.anchor(
            .top(15), .leading(),
            .trailing(), .height(self.sliderHeight))
        
        titleLabel.anchor(
            .top(sliderCollection.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        startButton.anchor(.leading(16), .trailing(-16), .bottom(-32))
        
        textView.anchor(
            .top(titleLabel.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16), .bottom(-150))
//            .height(298))
        
        //backButton.anchor(.top(view.layoutMarginsGuide.topAnchor, constant:16), .leading(16))
    }

    func handleOutput(_ output: GiftCardMainPresenterOutput) {
        switch output {
        case .showList(let data):
            self.giftCardList = data
            self.sliderCollection.reloadData()
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getGiftcards:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        self.view.addSubview(startButton)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(sliderCollection)
        contentView.addSubview(titleLabel)
        contentView.addSubview(textView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.dismissed),
        name: NSNotification.Name(rawValue: "giftcardAdded"),
        object: nil)
        
    }
    
    
    // MARK: - Action
    
    @objc func startButtonTouchUp() {
        App.router.start()
    }
    
    @objc func dismissed() {
        self.presenter.load()
    }
    
    //MARK: - Slider Pager
    
    private func indexOfMajorCell() -> Int {
        let itemWidth = UIScreen.main.bounds.width * (331/375)
        let proportionalOffset = sliderCollection.contentOffset.x / itemWidth
        let index = Int(round(proportionalOffset))
        let numberOfItems = sliderCollection.numberOfItems(inSection: 0)
        let safeIndex = max(0, min(numberOfItems - 1, index))
        return safeIndex
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        indexOfCellBeforeDragging = indexOfMajorCell()
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if let collectionView = scrollView as? UICollectionView {
            switch collectionView {
            case sliderCollection:
                // Stop scrollView sliding:
                targetContentOffset.pointee = scrollView.contentOffset
                
                // calculate where scrollView should snap to:
                let indexOfMajorCell = self.indexOfMajorCell()
                
                // calculate conditions:
                let dataSourceCount = giftCardList.count+1
                let swipeVelocityThreshold: CGFloat = 0.5 // after some trail and error
                let hasEnoughVelocityToSlideToTheNextCell = indexOfCellBeforeDragging + 1 < dataSourceCount && velocity.x > swipeVelocityThreshold
                let hasEnoughVelocityToSlideToThePreviousCell = indexOfCellBeforeDragging - 1 >= 0 && velocity.x < -swipeVelocityThreshold
                let majorCellIsTheCellBeforeDragging = indexOfMajorCell == indexOfCellBeforeDragging
                let didUseSwipeToSkipCell = majorCellIsTheCellBeforeDragging && (hasEnoughVelocityToSlideToTheNextCell || hasEnoughVelocityToSlideToThePreviousCell)
                
                if didUseSwipeToSkipCell {
                    let snapToIndex = indexOfCellBeforeDragging + (hasEnoughVelocityToSlideToTheNextCell ? 1 : -1)
                    let toValue = UIScreen.main.bounds.width * (331/375) * CGFloat(snapToIndex)
                    
                    // Damping equal 1 => no oscillations => decay animation:
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: velocity.x, options: .allowUserInteraction, animations: {
//                        scrollView.contentOffset = CGPoint(x: toValue, y: 0)
//                        scrollView.layoutIfNeeded()
                        let pageNumber = round(toValue / 331)
                        let indexPath = IndexPath(row: Int(pageNumber), section: 0)
                        self.sliderCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                    }, completion: nil)
                } else {
                    let indexPath = IndexPath(row: indexOfMajorCell, section: 0)
                    sliderCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                }
            default: break
            }
        }
        
    }
}

// MARK: - UICollectionView datasource and delegate

extension GiftCardMainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return giftCardList.count+1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == giftCardList.count {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GiftCardAddCell.ID, for: indexPath) as? GiftCardAddCell else { return UICollectionViewCell() }
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GiftCardCell.ID, for: indexPath) as? GiftCardCell else { return UICollectionViewCell() }
            cell.data = giftCardList[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == giftCardList.count {
            let viewController = GiftCardAddBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}

extension GiftCardMainViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}
