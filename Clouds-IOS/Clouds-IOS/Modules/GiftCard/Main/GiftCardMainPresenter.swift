//
//  GiftCardMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class GiftCardMainPresenter: GiftCardMainPresenterProtocol {
    
    private unowned let view: GiftCardMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: GiftCardMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getGiftCards()
    }

    private func getGiftCards() {
        self.view.handleOutput(.showActivity(true))
        service.getGiftcards { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getGiftcards))
                
            default: break
            }
            
        }
    }

}
