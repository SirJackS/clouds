//
//  GiftCardMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

// MARK: - Presenter

protocol GiftCardMainPresenterProtocol: class {
    func load()
}

enum GiftCardMainPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case showList([GiftCard])
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol GiftCardMainViewProtocol: class {
    func handleOutput(_ output: GiftCardMainPresenterOutput)
}
