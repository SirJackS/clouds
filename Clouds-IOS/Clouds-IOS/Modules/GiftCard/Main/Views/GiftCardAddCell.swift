//
//  GiftCardAddCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 09.01.21.
//

import UIKit

final class GiftCardAddCell: UICollectionViewCell {
    
    static let ID: String = "GiftCardAddCell"
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "EFEFEF")
        view.layer.cornerRadius = UIScreen.main.bounds.width * (15 / 375)
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var dashedView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        //view.clipsToBounds = false
        let height = UIScreen.main.bounds.width/375*56
        view.anchor(.height(height))
        view.addDashedBorder(color: UIColor.black.withAlphaComponent(0.87), height: height, width: UIScreen.main.bounds.width/375*315-80)
        return view
    }()
    
    private lazy var viewsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [logoView,titleLabel])
        stackView.axis = .horizontal
        stackView.alignment = .center
//        stackView.distribution = .fillEqually
        stackView.spacing = 10
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Add New Card".localized().uppercased()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 14)
        label.textAlignment = .left
        label.anchor(.height(16))
        label.numberOfLines = 1
        return label
    }()
    
    lazy var logoView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "add-circle-icon"))
        imageView.contentMode = .scaleAspectFit
        imageView.anchor(.size(width: 20, height: 20))
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(), .leading(8), .trailing(-8), .bottom())
        
        dashedView.anchor(.leading(40), .trailing(-40), .centerY())
        
        viewsStack.anchor(.centerX(), .centerY())
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(dashedView)
        
        dashedView.addSubview(viewsStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
}
