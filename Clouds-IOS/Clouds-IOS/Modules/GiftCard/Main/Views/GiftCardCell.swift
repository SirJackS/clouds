//
//  GiftCardCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit

final class GiftCardCell: UICollectionViewCell {
    
    static let ID: String = "GiftCardCell"
    
    var data: GiftCard! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "EFEFEF")
        view.layer.cornerRadius = UIScreen.main.bounds.width * (15 / 375)
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "gift-card-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        return imageView
    }()
    
    private lazy var logoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "logo-navbar")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        let width = UIScreen.main.bounds.width * (86 / 375)
        imageView.anchor(.size(width: width, height: width*(39/86)))
        return imageView
    }()
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, amountLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
//        stackView.distribution = .fillEqually
        stackView.spacing = 0
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Gift card"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 12)
        label.textAlignment = .left
        label.anchor(.height(16))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var amountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 20)
        label.textAlignment = .left
        label.anchor(.height(24))
        label.numberOfLines = 1
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(), .leading(8), .trailing(-8), .bottom())
        
        photoView.anchor(.fillSuperview())
        
        logoView.anchor(.top(16), .leading(16))
        
        labelsStack.anchor(.bottom(-16), .leading(16), .trailing(-90))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.amountLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(logoView)
        bodyView.addSubview(labelsStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
    
    private func configure() {
        self.amountLabel.text = "\(data.bonus) AZN"
    }
}
