//
//  GiftCardMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class GiftCardMainBuilder {
    
    static func make() -> GiftCardMainViewController {
        let view = GiftCardMainViewController()
        
        let presenter = GiftCardMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
