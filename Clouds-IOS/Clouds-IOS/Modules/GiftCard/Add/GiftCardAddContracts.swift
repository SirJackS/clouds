//
//  GiftCardAddContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 04.01.21.
//

import Foundation

// MARK: - Presenter

protocol GiftCardAddPresenterProtocol: class {
    func load()
    func addGiftcard(_ number: String)
}

enum  GiftCardAddPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case dismiss
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol GiftCardAddViewProtocol: class {
    func handleOutput(_ output: GiftCardAddPresenterOutput)
}
