//
//  GiftCardAddPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 04.01.21.
//

import Foundation

final class GiftCardAddPresenter: GiftCardAddPresenterProtocol {
    
    private unowned let view: GiftCardAddViewProtocol
    private var service: ServiceProtocol
    
    init(view: GiftCardAddViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {}
    
    
    func addGiftcard(_ number: String) {
        self.view.handleOutput(.showActivity(true))
        let request = AddGiftCardRequest(number: number)
        service.addGiftcard(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.dismiss)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addGiftcard))

            default: break
            }
        }
    }
}
