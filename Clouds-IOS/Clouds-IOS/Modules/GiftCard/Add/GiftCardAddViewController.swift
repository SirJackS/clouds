//
//  GiftCardAddViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 04.01.21.
//

import UIKit

final class GiftCardAddViewController: UIViewController, GiftCardAddViewProtocol {

    var presenter: GiftCardAddPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Rewards & Gift Cards".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Gift Card Number".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var textBackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var mainText: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .left
        textField.delegate = self
        textField.font = UIFont(weight: .regular, size: 16)
        textField.placeholder = ""
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    private lazy var applyButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("apply code".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.isEnabled = false
        button.alpha = 0.3
        button.addTarget(self, action: #selector(applyButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        bodyView.anchor(.top(31), .leading(16), .trailing(-16))
                
        titleLabel.anchor(.top(), .leading(), .trailing(), .height(26))
        
        textBackView.anchor(
            .top(titleLabel.bottomAnchor, constant: 4),
            .leading(), .trailing(), .height(56))
        
        
        lineView.anchor(.bottom(), .leading(), .trailing(), .height(0.3))
        
        mainText.anchor(
            .leading(12), .trailing(-12), .centerY(), .height(26))

        applyButton.anchor(
            .top(textBackView.bottomAnchor, constant: 62),
            .leading(), .trailing(), .bottom(-16))
    }

    func handleOutput(_ output: GiftCardAddPresenterOutput) {
        switch output {
        case .dismiss:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "giftcardAdded"), object: nil)
            self.popVC()
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .addGiftcard:
                        self.applyButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(textBackView)
        bodyView.addSubview(applyButton)
        
        textBackView.addSubview(mainText)
        textBackView.addSubview(lineView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if mainText.text?.trim().isEmpty == true {
            applyButton.isEnabled = false
            applyButton.alpha = 0.3
        } else {
            applyButton.isEnabled = true
            applyButton.alpha = 1.0
        }
    }
    
    // MARK: - Action
    
    @objc private func applyButtonTouchUp() {
        guard let number = self.mainText.text?.trim() else { return }
        self.presenter.addGiftcard(number)
    }
}

// MARK: - UITextFieldDelegate delegate

extension GiftCardAddViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor(hexString: "2F69FF")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
    }
}

// MARK: - BTNavbarView delegate

extension GiftCardAddViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}
//
//// MARK: - UITextField delegate
//
//extension MyDetailViewController: CustomTextViewDelegate {
//    func didBeginEditing(_ textView: CustomTextView) {
//        switch textView {
//        case dateOfBirthView:
//            dateOfBirthView.mainText.becomeFirstResponder()
//        default: break
//        }
//    }
//}
