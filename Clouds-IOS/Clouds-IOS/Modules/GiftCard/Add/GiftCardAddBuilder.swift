//
//  GiftCardAddBuilder.swift.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 04.01.21.
//

import Foundation

final class GiftCardAddBuilder {
    
    static func make() -> GiftCardAddViewController {
        let view = GiftCardAddViewController()
        
        let presenter = GiftCardAddPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
