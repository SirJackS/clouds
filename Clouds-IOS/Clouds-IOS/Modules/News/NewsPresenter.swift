//
//  NewsPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

final class NewsPresenter: NewsPresenterProtocol {
    
    private unowned let view: NewsViewProtocol
    private var service: ServiceProtocol
    private var selected: Int
    
    init(view: NewsViewProtocol,
         service: ServiceProtocol,
         selected: Int) {
        self.view = view
        self.service = service
        self.selected = selected
    }
    
    func load() {
        self.getCampaign()
    }
    
    private func getCampaign() {
//        self.view.handleOutput(.showActivity(true))
        service.getCampaignForSignUp { [weak self] (result) in
            guard let self = self else { return }
            
//            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showCampaign(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCampaignForSignUp))
            
            default: break
            }
        }
    }
}
