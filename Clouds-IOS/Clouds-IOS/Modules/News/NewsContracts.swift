//
//  NewsContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

// MARK: - Presenter

protocol NewsPresenterProtocol: class {
    func load()
}

enum NewsPresenterOutput {
    case showMessage(String)
    case showCampaign(BannerCampaign)
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol NewsViewProtocol: class {
    func handleOutput(_ output: NewsPresenterOutput)
}
