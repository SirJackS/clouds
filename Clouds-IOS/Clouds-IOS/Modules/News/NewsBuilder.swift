//
//  NewsBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

final class NewsBuilder {
    
    static func make() -> NewsViewController {
        let view = NewsViewController()
        
        let presenter = NewsPresenter(view: view, service: App.service, selected: 1)
        view.presenter = presenter
        
        return view
    }
}
