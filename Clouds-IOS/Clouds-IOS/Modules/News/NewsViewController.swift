//
//  NewsViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import UIKit
import SDWebImage

final class NewsViewController: UIViewController, NewsViewProtocol {

    var presenter: NewsPresenterProtocol!
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.isHidden = true
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "X-white-icon"), for: .normal)
        button.backgroundColor = .clear
        button.tintColor = UIColor.white
        button.anchor(.size(width: 24, height: 24))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(cancelButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont(weight: .bold, size: 24)
        label.numberOfLines = 1
        label.anchor(.height(32))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 2
        label.anchor(.height(44))
        return label
    }()
    
    private lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("sign up".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.backgroundColor = UIColor.clear
        button.setupBorder(width: 1, color: UIColor.white)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(36))
        button.addTarget(self, action: #selector(signupButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var gradientView: GradientView = {
        let view = GradientView()
        view.backgroundColor = UIColor.clear
//        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelButtonTouchUp))
        tap.delegate = self
        return tap
    }()

//    // MARK: - Data
//    private var data: News?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        
        self.presenter.load()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 0.02) {
//            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.14)
//        }
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.view.backgroundColor = UIColor.clear
//
//    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        bodyView.anchor(
            .height((UIScreen.main.bounds.width-32)/343*440),
            .leading(16), .trailing(-16), .centerY())
        
        photoView.anchor(.fillSuperview())
        
        backButton.anchor(.top(24), .trailing(-24))
        
        gradientView.anchor(
            .leading(), .trailing(), .bottom(), .height(261))
        
        signUpButton.anchor(.leading(16), .trailing(-16), .bottom(-24))
        
        descLabel.anchor(
            .leading(16), .trailing(-16),
            .bottom(signUpButton.topAnchor, constant: -40))
        
        titleLabel.anchor(
            .leading(16), .trailing(-16),
            .bottom(descLabel.topAnchor, constant: -16))
        
    }

    func handleOutput(_ output: NewsPresenterOutput) {
        switch output {
        case .showCampaign(let campaign):
            self.setupData(campaign)
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getCampaignForSignUp:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addGestureRecognizer(tapGesture)
        self.view.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(gradientView)
        bodyView.addSubview(backButton)
        
        gradientView.addSubview(titleLabel)
        gradientView.addSubview(descLabel)
        gradientView.addSubview(signUpButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor =  UIColor.black.withAlphaComponent(0.25)//UIColor.clear
    }
    
    private func setupData(_ campaign: BannerCampaign) {
        if let imageUrl = URL(string: campaign.cover ?? "") {
            self.photoView.sd_setImage(with: imageUrl) { (_,_, _, _) in
                self.bodyView.isHidden = false
            }
        }
        self.titleLabel.text = campaign.name
        self.descLabel.text = campaign.description
        
    }
    
    // MARK: - Action
    
    @objc private func cancelButtonTouchUp() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func signupButtonTouchUp() {
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed3"), object: nil)
    }
}

extension NewsViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
