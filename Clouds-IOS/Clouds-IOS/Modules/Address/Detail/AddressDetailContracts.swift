//
//  AddressDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import Foundation

// MARK: - Presenter

protocol AddressDetailPresenterProtocol: class {
    func load()
    func getCitiesBy(_ countryId: Int)
    func storeAddress(_ address: String?, _ floor: String?, _ selectedCountry: Country?, _ selectedCity: City?,_ phone: String?)
    func updateAddress(id addressId: Int,_ address: String?, _ floor: String?, _ selectedCountry: Country?, _ selectedCity: City?,_ phone: String?)
    func deleteAddress(id: Int)
    func getCountries()
    func getAddress(id: Int)
}

enum  AddressDetailPresenterOutput {
    case showMessage(String)
    case showDetail(AddressDetail)
    case showActivity(Bool)
    case showCountryList([Country])
    case showCitiesBy([City])
    case handleError([String: [String]])
    case dismiss
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol AddressDetailViewProtocol: class {
    func handleOutput(_ output: AddressDetailPresenterOutput)
}
