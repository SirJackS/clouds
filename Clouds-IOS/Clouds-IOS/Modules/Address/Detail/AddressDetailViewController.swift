//
//  AddressDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import UIKit

final class AddressDetailViewController: UIViewController, AddressDetailViewProtocol {

    var presenter: AddressDetailPresenterProtocol!
    
    private lazy var navBarView: BTBNavbarView = {
        let view = BTBNavbarView(title: "My Address".localized(), rightIcon: "basket-delete-icon")
        view.delegate = self
        view.rightButton.isHidden = true
        return view
    }()
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var textViewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [countryNameView, cityNameView,addressView,flatView, phoneNumberView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 12
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var countryNameView: CustomTextView = {
        let view = CustomTextView(title: "Country".localized())
        view.downButton.isHidden = false
//        view.mainText.delegate = self
        view.delegate = self
        view.mainText.tintColor = .clear
        view.mainText.inputAccessoryView = countryToolbar
        view.mainText.inputView = countryPickerView
        return view
    }()
    
    private lazy var countryPickerView: CustomPickerView = {
        let picker = CustomPickerView()
        picker.delegate = self
        picker.dataSource = self
//        if #available(iOS 13.4, *) {
//            picker. = .wheels
//        }
        return picker
    }()
    
    private lazy var cityNameView: CustomTextView = {
        let view = CustomTextView(title: "City".localized())
        view.downButton.isHidden = false
//        view.mainText.delegate = self
        view.delegate = self
        view.mainText.tintColor = .clear
        view.mainText.inputAccessoryView = cityToolbar
        view.mainText.inputView = cityPickerView
        view.mainText.isEnabled = false
        return view
    }()
    
    private lazy var cityPickerView: CustomPickerView = {
        let picker = CustomPickerView()
        picker.delegate = self
        picker.dataSource = self
//        if #available(iOS 13.4, *) {
//            picker. = .wheels
//        }
        return picker
    }()
    
    private lazy var addressView: CustomTextView = {
        let view = CustomTextView(title: "Address".localized())
        return view
    }()
    
    private lazy var flatView: CustomTextView = {
        let view = CustomTextView(title: "Floor / Flat / Etc".localized())
        return view
    }()
    
    private lazy var phoneNumberView: CustomPhoneTextView = {
        let view = CustomPhoneTextView(title: "Phone number".localized())
//        view.mainText.delegate = self
        view.codeText.tintColor = .clear
        view.codeText.inputAccessoryView = phoneToolbar
        view.codeText.inputView = phoneCodePicker
        return view
    }()
    
    private lazy var phoneCodePicker: CustomPickerView = {
        let picker = CustomPickerView()
        picker.delegate = self
        picker.dataSource = self
        return picker
    }()
    
    private lazy var countryToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barTintColor = .white
        toolbar.backgroundColor = .white
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(doneCountryPicker))
        doneButton.tintColor = .black
        let clearButton = UIBarButtonItem(title: "clear".localized().capitalized, style: .plain, target: self, action: #selector(clearCountryPicker))
        clearButton.tintColor = .black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelCountryPicker));
//        cancelButton.tintColor = .black
        toolbar.setItems([clearButton,spaceButton,doneButton], animated: false)
        return toolbar
    }()
    
    private lazy var cityToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barTintColor = .white
        toolbar.backgroundColor = .white
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(doneCityPicker))
        doneButton.tintColor = .black
        let clearButton = UIBarButtonItem(title: "clear".localized().capitalized, style: .plain, target: self, action: #selector(clearCityPicker))
        clearButton.tintColor = .black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelCityPicker));
//        cancelButton.tintColor = .black
        toolbar.setItems([clearButton,spaceButton,doneButton], animated: false)
        return toolbar
    }()
    
    private lazy var phoneToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barTintColor = .white
        toolbar.backgroundColor = .white
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(donePhonePicker))
        doneButton.tintColor = .black
        let clearButton = UIBarButtonItem(title: "clear".localized().capitalized, style: .plain, target: self, action: #selector(clearPhonePicker))
        clearButton.tintColor = .black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([clearButton,spaceButton,doneButton], animated: false)
        return toolbar
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("save changes".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Data
    private var addressData: AddressDetail?
    
    private var countries: [Country] = []
    private var selectedCountryId: Int?
    private var selectedCountry: Country?
    private var cities: [City] = []
    private var selectedCityId: Int?
    private var selectedCity: City?
    
    private var selectedPhoneCodeId: Int?
    private var selectedPhoneCode: Country?
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.getCountries()
        self.presenter.load()

    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.top(), .leading(), .trailing(), .bottom())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        textViewStack.anchor(.top(24), .leading(16), .trailing(-16))
        
        saveButton.anchor(
            .top(textViewStack.bottomAnchor, constant: 40),
            .leading(16), .trailing(-16), .bottom(-32))
        
    }

    func handleOutput(_ output: AddressDetailPresenterOutput) {
        switch output {
        case .showDetail(let data):
            self.addressData = data
            self.setupData(data)
        case .showCountryList(let data):
            self.countries = data
        case .showCitiesBy(let data):
            self.cities = data
            self.cityPickerView.reloadComponent(0)
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .dismiss:
            selectedCountry = nil
            selectedCity = nil
            countryNameView.mainText.text = nil
            cityNameView.mainText.text = nil
            addressView.mainText.text = nil
            flatView.mainText.text = nil
            phoneNumberView.phoneText.text = nil
            phoneNumberView.codeText.text = nil
            self.popVC()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addressDetailDismissed"), object: nil)
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .deleteAddress:
                        self.deleteButtonTouchUp()
                    case .getAddressDetail:
                        self.presenter.load()
                    case .storeAddress:
                        self.saveButtonTouchUp()
                    case .updateAddress:
                        self.saveButtonTouchUp()
                    case .getCitiesBy:
                        guard let id = self.selectedCountry?.id else { return }
                        self.presenter.getCitiesBy(id)
                    case .getCountries:
                        self.presenter.getCountries()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(textViewStack)
        contentView.addSubview(saveButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc func doneCountryPicker(){
        if let id = selectedCountryId {
            selectedCountry = countries[id]
            countryNameView.mainText.text = selectedCountry?.name
        } else {
            selectedCountry = countries[0]
            countryNameView.mainText.text = countries[0].name
        }
        
        self.cities.removeAll()
        self.presenter.getCitiesBy(selectedCountry!.id)
        cityNameView.mainText.isEnabled = true
        
        selectedCity = nil
        selectedCityId = nil
        cityNameView.mainText.text = ""
        cityPickerView.selectRow(0, inComponent: 0, animated: true)
        
        
//        guard let id = selectedCountry?.id else { return }
//        if self.cities.count == 0 {
//
//            self.presenter.getCitiesBy(id)
//            if cities.count > 0 {
//                self.cityNameView.mainText.isEnabled = true
//            }
//        }
        self.view.endEditing(true)
        //selectedCountryId = nil
    }
    
    @objc func clearCountryPicker(){
        selectedCountry = nil
        selectedCity = nil
        countryNameView.mainText.text = ""
        cityNameView.mainText.text = ""
        cityNameView.mainText.isEnabled = false
        self.view.endEditing(true)
    }
    
    @objc func doneCityPicker(){
        guard self.cities.count > 0 else { return }
        if let id = selectedCityId {
            selectedCity = cities[id]
            cityNameView.mainText.text = selectedCity?.name
        } else {
            selectedCity = cities[0]
            cityNameView.mainText.text = cities[0].name
        }
        self.view.endEditing(true)
        //selectedCityId = nil
    }
    
    @objc func clearCityPicker(){
        selectedCity = nil
        cityNameView.mainText.text = ""
        self.view.endEditing(true)
    }
    
    @objc func donePhonePicker(){
        if let id = selectedPhoneCodeId {
            selectedPhoneCode = countries[id]
            phoneNumberView.codeText.text = "+\(selectedPhoneCode!.phoneCode)"
        } else {
            selectedPhoneCode = countries[0]
            phoneNumberView.codeText.text  = "+\(countries[0].phoneCode)"
        }
        self.view.endEditing(true)
    }
    
    @objc func clearPhonePicker(){
        phoneNumberView.codeText.text = ""
        selectedPhoneCode = nil
        self.view.endEditing(true)
    }
    
    @objc func saveButtonTouchUp() {
        self.hideErrors()
        self.saveButton.isEnabled = false
        if let data = addressData {
            self.presenter.updateAddress(
                id: data.id,
                addressView.mainText.text ?? "",
                flatView.mainText.text ?? "",
                selectedCountry,
                selectedCity,
                phoneNumberView.phoneText.text ?? "")
            
        } else {
            self.presenter.storeAddress(
                addressView.mainText.text ?? "",
                flatView.mainText.text ?? "",
                selectedCountry,
                selectedCity,
                phoneNumberView.phoneText.text ?? "")
        }
        self.saveButton.isEnabled = true
    }
    
    @objc func deleteButtonTouchUp() {
        self.showDeleteError { (result) in
            if result {
                self.navBarView.rightButton.isEnabled = false
                guard let address = self.addressData else { return }
                self.presenter.deleteAddress(id: address.id)
                self.navBarView.rightButton.isEnabled = true
            }
        }
    }
    
    // MARK: - Setup Data
    
    private func setupData(_ data: AddressDetail) {
        self.navBarView.rightButton.isHidden = false
        self.selectedCountry = data.country
        self.selectedCity = data.city
        self.addressView.mainText.text = data.address
        self.flatView.mainText.text = data.floor
//        self.postCodeView.mainText.text = data.postCode
        self.phoneNumberView.codeText.text = data.country?.phoneCode ?? ""
        self.phoneNumberView.phoneText.text = data.phone
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "address":
            self.addressView.errorText = errorText
            self.addressView.isError = true
        case "country_id":
            self.countryNameView.errorText = errorText
            self.countryNameView.isError = true
        case "city_id":
            self.cityNameView.errorText = errorText
            self.cityNameView.isError = true
//        case "post_code":
//            self.postCodeView.errorText = errorText
//            self.postCodeView.isError = true
        case "floor":
            self.flatView.errorText = errorText
            self.flatView.isError = true
        case "phone":
            self.phoneNumberView.errorText = errorText
            self.phoneNumberView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.addressView.isError = false
        self.countryNameView.isError = false
        self.cityNameView.isError = false
//        self.postCodeView.isError = false
        self.flatView.isError = false
        self.phoneNumberView.isError = false
    }
    
//    private func openCountryPicker(countries: [Country]) {
//        let alert = UIAlertController(title: "Sizes", message: nil, preferredStyle: .actionSheet)
//        var selectedCountryy: Country?
//        let frameCountries: [CGFloat] = (0..<countries.count).map { CGFloat($0) }
//        let pickerViewValues: [[String]] = [countries.map { $0.name }]
//        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameCountries.firstIndex(of: CGFloat(frameCountries.count)) ?? 0)
//
//        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
//            if countries.count > 0 {
//                selectedCountryy = countries[index.row]
//            }
//        }
//        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) in
//            if action.style == .default {
//                guard let data = selectedCountryy else { return }
//                self.selectedCountry = data
//                self.countryNameView.mainText.text = data.name
//                self.cities.removeAll()
//                self.selectedCity = nil
//                self.cityNameView.mainText.text = ""
//                self.presenter.getCitiesBy(data.id)
//                self.cityNameView.mainText.isEnabled = true
//
//            }
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (actionn) in
//            if actionn.style == .cancel {
//                alert.dismiss(animated: true, completion: nil)
//            }
//        }))
//        self.present(alert, animated: true, completion: nil)
//    }
//
//    private func openCityPicker(cities: [City]) {
//        let alert = UIAlertController(title: "Cities", message: nil, preferredStyle: .actionSheet)
//        var selectedCityy: City?
//        let frameCities: [CGFloat] = (0..<cities.count).map { CGFloat($0) }
//        let pickerViewValues: [[String]] = [cities.map { $0.name }]
//        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameCities.firstIndex(of: CGFloat(frameCities.count)) ?? 0)
//
//        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
//            if cities.count > 0 {
//                selectedCityy = cities[index.row]
//            }
//        }
//        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) in
//            if action.style == .default {
//                guard let data = selectedCityy else { return }
//                self.selectedCity = data
//                self.cityNameView.mainText.text = data.name
//            }
//        }))
//        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (actionn) in
//            if actionn.style == .cancel {
//                alert.dismiss(animated: true, completion: nil)
//            }
//        }))
//        self.present(alert, animated: true, completion: nil)
//    }
}

// MARK: - UIScrollView delegate

extension AddressDetailViewController: UIScrollViewDelegate {
    
}

// MARK: - BTNavbarView delegate

extension AddressDetailViewController: BTBNavbarViewDelegate {
    func rightButtonClick() {
        self.deleteButtonTouchUp()
    }
    
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField delegate
//extension AddressDetailViewController: UITextFieldDelegate {
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        switch textField {
//        case countryNameView.mainText:
//            if let country = selectedCountry {
//                let index = countries.first
//            }
//            self.countryPickerView.selectRow(, inComponent: , animated: )
//        case cityNameView.mainText:
//            if countryNameView.mainText.text != "" {
//                cityNameView.mainText.becomeFirstResponder()
//            }
//        default: break
//        }
//    }
//}

extension AddressDetailViewController: CustomTextViewDelegate {
    func downButtonClick(_ textView: CustomTextView) {
        switch textView {
        case countryNameView:
            countryNameView.mainText.becomeFirstResponder()
        case cityNameView:
            if countryNameView.mainText.text != "" {
                cityNameView.mainText.becomeFirstResponder()
            }
        default: break
        }
    }
}

extension AddressDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case countryPickerView:
            return countries.count
        case cityPickerView:
            return cities.count
        case phoneCodePicker:
            return countries.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 70.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.lineBreakMode = .byWordWrapping
            pickerLabel?.font = UIFont(weight: .medium, size: 20)
            pickerLabel?.textAlignment = .center
            pickerLabel?.numberOfLines = 2
        }
        switch pickerView {
        case countryPickerView:
            if self.countries.count > 0 {
                pickerLabel?.text = self.countries[row].name
            }
        case cityPickerView:
            if self.cities.count > 0 {
                pickerLabel?.text = cities[row].name//error
            }
        case phoneCodePicker:
            pickerLabel?.text = "+"+countries[row].phoneCode+" "+countries[row].name
        default:
            pickerLabel?.text = ""
        }
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //self.pickerSelectedId = row
        switch pickerView {
        case countryPickerView:
            selectedCountryId = row
        case cityPickerView:
            selectedCityId = row
        case phoneCodePicker:
            selectedPhoneCodeId = row
        default: break
        }
    }
}
