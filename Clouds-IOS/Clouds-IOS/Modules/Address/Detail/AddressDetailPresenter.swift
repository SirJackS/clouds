//
//  AddressDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import Foundation

final class AddressDetailPresenter: AddressDetailPresenterProtocol {
    
    private unowned let view: AddressDetailViewProtocol
    private var service: ServiceProtocol
    private var index: Int
    
    init(view: AddressDetailViewProtocol,
         service: ServiceProtocol,
         index: Int) {
        self.view = view
        self.service = service
        self.index = index
    }
    
    func load() {
        if index != -1 {
            self.getAddress(id: index)
        }
    }
    
    func deleteAddress(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.deleteAddress(id) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                self.view.handleOutput(.dismiss)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.deleteAddress))

            default: break
            }
        }
    }
    
    func getAddress(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.getAddressDetail(id) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showDetail(data))
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getAddressDetail))

            default: break
            }
        }
    }
    
    func storeAddress(_ address: String?, _ floor: String?, _ selectedCountry: Country?, _ selectedCity: City?, _ phone: String?) {
        self.view.handleOutput(.showActivity(true))
        
        var request = AddressDetailRequest(address: address, floor: floor, countryId: nil, cityId: nil, phone: phone)
        if let country = selectedCountry {
            request.countryId = "\(country.id)"
        } else {
            request.countryId = nil
        }
        
        if let city = selectedCity {
            request.cityId = "\(city.id)"
        } else {
            request.cityId = nil
        }
        
        service.storeAddress(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.dismiss)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.storeAddress))

            default: break
            }
        }
    }
    
    func updateAddress(id addressId: Int,_ address: String?, _ floor: String?, _ selectedCountry: Country?, _ selectedCity: City?, _ phone: String?) {
        self.view.handleOutput(.showActivity(true))
        
        var request = AddressDetailRequest(address: address, floor: floor, countryId: nil, cityId: nil, phone: phone)
        if let country = selectedCountry {
            request.countryId = "\(country.id)"
        } else {
            request.countryId = nil
        }
        
        if let city = selectedCity {
            request.cityId = "\(city.id)"
        } else {
            request.cityId = nil
        }
        
        service.updateAddress(addressId, request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.dismiss)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.updateAddress))

            default: break
            }
        }
    }
    
    func getCitiesBy(_ countryId: Int) {
        self.view.handleOutput(.showActivity(true))
        service.getCitiesByCountry(countryId) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showCitiesBy(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCitiesBy))
                
            default: break
            }
        }
    }
    
    func getCountries() {
        self.view.handleOutput(.showActivity(true))
        service.getCountries { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showCountryList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCountries))
                
            default: break
            }
        }
    }
}
