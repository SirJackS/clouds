//
//  AddressDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import Foundation

final class AddressDetailBuilder {
    
    static func make(index: Int = -1) -> AddressDetailViewController {
        let view = AddressDetailViewController()
        
        let presenter = AddressDetailPresenter(view: view, service: App.service, index: index)
        view.presenter = presenter
        
        return view
    }
}
