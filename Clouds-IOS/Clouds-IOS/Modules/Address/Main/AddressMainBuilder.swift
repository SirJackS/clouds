//
//  AddressMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

final class AddressMainBuilder {
    
    static func make() -> AddressMainViewController {
        let view = AddressMainViewController()
        
        let presenter = AddressMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
