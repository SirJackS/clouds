//
//  AddressMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit
import SDWebImage

final class AddressMainViewController: UIViewController, AddressMainViewProtocol {
 
    var presenter: AddressMainPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "My Addresses".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(AddressMainCell.self, forCellReuseIdentifier: AddressMainCell.ID)
        tableView.register(AddressAddButtonCell.self, forCellReuseIdentifier: AddressAddButtonCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.isHidden = true
        tableView.rowHeight = UITableView.automaticDimension
        //tableView.estimatedRowHeight = 143
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 24, right: 0)
        return tableView
    }()
    
//    private var noScroll: Bool = false
    
    private var data: [AddressMain] = []
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: AddressMainPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
            self.tableView.isHidden = false
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getAllAddresses:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.handleModalDismissed),
        name: NSNotification.Name(rawValue: "addressDetailDismissed"),
        object: nil)
        
    }
    
    // MARK: - Action
    
    @objc func handleModalDismissed() {
        self.tableView.isHidden = true
        self.presenter.load()
        self.tableView.isHidden = false
    }
    
}

extension AddressMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == data.count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddressAddButtonCell.ID, for: indexPath) as? AddressAddButtonCell else { return UITableViewCell() }
            cell.delegate = self
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddressMainCell.ID, for: indexPath) as? AddressMainCell else { return UITableViewCell() }
            cell.titleLabel.text = "Address \(indexPath.row+1)"
            cell.data = data[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != data.count {
            let viewController = AddressDetailBuilder.make(index: data[indexPath.row].id)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension AddressMainViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

extension AddressMainViewController: AddressAddButtonCellDelegate {
    func addButtonClick() {
        let viewController = AddressDetailBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
