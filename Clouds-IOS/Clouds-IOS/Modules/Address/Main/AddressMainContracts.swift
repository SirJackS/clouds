//
//  AddressMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

// MARK: - Presenter

protocol AddressMainPresenterProtocol: class {
    func load()
}

enum AddressMainPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case showList([AddressMain])
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol AddressMainViewProtocol: class {
    func handleOutput(_ output: AddressMainPresenterOutput)
}
