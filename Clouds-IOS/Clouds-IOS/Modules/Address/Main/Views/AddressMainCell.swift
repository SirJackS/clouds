//
//  AddressMainCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit
import SDWebImage

final class AddressMainCell: UITableViewCell {

    static let ID: String = "AddressMainCell"
    
    var data: AddressMain! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var viewsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topView, addressView])
        stackView.axis = .vertical
//        stackView.alignment = .fill
//        stackView.distribution = .fillEqually
        stackView.spacing = 3
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var addressView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [addressLabel, floorLabel, cityLabel])
        stackView.axis = .vertical
//        stackView.alignment = .fill
//        stackView.distribution = .fillEqually
        stackView.spacing = 3
        stackView.clipsToBounds = true
        return stackView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 16)
        label.textAlignment = .left
        label.anchor(.height(26))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var floorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var cityLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var forwardView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "right-arrow-icon")
        imageView.backgroundColor = .clear
        imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addressLabel.text = ""
        self.titleLabel.text = ""
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        viewsStack.anchor(.top(24), .leading(16), .trailing(-16), .bottom())
        
        titleLabel.anchor(
            .top(), .leading(), .bottom(),
            .trailing(forwardView.leadingAnchor, constant: -8))
        
        forwardView.anchor(.centerY(), .trailing())
        
        labelsStack.anchor(
            .top(12), .leading(16), .trailing(-16),
            .bottomLess(addressView.bottomAnchor, constant: -12))
        
        addressView.anchor(.height(90))
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(viewsStack)
        
        topView.addSubview(titleLabel)
        topView.addSubview(forwardView)
        
        addressView.addSubview(labelsStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        if data.floor == nil {
            self.floorLabel.isHidden = true
        }
        self.floorLabel.text = data.floor
        if let city = data.city?.name {
            self.addressLabel.text = "\(city),\(data.address ?? "")"
            self.cityLabel.text = "\(city), \(data.country?.name ?? "")"
        }
    }
}
