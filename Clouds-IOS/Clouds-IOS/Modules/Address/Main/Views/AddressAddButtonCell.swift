//
//  AddressAddButtonCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit

protocol AddressAddButtonCellDelegate: class {
    func addButtonClick()
}

final class AddressAddButtonCell: UITableViewCell {

    static let ID: String = "AddressAddButtonCell"
    
    weak var delegate: AddressAddButtonCellDelegate?
    
    var title: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var addButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Add New Address".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.setImage(UIImage(named: "add-circle-icon"), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        //button.setupBorder(width: 1, color: UIColor.black.withAlphaComponent(0.87))
        button.addDashedBorder()
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(56))
//        button.contentHorizontalAlignment = .center

        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(addButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        addButton.anchor(
            .top(24), .leading(16),
            .bottom(-24), .trailing(-16))
        
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(addButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {

    }
    
    // MARK: - Action
    
    @objc private func addButtonTouchUp() {
        self.delegate?.addButtonClick()
    }
}
