//
//  AddressMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

final class AddressMainPresenter: AddressMainPresenterProtocol {
    
    private unowned let view: AddressMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: AddressMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getAddresses()
    }
    
    private func getAddresses() {
        self.view.handleOutput(.showActivity(true))
        service.getAllAddresses { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getAllAddresses))
                
            default: break
            }
            
        }
    }
}
