//
//  OrderResponseViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import UIKit
import SDWebImage

final class OrderResponseViewController: UIViewController, OrderResponseViewProtocol {

    var presenter: OrderResponsePresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Checkout".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        let height = UIScreen.main.bounds.width * (80 / 375)
//        imageView.layer.cornerRadius = height/2
//        imageView.clipsToBounds = true
        imageView.anchor(.size(width: height, height: height))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 34)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.38)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 3
        return label
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("continue shopping".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        return button
    }()
    
//    private var data: OrderResponse?
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
//        bodyView.anchor(.leading(16), .trailing(-16), .centerY())
        bodyView.anchor(.leading(16), .trailing(-16), .top(136))
        
        photoView.anchor(.top(), .centerX())
        
        titleLabel.anchor(
            .top(photoView.bottomAnchor, constant: 32),
            .leading(), .trailing())
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 16),
            .leading(8), .trailing(-8), .bottom())
        
        continueButton.anchor(
            .leading(16), .trailing(-16),
            .bottom(view.safeAreaLayoutGuide.bottomAnchor, constant: -6))
    }

    func handleOutput(_ output: OrderResponsePresenterOutput) {
        switch output {
        case .showData(let data):
//            self.data = data
            self.configureData(data)
        default: break
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(bodyView)
        self.view.addSubview(continueButton)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(descLabel)
        
        self.updateViewConstraints()
    }
    
    private func configureData(_ response: OrderResponse) {
        self.photoView.image = UIImage(named: response.icon)
        self.titleLabel.text = response.title
        self.descLabel.text = response.desc
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
    }

}

extension OrderResponseViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}
