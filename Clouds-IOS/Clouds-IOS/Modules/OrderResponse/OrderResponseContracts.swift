//
//  OrderResponseContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

// MARK: - Presenter

protocol OrderResponsePresenterProtocol: class {
    func load()
}

enum OrderResponsePresenterOutput {
    case showMessage(String)
    case showData(OrderResponse)
}


// MARK: - View

protocol OrderResponseViewProtocol: class {
    func handleOutput(_ output: OrderResponsePresenterOutput)
}
