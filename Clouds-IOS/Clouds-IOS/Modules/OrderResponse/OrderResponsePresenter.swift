//
//  OrderResponsePresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

final class OrderResponsePresenter: OrderResponsePresenterProtocol {
    
    private unowned let view: OrderResponseViewProtocol
    private var service: ServiceProtocol
    private var success: Bool
    
    init(view: OrderResponseViewProtocol,
         service: ServiceProtocol,
         success: Bool) {
        self.view = view
        self.service = service
        self.success = success
    }
    
    func load() {
        getResponse()
    }
    
    private func getResponse() {
        var orderResponse: OrderResponse?
        if success {
            orderResponse = OrderResponse(icon: "success-icon", title: "Order Success".localized(), desc: "Your order has been places successfully! For more details, go to my orders".localized())
        } else {
            orderResponse = OrderResponse(icon: "error-icon", title: "Oops!\nSomething went wrong".localized(), desc: "Your order has been places successfully! For more details, go to my orders".localized())
        }
            
        self.view.handleOutput(.showData(orderResponse!))
    }
}
