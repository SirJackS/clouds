//
//  OrderResponseBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

final class OrderResponseBuilder {
    
    static func make(success: Bool) -> OrderResponseViewController {
        let view = OrderResponseViewController()
        
        let presenter = OrderResponsePresenter(view: view, service: App.service, success: success)
        view.presenter = presenter
        
        return view
    }
}

