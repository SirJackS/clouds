//
//  FilterPriceViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

final class FilterPriceViewController: UIViewController, FilterPriceViewProtocol {

    var presenter: FilterPricePresenterProtocol!
    
    private lazy var navBarView: BTBNavbarView = {
        let view = BTBNavbarView(title: "Price", rightIcon: "arrow-clockwise")
        view.delegate = self
        return view
    }()
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Filter by Price".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var rangeSlider: RangeSlider = {
        let slider = RangeSlider(frame: CGRect.zero)
        slider.trackHighlightTintColor = UIColor.black.withAlphaComponent(0.87)
        slider.thumbTintColor = UIColor.black.withAlphaComponent(0.87)
        slider.trackTintColor = UIColor(hexString: "E9E9E9")
//        slider.minimumValue = 0
//        slider.maximumValue = 1500
//        slider.curvaceousness = 0.0
        slider.clipsToBounds = false
        slider.addTarget(self, action: #selector(rangeSliderValueChanged(_:)), for: .valueChanged)
        return slider
    }()
    
    private lazy var fieldsStack: UIView = {
        let stackView = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return stackView
    }()
    
    private lazy var minusView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "price-minus-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    
    // First Field
    private lazy var text1Stack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [text1Label, text1TextView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var text1Label: UILabel = {
        let label = UILabel()
        label.text = "From".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var text1TextView: UIView = {
        let view = UIView(backgroundColor: UIColor.clear)
        view.setupBorder(width: 1, color: UIColor(hexString: "EFEFEF"))
        //let width = UIScreen.main.bounds.width - 263
        view.anchor(.height(54))
        return view
    }()
    
    private lazy var firstTextField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.6)
        textField.font = UIFont(weight: .medium, size: 16)
        textField.text = "0"
        
//        textField.returnKeyType = UIReturnKeyType.search
//        textField.tintColor = .black
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldChangedEditing(_:)), for: .editingChanged)
        return textField
    }()
    
    
    // Second Field
    private lazy var text2Stack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [text2Label, text2TextView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var text2Label: UILabel = {
        let label = UILabel()
        label.text = "To".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var text2TextView: UIView = {
        let view = UIView(backgroundColor: UIColor.clear)
        view.setupBorder(width: 1, color: UIColor(hexString: "EFEFEF"))
//        let width = UIScreen.main.bounds.width - 263
        view.anchor(.height(54))
        return view
    }()
    
    private lazy var secondTextField: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.font = UIFont(weight: .medium, size: 16)
        textField.text = "10000"
//        textField.returnKeyType = UIReturnKeyType.search
//        textField.tintColor = .black
        textField.delegate = self
        textField.addTarget(self, action: #selector(textFieldChangedEditing(_:)), for: .editingChanged)
        return textField
    }()
    
    private lazy var viewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("view items".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        return button
    }()
    
    private var data: [DetailFilter] = []
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        bodyView.anchor(.top(16), .leading(16), .trailing(-16))
        
        titleLabel.anchor(.top(16), .leading(16), .trailing(-16), .height(24))
        
        rangeSlider.anchor(.top(titleLabel.bottomAnchor, constant: 16), .leading(16), .trailing(-16), .height(24))
        
        fieldsStack.anchor(
            .top(rangeSlider.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16), .bottom(-16))
        
        firstTextField.anchor( .leading(8), .trailing(-8), .centerY(), .height(18))
        
        secondTextField.anchor( .leading(8), .trailing(-8), .centerY(), .height(18))
        
        minusView.anchor(.centerX(), .bottom(-15))
        
        text1Stack.anchor(.leading(), .top(), .bottom(), .trailing(minusView.leadingAnchor, constant: -24))
        
        text2Stack.anchor(.leading(minusView.trailingAnchor, constant: 24), .top(), .bottom(), .trailing())
        
        viewButton.anchor(
            .leading(16), .trailing(-16),
            .bottom(view.safeAreaLayoutGuide.bottomAnchor, constant: -6))
        
    }

    func handleOutput(_ output: FilterPricePresenterOutput) {
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(bodyView)
        self.view.addSubview(viewButton)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(fieldsStack)
        bodyView.addSubview(rangeSlider)
        
        fieldsStack.addSubview(text1Stack)
        fieldsStack.addSubview(text2Stack)
        fieldsStack.addSubview(minusView)
        
        text1TextView.addSubview(firstTextField)
        text2TextView.addSubview(secondTextField)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        firstTextField.text = String(Int(10000*rangeSlider.lowerValue))
        secondTextField.text = String(Int(10000*rangeSlider.upperValue))
    }

}


extension FilterPriceViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    @objc func textFieldChangedEditing(_ textField: UITextField) {
        switch textField {
        case firstTextField:
            let fieldText = Double(firstTextField.text?.trim() ?? "") ?? 0
            rangeSlider.lowerValue = fieldText/10000.0
        case secondTextField:
            let fieldText = Double(secondTextField.text?.trim() ?? "") ?? 0
            rangeSlider.upperValue = fieldText/10000.0
        default: break
        }
            
    }
}

extension FilterPriceViewController: BTBNavbarViewDelegate {
    func rightButtonClick() {
        FilterManager.instance.removePrice()
        rangeSlider.lowerValue = 0.0
        rangeSlider.upperValue = 1.0
        firstTextField.text = "0"
        secondTextField.text = "10000"
    }
    
    func backButtonClick() {
        FilterManager.instance.price = "\(firstTextField.text ?? "0")" + " - " + "\(secondTextField.text ?? "10000")"
        FilterManager.instance.minPrice = Int(firstTextField.text ?? "0") ?? 0
        FilterManager.instance.maxPrice = Int(secondTextField.text ?? "10000") ?? 10000
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectionMade"), object: nil)
        self.popVC()
    }
}
