//
//  FilterPricePresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class FilterPricePresenter: FilterPricePresenterProtocol {
    
    private unowned let view: FilterPriceViewProtocol
    private var service: ServiceProtocol
    
    init(view: FilterPriceViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
    }
}
