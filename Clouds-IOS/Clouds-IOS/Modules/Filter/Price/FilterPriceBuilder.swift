//
//  FilterPriceBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class FilterPriceBuilder {
    
    static func make() -> FilterPriceViewController {
        let view = FilterPriceViewController()
        
        let presenter = FilterPricePresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}

