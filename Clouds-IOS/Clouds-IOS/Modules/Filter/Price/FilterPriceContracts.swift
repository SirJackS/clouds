//
//  FilterPriceContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

// MARK: - Presenter

protocol FilterPricePresenterProtocol: class {
    func load()
}

enum FilterPricePresenterOutput {
    case showMessage(String)
}


// MARK: - View

protocol FilterPriceViewProtocol: class {
    func handleOutput(_ output: FilterPricePresenterOutput)
}
