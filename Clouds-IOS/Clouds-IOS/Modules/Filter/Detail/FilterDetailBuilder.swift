//
//  FilterDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class FilterDetailBuilder {
    
    static func make(by type: FilterTypeEnum) -> FilterDetailViewController {
        let view = FilterDetailViewController()
        
        let presenter = FilterDetailPresenter(view: view, service: App.service, type: type)
        view.presenter = presenter
        view.type = type
        
        return view
    }
}

