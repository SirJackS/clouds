//
//  FilterDetailCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit

final class FilterDetailCell: UITableViewCell {

    static let ID: String = "FilterDetailCell"
    
    var data: DetailFilter! {
        didSet {
            self.configure()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.setSelect()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()
    
    private lazy var checkView: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.image = UIImage(named: "check-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.leading(), .trailing(), .top(), .bottom())
        
        titleLabel.anchor(
            .leading(), .centerY(),
            .trailing(checkView.leadingAnchor, constant: -50),
            .height(26))
        
        checkView.anchor(.trailing(), .centerY())
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.checkView.isHidden = true
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(checkView)
        bodyView.addSubview(lineView)
        
        self.updateConstraints()
        
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
//
        if let cnt = data.cnt {
            self.titleLabel.text = "\(data.name) (\(cnt))"
        } else {
            self.titleLabel.text = "\(data.name)"
        }
    }
    
    private func setSelect() {
        checkView.isHidden = isSelected ? false: true
    }
    
    // MARK: - Action
    
    @objc private func okButtonTouchUp() {
        
    }
}
