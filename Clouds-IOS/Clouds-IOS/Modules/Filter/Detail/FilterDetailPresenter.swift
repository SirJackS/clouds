//
//  FilterDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

enum FilterTypeEnum {
    case category
    case productType
    case brand
    case color
    case size
}

final class FilterDetailPresenter: FilterDetailPresenterProtocol {
    
    private unowned let view: FilterDetailViewProtocol
    private var service: ServiceProtocol
    private var type: FilterTypeEnum
    
    init(view: FilterDetailViewProtocol,
         service: ServiceProtocol,
         type: FilterTypeEnum) {
        self.view = view
        self.service = service
        self.type = type
    }
    
    func load() {
        self.getFilterData()
    }
    
    private func getFilterData() {
        switch self.type {
        case .category:
            self.view.handleOutput(.setNavbarTitle("Category".localized()))
            getCategories()
        case .productType:
            self.view.handleOutput(.setNavbarTitle("Product Type".localized()))
            getProductTypes()
        case .brand:
            self.view.handleOutput(.setNavbarTitle("Brand".localized()))
            getBrands()
        case .color:
            self.view.handleOutput(.setNavbarTitle("Color".localized()))
            getProductColors()
        case .size:
            self.view.handleOutput(.setNavbarTitle("Size".localized()))
            getProductSizes()
        }
    }
    
    private func getCategories() {
        self.view.handleOutput(.showActivity(true))
        service.getFilterCategories { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError)
                
            default: break
            }
            
        }
    }
    
    private func getProductTypes() {
        self.view.handleOutput(.showActivity(true))
        service.getFilterProductTypes { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError)
                
            default: break
            }
            
        }
    }
    
    private func getBrands() {
        self.view.handleOutput(.showActivity(true))
        service.getFilterBrands { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError)
                
            default: break
            }
            
        }
    }
    
    private func getProductColors() {
        self.view.handleOutput(.showActivity(true))
        service.getFilterColors { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError)
                
            default: break
            }
            
        }
    }
    
    private func getProductSizes() {
        self.view.handleOutput(.showActivity(true))
        service.getFilterSizes { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError)
                
            default: break
            }
            
        }
    }
}
