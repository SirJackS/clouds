//
//  FilterDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

// MARK: - Presenter

protocol FilterDetailPresenterProtocol: class {
    func load()
}

enum FilterDetailPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case showList([DetailFilter])
    case setNavbarTitle(String)
    case showNetworkError
}


// MARK: - View

protocol FilterDetailViewProtocol: class {
    func handleOutput(_ output: FilterDetailPresenterOutput)
}
