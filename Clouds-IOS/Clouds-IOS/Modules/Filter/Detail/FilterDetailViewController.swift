//
//  FilterDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

final class FilterDetailViewController: UIViewController, FilterDetailViewProtocol {

    var presenter: FilterDetailPresenterProtocol!
    var type: FilterTypeEnum!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = true
        tableView.register(FilterDetailCell.self, forCellReuseIdentifier: FilterDetailCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 110, right: 0)
        return tableView
    }()
    
    private lazy var viewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("view items".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
//        button.contentHorizontalAlignment = .center
        
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(viewButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private var navbarTitle: String = "" {
        didSet {
            self.setNavbarTitle(title: navbarTitle)
        }
    }
    private var selectedData: [DetailFilter] = []
    private var data: [DetailFilter] = []
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview(leading: 16,trailing: -16))
        
        viewButton.anchor(
            .leading(16), .trailing(-16),
            .bottom(view.safeAreaLayoutGuide.bottomAnchor, constant: -6))
        
    }

    func handleOutput(_ output: FilterDetailPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
            self.configureSelections()
        case .setNavbarTitle(let title):
            self.navbarTitle = title
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError:
            self.showNetworkError { (result) in
                if result {
                    self.presenter.load()
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        self.view.addSubview(viewButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    private func setNavbarTitle(title: String) {
        navBarView.titleText = title
    }
    
    private func getSelectedDataFromTableView() -> [IndexPath] {
        guard let indexPaths = self.tableView.indexPathsForSelectedRows else { return [] }
        selectedData = []
        
        for indexPath in indexPaths {
            selectedData.append(data[indexPath.row])
        }
        
        return indexPaths
    }
    
    private func configureSelections() {
        switch self.type {
        case .category:
            for (index,element) in FilterManager.instance.categoriesIndexs.enumerated() {
                if data.count > index {
                    self.tableView.selectRow(at: element, animated: false, scrollPosition: .none)
                }
            }
        case .productType:
            for (index,element) in FilterManager.instance.productTypesIndexs.enumerated() {
                if data.count > index {
                    self.tableView.selectRow(at: element, animated: false, scrollPosition: .none)
                }
            }
        case .brand:
            for (index,element) in FilterManager.instance.brandsIndexs.enumerated() {
                if data.count > index {
                    self.tableView.selectRow(at: element, animated: false, scrollPosition: .none)
                }
            }
        case .color:
            for (index,element) in FilterManager.instance.colorsIndexs.enumerated() {
                if data.count > index {
                    self.tableView.selectRow(at: element, animated: false, scrollPosition: .none)
                }
            }
        case .size:
            for (index,element) in FilterManager.instance.sizesIndexs.enumerated() {
                if data.count > index {
                    self.tableView.selectRow(at: element, animated: false, scrollPosition: .none)
                }
            }
        default:
            return
        }
        
    }

    
    //MARK: - Action
    
    @objc func viewButtonTouchUp() {
        self.configureFilter()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "filterViewPressed"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func configureFilter() {
        switch self.type {
        case .category:
            FilterManager.instance.categoriesIndexs = self.getSelectedDataFromTableView()
            FilterManager.instance.categories = selectedData
            FilterManager.instance.categoriesName = FilterManager.instance.getNames(selectedData)
        case .productType:
            FilterManager.instance.productTypesIndexs = self.getSelectedDataFromTableView()
            FilterManager.instance.productTypes = selectedData
            FilterManager.instance.productTypesName = FilterManager.instance.getNames(selectedData)
        case .brand:
            FilterManager.instance.brandsIndexs = self.getSelectedDataFromTableView()
            FilterManager.instance.brands = selectedData
            FilterManager.instance.brandsName = FilterManager.instance.getNames(selectedData)
        case .color:
            FilterManager.instance.colorsIndexs = self.getSelectedDataFromTableView()
            FilterManager.instance.colors = selectedData
            FilterManager.instance.colorsName = FilterManager.instance.getNames(selectedData)
        case .size:
            FilterManager.instance.sizesIndexs = self.getSelectedDataFromTableView()
            FilterManager.instance.sizes = selectedData
            FilterManager.instance.sizesName = FilterManager.instance.getNames(selectedData)
        default:
            return
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "selectionMade"), object: nil)
    }
}


extension FilterDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterDetailCell.ID, for: indexPath) as? FilterDetailCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.isSelected = true
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.isSelected = false
        }
    }
}

extension FilterDetailViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.configureFilter()
        self.popVC()
    }
}
