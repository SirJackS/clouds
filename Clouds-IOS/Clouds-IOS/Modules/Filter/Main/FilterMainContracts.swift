//
//  FilterMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

// MARK: - Presenter

protocol FilterMainPresenterProtocol: class {
    func load()
}

enum FilterMainPresenterOutput {
    case showMessage(String)
    case showList([MainFilter])
}


// MARK: - View

protocol FilterMainViewProtocol: class {
    func handleOutput(_ output: FilterMainPresenterOutput)
}
