//
//  FilterMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class FilterMainPresenter: FilterMainPresenterProtocol {
    
    private unowned let view: FilterMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: FilterMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
//        FilterManager.instance.removeFilterData()
    }
}
