//
//  FilterMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class FilterMainBuilder {
    
    static func make() -> FilterMainViewController {
        let view = FilterMainViewController()
        
        let presenter = FilterMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
