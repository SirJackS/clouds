//
//  FilterMainCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit

final class FilterMainCell: UITableViewCell {

    static let ID: String = "FilterMainCell"
    
    var title: String? {
        didSet {
            self.configureTitle()
        }
    }
    
    var desc: String? {
        didSet {
            self.configureDesc()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.textAlignment = .left
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.text = "All".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.textAlignment = .right
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.leading(), .trailing(), .top(), .bottom())
        
        titleLabel.anchor(
            .leading(), .centerY(),
            .trailing(descLabel.leadingAnchor, constant: -50),
            .height(26), .widthGreater(50))
        
        descLabel.anchor(.trailing(), .centerY(), .height(26), .widthGreater(50))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.descLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(descLabel)
        bodyView.addSubview(lineView)
        
        self.updateConstraints()
        
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configureTitle() {
        self.titleLabel.text = title
    }
    
    private func configureDesc() {
        self.descLabel.text = desc
    }
}
