//
//  FilterMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

final class FilterMainViewController: UIViewController, FilterMainViewProtocol {

    var presenter: FilterMainPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Filter",backIcon: "x-icon")
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(FilterMainCell.self, forCellReuseIdentifier: FilterMainCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 110, right: 0)
        return tableView
    }()
    
    private lazy var viewButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("View items".uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.addTarget(self, action: #selector(viewButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private var data: [MainFilter] = []
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview(leading: 16, trailing: -16))
        
        viewButton.anchor(
            .leading(16), .trailing(-16),
            .bottom(view.safeAreaLayoutGuide.bottomAnchor, constant: -6))
        
    }

    func handleOutput(_ output: FilterMainPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
        default: break
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        self.view.addSubview(viewButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.selectionMade),
        name: NSNotification.Name(rawValue: "selectionMade"),
        object: nil)
        
    }
    
    // MARK: - Action
    
    @objc func selectionMade() {
        self.tableView.reloadData()
    }
    
    @objc func viewButtonTouchUp() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "filterViewPressed"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}


extension FilterMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterMainCell.ID, for: indexPath) as? FilterMainCell else { return UITableViewCell() }
        switch indexPath.row {
        case 0:
            cell.title = "Category".localized()
            cell.desc = (FilterManager.instance.categories.count != 0) ? FilterManager.instance.categoriesName: "All".localized()
        case 1:
            cell.title = "Product Type".localized()
            cell.desc = (FilterManager.instance.productTypes.count != 0) ? FilterManager.instance.productTypesName: "All".localized()
        case 2:
            cell.title = "Brand".localized()
            cell.desc = (FilterManager.instance.brands.count != 0) ? FilterManager.instance.brandsName: "All".localized()
        case 3:
            cell.title = "Color".localized()
            cell.desc = (FilterManager.instance.colors.count != 0) ? FilterManager.instance.colorsName: "All".localized()
        case 4:
            cell.title = "Size".localized()
            cell.desc = (FilterManager.instance.sizes.count != 0) ? FilterManager.instance.sizesName: "All".localized()
        case 5:
            cell.title = "Price".localized()
            cell.desc = (FilterManager.instance.price != "") ? FilterManager.instance.price: "All".localized()
        default:
            print("error")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let viewController = FilterDetailBuilder.make(by: .category)
            self.navigationController?.pushViewController(viewController, animated: true)
        case 1:
            let viewController = FilterDetailBuilder.make(by: .productType)
            self.navigationController?.pushViewController(viewController, animated: true)
        case 2:
            let viewController = FilterDetailBuilder.make(by: .brand)
            self.navigationController?.pushViewController(viewController, animated: true)
        case 3:
            let viewController = FilterDetailBuilder.make(by: .color)
            self.navigationController?.pushViewController(viewController, animated: true)
        case 4:
            let viewController = FilterDetailBuilder.make(by: .size)
            self.navigationController?.pushViewController(viewController, animated: true)
        case 5:
            let viewController = FilterPriceBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        default: break
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension FilterMainViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        FilterManager.instance.removeFilterData()
        self.dismiss(animated: true, completion: nil)
    }
}
