//
//  LangDropUpContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

// MARK: - Presenter

protocol LangDropUpPresenterProtocol: class {
    func load()
}

enum LangDropUpPresenterOutput {
    case showMessage(String)
    case showList([String])
}


// MARK: - View

protocol LangDropUpViewProtocol: class {
    func handleOutput(_ output: LangDropUpPresenterOutput)
}
