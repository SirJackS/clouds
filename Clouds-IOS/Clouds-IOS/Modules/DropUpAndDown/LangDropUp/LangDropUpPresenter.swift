//
//  LangDropUpPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class LangDropUpPresenter: LangDropUpPresenterProtocol {
    
    private unowned let view: LangDropUpViewProtocol
    private var service: ServiceProtocol
    
    init(view: LangDropUpViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getKeywords()
    }
    
//    func search() {
//        let keywords = self.restaurantData?.keywords ?? []
//        self.view.handleOutput(.pushSearch(restaurant, keywords))
//    }
    
    private func getKeywords() {
        let keywords: [String] = [
            "Azərbaycan dili",
            "Русский",
            "English"
        ]
        self.view.handleOutput(.showList(keywords))
    }
}
