//
//  LangDropUpViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

final class LangDropUpViewController: UIViewController, LangDropUpViewProtocol {

    var presenter: LangDropUpPresenterProtocol!
    
    private lazy var headerView: LangDropUpHeaderView = {
        let view = LangDropUpHeaderView()
        return view
    }()
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var flexibleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(LangDropUpCell.self, forCellReuseIdentifier: LangDropUpCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.isScrollEnabled = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        return tableView
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(UIColor(hexString: "C70D3A"), for: .normal)
        button.tintColor = UIColor(hexString: "C70D3A")
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = UIFont(weight: .regular, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(cancelButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelButtonTouchUp))
        tap.delegate = self
        return tap
    }()

    // MARK: - Data
    private var data: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        
        self.presenter.load()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 0.02) {
//            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.14)
//        }
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1.0) { //1.41.4
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
//            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.backgroundColor = UIColor.clear
        
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        bodyView.anchor(.leading(), .trailing(), .bottom(view.layoutMarginsGuide.bottomAnchor))
        
        flexibleView.anchor(.leading(), .trailing(), .bottom(), .top(bodyView.bottomAnchor))
        
        tableView.anchor(
            .top(),.leading(), .trailing(),
            .bottom(cancelButton.topAnchor), .height(194))
        
        cancelButton.anchor(.leading(), .trailing(),.bottom())
        
    }

    func handleOutput(_ output: LangDropUpPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
//            self.setupData()
        default: break
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addGestureRecognizer(tapGesture)
        self.view.addSubview(bodyView)
        self.view.addSubview(flexibleView)
        
        bodyView.addSubview(tableView)
        bodyView.addSubview(cancelButton)
        
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.clear
        
        self.tableView.setTableHeaderView(headerView: headerView)
        self.tableView.updateHeaderViewFrame()
    }
    
    
    @objc private func cancelButtonTouchUp() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed1"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}


extension LangDropUpViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let count = data[indexPath.section].foods?.count ?? 0
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LangDropUpCell.ID, for: indexPath) as? LangDropUpCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
//        cell.isLast = (indexPath.row == count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        switch indexPath.row {
//        case 0:
//            LanguageManager.shared.setCurrentLanguage(to: .az)
//        case 1:
//            LanguageManager.shared.setCurrentLanguage(to: .ru)
//        case 2:
//            LanguageManager.shared.setCurrentLanguage(to: .en)
//        default:
//            print("error")
//        }
        var lang = "az"
        switch indexPath.row {
        case 0:
            lang = "az"
//            L102Language.shared.setAppleLanguageTo(lang: "az")
        case 1:
            lang = "ru"
//            L102Language.shared.setAppleLanguageTo(lang: "ru")
        case 2:
            lang = "en"
//            L102Language.shared.setAppleLanguageTo(lang: "en")
        default:
            print("error")
        }
        
        Defaults.instance.language = lang
        print("qozz",Defaults.instance.language)
        L102Language.setAppleLAnguageTo(lang: lang)
        L012Localizer.Swizzling()
        L012Localizer.Swizzling()
        App.router.start()
        
    }
}

extension LangDropUpViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
