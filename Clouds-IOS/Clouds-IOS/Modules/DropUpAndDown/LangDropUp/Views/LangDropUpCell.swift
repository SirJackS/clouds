//
//  LangDropUpCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

final class LangDropUpCell: UITableViewCell {

    static let ID: String = "LangDropUpCell"
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .center
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.anchor(.leading(16),.trailing(-16), .centerY(), .height(22))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(titleLabel)
        self.addSubview(lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        //self.selectionStyle = .none
    }
    
    private func configure() {
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
//
        self.titleLabel.text = data
    }
}
