//
//  LangDropUpHeaderView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

final class LangDropUpHeaderView: UIView {
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Language select".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    init() {
        super.init(frame: .zero)
        
        self.addSubviews()
        self.setupUI()
        
//        titleLabel.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        titleLabel.anchor(
            .fillSuperview(top: 15, leading: 16, bottom: -15, trailing: -16) ,.height(20))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))

        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(titleLabel)
        self.addSubview(lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
