//
//  LangDropUpBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class LangDropUpBuilder {
    
    static func make() -> LangDropUpViewController {
        let view = LangDropUpViewController()
        
        let presenter = LangDropUpPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
