//
//  ProductDetailPickerPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import Foundation

final class ProductDetailPickerPresenter: ProductDetailPickerPresenterProtocol {
    
    private unowned let view: ProductDetailPickerViewProtocol
    private var service: ServiceProtocol
    var data: [Size]
    
    init(view: ProductDetailPickerViewProtocol,
         service: ServiceProtocol,
         data: [Size]) {
        self.view = view
        self.service = service
        self.data = data
    }
    
    func load() {
        self.view.handleOutput(.showList(data))
    }
}
