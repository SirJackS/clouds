//
//  ProductDetailPickerContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import Foundation

// MARK: - Presenter

protocol ProductDetailPickerPresenterProtocol: class {
    var data: [Size] { get }
    
    func load()
}

enum ProductDetailPickerPresenterOutput {
    case showMessage(String)
    case showList([Size])
}


// MARK: - View

protocol ProductDetailPickerViewProtocol: class {
    func handleOutput(_ output: ProductDetailPickerPresenterOutput)
}
