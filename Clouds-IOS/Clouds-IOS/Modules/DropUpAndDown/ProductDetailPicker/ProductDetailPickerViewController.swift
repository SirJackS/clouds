//
//  ProductDetailPickerViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import UIKit

protocol ProductDetailPickerViewDelegate: class {
    func selected(data: Size)
}

final class ProductDetailPickerViewController: UIViewController, ProductDetailPickerViewProtocol {
 
    var presenter: ProductDetailPickerPresenterProtocol!
    
    weak var delegate: ProductDetailPickerViewDelegate?
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var pickerView: CustomPickerView = {
        let pickerView = CustomPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        return pickerView
    }()
    
    private lazy var toolbarView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.anchor(.height(42))
        return view
    }()
    
    private lazy var flexibleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var sizeChartButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Size chart".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(weight: .medium, size: 16)
        button.contentHorizontalAlignment = .left
        button.anchor(.height(26))
//        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(sizeChartButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var doneButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("done".localized().capitalized, for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(weight: .medium, size: 16)
        button.contentHorizontalAlignment = .right
        button.anchor(.height(26))
//
//        //change image and text place
////        button.semanticContentAttribute = UIApplication.shared
////            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
////        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
////        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(doneButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var topLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0))
    }()
    
    private lazy var bottomLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelButtonTouchUp))
        return tap
    }()
    
    
    private var data: [Size] = []
    private var selectedRow: Size?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        
        self.presenter.load()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 1.0) {
            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.backgroundColor = UIColor.clear
        
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        bodyView.anchor(.leading(), .trailing(), .bottom(view.layoutMarginsGuide.bottomAnchor))
        
        flexibleView.anchor(.leading(), .trailing(), .bottom(), .top(bodyView.bottomAnchor))
        
        pickerView.anchor(.leading(), .trailing(), .bottom(), .height(216))
        
        toolbarView.anchor(.top(), .bottom(pickerView.topAnchor, constant: 0), .leading(), .trailing())
        
        sizeChartButton.anchor(.leading(16), .centerY(), .width(100))
        
        doneButton.anchor(.trailing(-16), .centerY(), .width(90))
        
        topLineView.anchor(.top(), .leading(), .trailing(), .height(0.3))
        
        bottomLineView.anchor(.bottom(), .leading(), .trailing(), .height(0.3))
    }

    func handleOutput(_ output: ProductDetailPickerPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.pickerView.reloadComponent(0)
//            guard let selected = self.presenter.selected else { return }
//            self.pickerView.selectRow(selected, inComponent: 0, animated: true)
            
        default: break
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(bodyView)
        self.view.addSubview(flexibleView)
        self.view.addGestureRecognizer(tapGesture)
        
        self.bodyView.addSubview(toolbarView)
        self.bodyView.addSubview(pickerView)
        
        toolbarView.addSubview(sizeChartButton)
        toolbarView.addSubview(doneButton)
        toolbarView.addSubview(topLineView)
        toolbarView.addSubview(bottomLineView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .clear //UIColor.black.withAlphaComponent(0.2)
    }
    
    // MARK: - Action
    
    @objc private func sizeChartButtonTouchUp() {
        
    }
    
    @objc private func doneButtonTouchUp() {
        if let selected = selectedRow {
            self.delegate?.selected(data: selected)
            self.cancelButtonTouchUp()
        } else {
            self.delegate?.selected(data: data[0])
            self.cancelButtonTouchUp()
        }
        
    }
    
    @objc private func cancelButtonTouchUp() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "modalIsDimissed"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProductDetailPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
        
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 34.0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var pickerLabel: UILabel? = (view as? UILabel)
        if pickerLabel == nil {
            pickerLabel = UILabel()
            pickerLabel?.lineBreakMode = .byWordWrapping
            //pickerLabel?.sizeToFit()
            pickerLabel?.font = UIFont(weight: .medium, size: 20)
            pickerLabel?.textAlignment = .center
            pickerLabel?.numberOfLines = 1
        }
        
//        let text = data[row].inStock ?  data[row].name.removeStrike() : data[row].name.strikeRedThrough()
        pickerLabel?.text = data[row].name
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRow = data[row]
    }
    
    
}
