//
//  ProductDetailPickerBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import Foundation

final class ProductDetailPickerBuilder {
    
    static func make(data: [Size]) -> ProductDetailPickerViewController {
        let view = ProductDetailPickerViewController()
        
        let presenter = ProductDetailPickerPresenter(view: view, service: App.service, data: data)
        view.presenter = presenter
        
        return view
    }
}
