//
//  SortDropDownViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

final class SortDropDownViewController: UIViewController, SortDropDownViewProtocol {

    var presenter: SortDropDownPresenterProtocol!
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    private lazy var curvedView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.setUpTriangle(height: 20, width: 22)
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(SortDropDownCell.self, forCellReuseIdentifier: SortDropDownCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.isScrollEnabled = false
        tableView.rowHeight = 40
        tableView.estimatedRowHeight = 40
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(cancelButtonTouchUp))
        tap.delegate = self
        return tap
    }()

    // MARK: - Data
    private var data: [String] = []
    private var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        
        self.presenter.load()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        UIView.animate(withDuration: 0.02) {
//            self.view.backgroundColor = UIColor.black.withAlphaComponent(0.14)
//        }
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.view.backgroundColor = UIColor.clear
//
//    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        curvedView.anchor(
            .bottom(bodyView.topAnchor, constant: 0), .leading(UIScreen.main.bounds.width/4),
            .size(width: 22, height: 20))
        
        bodyView.anchor(
            .top(view.safeAreaLayoutGuide.topAnchor, constant: 76),//140
            .leading(16), .trailing(-16))
        
        tableView.anchor(
            .top(8),.leading(), .trailing(),
            .bottom(-8), .height(120))
        
    }

    func handleOutput(_ output: SortDropDownPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
        case .selectIndex(let index):
            self.selectedIndex = index
            self.tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
            //self.tableView.reloadData()
        default: break
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addGestureRecognizer(tapGesture)
        self.view.addSubview(bodyView)
        self.view.addSubview(curvedView)
        
        bodyView.addSubview(tableView)
        
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor =  UIColor.black.withAlphaComponent(0.25)//UIColor.clear
    }
    
    
    @objc private func cancelButtonTouchUp() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sortDimissed"), object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}


extension SortDropDownViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let count = data[indexPath.section].foods?.count ?? 0
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SortDropDownCell.ID, for: indexPath) as? SortDropDownCell else { return UITableViewCell() }
        cell.data = data[indexPath.row].localized()
        cell.isLast = (indexPath.row == data.count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.isSelected = true
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "sortUpdated"), object: nil, userInfo: ["id": indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.isSelected = false
        }
    }
}

extension SortDropDownViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
