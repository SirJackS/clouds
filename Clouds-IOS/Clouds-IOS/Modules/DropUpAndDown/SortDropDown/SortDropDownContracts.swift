//
//  SortDropDownContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

// MARK: - Presenter

protocol SortDropDownPresenterProtocol: class {
    func load()
}

enum SortDropDownPresenterOutput {
    case showMessage(String)
    case showList([String])
    case selectIndex(Int)
}


// MARK: - View

protocol SortDropDownViewProtocol: class {
    func handleOutput(_ output: SortDropDownPresenterOutput)
}
