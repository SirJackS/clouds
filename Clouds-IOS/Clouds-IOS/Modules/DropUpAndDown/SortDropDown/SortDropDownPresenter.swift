//
//  SortDropDownPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class SortDropDownPresenter: SortDropDownPresenterProtocol {
    
    private unowned let view: SortDropDownViewProtocol
    private var service: ServiceProtocol
    private var selected: Int
    
    init(view: SortDropDownViewProtocol,
         service: ServiceProtocol,
         selected: Int) {
        self.view = view
        self.service = service
        self.selected = selected
    }
    
    func load() {
        self.getKeywords()
        self.view.handleOutput(.selectIndex(self.selected))
    }
    
//    func search() {
//        let keywords = self.restaurantData?.keywords ?? []
//        self.view.handleOutput(.pushSearch(restaurant, keywords))
//    }
    
    private func getKeywords() {
        let keywords: [String] = [
            "What’s New",
            "Price: High to Low",
            "Price: Low to High"
        ]
        self.view.handleOutput(.showList(keywords))
    }
}
