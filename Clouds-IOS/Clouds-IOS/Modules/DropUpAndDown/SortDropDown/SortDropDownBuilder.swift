//
//  SortDropDownBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class SortDropDownBuilder {
    
    static func make(selected: Int) -> SortDropDownViewController {
        let view = SortDropDownViewController()
        
        let presenter = SortDropDownPresenter(view: view, service: App.service, selected: selected)
        view.presenter = presenter
        
        return view
    }
}
