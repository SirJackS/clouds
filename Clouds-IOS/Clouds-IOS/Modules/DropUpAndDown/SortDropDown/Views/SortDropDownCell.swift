//
//  SortDropDownCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

final class SortDropDownCell: UITableViewCell {

    static let ID: String = "SortDropDownCell"
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    var isLast: Bool = false {
        didSet {
            lineView.isHidden = isLast
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.setSelect()
        }
    }
    
    private lazy var checkBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var checkView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        view.isHidden = true
        view.anchor(.size(width: 4, height: 4))
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 14)
        label.textAlignment = .left
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        titleLabel.text = ""
        checkView.isHidden = true
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        checkBackView.anchor(
            .top(),.bottom(),.leading(),
            .trailing(titleLabel.leadingAnchor))
        
        checkView.anchor(.centerY(), .centerX(checkBackView.centerXAnchor, constant: 2))
        
        titleLabel.anchor(.leading(40),.trailing(-16), .centerY(), .height(20))
        
        lineView.anchor(.leading(40), .bottom(), .trailing(), .height(0.3))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(titleLabel)
        self.addSubview(lineView)
        self.addSubview(checkBackView)
        
        checkBackView.addSubview(checkView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
//
        self.titleLabel.text = data
    }
    
    private func setSelect() {
        checkView.isHidden = isSelected ? false: true
    }
}
