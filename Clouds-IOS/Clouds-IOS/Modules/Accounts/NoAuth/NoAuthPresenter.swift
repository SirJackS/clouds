//
//  NoAuthPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.01.21.
//

import Foundation

final class NoAuthPresenter: NoAuthPresenterProtocol {
    
    private unowned let view: NoAuthViewProtocol
    private var service: ServiceProtocol
    private var navbarTitle: String
    private var descTitle: String
    
    init(view: NoAuthViewProtocol,
         service: ServiceProtocol,
         navbarTitle: String,
         descTitle: String) {
        self.view = view
        self.service = service
        self.navbarTitle = navbarTitle
        self.descTitle = descTitle
    }
    
    func load() {
        self.view.handleOutput(.showNavTitle(navbarTitle))
        self.view.handleOutput(.showDescTitle(descTitle))
    }
}
