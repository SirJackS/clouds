//
//  NoAuthContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.01.21.
//

import Foundation

// MARK: - Presenter

protocol NoAuthPresenterProtocol: class {
    func load()
}

enum NoAuthPresenterOutput {
    case showNavTitle(String)
    case showDescTitle(String)
}

// MARK: - View

protocol NoAuthViewProtocol: class {
    func handleOutput(_ output: NoAuthPresenterOutput)
}
