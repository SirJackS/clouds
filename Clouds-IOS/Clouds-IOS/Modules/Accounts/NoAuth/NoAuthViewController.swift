//
//  NoAuthViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.01.21.
//

import UIKit

final class NoAuthViewController: UIViewController, NoAuthViewProtocol {

    var presenter: NoAuthPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(hideBack: true)
        view.delegate = self
        return view
    }()
    
    private lazy var labelsView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.text = "You currently have no account.Sign up or Login\n to continue shopping.".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 2
        return label
    }()
    
    private lazy var buttonsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [loginButton, signUpButton])
        stackView.axis = .vertical
        stackView.alignment = .fill
        //stackView.distribution = .fillEqually
        stackView.spacing = 24
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("sign up".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(signupButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("login".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.white
        button.setupBorder(width: 1, color: UIColor.black.withAlphaComponent(0.87))
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(loginButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        labelsView.anchor(.top(116), .leading(16), .trailing(-16))
        
        titleLabel.anchor(.top(), .leading(), .trailing())
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 24),
            .leading(), .trailing(), .bottom())
        
        buttonsStack.anchor(
//            .topGreater(labelsView.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16), .bottom(-64))
    }

    func handleOutput(_ output: NoAuthPresenterOutput) {
        switch output {
        case .showNavTitle(let data):
            navBarView.titleText = data
        case .showDescTitle(let data):
            titleLabel.text = String(format: "Login or Sign in to add items\n to ***".localized(), data)
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(labelsView)
        self.view.addSubview(buttonsStack)
        
        labelsView.addSubview(titleLabel)
        labelsView.addSubview(descLabel)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc private func loginButtonTouchUp() {
        let viewController = LoginBuilder.make()
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @objc private func signupButtonTouchUp() {
        let viewController = CreateAccountBuilder.make()
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - UIScrollView delegate

extension NoAuthViewController: UIScrollViewDelegate {}

// MARK: - BTNavbarView delegate

extension NoAuthViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

