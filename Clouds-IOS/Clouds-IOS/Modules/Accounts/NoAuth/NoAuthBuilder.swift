//
//  NoAuthBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.01.21.
//

import Foundation

final class NoAuthBuilder {
    
    static func make(navTitle: String, descTitle: String) -> NoAuthViewController {
        let view = NoAuthViewController()
        
        let presenter = NoAuthPresenter(view: view, service: App.service,navbarTitle: navTitle, descTitle: descTitle)
        view.presenter = presenter
        
        return view
    }
}

