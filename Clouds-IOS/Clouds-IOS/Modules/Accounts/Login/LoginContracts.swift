//
//  LoginContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

// MARK: - Presenter

protocol LoginPresenterProtocol: class {
    func login(email: String, password: String)
    func facebookBy(_ token: String)
    func googleBy(_ token: String)
}

enum LoginPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case pushMain
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol LoginViewProtocol: class {
    func handleOutput(_ output: LoginPresenterOutput)
}
