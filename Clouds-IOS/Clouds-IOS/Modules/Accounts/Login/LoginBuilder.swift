//
//  LoginBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

final class LoginBuilder {
    
    static func make() -> LoginViewController {
        let view = LoginViewController()
        
        let presenter = LoginPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
