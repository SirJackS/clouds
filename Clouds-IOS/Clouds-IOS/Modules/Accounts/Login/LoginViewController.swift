//
//  LoginVC.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

final class LoginViewController: UIViewController, UIScrollViewDelegate, LoginViewProtocol {

    var presenter: LoginPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Welcome!".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 34)
        label.numberOfLines = 1
        label.anchor(.height(41))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.text = "Log in to your account to continue ".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var textViewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [emailAddressView, passwordView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 12
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var emailAddressView: CustomTextView = {
        let view = CustomTextView(title: "Email Address".localized())
        view.mainText.textContentType = .emailAddress
        return view
    }()
    
    private lazy var passwordView: CustomPasswordView = {
        let view = CustomPasswordView(title: "Password".localized())
        return view
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("login".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(loginButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var forgotPassButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Forgot Password?".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        button.anchor(.height(22))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(forgotButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var signUpButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        button.colorStrings(string1: "Don’t have an account? ".localized(), string2: "Sign up".localized())
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = UIColor.clear
        button.anchor(.height(26))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(signUpButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [orLabel,signUpWithLabel])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var orLabel: UILabel = {
        let label = UILabel()
        label.text = "Or".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        label.anchor(.height(24))
        return label
    }()
    
    private lazy var signUpWithLabel: UILabel = {
        let label = UILabel()
        label.text = "Sign in with".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 18)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var buttonsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [appleView,googleView,facebookView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 20
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var appleView: KitButtonView = {
        let view = KitButtonView(title: "Apple", icon: "apple-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(appleButtonTouchUp), for: .touchUpInside)
        return view
    }()
    
    private lazy var googleView: KitButtonView = {
        let view = KitButtonView(title: "Google", icon: "google-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(googleButtonTouchUp), for: .touchUpInside)
        return view
    }()
    
    private lazy var facebookView: KitButtonView = {
        let view = KitButtonView(title: "Facebook", icon: "facebook-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(facebookButtonTouchUp), for: .touchUpInside)
        return view
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        self.setupSignIns()
        
        //self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.fillSuperview())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        titleLabel.anchor(.top(40), .leading(16), .trailing(-16))
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16))
        
        textViewStack.anchor(
            .top(descLabel.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        loginButton.anchor(
            .top(textViewStack.bottomAnchor, constant: 64),
            .leading(16), .trailing(-16))
        
        forgotPassButton.anchor(
            .top(loginButton.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16))
        
        signUpButton.anchor(
            .top(forgotPassButton.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16))
        
        labelsStack.anchor(
            .top(signUpButton.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        buttonsStack.anchor(
            .top(labelsStack.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16), .bottom(-100))
        
    }

    func handleOutput(_ output: LoginPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .pushMain:
            let viewController = TabBarBuilder.make()
//            viewController.modalTransitionStyle = .
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .login:
                        self.loginButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(descLabel)
        contentView.addSubview(textViewStack)
        contentView.addSubview(loginButton)
        contentView.addSubview(forgotPassButton)
        contentView.addSubview(signUpButton)
        contentView.addSubview(labelsStack)
        contentView.addSubview(buttonsStack)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    func setupSignIns() {
//        if let token = AccessToken.current,
//                !token.isExpired {
//            print(token)
//                // User is logged in, do work such as go to next view controller.
//        }
        if AccessToken.current != nil {
            LoginManager().logOut()
            return
        }
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
    // MARK: - Action
    
    @objc func appleButtonTouchUp() {
//        let provider = ASAuthorizationAppleIDProvider()
//        let request = provider.createRequest()
//        request.requestedScopes = [.fullName, .email]
//
//        let controller = ASAuthorizationController(authorizationRequests: [request])
//        controller.delegate = self
//        controller.presentationContextProvider = self
//        controller.performRequests()
    }
    
    @objc func googleButtonTouchUp() {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @objc func facebookButtonTouchUp() {
        let loginManager = LoginManager()

        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if error == nil {
                if let accessToken = result?.token?.tokenString {
                    self.presenter.facebookBy(accessToken)
                }
            }
        }
    }
    
    @objc private func forgotButtonTouchUp() {
        let viewController = ForgotPasswordBuilder.make()
        //viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc private func signUpButtonTouchUp() {
        let viewController = CreateAccountBuilder.make()
        //viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc private func loginButtonTouchUp() {
        self.hideErrors()
        self.loginButton.isEnabled = false
        self.emailAddressView.mainText.resignFirstResponder()
        self.passwordView.mainText.resignFirstResponder()
        self.presenter.login(email: emailAddressView.mainText.text ?? "", password: passwordView.mainText.text ?? "")
        self.loginButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "email":
            self.emailAddressView.errorText = errorText
            self.emailAddressView.isError = true
        case "password":
            self.passwordView.errorText = errorText
            self.passwordView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.emailAddressView.isError = false
        self.passwordView.isError = false
    }
}

// MARK: - BTNavbarView delegate

extension LoginViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - GIDSignIn delegate

extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        // Check for sign in error
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                self.showMessage("The user has not signed in before or they have since signed out.".localized())
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        guard let user = user else {
            self.showMessage("Uh oh. The user cancelled the Google login.".localized())
            return
        }
        // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
//        let email = user.profile.email
        
        self.presenter.googleBy(idToken ?? "")

    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        GIDSignIn.sharedInstance().signOut()
    }
}

// MARK: - Facebook delegate

extension LoginViewController: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
       LoginManager().logOut()
    }
}
