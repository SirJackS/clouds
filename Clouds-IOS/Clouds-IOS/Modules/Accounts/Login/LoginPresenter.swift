//
//  LoginPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

final class LoginPresenter: LoginPresenterProtocol {
    
    private unowned let view: LoginViewProtocol
    private var service: ServiceProtocol
    
    init(view: LoginViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func login(email: String, password: String) {
        self.view.handleOutput(.showActivity(true))
        let request = LoginRequest(email: email ,password: password)
        service.login(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true
                UserDefaults.standard.removeObject(forKey: KEYWORDS)
                self.view.handleOutput(.pushMain)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.login))
                
            default: break
            }
        }
    }
    
    func facebookBy(_ token: String) {
        self.view.handleOutput(.showActivity(true))
        let request = SocialRequest(accessToken: token)
        service.facebookBy(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true
                self.view.handleOutput(.pushMain)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.facebookBy))
                
            default: break
            }
        }
    }
    
    func googleBy(_ token: String) {
        self.view.handleOutput(.showActivity(true))
        let request = SocialRequest(accessToken: token)
        service.googleBy(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true
                self.view.handleOutput(.pushMain)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.googleBy))
                
            default: break
            }
        }
    }
}
