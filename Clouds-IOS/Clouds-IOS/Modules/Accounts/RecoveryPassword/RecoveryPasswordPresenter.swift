//
//  RecoveryPasswordPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

final class RecoveryPasswordPresenter: RecoveryPasswordPresenterProtocol {
    
    private unowned let view: RecoveryPasswordViewProtocol
    private var service: ServiceProtocol
    
    init(view: RecoveryPasswordViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func recoveryPassBy(password: String, passwordConf: String) {
        self.view.handleOutput(.showActivity(true))
        let request = RecoveryPasswordRequest(password: password, passwordConfirmation: passwordConf, token: Defaults.instance.otpToken)
        service.recoveryPassBy(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print("response: ",response)
                UserDefaults.standard.removeObject(forKey: OTP_TOKEN_KEY)
                self.view.handleOutput(.pushLogin)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.recoveryPassBy))
            default: break
            }
        }
    }
}
