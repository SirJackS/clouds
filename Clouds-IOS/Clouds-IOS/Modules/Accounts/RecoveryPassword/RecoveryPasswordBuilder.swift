//
//  RecoveryPasswordBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

final class RecoveryPasswordBuilder {
    
    static func make() -> RecoveryPasswordViewController {
        let view = RecoveryPasswordViewController()
        
        let presenter = RecoveryPasswordPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
