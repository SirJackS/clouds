//
//  RecoveryPasswordContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

// MARK: - Presenter

protocol RecoveryPasswordPresenterProtocol: class {
    func recoveryPassBy(password: String, passwordConf: String)
}

enum RecoveryPasswordPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case pushLogin
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol RecoveryPasswordViewProtocol: class {
    func handleOutput(_ output: RecoveryPasswordPresenterOutput)
}
