//
//  RecoveryPasswordViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import UIKit

final class RecoveryPasswordViewController: UIViewController, RecoveryPasswordViewProtocol {

    var presenter: RecoveryPasswordPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Generate\nNew Password".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 34)
        label.numberOfLines = 2
        label.anchor(.height(82))
        return label
    }()
    
    private lazy var textFieldsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [newPassView, newPassConfView])
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 12
        return stackView
    }()
    
    private lazy var newPassView: CustomPasswordView = {
        let view = CustomPasswordView(title: "New Password".localized())
        view.mainText.isSecureTextEntry = true
        return view
    }()
    
    private lazy var newPassConfView: CustomPasswordView = {
        let view = CustomPasswordView(title: "New Password Confirmation".localized())
        view.mainText.isSecureTextEntry = true
        return view
    }()
    
    
    private lazy var doneButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("done".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(sendButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        //self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        titleLabel.anchor(.top(40), .leading(16), .trailing(-16))
        
        textFieldsStack.anchor(
            .top(titleLabel.bottomAnchor, constant: 54),
            .leading(16), .trailing(-16))
        
        doneButton.anchor(
            .top(textFieldsStack.bottomAnchor, constant: 40),
            .leading(16), .trailing(-16))
    }

    func handleOutput(_ output: RecoveryPasswordPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
//            emailAddressView.mainText.text = nil
        case .showActivity(let bool):
            self.showLoader(bool)
        case .pushLogin:
            self.newPassView.mainText.text = nil
            self.newPassConfView.mainText.text = nil
            let viewController = LoginBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .recoveryPassBy:
                        self.sendButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(titleLabel)
        self.view.addSubview(textFieldsStack)
        self.view.addSubview(doneButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc private func sendButtonTouchUp() {
        self.hideErrors()
        self.doneButton.isEnabled = false
        self.presenter.recoveryPassBy(
            password: newPassView.mainText.text ?? "",
            passwordConf: newPassConfView.mainText.text ?? "")
        self.doneButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "password":
            self.newPassView.errorText = errorText
            self.newPassView.isError = true
        case "password_confirmation":
            self.newPassConfView.errorText = errorText
            self.newPassConfView.isError = true
        case "token":
            self.showMessage(errorText)
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.newPassView.isError = false
        self.newPassConfView.isError = false
    }
    
}

// MARK: - UIScrollView delegate

extension RecoveryPasswordViewController: UIScrollViewDelegate {
    
}

// MARK: - BTNavbarView delegate

extension RecoveryPasswordViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        
        self.popBack(3)
    }
}

// MARK: - UITextField delegate

extension RecoveryPasswordViewController: CustomTextViewDelegate {
    func didBeginEditing(_ textView: CustomTextView) {
//        switch textView {
//        case dateOfBirthView:
//            //dateOfBirthView.mainText.becomeFirstResponder()
//        default: break
//        }
    }
}

