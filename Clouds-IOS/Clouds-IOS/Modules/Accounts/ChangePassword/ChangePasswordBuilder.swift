//
//  ChangePasswordBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.01.21.
//

import Foundation

final class ChangePasswordBuilder {
    
    static func make() -> ChangePasswordViewController {
        let view = ChangePasswordViewController()
        
        let presenter = ChangePasswordPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
