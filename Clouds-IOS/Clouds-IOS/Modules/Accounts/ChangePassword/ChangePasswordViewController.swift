//
//  ChangePasswordViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.01.21.
//

import UIKit

final class ChangePasswordViewController: UIViewController, ChangePasswordViewProtocol {

    var presenter: ChangePasswordPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Change Password".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var infoView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.anchor(.height(20))
        return view
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = "Min 8 characters".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.6)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        return label
    }()

    private lazy var cubeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        view.anchor(.size(width: 4, height: 4))
        return view
    }()
    
    private lazy var oldPasswordView: CustomPasswordView = {
        let view = CustomPasswordView(title: "Old Password".localized())
        return view
    }()
    
    private lazy var passwordView: CustomPasswordView = {
        let view = CustomPasswordView(title: "Current Password".localized())
        return view
    }()
    
    private lazy var passwordConfView: CustomPasswordView = {
        let view = CustomPasswordView(title: "New Password".localized())
        return view
    }()
    
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("save password".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        //self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        oldPasswordView.anchor(.top(16), .leading(16), .trailing(-16))
        
        passwordView.anchor(
            .top(oldPasswordView.bottomAnchor, constant: 12),
            .leading(16), .trailing(-16))
        
        passwordConfView.anchor(
            .top(passwordView.bottomAnchor, constant: 12),
            .leading(16), .trailing(-16))
        
        infoView.anchor(
            .top(passwordConfView.bottomAnchor),
            .leading(16), .trailing(-16))
        
        cubeView.anchor(.centerY(), .leading())
        
        infoLabel.anchor(
            .top(), .bottom(),
            .leading(cubeView.trailingAnchor, constant: 16), .trailing())
            
        saveButton.anchor(
            .top(passwordConfView.bottomAnchor, constant: 128),
            .leading(16), .trailing(-16))
    }

    func handleOutput(_ output: ChangePasswordPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .dismiss:
            self.popVC()
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .changePasswordBy:
                        self.saveButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(oldPasswordView)
        self.view.addSubview(passwordView)
        self.view.addSubview(passwordConfView)
        self.view.addSubview(infoView)
        self.view.addSubview(saveButton)
        
        infoView.addSubview(cubeView)
        infoView.addSubview(infoLabel)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc private func saveButtonTouchUp() {
        self.hideErrors()
        self.saveButton.isEnabled = false
        self.presenter.changePassBy(oldPasswordView.mainText.text ?? "", passwordView.mainText.text ?? "", passwordConfView.mainText.text ?? "")
        self.saveButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "old_password":
            self.oldPasswordView.errorText = errorText
            self.oldPasswordView.isError = true
        case "password":
            self.passwordView.errorText = errorText
            self.passwordView.isError = true
        case "password_confirmation":
            self.passwordConfView.errorText = errorText
            self.passwordConfView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.oldPasswordView.isError = false
        self.passwordView.isError = false
        self.passwordConfView.isError = false
    }
}

// MARK: - UIScrollView delegate

extension ChangePasswordViewController: UIScrollViewDelegate {}

// MARK: - BTNavbarView delegate

extension ChangePasswordViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField delegate

extension ChangePasswordViewController: CustomTextViewDelegate {
    func didBeginEditing(_ textView: CustomTextView) {
//        switch textView {
//        case dateOfBirthView:
//            //dateOfBirthView.mainText.becomeFirstResponder()
//        default: break
//        }
    }
}

