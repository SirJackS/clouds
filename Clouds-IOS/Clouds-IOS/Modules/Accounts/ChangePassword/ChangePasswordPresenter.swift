//
//  ChangePasswordPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.01.21.
//

import Foundation

final class ChangePasswordPresenter: ChangePasswordPresenterProtocol {
    
    private unowned let view: ChangePasswordViewProtocol
    private var service: ServiceProtocol
    
    init(view: ChangePasswordViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func changePassBy(_ old_password: String,_ new_password: String,_ password_conf: String) {
        self.view.handleOutput(.showActivity(true))
        let request = ChangePasswordRequest(oldPassword: old_password, password: new_password, passwordConfirmation: password_conf)
        service.changePasswordBy(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print("response: ",response)
                self.view.handleOutput(.dismiss)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.changePasswordBy))
            default: break
            }
        }
    }
    
}
