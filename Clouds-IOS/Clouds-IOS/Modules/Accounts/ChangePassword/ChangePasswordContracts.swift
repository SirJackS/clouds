//
//  ChangePasswordContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.01.21.
//

import Foundation

// MARK: - Presenter

protocol ChangePasswordPresenterProtocol: class {
    func changePassBy(_ old_password: String,_ new_password: String,_ password_conf: String)
}

enum ChangePasswordPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case dismiss
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol ChangePasswordViewProtocol: class {
    func handleOutput(_ output: ChangePasswordPresenterOutput)
}
