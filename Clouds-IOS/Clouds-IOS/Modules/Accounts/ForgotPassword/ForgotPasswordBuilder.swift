//
//  ForgotPasswordBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class ForgotPasswordBuilder {
    
    static func make() -> ForgotPasswordViewController {
        let view = ForgotPasswordViewController()
        
        let presenter = ForgotPasswordPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
