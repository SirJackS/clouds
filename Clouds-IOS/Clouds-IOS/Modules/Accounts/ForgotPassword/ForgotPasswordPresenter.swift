//
//  ForgotPasswordPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class ForgotPasswordPresenter: ForgotPasswordPresenterProtocol {
    
    private unowned let view: ForgotPasswordViewProtocol
    private var service: ServiceProtocol
    
    init(view: ForgotPasswordViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func resetPassBy(email: String) {
        self.view.handleOutput(.showActivity(true))
        service.resetPassBy(email: email) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                Defaults.instance.otpToken = data.token
                self.view.handleOutput(.pushOtp)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.resetPassBy))
            default: break
            }
        }
    }
    
}
