//
//  ForgotPasswordContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

// MARK: - Presenter

protocol ForgotPasswordPresenterProtocol: class {
    func resetPassBy(email: String)
}

enum ForgotPasswordPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case pushOtp
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol ForgotPasswordViewProtocol: class {
    func handleOutput(_ output: ForgotPasswordPresenterOutput)
}
