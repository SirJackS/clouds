//
//  ForgotPasswordViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit

final class ForgotPasswordViewController: UIViewController, ForgotPasswordViewProtocol {

    var presenter: ForgotPasswordPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Forgot Password?".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 34)
        label.numberOfLines = 1
        label.anchor(.height(41))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.text = "Pleace enter your e-mail address to continue".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var emailAddressView: CustomTextView = {
        let view = CustomTextView(title: "Email Address".localized())
        view.mainText.textContentType = .emailAddress
        return view
    }()
    
    
    private lazy var sendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("sent".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(sendButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        //self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        titleLabel.anchor(.top(40), .leading(16), .trailing(-16))
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16))
        
        emailAddressView.anchor(
            .top(descLabel.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        sendButton.anchor(
            .top(emailAddressView.bottomAnchor, constant: 128),
            .leading(16), .trailing(-16))
        
    }

    func handleOutput(_ output: ForgotPasswordPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .pushOtp:
            let viewController = OTPBuilder.make(email: emailAddressView.mainText.text ?? "")
            emailAddressView.mainText.text = nil
            self.navigationController?.pushViewController(viewController, animated: true)
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .resetPassBy:
                        self.sendButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(titleLabel)
        self.view.addSubview(descLabel)
        self.view.addSubview(emailAddressView)
        self.view.addSubview(sendButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc private func sendButtonTouchUp() {
        self.hideErrors()
        self.sendButton.isEnabled = false
        self.presenter.resetPassBy(email: emailAddressView.mainText.text ?? "")
        self.sendButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "email":
            self.emailAddressView.errorText = errorText
            self.emailAddressView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.emailAddressView.isError = false
    }
    
}

// MARK: - UIScrollView delegate

extension ForgotPasswordViewController: UIScrollViewDelegate {
    
}

// MARK: - BTNavbarView delegate

extension ForgotPasswordViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField delegate

extension ForgotPasswordViewController: CustomTextViewDelegate {
    func didBeginEditing(_ textView: CustomTextView) {
//        switch textView {
//        case dateOfBirthView:
//            //dateOfBirthView.mainText.becomeFirstResponder()
//        default: break
//        }
    }
}

