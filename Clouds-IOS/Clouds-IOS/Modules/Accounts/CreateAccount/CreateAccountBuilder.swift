//
//  CreateAccountBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class CreateAccountBuilder {
    
    static func make() -> CreateAccountViewController {
        let view = CreateAccountViewController()
        
        let presenter = CreateAccountPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
