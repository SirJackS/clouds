//
//  CreateAccountContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

// MARK: - Presenter

protocol CreateAccountPresenterProtocol: class {
    func register(firstname: String, lastname: String, birthdate: String, interestedIn: String, email: String, password: String, passwordConfirmation: String)
    func facebookBy(_ token: String)
    func googleBy(_ token: String)
}

enum CreateAccountPresenterOutput {
    case showActivity(Bool)
    case showMessage(String)
    case handleError([String: [String]])
    case pushMain
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol CreateAccountViewProtocol: class {
    func handleOutput(_ output: CreateAccountPresenterOutput)
}
