//
//  CreateAccountPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class CreateAccountPresenter: CreateAccountPresenterProtocol {
    
    private unowned let view: CreateAccountViewProtocol
    private var service: ServiceProtocol
    
    init(view: CreateAccountViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func register(firstname: String, lastname: String, birthdate: String, interestedIn: String, email: String, password: String, passwordConfirmation: String) {
        self.view.handleOutput(.showActivity(true))
        var date = ""
        if birthdate != "MM / DD / YYYY" {
            date = birthdate.getFormattedDateForDayCell(fromFormat: "MM / dd / yyyy", toFormat: "yyyy-MM-dd")
        }
        
        let request = RegisterRequest(firstname: firstname, lastname: lastname, birthdate: date, interestedIn: interestedIn, email: email, password: password, passwordConfirmation: passwordConfirmation)
        service.register(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true                
                self.view.handleOutput(.pushMain)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.register))
                
            default: break
            }
        }
    }
    
    func facebookBy(_ token: String) {
        self.view.handleOutput(.showActivity(true))
        let request = SocialRequest(accessToken: token)
        service.facebookBy(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true
                self.view.handleOutput(.pushMain)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.facebookBy))
                
            default: break
            }
        }
    }
    
    func googleBy(_ token: String) {
        self.view.handleOutput(.showActivity(true))
        let request = SocialRequest(accessToken: token)
        service.googleBy(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
//                self.view.handleOutput(.showMessage(response.message ?? ""))
                Defaults.instance.authToken = data.accessToken
                Defaults.instance.isLoggedIn = true
                self.view.handleOutput(.pushMain)
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.googleBy))
                
            default: break
            }
        }
    }
}
