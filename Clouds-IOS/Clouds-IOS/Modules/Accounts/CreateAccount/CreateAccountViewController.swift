//
//  CreateAccountViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit
//import AuthenticationServices

final class CreateAccountViewController: UIViewController, CreateAccountViewProtocol {

    var presenter: CreateAccountPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create an account".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 34)
        label.numberOfLines = 1
        label.anchor(.height(41))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.text = "Create an account continue shopping".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var textViewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstNameView,surNameView,emailAddressView, passwordView, passwordConfView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 12
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var firstNameView: CustomTextView = {
        let view = CustomTextView(title: "Name".localized())
        return view
    }()
    
    private lazy var surNameView: CustomTextView = {
        let view = CustomTextView(title: "Surname".localized())
        return view
    }()
    
    private lazy var emailAddressView: CustomTextView = {
        let view = CustomTextView(title: "Email Address".localized())
        view.mainText.textContentType = .emailAddress
        return view
    }()
    
    private lazy var passwordView: CustomTextView = {
        let view = CustomTextView(title: "Password".localized())
        view.mainText.isSecureTextEntry = true
        return view
    }()
    
    private lazy var passwordConfView: CustomTextView = {
        let view = CustomTextView(title: "Password Confirmation".localized())
        view.mainText.isSecureTextEntry = true
        return view
    }()
    
    private lazy var dateOfBirthView: CustomDateView = {
        let view = CustomDateView(title: "Date of birth".localized())
        view.delegate = self
        view.mainText.inputAccessoryView = toolbar
        view.mainText.inputView = datePicker
        return view
    }()
    
    private lazy var dateErrorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "EE0005")
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.isHidden = true
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var datePicker: CustomDatePickerView = {
        let picker = CustomDatePickerView()
        picker.datePickerMode = .date
//        let loc = Locale(identifier: LanguageManager.shared.getCurrentLanguage()?.rawValue ?? "az")
//        picker.locale = loc
        picker.minimumDate = Date().getDateOf(year: 150)
        picker.maximumDate = Date()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        return picker
    }()
    
    private lazy var toolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barTintColor = .white
        toolbar.backgroundColor = .white
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(doneDatePicker))
        doneButton.tintColor = .black
        let clearButton = UIBarButtonItem(title: "clear".localized().capitalized, style: .plain, target: self, action: #selector(clearDatePicker))
        clearButton.tintColor = .black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel".localized().capitalized, style: .plain, target: self, action: #selector(cancelDatePicker));
        cancelButton.tintColor = .black
        toolbar.setItems([doneButton,clearButton,spaceButton,cancelButton], animated: false)
        return toolbar
    }()
    
    private lazy var termAndCondLbl: UILabel = {
        let label = UILabel()
        label.text = "Pleace read and agree to our".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.6)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var termAndCondButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont(weight: .medium, size: 16)
        button.setTitle("Terms and Conditions".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = UIColor.clear
        button.anchor(.height(26))
        //button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
//        button.addTarget(self, action: #selector(pieceButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var designTextViewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [orLabel,signUpWithLabel])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var orLabel: UILabel = {
        let label = UILabel()
        label.text = "Or".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        label.anchor(.height(24))
        return label
    }()
    
    private lazy var signUpWithLabel: UILabel = {
        let label = UILabel()
        label.text = "Sign up with".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 18)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var createAccButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("create an account".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(createButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var logInButton: UIButton = {
        let button = UIButton(type: .system)
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        button.colorStrings(string1: "Already have an account? ".localized(), string2: "Log In".localized())
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = UIColor.clear
        button.anchor(.height(26))
        button.contentHorizontalAlignment = .center
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
//        button.addTarget(self, action: #selector(pieceButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var buttonsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [appleView,googleView,facebookView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.spacing = 20
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var appleView: KitButtonView = {
        let view = KitButtonView(title: "Apple", icon: "apple-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(appleButtonTouchUp), for: .touchUpInside)
        return view
    }()
    
    private lazy var googleView: KitButtonView = {
        let view = KitButtonView(title: "Google", icon: "google-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(googleButtonTouchUp), for: .touchUpInside)
        return view
    }()
    
    private lazy var facebookView: KitButtonView = {
        let view = KitButtonView(title: "Facebook", icon: "facebook-icon")
        view.anchor(.height(46))
        view.backgroundColor = .clear
        view.kitButton.addTarget(self, action: #selector(facebookButtonTouchUp), for: .touchUpInside)
        return view
    }()

    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.setupSignIns()
        
        //self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.fillSuperview())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        titleLabel.anchor(.top(40), .leading(16), .trailing(-16))
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16))
        
        textViewStack.anchor(
            .top(descLabel.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        dateOfBirthView.anchor(
            .top(textViewStack.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16))
        
        dateErrorLabel.anchor(
            .top(dateOfBirthView.bottomAnchor, constant: 4),
            .leading(20), .trailing(-20))
        
        termAndCondLbl.anchor(
            .top(dateOfBirthView.bottomAnchor, constant: 40),
            .leading(16), .trailing(-16))
        
        termAndCondButton.anchor(
            .top(termAndCondLbl.bottomAnchor, constant: 8),
            .leading(16), .trailing(-16))
        
        createAccButton.anchor(
            .top(termAndCondButton.bottomAnchor, constant: 32),
            .leading(16), .trailing(-16))
        
        logInButton.anchor(
            .top(createAccButton.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16))
        
        designTextViewStack.anchor(
            .top(logInButton.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16))
        
        buttonsStack.anchor(
            .top(designTextViewStack.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16), .bottom(-100))
        
    }

    func handleOutput(_ output: CreateAccountPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
//            firstNameView.mainText.text = nil
//            surNameView.mainText.text = nil
//            emailAddressView.mainText.text = nil
//            passwordView.mainText.text = nil
//            passwordConfView.mainText.text = nil
//            dateOfBirthView.mainText.text = "MM / DD / YYYY"
        case .showActivity(let bool):
            self.showLoader(bool)
        case .pushMain:
            let viewController = TabBarBuilder.make()
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .register:
                        self.createButtonTouchUp()
                    case .facebookBy:
                        self.facebookButtonTouchUp()
                    case .googleBy:
                        self.googleButtonTouchUp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(descLabel)
        contentView.addSubview(textViewStack)
        contentView.addSubview(dateOfBirthView)
        contentView.addSubview(dateErrorLabel)
        contentView.addSubview(termAndCondLbl)
        contentView.addSubview(termAndCondButton)
        contentView.addSubview(createAccButton)
        contentView.addSubview(logInButton)
        contentView.addSubview(designTextViewStack)
        contentView.addSubview(buttonsStack)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    func setupSignIns() {
//        if let token = AccessToken.current,
//                !token.isExpired {
//            print(token)
//                // User is logged in, do work such as go to next view controller.
//        }
        if AccessToken.current != nil {
            LoginManager().logOut()
            return
        }
        
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
    @objc func doneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM / dd / yyyy"
        //toDate = toDatePicker.date
//        let loc = Locale(identifier: LanguageManager.shared.getCurrentLanguage()?.rawValue ?? "az")
//        formatter.locale = loc
        dateOfBirthView.mainText.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func clearDatePicker(){
        dateOfBirthView.mainText.text = "MM / DD / YYYY"
//        toDate = Date()
        self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @objc func appleButtonTouchUp() {
//        let provider = ASAuthorizationAppleIDProvider()
//        let request = provider.createRequest()
//        request.requestedScopes = [.fullName, .email]
//
//        let controller = ASAuthorizationController(authorizationRequests: [request])
//        controller.delegate = self
//        controller.presentationContextProvider = self
//        controller.performRequests()
    }
    
    @objc func googleButtonTouchUp() {
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @objc func facebookButtonTouchUp() {
        let loginManager = LoginManager()
        
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if error == nil {
                if let accessToken = result?.token?.tokenString {
                    self.presenter.facebookBy(accessToken)
                }
            }
        }
        

    }
    
    @objc func createButtonTouchUp() {
        self.hideErrors()
        self.createAccButton.isEnabled = false
        self.presenter.register(
            firstname: firstNameView.mainText.text ?? "",
            lastname: surNameView.mainText.text ?? "",
            birthdate: dateOfBirthView.mainText.text ?? "MM / DD / YYYY",
            interestedIn: "1,2",
            email: emailAddressView.mainText.text ?? "",
            password: passwordView.mainText.text ?? "",
            passwordConfirmation: passwordConfView.mainText.text ?? "")
        self.createAccButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "firstname":
            self.firstNameView.errorText = errorText
            self.firstNameView.isError = true
        case "lastname":
            self.surNameView.errorText = errorText
            self.surNameView.isError = true
        case "birthdate":
            self.dateErrorLabel.text = errorText
            self.dateErrorLabel.isHidden = false
//        case "interested_in":
//            self.firstNameView.errorText = errorText
//            self.firstNameView.isError = true
        case "email":
            self.emailAddressView.errorText = errorText
            self.emailAddressView.isError = true
        case "password":
            self.passwordView.errorText = errorText
            self.passwordView.isError = true
        case "password_confirmation":
            self.passwordConfView.errorText = errorText
            self.passwordConfView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.firstNameView.isError = false
        self.surNameView.isError = false
        self.dateErrorLabel.isHidden = true
        self.emailAddressView.isError = false
        self.passwordView.isError = false
        self.passwordConfView.isError = false
    }
    
}

// MARK: - UIScrollView delegate

extension CreateAccountViewController: UIScrollViewDelegate {}

// MARK: - BTNavbarView delegate

extension CreateAccountViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField delegate

extension CreateAccountViewController: CustomDateViewDelegate {
    func downButtonClick() {
        self.dateOfBirthView.mainText.becomeFirstResponder()
    }
}

// MARK: - GIDSignIn delegate

extension CreateAccountViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        // Check for sign in error
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                self.showMessage("The user has not signed in before or they have since signed out.".localized())
            } else {
                print("\(error.localizedDescription)")
            }
            return
        }
        guard let user = user else {
            self.showMessage("Uh oh. The user cancelled the Google login.".localized())
            return
        }
        // For client-side use only!
        let idToken = user.authentication.idToken // Safe to send to the server
//        let email = user.profile.email
        
        self.presenter.googleBy(idToken ?? "")

    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        GIDSignIn.sharedInstance().signOut()
    }
}

// MARK: - Facebook delegate

extension CreateAccountViewController: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
       LoginManager().logOut()
    }
}

//extension CreateAccountViewController: ASAuthorizationControllerDelegate {
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
//        print("failed")
//    }
//
//    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
//        switch authorization.credential {
//        case let credentials as ASAuthorizationAppleIDCredential:
//            let email =  credentials.email
//            print("email", email)
//            break
//        default:
//            break
//        }
//    }
//}
//
//extension CreateAccountViewController: ASAuthorizationControllerPresentationContextProviding {
//    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
//        return view.window!
//    }
//}
