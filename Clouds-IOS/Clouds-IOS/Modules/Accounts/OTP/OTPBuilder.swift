//
//  OTPBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class OTPBuilder {
    
    static func make(email: String) -> OTPViewController {
        let view = OTPViewController()
        
        let presenter = OTPPresenter(view: view, service: App.service, email: email)
        view.presenter = presenter
        
        return view
    }
}
