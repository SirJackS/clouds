//
//  OTPViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit

final class OTPViewController: UIViewController, OTPViewProtocol {

    var presenter: OTPPresenterProtocol!
    
    private let maxLength = 4
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "")
        view.delegate = self
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 3
        label.anchor(.heightGreater(48))
        return label
    }()
    
    private lazy var fieldsStack: UIStackView = {
        var views: [OTPFieldView] = []
        for _ in 1...4 {
            let item = OTPFieldView()
            item.anchor(.size(width: 56, height: 60))
            views.append(item)
        }
        
        let stackView = UIStackView(arrangedSubviews: views)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        //stackView.spacing = 25
        return stackView
    }()
    
    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.delegate = self
        textField.textColor = .clear
        textField.tintColor = .clear
        textField.alpha = 0.1
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return textField
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.text = "The code you entered does not match. Try again.".localized()
        label.font = UIFont(ofSize: 12)
        label.textColor = .errorRed
        label.numberOfLines = 0
        label.isHidden = true
        return label
    }()
    
    private lazy var verificationButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("verification".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(verificationButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var didNotReceiveLabel: UILabel = {
        let label = UILabel()
        label.text = "Didn’t recieve the verification code?".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 16)
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var resendButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Resend VERIFICATION".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont(weight: .medium, size: 16)
        button.anchor(.height(26))
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(resendButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    private var errorText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        titleLabel.anchor(.top(40), .leading(16), .trailing(-16))
        
        fieldsStack.anchor(
            .top(titleLabel.bottomAnchor, constant: 54),
            .leading(40), .trailing(-40))
        
        textField.anchor(
            .top(fieldsStack.topAnchor),
            .leading(fieldsStack.leadingAnchor),
            .trailing(fieldsStack.trailingAnchor),
            .bottom(fieldsStack.bottomAnchor))
        
        errorLabel.anchor(
            .top(fieldsStack.bottomAnchor, constant: 20),
            .leading(40), .trailing(-40))
        
        verificationButton.anchor(
            .top(fieldsStack.bottomAnchor, constant: 142),
            .leading(16), .trailing(-16))
        
        didNotReceiveLabel.anchor(
            .top(verificationButton.bottomAnchor, constant: 24),
            .leading(16), .trailing(-16))
        
        resendButton.anchor(
            .top(didNotReceiveLabel.bottomAnchor, constant: 16),
            .leading(16), .trailing(-16))
        
    }

    func handleOutput(_ output: OTPPresenterOutput) {
        switch output {
        case .setTitle(let email):
            self.titleLabel.text = String(format: "We have just sent a verification\ncode to ***".localized(), email.trim())
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .push:
            self.textField.text = ""
            self.changeText()
            let viewController = RecoveryPasswordBuilder.make()
            self.navigationController?.pushViewController(viewController, animated: true)
        case .handleError(let data):
            self.errorLabel.isHidden = false
            errorText = ""
            data.forEach({ (key, value) in
                errorText += value.first ?? ""
            })
            self.errorLabel.text = errorText
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .otpVerifyBy:
                        self.verify()
                    case .resendOtpBy:
                        self.presenter.resendOtp()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(titleLabel)
        
        self.view.addSubview(fieldsStack)
        self.view.addSubview(textField)
        self.view.addSubview(errorLabel)
        
        self.view.addSubview(verificationButton)
        self.view.addSubview(didNotReceiveLabel)
        self.view.addSubview(resendButton)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    private func changeText() {
        let text = textField.text
        let textCount = textField.text?.count ?? 0
        let viewItems = fieldsStack.arrangedSubviews as! [OTPFieldView]
        
        let maxIndex = self.maxLength-1
        let currentIndex = textCount-1
        
        for index in 0...maxIndex {
            if currentIndex <= index {
                if currentIndex == index {
                    viewItems[index].text = index > 0 ? text!.substring(fromIndex: index) : text
                }
                else {
                    viewItems[index].text = ""
                }
            }
        }
    }
    
    private func verify() {
        guard let code = self.textField.text else { return }
        errorLabel.isHidden = true
        self.presenter.otpVerify(otp: code)
    }
    
    // MARK: - Action
    
    @objc func textFieldDidChange() {
        self.changeText()
        
        if self.textField.text!.count == self.maxLength {
            self.textField.resignFirstResponder()
//            self.verify()
        }
    }
    
    @objc private func resendButtonTouchUp() {
        self.presenter.resendOtp()
    }
    
    @objc private func verificationButtonTouchUp() {
        self.verify()
    }
}

// MARK: - UIScrollView delegate

extension OTPViewController: UIScrollViewDelegate {}

// MARK: - BTNavbarView delegate

extension OTPViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField Delegate

extension OTPViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        return newString.length <= self.maxLength
    }
}
