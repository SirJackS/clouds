//
//  OTPFieldView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit

final class OTPFieldView: UIView {

    var text: String! {
        didSet {
//            self.dotLabel.isHidden = !text.isEmpty
            self.textLabel.text = text
            self.lineView.backgroundColor = text.isEmpty ? UIColor.black.withAlphaComponent(0.6) : .secondaryColor
        }
    }
    
    var isError: Bool = false {
        didSet {
            textLabel.textColor = isError ? .errorRed : UIColor.black.withAlphaComponent(0.6)
            lineView.backgroundColor = isError ? .errorRed : UIColor.black.withAlphaComponent(0.6)
        }
    }
    
    
//    private lazy var dotLabel: UILabel = {
//        let label = UILabel()
//        label.text = "•"
//        label.textColor = UIColor(hexString: "242134").withAlphaComponent(0.5)
//        label.font = UIFont(ofSize: 15)
//        label.textAlignment = .center
//        //label.backgroundColor = .red
//        return label
//    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.6)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        return view
    }()
    
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    override func updateConstraints() {
//        dotLabel.anchor(.fillSuperview())
        
        textLabel.anchor(
            .top(16) ,.leading(12), .trailing(-12),
            .bottom(lineView.topAnchor, constant: -6))
        
        lineView.anchor(.leading(16), .trailing(-16), .bottom(-16),.height(1))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        return .init(width: 1, height: 1)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
//        self.addSubview(dotLabel)
        self.addSubview(textLabel)
        self.addSubview(lineView)
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        self.setupBorder(width: 1, color: UIColor(hexString: "EFEFEF"))
    }
}
