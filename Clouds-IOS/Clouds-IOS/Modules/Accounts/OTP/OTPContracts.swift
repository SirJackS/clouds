//
//  OTPContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

// MARK: - Presenter

protocol OTPPresenterProtocol: class {
    func otpVerify(otp: String)
    func resendOtp()
    func load()
}

enum OTPPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case showNetworkError(NetworkErrorType)
    case setTitle(String)
    case push
}

// MARK: - View

protocol OTPViewProtocol: class {
    func handleOutput(_ output: OTPPresenterOutput)
}
