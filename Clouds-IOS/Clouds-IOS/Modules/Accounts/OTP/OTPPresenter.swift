//
//  OTPPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import Foundation

final class OTPPresenter: OTPPresenterProtocol {
    
    private unowned let view: OTPViewProtocol
    private var service: ServiceProtocol
    private var email: String
    
    init(view: OTPViewProtocol,
         service: ServiceProtocol,
         email: String) {
        self.view = view
        self.service = service
        self.email = email
    }
    
    func load() {
        self.view.handleOutput(.setTitle(self.email))
    }
    
    func otpVerify(otp: String) {
        self.view.handleOutput(.showActivity(true))
        let request = OTPRequest(otpCode: otp, token: Defaults.instance.otpToken)
        service.otpVerifyBy(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.push)

            case .wrong(let response):
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.otpVerifyBy))

            default: break
            }
        }
    }
    
    func resendOtp() {
        self.view.handleOutput(.showActivity(true))
        let request = ResendOTPRequest(token: Defaults.instance.otpToken)
        service.resendOtpBy(request: request) { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                Defaults.instance.otpToken = data.token

            case .wrong(let response):
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.resendOtpBy))

            default: break
            }
        }
    }
}
