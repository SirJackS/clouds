//
//  OrderMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class OrderMainPresenter: OrderMainPresenterProtocol {
    
    private unowned let view: OrderMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: OrderMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getOrders()
    }
    
    private func getOrders() {
        self.view.handleOutput(.showActivity(true))
        service.getCustomerOrders { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCustomerOrders))

            default: break
            }

        }
    }
}
