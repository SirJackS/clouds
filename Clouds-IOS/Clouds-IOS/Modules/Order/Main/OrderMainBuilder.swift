//
//  OrderMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class OrderMainBuilder {
    
    static func make() -> OrderMainViewController {
        let view = OrderMainViewController()
        
        let presenter = OrderMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
