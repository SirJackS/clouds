//
//  OrderListCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit
import SDWebImage

protocol OrderListCellDelegate: class {
    func forwardButtonClick()
}


final class OrderListCell: UITableViewCell {

    static let ID: String = "OrderListCell"
    
    var data: OrderList! {
        didSet {
            self.configure()
        }
    }
    
//    private lazy var viewsStack: UIStackView = {
//        let stackView = UIStackView(arrangedSubviews: [topView, trackView])
//        stackView.axis = .vertical
////        stackView.alignment = .fill
////        stackView.distribution = .fillEqually
//        stackView.spacing = 16
//        stackView.clipsToBounds = true
//        return stackView
//    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
//    private lazy var trackView: TrackingProgressView = {
//        let view = TrackingProgressView()
//        view.clipsToBounds = true
//        view.backgroundColor = .clear
//        return view
//    }()
    
    private var photoHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (116 / 375) * width
    }
    
    private var photoWidth: CGFloat {
        let width = UIScreen.main.bounds.width
        return (112 / 375) * width
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: photoWidth, height: photoHeight))
        return imageView
    }()
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [titleLabel, noLabel, dateLabel, pieceLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
//        stackView.distribution = .fillEqually
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.textAlignment = .left
        label.anchor(.height(26))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var noLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var pieceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    private lazy var forwardView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "right-arrow-icon")
        imageView.backgroundColor = .clear
        imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.5 : 1.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
        self.dateLabel.text = ""
        self.noLabel.text = ""
        self.pieceLabel.text = ""
//        self.trackView.progress = "Empty"
//        self.trackView.isHidden = false

    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
//        viewsStack.anchor(.top(24), .leading(16), .trailing(-16), .bottom(-16))
        
        topView.anchor(.top(16), .leading(16), .trailing(-16), .bottom(-16))
        
        photoView.anchor(.top(), .leading(), .bottom())
        
        forwardView.anchor(.top(12), .trailing())
        
        labelsStack.anchor(
            .top(4), .leading(photoView.trailingAnchor, constant: 8),
            .bottom(-8), .trailing(forwardView.leadingAnchor, constant: -1))
        
//        trackView.anchor(.height(42))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
//        self.contentView.addSubview(viewsStack)
        self.contentView.addSubview(topView)
        self.contentView.addSubview(lineView)
        self.addSubview(lineView)
        
        topView.addSubview(photoView)
        topView.addSubview(labelsStack)
        topView.addSubview(forwardView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
        self.titleLabel.text = data.title
        self.dateLabel.text = String(format: "Shipped Date: ***".localized(), data.date ?? "")
        self.noLabel.text = String(format: "Order No: *".localized(), data.orderNo)
        self.pieceLabel.text = String(format: "* pieces".localized(), data.pieces)
        
//        if data.tracking == "Delivered" {
//            trackView.isHidden = true
//            self.layoutIfNeeded()
//        } else {
//            self.trackView.progress = data.tracking
//        }
    }
}

