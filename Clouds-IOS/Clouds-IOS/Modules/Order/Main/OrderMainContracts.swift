//
//  OrderMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

// MARK: - Presenter

protocol OrderMainPresenterProtocol: class {
    func load()
}

enum OrderMainPresenterOutput {
    case showMessage(String)
    case showList([OrderList])
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol OrderMainViewProtocol: class {
    func handleOutput(_ output: OrderMainPresenterOutput)
}
