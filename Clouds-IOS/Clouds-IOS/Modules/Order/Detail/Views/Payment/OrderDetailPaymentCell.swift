//
//  OrderDetailPaymentCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import UIKit

final class OrderDetailPaymentCell: UITableViewCell {

    static let ID: String = "OrderDetailPaymentCell"

    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(
            .top(4), .leading(16), .trailing(-16),
            .bottom(-4))
        
        titleLabel.anchor(.leading(16), .trailing(-16), .centerY())
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data.uppercased()
    }
}
