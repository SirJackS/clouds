//
//  OrderDetailPaymentTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit

final class OrderDetailPaymentTCell: UITableViewCell {

    static let ID: String = "OrderDetailPaymentTCell"

    var data: [String] = [] {
        didSet {
            self.configure()
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Payment details".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(OrderDetailPaymentCell.self, forCellReuseIdentifier: OrderDetailPaymentCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 52
        tableView.estimatedRowHeight = 52
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.anchor(.top(), .leading(16), .trailing(-16))
        
        tableView.anchor(
            .top(titleLabel.bottomAnchor),
            .leading(), .trailing(), .bottom(-20),
            .height(52*CGFloat(data.count)))
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(tableView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        tableView.reloadData()
        self.updateConstraints()
    }
    
    // MARK: - Action
    
//    @objc private func addBagButtonTouchUp() {
//        self.delegate?.addBagButtonClick()
//    }
//
//    @objc private func infoButtonTouchUp() {
//        self.delegate?.infoButtonClick()
//    }
}

extension OrderDetailPaymentTCell: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailPaymentCell.ID, for: indexPath) as? OrderDetailPaymentCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        return cell
    }
}
