//
//  OrderDetailMainTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit
import SDWebImage

protocol OrderDetailMainTCellDelegate: class {
    func selectedId(id: Int)
}


final class OrderDetailMainTCell: UITableViewCell {

    static let ID: String = "OrderDetailMainTCell"
    
    weak var delegate: OrderDetailMainTCellDelegate?
    
    var data: [OrderDetailItem] = [] {
        didSet {
            self.configure()
        }
    }
    
    var orderNo: String? {
        didSet {
            self.idLabel.text = orderNo
        }
    }
    
    var date: String? {
        didSet {
            self.dateLabel.text = date
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var idLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .right
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(OrderDetailProductCell.self, forCellReuseIdentifier: OrderDetailProductCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
//        let cellHeight = UIScreen.main.bounds.width * (116/375) + 16
        tableView.rowHeight = UITableView.automaticDimension
//        tableView.estimatedRowHeight = cellHeight
        tableView.isScrollEnabled = false
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(24), .leading(16), .trailing(-16), .bottom())
        
        idLabel.anchor(
            .top(), .leading(),
            .trailing(dateLabel.leadingAnchor, constant: -8))
        
        dateLabel.anchor(.top(),.trailing(), .width(100))
        
        
        let cellHeight = UIScreen.main.bounds.width * (116/375) + 16
        
        tableView.anchor(
            .top(idLabel.bottomAnchor, constant: 8),
            .leading(), .trailing(), .bottom(-16),
            .height(CGFloat(data.count) * cellHeight))
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(idLabel)
        bodyView.addSubview(dateLabel)
        bodyView.addSubview(tableView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
//        let cellHeight = UIScreen.main.bounds.width * (116/375) + 16
//        tableView.anchor(.height(cellHeight*CGFloat(data.products.count)))
//
        self.tableView.reloadData()
        
        self.updateConstraints()
        
    }
    
    // MARK: - Action
    
//    @objc private func addBagButtonTouchUp() {
//        self.delegate?.addBagButtonClick()
//    }
//
//    @objc private func infoButtonTouchUp() {
//        self.delegate?.infoButtonClick()
//    }
}

extension OrderDetailMainTCell: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailProductCell.ID, for: indexPath) as? OrderDetailProductCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedId(id: data[indexPath.row].id)
    }
}
