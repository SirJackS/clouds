//
//  OrderDetailProductCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import UIKit
import SDWebImage

final class OrderDetailProductCell: UITableViewCell {

    static let ID: String = "OrderDetailProductCell"
    
    var data: OrderDetailItem! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        let height = UIScreen.main.bounds.width * (116/375)
        let width = UIScreen.main.bounds.width * (114/375)
        imageView.anchor(.size(width: width, height: height))
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.textAlignment = .left
        label.anchor(.height(26))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .medium, size: 16)
        label.textAlignment = .left
        label.anchor(.height(26))
        label.numberOfLines = 1
        return label
    }()
    
    
    private lazy var titleStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [colorTitleLabel, sizeTitleLabel, qtyTitleLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
//        stackView.distribution = .fillEqually
//        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var descStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [colorDescLabel, sizeDescLabel, qtyDescLabel])
        stackView.axis = .vertical
        stackView.alignment = .leading
//        stackView.distribution = .fillEqually
//        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var colorTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Color:"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var colorDescLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var sizeTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Size:".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var sizeDescLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var qtyTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Qty:".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var qtyDescLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.anchor(.height(22))
        label.numberOfLines = 1
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.alpha = highlighted ? 0.5 : 1.0
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
        self.priceLabel.text = ""
        self.qtyDescLabel.text = ""
        self.colorDescLabel.text = ""
        self.sizeDescLabel.text = ""

    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(top: 8, bottom: -8))
        
        photoView.anchor(.top(), .leading(), .bottom())
        
        titleLabel.anchor(.top(), .trailing(),
            .leading(photoView.trailingAnchor, constant: 8))
        
        priceLabel.anchor(.top(titleLabel.bottomAnchor), .trailing(),
            .leading(photoView.trailingAnchor, constant: 8))
        
        titleStack.anchor(
            .topGreater(priceLabel.bottomAnchor), //.width(50),
            .leading(photoView.trailingAnchor, constant: 8), .bottom())
        
        descStack.anchor(
            .heightTo(titleStack),.trailingLess(), .centerY(titleStack.centerYAnchor),
            .leading(titleStack.trailingAnchor, constant: 8))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(photoView)
        
        bodyView.addSubview(photoView)
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(priceLabel)
        bodyView.addSubview(titleStack)
        bodyView.addSubview(descStack)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }

//        self.photoView.image = UIImage(named: data.image)
        self.titleLabel.text = data.title
        self.priceLabel.text = "\(data.price) AZN"
        self.colorDescLabel.text = data.color
        self.sizeDescLabel.text = data.size
        self.qtyDescLabel.text = "\(data.qty)"
    }
}
