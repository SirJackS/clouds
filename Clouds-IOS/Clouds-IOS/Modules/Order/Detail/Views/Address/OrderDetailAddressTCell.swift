//
//  OrderDetailAddressTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//


import UIKit
import SDWebImage

final class OrderDetailAddressTCell: UITableViewCell {

    static let ID: String = "OrderDetailAddressTCell"
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var viewsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topView, addressView])
        stackView.axis = .vertical
//        stackView.alignment = .fill
//        stackView.distribution = .fillEqually
        stackView.spacing = 4
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var addressView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Order Address".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 16)
        label.textAlignment = .left
        label.anchor(.height(26))
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        label.textAlignment = .left
        label.numberOfLines = 3
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addressLabel.text = ""
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        viewsStack.anchor(.top(), .leading(16), .trailing(-16), .bottom(-24))
        
        titleLabel.anchor(
            .top(), .leading(), .bottom(),
            .trailing())
        
        addressLabel.anchor(
            .top(12), .leading(16),
            .bottom(-12), .trailing(-16))
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(viewsStack)
        
        topView.addSubview(titleLabel)
        
        addressView.addSubview(addressLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.addressLabel.text = data
    }
}
