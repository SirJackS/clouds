//
//  OrderDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class OrderDetailPresenter: OrderDetailPresenterProtocol {
    
    private unowned let view: OrderDetailViewProtocol
    private var service: ServiceProtocol
    private var id: Int
    
    init(view: OrderDetailViewProtocol,
         service: ServiceProtocol,
         id: Int) {
        self.view = view
        self.service = service
        self.id = id
    }
    
    func load() {
        self.getOrder(id: self.id)
    }
    
    private func getOrder(id: Int) {
        self.view.handleOutput(.showActivity(true))
        service.getOrderDetailBy(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showData(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getOrderDetailBy))

            default: break
            }

        }
    }
}
