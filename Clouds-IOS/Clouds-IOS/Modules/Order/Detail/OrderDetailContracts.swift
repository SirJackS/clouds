//
//  OrderDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

// MARK: - Presenter

protocol OrderDetailPresenterProtocol: class {
    func load()
}

enum OrderDetailPresenterOutput {
    case showMessage(String)
    case showData(OrderDetail)
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol OrderDetailViewProtocol: class {
    func handleOutput(_ output: OrderDetailPresenterOutput)
}
