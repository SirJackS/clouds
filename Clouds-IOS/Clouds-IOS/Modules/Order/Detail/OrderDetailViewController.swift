//
//  OrderDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit
import SDWebImage

final class OrderDetailViewController: UIViewController, OrderDetailViewProtocol {
 
    var presenter: OrderDetailPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Order Detail")
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(OrderDetailMainTCell.self, forCellReuseIdentifier: OrderDetailMainTCell.ID)
        tableView.register(OrderDetailPaymentTCell.self, forCellReuseIdentifier: OrderDetailPaymentTCell.ID)
        tableView.register(OrderDetailAddressTCell.self, forCellReuseIdentifier: OrderDetailAddressTCell.ID)
        tableView.register(TotalTCell.self, forCellReuseIdentifier: TotalTCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.isHidden = true
        return tableView
    }()
    
    
    // MARK: - Data
    private var orderList: [OrderDetailItem] = []
    private var data: OrderDetail?
    private var orderTotal: [OrderTotal] = []
    private var orderPayment: [String] = []
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: OrderDetailPresenterOutput) {
        switch output {
        case .showData(let data):
            self.data = data
            self.orderList = data.items ?? []
            self.orderTotal = [
                OrderTotal(name: "Sub-Total:".localized(), price: data.subTotal),
                OrderTotal(name: "Delivery:".localized(), price: data.delivery),
                OrderTotal(name: "Total:".localized(), price: data.total),
            ]
            self.orderPayment = []
            if data.debetcard {
                self.orderPayment.append("debetcard")
            }
            if data.giftcard {
                self.orderPayment.append("gift card")
            }
            if data.reward {
                self.orderPayment.append("rewards")
            }
            
            self.tableView.reloadData()
            self.tableView.isHidden = false
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getOrderDetailBy:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
}


extension OrderDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailMainTCell.ID, for: indexPath) as? OrderDetailMainTCell else { return UITableViewCell() }
            cell.data = orderList
            cell.orderNo = data?.orderNo
            cell.date = data?.date
            cell.delegate = self
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailAddressTCell.ID, for: indexPath) as? OrderDetailAddressTCell else { return UITableViewCell() }
            cell.data = data?.address ?? "No Address".localized()
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: OrderDetailPaymentTCell.ID, for: indexPath) as? OrderDetailPaymentTCell else { return UITableViewCell() }
            cell.data = orderPayment
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalTCell.ID, for: indexPath) as? TotalTCell else { return UITableViewCell() }
            cell.data = orderTotal
            return cell
        default: break
        }
        return UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let viewController = ProductDetailBuilder.make()
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
}

extension OrderDetailViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

extension OrderDetailViewController: OrderDetailMainTCellDelegate {
    func selectedId(id: Int) {
        let viewController = ProductDetailBuilder.make(id: id)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

