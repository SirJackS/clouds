//
//  OrderDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

final class OrderDetailBuilder {
    
    static func make(id: Int) -> OrderDetailViewController {
        let view = OrderDetailViewController()
        
        let presenter = OrderDetailPresenter(view: view, service: App.service, id: id)
        view.presenter = presenter
        
        return view
    }
}
