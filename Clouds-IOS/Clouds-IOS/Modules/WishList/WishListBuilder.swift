//
//  WishListBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

final class WishListBuilder {
    
    static func make() -> WishListViewController {
        let view = WishListViewController()
        
        let presenter = WishListPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
