//
//  WishListCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import UIKit
import SDWebImage

//protocol WishListCellDelegate: class {
//    func addBagButtonClick(_ indexPath: IndexPath)
//}


final class WishListCell: UITableViewCell {

    static let ID: String = "WishListCell"
    
//    weak var delegate: WishListCellDelegate?
    
    var data: Wishlist! {
        didSet {
            self.configure()
        }
    }
    
    var bagSelected: Bool = false {
        didSet {
            
        }
    }
    
    var indexPath: IndexPath!
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.anchor(.size(width: 112, height: 116))
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var realPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var discountPriceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "C70D3A")
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()
    
//    private lazy var addToBagButton: UIButton = {
//        let button = UIButton(type: .system)
//        button.setTitle("add to bag".localized().uppercased(), for: .normal)
//        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
//        button.tintColor = UIColor.black.withAlphaComponent(0.87)
//        button.backgroundColor = UIColor.white
//        button.layer.borderWidth = 1
//        button.layer.borderColor = UIColor.black.withAlphaComponent(0.87).cgColor
//        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
////        button.titleLabel?.textAlignment = .right
////        button.contentHorizontalAlignment = .right
//        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
//        button.contentHorizontalAlignment = .leading
//        button.addTarget(self, action: #selector(addBagButtonTouchUp), for: .touchUpInside)
//        return button
//    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.5 : 1.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.fillSuperview(leading: 16, trailing: -16))
        
        photoView.anchor(.top(16), .bottom(-16), .leading())
        
        titleLabel.anchor(.top(realPriceLabel.bottomAnchor, constant: 4),
            .leading(photoView.trailingAnchor, constant: 16),
            .trailing(-16), .heightGreater(22))
        
        discountPriceLabel.anchor(.trailing(-16), .top(12))
        
        realPriceLabel.anchor(
            .leading(photoView.trailingAnchor, constant: 16),
            .trailing(discountPriceLabel.leadingAnchor, constant: -7), .top(12))
        
//        addToBagButton.anchor(.bottom(-16), .trailing(),.height(30), .width(103))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
        self.titleLabel.text = ""
        self.discountPriceLabel.text = ""
        self.realPriceLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(photoView)
        bodyView.addSubview(lineView)
        bodyView.addSubview(realPriceLabel)
        bodyView.addSubview(discountPriceLabel)
//        bodyView.addSubview(addToBagButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
        self.titleLabel.text = data.name
        
        if let discount = data.salePrice {
            self.discountPriceLabel.text = "\(discount) AZN"
            guard let realPrice = data.price else { return }
            self.realPriceLabel.attributedText = "\(realPrice) AZN".strikeThrough()
        } else {
            guard let realPrice = data.price else { return }
            self.realPriceLabel.text = "\(realPrice) AZN"
            self.realPriceLabel.textColor = UIColor.black.withAlphaComponent(0.87)
            self.realPriceLabel.font = UIFont(weight: .medium, size: 16)
        }
//        if let hex = data.colorHex {
//            self.colorView.backgroundColor = UIColor(hexString: "\(hex)")
        
    }
    
    // MARK: - Action
    
//    @objc private func addBagButtonTouchUp() {
//        self.delegate?.addBagButtonClick(self.indexPath)
//    }
}

extension WishListCell {
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return touch.view == gestureRecognizer.view
    }
}
