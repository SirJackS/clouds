//
//  WishListViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import UIKit
import SDWebImage

final class WishListViewController: UIViewController, WishListViewProtocol {
 
    var presenter: WishListPresenterProtocol!
    
    private lazy var navBarView: WishListNavbarView = {
        let view = WishListNavbarView(title: "Saved Items".localized())
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(WishListCell.self, forCellReuseIdentifier: WishListCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 148
        tableView.estimatedRowHeight = 148
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        tableView.isHidden = true
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refreshWishlist), for: .valueChanged)
        return refresh
    }()
    
    private var data: [Wishlist] = []
    private var colorList: [Color] = []
    private var sizeList: [Size] = []
    private var removedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.addObservers()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        tableView.anchor(.fillSuperview())
    }

    func handleOutput(_ output: WishListPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data.removeAll()
            self.data = data
            self.navBarView.titleLabel.text = String(format: "Saved Items (*)".localized(), self.data.count)
            self.tableView.reloadData()
            self.tableView.isHidden = false
        case .refreshBag:
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshBag"), object: nil)
            UINotificationFeedbackGenerator().notificationOccurred(.success)
//            self.addToBagButton.isSelected = !self.addToBagButton.isSelected
        case .remove(let indexPath):
            self.data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getWishlist:
                        self.presenter.load()
                    case .removeFromWishlist:
                        guard let indexPath = self.removedIndexPath else { return }
                        self.presenter.removeBy(id: self.data[indexPath.row].id, indexPath: indexPath)
                    default:
                        return
                    }
                }
            }
        case .showColorList(let colors, let indexPath):
            self.openColorPicker(colors: colors, indexPath: indexPath)
        case .showSizeList(let sizes, let indexPath):
            self.openSizePicker(sizes: sizes, indexPath: indexPath)
        case .showColorSize(let data, let indexPath, let type):
            if let sizes = data.sizes {
                if type == .size {
                    self.openSizePicker(sizes: sizes, indexPath: indexPath)
                }
            }
            if let colors = data.colors {
                if type == .color {
                    self.openColorPicker(colors: colors, indexPath: indexPath)
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        tableView.addSubview(refreshControl)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.view.backgroundColor = UIColor.white
        self.tableView.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    // MARK: - Add Observers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.refreshWishlist),
        name: NSNotification.Name(rawValue: "refreshWishlist"),
        object: nil)
    }
    
    // MARK: - Action
    
    @objc func refreshWishlist() {
        self.refreshControl.beginRefreshing()
        self.presenter.load()
        self.refreshControl.endRefreshing()
    }
    
    private func openColorPicker(colors: [Color], indexPath: IndexPath) {
        let alert = UIAlertController(title: "Colors".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedColor: Color?
        if colors.count > 0 {
            selectedColor = colors[0]
        }
        let frameSizes: [CGFloat] = (0..<colors.count).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [colors.map { $0.name ?? "null"}]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if colors.count > 0 {
                selectedColor = colors[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                guard let color = selectedColor?.name else {return}
                
                if let size = self.data[indexPath.row].size {
                    self.presenter.updateOptionsInWishlist(id: self.data[indexPath.row].id, color: color, size: size)
                } else {
                    self.presenter.updateOptionsInWishlist(id: self.data[indexPath.row].id, color: color, size: "")
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openSizePicker(sizes: [Size], indexPath: IndexPath) {
        let alert = UIAlertController(title: "Sizes".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedSize: Size?
        if sizes.count > 0 {
            selectedSize = sizes[0]
        }
        let frameSizes: [CGFloat] = (0..<sizes.count).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [sizes.map { $0.name ?? "null"}]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if sizes.count > 0 {
                selectedSize = sizes[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                guard let size = selectedSize?.name else {return }
                
                if let color = self.data[indexPath.row].color {
                    self.presenter.updateOptionsInWishlist(id: self.data[indexPath.row].id, color: color, size: size)
                } else {
                    self.presenter.updateOptionsInWishlist(id: self.data[indexPath.row].id, color: "", size: size)
                }
//                data[indexPath.row].id
                
//                self.data[indexPath.row].size = size.name
//                self.tableView.reloadData()
            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
}


extension WishListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WishListCell.ID, for: indexPath) as? WishListCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        cell.indexPath = indexPath
//        cell.delegate = self
//        cell.isLast = (indexPath.row == count-1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .normal, title: "Delete".localized()) { (action, view, completion) in
            // Perform your action here
            self.removedIndexPath = indexPath
            self.presenter.removeBy(id: self.data[indexPath.row].id, indexPath: indexPath)
            completion(true)
        }
        deleteAction.image = UIImage(named: "swipe-x-icon")
        deleteAction.backgroundColor = UIColor(hexString: "C70D3A")
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = ProductDetailBuilder.make(id: data[indexPath.row].productId)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//extension WishListViewController: WishListCellDelegate {
//    func addBagButtonClick(_ indexPath: IndexPath) {
//       guard Defaults.instance.isLoggedIn else {return}
//        let item = self.data[indexPath.row]
//        let viewController = ProductDetailBuilder.make(id: item.productId)
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
//}

