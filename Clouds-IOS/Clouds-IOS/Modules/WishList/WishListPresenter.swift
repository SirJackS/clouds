//
//  WishListPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

enum WishlistClickType {
    case color
    case size
}

final class WishListPresenter: WishListPresenterProtocol {
    
    private unowned let view: WishListViewProtocol
    private var service: ServiceProtocol
    
    init(view: WishListViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getWishlist()
    }
    
    func removeBy(id: Int, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.removeFromWishlist(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.remove(indexPath))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.removeFromWishlist))
                
            default: break
            }
        }
    }
    
    private func getWishlist() {
        self.view.handleOutput(.showActivity(true))
        service.getWishlist { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func getColorsWithSize(id: Int, size: String, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.getColorsWithSize(id, size) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showColorList(data,indexPath))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func getSizesWithColor(id: Int, color: String, indexPath: IndexPath) {
        self.view.handleOutput(.showActivity(true))
        service.getSizesWithColor(id, color) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showSizeList(data,indexPath))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func updateOptionsInWishlist(id: Int, color: String?, size: String?) {
        self.view.handleOutput(.showActivity(true))
        let request = UpdateOptionsInWishlistRequest(color: color, size: size)
        service.updateOptionsInWishlist(id, request: request) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                self.getWishlist()

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
//                self.view.handleOutput(.showNetworkError(.getWishlist))

            default: break
            }

        }
    }
    
    func addBagBy(id: Int, quantity: Int, size: String, color: String) {
        self.view.handleOutput(.showActivity(true))
        let request = AddBagItemRequest(productId: id, quantity: quantity, size: size, color: color)
        service.addBagItem(request){ [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                self.view.handleOutput(.refreshBag)
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.addToWishlist))
                
            default: break
            }
            
        }
    }
    
    func getSizesAndColorsBy(id: Int, indexPath: IndexPath, type: WishlistClickType) {
        self.view.handleOutput(.showActivity(true))
        service.getSizesAndColorsBy(id) { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showColorSize(data,indexPath,type))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showMessage(error.localizedDescription))

            default: break
            }

        }
    }
}
