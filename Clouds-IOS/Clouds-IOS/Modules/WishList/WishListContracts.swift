//
//  WishListContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

// MARK: - Presenter

protocol WishListPresenterProtocol: class {
    func load()
    func removeBy(id: Int, indexPath: IndexPath)
    func getSizesWithColor(id: Int, color: String, indexPath: IndexPath)
    func getColorsWithSize(id: Int, size: String, indexPath: IndexPath)
    func updateOptionsInWishlist(id: Int, color: String?, size: String?)
    func addBagBy(id: Int, quantity: Int, size: String, color: String)
    func getSizesAndColorsBy(id: Int, indexPath: IndexPath, type: WishlistClickType)
}

enum WishListPresenterOutput {
    case refreshBag
    case remove(IndexPath)
    case showMessage(String)
    case showActivity(Bool)
    case showList([Wishlist])
    case showColorList([Color],IndexPath)
    case showSizeList([Size],IndexPath)
    case showColorSize(ColorSize,IndexPath,WishlistClickType)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol WishListViewProtocol: class {
    func handleOutput(_ output: WishListPresenterOutput)
}
