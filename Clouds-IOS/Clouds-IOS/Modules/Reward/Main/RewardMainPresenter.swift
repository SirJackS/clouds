//
//  RewardMainPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class RewardMainPresenter: RewardMainPresenterProtocol {
    
    private unowned let view: RewardMainViewProtocol
    private var service: ServiceProtocol
    
    init(view: RewardMainViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getRewards()
    }
    
    private func getRewards() {
        self.view.handleOutput(.showActivity(true))
        service.getRewards { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showData(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getRewards))

            default: break
            }

        }
    }
}
