//
//  RewardMainContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

// MARK: - Presenter

protocol RewardMainPresenterProtocol: class {
    func load()
}

enum RewardMainPresenterOutput {
    case showMessage(String)
    case showData(Reward)
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol RewardMainViewProtocol: class {
    func handleOutput(_ output: RewardMainPresenterOutput)
}
