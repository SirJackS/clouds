//
//  RewardMainBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class RewardMainBuilder {
    
    static func make() -> RewardMainViewController {
        let view = RewardMainViewController()
        
        let presenter = RewardMainPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
