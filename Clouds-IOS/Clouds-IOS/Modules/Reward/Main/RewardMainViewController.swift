//
//  RewardMainViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

final class RewardMainViewController: UIViewController, RewardMainViewProtocol {
 
    var presenter: RewardMainPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "My Rewards".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var headerView: RewardMainHeaderView = {
        let view = RewardMainHeaderView()
        return view
    }()
    
    private lazy var emptyView: EmptyView = {
        let view = EmptyView(icon: "myrewards-icon", title: "You have no Rewards yet".localized(), desc: "You currently have no\n rewards.Start shopping to earn\n rewards and change them to gifts\n you like.".localized(), forButton: "start shopping".localized())
        view.isHidden = true
        return view
    }()
    
    private lazy var mainTableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(RewardMainCell.self, forCellReuseIdentifier: RewardMainCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 84
        tableView.estimatedRowHeight = 84
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        return tableView
    }()
    
    private var data: Reward?
    private var logs: [RewardItem] = []
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainTableView.anchor(.fillSuperview())
        
        emptyView.anchor(.top(), .leading(), .trailing())
        
    }

    func handleOutput(_ output: RewardMainPresenterOutput) {
        switch output {
        case .showData(let data):
            self.headerView.reward = "\(data.total) REWARD"
            guard let logs = data.logs else { return }
            self.logs = logs
            if logs.count > 0 {
                self.mainTableView.reloadData()
            } else {
                self.mainTableView.isHidden = true
                self.emptyView.isHidden = false
            }
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getRewards:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainTableView)
        self.view.addSubview(emptyView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.mainTableView.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
        
        self.mainTableView.setTableHeaderView(headerView: headerView)
        self.mainTableView.updateHeaderViewFrame()
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
}


extension RewardMainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case mainTableView:
            return logs.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case mainTableView:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: RewardMainCell.ID, for: indexPath) as? RewardMainCell else { return UITableViewCell() }
            cell.data = logs[indexPath.row]
            return cell
        default:
            return UITableViewCell()
        }
    }

}

extension RewardMainViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

