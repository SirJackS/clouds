//
//  RewardMainCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

protocol RewardMainCellDelegate: class {
    func pieceButtonClick()
}

final class RewardMainCell: UITableViewCell {

    static let ID: String = "RewardMainCell"
    
    weak var delegate: RewardMainCellDelegate?
    
    var data: RewardItem! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var fieldView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(20))
        return label
    }()
    
    private lazy var bonusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .right
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var starView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "star-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(8), .bottom(-8), .leading(16), .trailing(-16))
        
        titleLabel.anchor(.leading(16), .centerY(), .trailing(bonusLabel.leadingAnchor, constant: -40))
        
        starView.anchor(.trailing(-16), .centerY())
        
        bonusLabel.anchor(.centerY(), .trailing(starView.leadingAnchor, constant: -16))
        
        dateLabel.anchor(.top(), .leading(), .trailing())
        
        fieldView.anchor(
            .top(dateLabel.bottomAnchor, constant: 8),
            .leading(), .trailing(), .bottom(), .height(44))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.bonusLabel.text = ""
        self.dateLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(dateLabel)
        bodyView.addSubview(fieldView)
        
        fieldView.addSubview(titleLabel)
        fieldView.addSubview(starView)
        fieldView.addSubview(bonusLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        self.titleLabel.text = data.name
        self.bonusLabel.text = "+\(data.amount) reward"
//        self.dateLabel.text = data.date
    }
}
