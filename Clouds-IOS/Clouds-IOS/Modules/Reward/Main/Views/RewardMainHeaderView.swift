//
//  RewardMainHeaderView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit

final class RewardMainHeaderView: UICollectionReusableView {
    
    static let ID: String = "RewardMainHeaderView"
    
    var reward: String = "0" {
        didSet {
            self.configure()
        }
    }
    
    private lazy var totalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.secondaryColor
        view.anchor(.height(46))
        return view
    }()
    
    private lazy var giltView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "gilt-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    private lazy var totalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 14)
        label.numberOfLines = 1
        return label
    }()
    
    private var giltHeight: CGFloat {
        let width = UIScreen.main.bounds.width
        return (218 / 375) * width
    }
    
    private var giltWidth: CGFloat {
        let width = UIScreen.main.bounds.width
        return (224 / 375) * width
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        giltView.anchor(.top(24), .centerX(), .height(giltHeight), .width(giltWidth))
        
        totalView.anchor(
            .top(giltView.bottomAnchor, constant: 40), .centerX(),
            .width(giltWidth-4), .bottom(-12))
        
        totalLabel.anchor(.leading(24), .trailing(-24), .centerY() ,.height(16))

        super.updateConstraints()
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(giltView)
        self.addSubview(totalView)
        
        totalView.addSubview(totalLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configure() {
        totalLabel.text = reward
    }
    
}
