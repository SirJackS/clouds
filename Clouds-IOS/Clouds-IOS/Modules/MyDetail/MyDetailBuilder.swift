//
//  MyDetailBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

final class MyDetailBuilder {
    
    static func make() -> MyDetailViewController {
        let view = MyDetailViewController()
        
        let presenter = MyDetailPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
