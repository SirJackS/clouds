//
//  MyDetailContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

// MARK: - Presenter

protocol MyDetailPresenterProtocol: class {
    func load()
    func updateMe(firstname: String, lastname: String, birthdate: String, email: String, interestedIn: String)
}

enum  MyDetailPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case handleError([String: [String]])
    case showMe(Me)
    case showNetworkError(NetworkErrorType)
}

// MARK: - View

protocol MyDetailViewProtocol: class {
    func handleOutput(_ output: MyDetailPresenterOutput)
}
