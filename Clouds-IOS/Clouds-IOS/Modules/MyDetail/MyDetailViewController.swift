//
//  MyDetailViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit

final class MyDetailViewController: UIViewController, MyDetailViewProtocol {

    var presenter: MyDetailPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "My Details".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var mainScrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        scrollView.isHidden = true
        return scrollView
    }()
    
    private lazy var contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var textViewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [firstNameView, lastNameView,emailAddressView,dateOfBirthView,genderView])
        stackView.axis = .vertical
//        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.spacing = 12
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var firstNameView: CustomTextView = {
        let view = CustomTextView(title: "First Name".localized())
        return view
    }()
    
    private lazy var lastNameView: CustomTextView = {
        let view = CustomTextView(title: "Last Name".localized())
        return view
    }()
    
    private lazy var emailAddressView: CustomTextView = {
        let view = CustomTextView(title: "Email Address".localized())
        view.mainText.textContentType = .emailAddress
        return view
    }()
    
    private lazy var dateOfBirthView: CustomTextView = {
        let view = CustomTextView(title: "Date of birth".localized())
        view.delegate = self
        view.mainText.inputAccessoryView = toolbar
        view.mainText.inputView = datePicker
        return view
    }()
    
    private lazy var genderView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var datePicker: CustomDatePickerView = {
        let picker = CustomDatePickerView()
        picker.datePickerMode = .date
//        let loc = Locale(identifier: LanguageManager.shared.getCurrentLanguage()?.rawValue ?? "az")
//        picker.locale = loc
        let loc = Locale(identifier: L102Language.currentAppleLanguage())
        picker.locale = loc
        picker.minimumDate = Date().getDateOf(year: 150)//Date(timeIntervalSince1970: 0)
        picker.maximumDate = Date()
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }
        return picker
    }()
    
    private lazy var toolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barTintColor = .white
        toolbar.backgroundColor = .white
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(doneDatePicker))
        doneButton.tintColor = .black
        let clearButton = UIBarButtonItem(title: "clear".localized().capitalized, style: .plain, target: self, action: #selector(clearDatePicker))
        clearButton.tintColor = .black
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([clearButton,spaceButton,doneButton], animated: false)
        return toolbar
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("save changes".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.addTarget(self, action: #selector(saveButtonTouchUp), for: .touchUpInside)
        button.anchor(.height(46))
        return button
    }()

    private lazy var genderTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Mostly interested in".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var interestedErrorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "EE0005")
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
//        label.isHidden = true
        label.anchor(.height(16))
        return label
    }()

    //buttons
    
    private lazy var buttonsStack: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var girlButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Girl’s wear".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        button.anchor(.height(32))
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(girlButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var boyButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Boy’s wear".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.60)
        button.backgroundColor = .clear
        button.titleLabel?.font = UIFont(weight: .regular, size: 16)
        button.anchor(.height(32))
        button.contentHorizontalAlignment = .left
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 38, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(boyButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var girlRadioView: GenderRadioView = {
        let view = GenderRadioView()
        return view
    }()
    
    private lazy var boyRadioView: GenderRadioView = {
        let view = GenderRadioView()
        return view
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    // MARK: - Data
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        mainScrollView.anchor(.top(), .leading(), .trailing(), .bottom())
                
        contentView.anchor(.fillSuperview())
        contentView.anchor(.widthTo(mainScrollView))
        
        
        textViewStack.anchor(.top(24), .leading(16), .trailing(-16))
        
        genderTitleLabel.anchor(.top(), .leading(), .trailing())

        buttonsStack.anchor(
            .top(genderTitleLabel.bottomAnchor, constant: 4),
            .leading(), .trailing(), .bottom())
        
        girlButton.anchor(.top(), .leading(), .trailing())
        
        lineView.anchor(
            .top(girlButton.bottomAnchor), .leading(), .trailing(), .height(0.3))
        
        boyButton.anchor(.top(lineView.bottomAnchor), .leading(), .trailing(), .bottom())
        
        girlRadioView.anchor(
            .leading(girlButton.leadingAnchor, constant: 4),
            .centerY(girlButton.centerYAnchor))
        
        boyRadioView.anchor(
            .leading(boyButton.leadingAnchor, constant: 4),
            .centerY(boyButton.centerYAnchor))
        
        saveButton.anchor(
            .top(textViewStack.bottomAnchor, constant: 40),
            .leading(16), .trailing(-16), .bottom(-32))
        
        interestedErrorLabel.anchor(
            .top(textViewStack.bottomAnchor, constant: 4),
            .leading(20), .trailing(-20))
        
    }

    func handleOutput(_ output: MyDetailPresenterOutput) {
        switch output {
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showMe(let data):
            self.mainScrollView.isHidden = false
            self.firstNameView.mainText.text = data.firstname
            self.lastNameView.mainText.text = data.lastname
            self.emailAddressView.mainText.text = data.email
            if let date = data.birthdate {
                self.dateOfBirthView.mainText.text = date.getFormattedDateForDayCell(fromFormat: "dd.MM.yyyy", toFormat: "d MMMM yyyy")
            }
            guard let interest = data.interestedIn else { return }
            switch interest {
            case "1,2":
                self.girlRadioView.isSelected = true
                self.boyRadioView.isSelected = true
            case "1":
                self.boyRadioView.isSelected = true
                self.girlRadioView.isSelected = false
            case "2":
                self.girlRadioView.isSelected = true
                self.boyRadioView.isSelected = false
            default:
                return
            }
        case .handleError(let data):
            data.forEach({ (key, value) in
                self.handleErrorBy(key, value.first ?? "")
            })
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .me:
                        self.saveButtonTouchUp()
                    case .updateMe:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(mainScrollView)
        
        mainScrollView.addSubview(contentView)
        
        contentView.addSubview(textViewStack)
        contentView.addSubview(interestedErrorLabel)
        contentView.addSubview(saveButton)
        
        genderView.addSubview(genderTitleLabel)
        genderView.addSubview(buttonsStack)
        
        buttonsStack.addSubview(girlButton)
        buttonsStack.addSubview(boyButton)
        buttonsStack.addSubview(girlRadioView)
        buttonsStack.addSubview(boyRadioView)
        buttonsStack.addSubview(lineView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
    @objc func girlButtonTouchUp() {
        girlRadioView.isSelected = !girlRadioView.isSelected
    }
    
    @objc func boyButtonTouchUp() {
        boyRadioView.isSelected = !boyRadioView.isSelected
    }
    
    @objc func doneDatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
//        let loc = Locale(identifier: LanguageManager.shared.getCurrentLanguage()?.rawValue ?? "az")
//        formatter.locale = loc
        let loc = Locale(identifier: L102Language.currentAppleLanguage())
        formatter.locale = loc
        //toDate = toDatePicker.date
        dateOfBirthView.mainText.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func clearDatePicker(){
        dateOfBirthView.mainText.text = ""
        self.view.endEditing(true)
    }

    @objc func saveButtonTouchUp() {
        self.hideErrors()
        self.saveButton.isEnabled = false
        var interest = ""
        if boyRadioView.isSelected && !girlRadioView.isSelected  {
            interest = "1"
        } else if !boyRadioView.isSelected && girlRadioView.isSelected  {
            interest = "2"
        } else if boyRadioView.isSelected && girlRadioView.isSelected {
            interest = "1,2"
        }
        self.presenter.updateMe(
            firstname: firstNameView.mainText.text ?? "",
            lastname: lastNameView.mainText.text ?? "",
            birthdate: dateOfBirthView.mainText.text ?? "",
            email: emailAddressView.mainText.text ?? "",
            interestedIn: interest)
        self.saveButton.isEnabled = true
    }
    
    // MARK: - Error Handler
    
    private func handleErrorBy(_ field: String,_ errorText: String) {
        switch field {
        case "firstname":
            self.firstNameView.errorText = errorText
            self.firstNameView.isError = true
        case "lastname":
            self.lastNameView.errorText = errorText
            self.lastNameView.isError = true
        case "interested_in":
            self.interestedErrorLabel.text = errorText
            self.interestedErrorLabel.isHidden = false
        case "email":
            self.emailAddressView.errorText = errorText
            self.emailAddressView.isError = true
        case "birthdate":
            self.dateOfBirthView.errorText = errorText
            self.dateOfBirthView.isError = true
        default:
            return
        }
    }
    
    private func hideErrors() {
        self.firstNameView.isError = false
        self.lastNameView.isError = false
        self.dateOfBirthView.isError = false
        self.emailAddressView.isError = false
        self.interestedErrorLabel.isHidden = true
    }
    
}

// MARK: - UIScrollView delegate

extension MyDetailViewController: UIScrollViewDelegate {}

// MARK: - BTNavbarView delegate

extension MyDetailViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

// MARK: - UITextField delegate

extension MyDetailViewController: CustomTextViewDelegate {
    func didBeginEditing(_ textView: CustomTextView) {
        switch textView {
        case dateOfBirthView:
            dateOfBirthView.mainText.becomeFirstResponder()
        default: break
        }
    }
}
