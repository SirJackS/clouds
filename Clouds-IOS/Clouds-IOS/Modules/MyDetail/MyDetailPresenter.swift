//
//  MyDetailPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

final class MyDetailPresenter: MyDetailPresenterProtocol {
    
    private unowned let view: MyDetailViewProtocol
    private var service: ServiceProtocol
    
    init(view: MyDetailViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getMe()
    }
    
    private func getMe() {
        self.view.handleOutput(.showActivity(true))
        service.me { [weak self] (result) in
            guard let self = self else { return }
            
            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showMe(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.me))
                
            default: break
            }
        }
    }
    
    func updateMe(firstname: String, lastname: String, birthdate: String, email: String, interestedIn: String) {
        self.view.handleOutput(.showActivity(true))
        var date = ""
        if birthdate != "" {
            print("birthdate",birthdate)
            date = birthdate.getFormattedDateForDayCellWithLocale(fromFormat: "d MMMM yyyy", toFormat: "yyyy-MM-dd")
        }
        
        let request = MeRequest(firstname: firstname, lastname: lastname, email: email, birthdate: date, interestedIn: interestedIn)
        service.updateMe(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            
            switch result {
            case .success(let response):
                print(response)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "myDetailUpdated"), object: nil)
                self.view.handleOutput(.showMessage(response.message ?? ""))
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                guard let errors = response.errors else { return }
                self.view.handleOutput(.handleError(errors))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.updateMe))
                
            default: break
            }
        }
    }
}
