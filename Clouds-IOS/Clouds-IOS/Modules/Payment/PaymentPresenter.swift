//
//  PaymentPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.03.21.
//

import Foundation

final class PaymentPresenter: PaymentPresenterProtocol {
    
    private unowned let view: PaymentViewProtocol
    private var service: ServiceProtocol
    private var url: String
    
    init(view: PaymentViewProtocol,
         service: ServiceProtocol,
         url: String) {
        self.view = view
        self.service = service
        self.url = url
    }
    
    func load() {
        self.view.handleOutput(.showData(self.url))
    }
    
    private func getData() {
//        let data: String = "<html><body><p>Hello!</p></body></html>"
        
    }
}
