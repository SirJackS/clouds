//
//  PaymentViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.03.21.
//

import UIKit
import WebKit

final class PaymentViewController: UIViewController, PaymentViewProtocol {

    var presenter: PaymentPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Payment".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var webView: WKWebView = {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        return webView
    }()
    
    // MARK: - Data
    private var data: String = ""
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        webView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: PaymentPresenterOutput) {
        switch output {
        case .showData(let data):
            self.data = data
//            webView.loadHTMLString(data, baseURL: nil)
            webView.load(URLRequest(url: URL(string: data)!))
            
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getGiftcards:
                        self.presenter.load()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(webView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    // MARK: - Action
    
}

extension PaymentViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

extension PaymentViewController: WKUIDelegate {
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
    
    }
    
}

extension PaymentViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("asdasd",webView.url?.absoluteString)
        //"https://e-commerce.kapitalbank.az/index.jsp?ORDERID=18695787&SESSIONID=2A10C0F6EDF1BE72BADDD939A73DF543&"
        
    }
}
