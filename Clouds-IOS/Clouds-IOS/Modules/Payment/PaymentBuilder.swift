//
//  PaymentBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.03.21.
//

import Foundation

final class PaymentBuilder {
    
    static func make(url: String) -> PaymentViewController {
        let view = PaymentViewController()
        
        let presenter = PaymentPresenter(view: view, service: App.service, url: url)
        view.presenter = presenter
        
        return view
    }
}
