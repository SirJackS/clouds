//
//  PaymentContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.03.21.
//

import Foundation

// MARK: - Presenter

protocol PaymentPresenterProtocol: class {
    func load()
}

enum PaymentPresenterOutput {
    case showMessage(String)
    case showActivity(Bool)
    case showData(String)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol PaymentViewProtocol: class {
    func handleOutput(_ output: PaymentPresenterOutput)
}
