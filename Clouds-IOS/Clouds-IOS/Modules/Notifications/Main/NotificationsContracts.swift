//
//  NotificationsContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

// MARK: - Presenter

protocol NotificationsPresenterProtocol: class {
    func load()
}

enum NotificationsPresenterOutput {
    case showMessage(String)
    case showList([Notification])
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
}


// MARK: - View

protocol NotificationsViewProtocol: class {
    func handleOutput(_ output: NotificationsPresenterOutput)
}
