//
//  NotificationsBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class NotificationsBuilder {
    
    static func make() -> NotificationsViewController {
        let view = NotificationsViewController()
        
        let presenter = NotificationsPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
