//
//  NotificationsPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

final class NotificationsPresenter: NotificationsPresenterProtocol {
    
    private unowned let view: NotificationsViewProtocol
    private var service: ServiceProtocol
    
    init(view: NotificationsViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getNotifications()
    }
    
    private func getNotifications() {
        self.view.handleOutput(.showActivity(true))
        service.getNotifications { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getNotifications))
                
            default: break
            }
            
        }
    }
}
