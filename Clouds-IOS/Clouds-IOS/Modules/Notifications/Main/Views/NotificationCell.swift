//
//  NotificationCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import UIKit
import SDWebImage

final class NotificationCell: UITableViewCell {

    static let ID: String = "NotificationCell"
    
    var data: Notification! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var iconBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .secondaryColor
        let height = UIScreen.main.bounds.width * 0.106
        view.anchor(.size(width: height, height: height))
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.tintColor = .white
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "reward-icon")
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.38)
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(24), .leading(16), .trailing(-16), .bottom())
        
//        iconBackView.anchor(.top(), .leading(), .bottomLess(dateLabel.topAnchor, constant: -16))
        
        iconBackView.anchor(.top(), .leading())
        
        iconView.anchor(.fillSuperviewPadding(8))
        
        titleLabel.anchor(
            .leading(iconBackView.trailingAnchor, constant: 16),
            .trailing(-16), .top())
        
        dateLabel.anchor(
            .leading(iconBackView.trailingAnchor, constant: 16),
            .trailing(-16), .top(titleLabel.bottomAnchor, constant: 12),
            .bottom(-8), .height(16))
        
        lineView.anchor(.leading(), .bottom(), .trailing(), .height(0.3))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//        self.iconView.image = nil
        self.titleLabel.text = ""
        self.dateLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(iconBackView)
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(dateLabel)
        bodyView.addSubview(lineView)
        
        iconBackView.addSubview(iconView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
//        if let imageUrl = URL(string: data.image ?? "") {
//            self.photoView.sd_setImage(with: imageUrl)
//        }
//
        self.titleLabel.text = data.title
        self.dateLabel.text = data.date
    }
}
