//
//  NotificationSettingsViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import UIKit
import SDWebImage

final class NotificationSettingsViewController: UIViewController, NotificationSettingsViewProtocol {

    var presenter: NotificationSettingsPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Notification Settings")
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(NotificationSettingsCell.self, forCellReuseIdentifier: NotificationSettingsCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 48
        tableView.estimatedRowHeight = 48
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 16, right: 0)
        return tableView
    }()
    
    private var data: [String] = []
    
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: NotificationSettingsPresenterOutput) {
        switch output {
        case .showList(let data):
            self.data = data
            self.tableView.reloadData()
        default: break
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }

}


extension NotificationSettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotificationSettingsCell.ID, for: indexPath) as? NotificationSettingsCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        cell.delegate = self
        return cell
    }
}

extension NotificationSettingsViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        print("Back Pressed")
        self.popVC()
    }
}

extension NotificationSettingsViewController: NotificationSettingsCellDelegate {
    func switchValueChange(value: Bool) {
        print("switch",value)
    }
}
