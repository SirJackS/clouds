//
//  NotificationSettingsBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

final class NotificationSettingsBuilder {
    
    static func make() -> NotificationSettingsViewController {
        let view = NotificationSettingsViewController()
        
        let presenter = NotificationSettingsPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
