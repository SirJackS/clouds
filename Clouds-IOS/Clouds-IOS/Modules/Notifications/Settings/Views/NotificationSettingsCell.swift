//
//  NotificationSettingsCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import UIKit

protocol NotificationSettingsCellDelegate: class {
    func switchValueChange(value: Bool)
}

final class NotificationSettingsCell: UITableViewCell {
    
    static let ID: String = "NotificationSettingsCell"
    
    weak var delegate: NotificationSettingsCellDelegate?
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var switchItem: UISwitch = {
        let switchItem = UISwitch()
        switchItem.onTintColor = UIColor.black.withAlphaComponent(0.6)
//        switchItem.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
//        switchItem.anchor(.size(width: 24, height: 16))
        switchItem.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        return switchItem
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(18))
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.5 : 1.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        switchItem.anchor(.trailing(-32), .centerY())
        
        titleLabel.anchor(
            .leading(32),
            .trailing(switchItem.leadingAnchor, constant: -16),
            .centerY())
//            .centerY(switchItem.centerYAnchor, constant: -4))
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.switchItem.setOn(false, animated: false)
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(switchItem)
        self.contentView.addSubview(titleLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        self.backgroundColor = .clear

        print("size", switchItem.frame.width, switchItem.frame.height)
    }
    
    private func configure() {
        self.titleLabel.text = data
    }
    
    // MARK: - Action
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if (sender.isOn == true) {
            print("on")
        } else {
            print("off")
        }
        self.delegate?.switchValueChange(value: sender.isOn)
    }
}

