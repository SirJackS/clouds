//
//  NotificationSettingsPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

final class NotificationSettingsPresenter: NotificationSettingsPresenterProtocol {
    
    private unowned let view: NotificationSettingsViewProtocol
    private var service: ServiceProtocol
    
    init(view: NotificationSettingsViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getFields()
    }
    
//    func search() {
//        let keywords = self.restaurantData?.keywords ?? []
//        self.view.handleOutput(.pushSearch(restaurant, keywords))
//    }
    
    private func getFields() {
        let fields: [String] = [
            "Push 1",
            "Push 2",
            "Push 3"
        ]
        self.view.handleOutput(.showList(fields))
    }
}
