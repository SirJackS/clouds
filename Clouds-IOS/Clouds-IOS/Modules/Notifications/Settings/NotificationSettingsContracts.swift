//
//  NotificationSettingsContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import Foundation

// MARK: - Presenter

protocol NotificationSettingsPresenterProtocol: class {
    func load()
}

enum NotificationSettingsPresenterOutput {
    case showMessage(String)
    case showList([String])
}


// MARK: - View

protocol NotificationSettingsViewProtocol: class {
    func handleOutput(_ output: NotificationSettingsPresenterOutput)
}
