//
//  CheckoutBuilder.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class CheckoutBuilder {
    
    static func make() -> CheckoutViewController {
        let view = CheckoutViewController()
        
        let presenter = CheckoutPresenter(view: view, service: App.service)
        view.presenter = presenter
        
        return view
    }
}
