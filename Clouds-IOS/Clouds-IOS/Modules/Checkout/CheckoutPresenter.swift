//
//  CheckoutPresenter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

final class CheckoutPresenter: CheckoutPresenterProtocol {
    
    private unowned let view: CheckoutViewProtocol
    private var service: ServiceProtocol
    
    init(view: CheckoutViewProtocol,
         service: ServiceProtocol) {
        self.view = view
        self.service = service
    }
    
    func load() {
        self.getCheckout()
        self.getAddresses()
        self.getGiftCards()
        self.getRewards()
    }
    
    private func getCheckout() {
        self.view.handleOutput(.showActivity(true))
        service.getCheckout { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showOrder(data))

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getCheckout))

            default: break
            }

        }
    }
    
    private func getAddresses() {
        self.view.handleOutput(.showActivity(true))
        service.getAllAddresses { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showAddressList(data))
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getAllAddresses))
                
            default: break
            }
            
        }
    }
    
    private func getGiftCards() {
        self.view.handleOutput(.showActivity(true))
        service.getGiftcards { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showGiftcards(data))
                
                
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))
                
            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getGiftcards))
                
            default: break
            }
            
        }
    }
    
    private func getRewards() {
        self.view.handleOutput(.showActivity(true))
        service.getRewards { [weak self] (result) in
            guard let self = self else { return }
            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showReward(data))
                self.view.handleOutput(.showVC)

            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.getRewards))

            default: break
            }

        }
    }
    
    func storeOrder(cartId: Int, addressId: Int, note: String, giftCard: Int?, total: Float, reward: Int?) {
        self.view.handleOutput(.showActivity(true))
        
        let request = OrderRequest(cartId: cartId, addressId: addressId, note: note, subtotal: 0.0, delivery: 0.0, giftCard: giftCard, debitCard: 0.0, total: total, reward: reward)
        
        service.storeOrder(request: request) { [weak self] (result) in
            guard let self = self else { return }

            self.view.handleOutput(.showActivity(false))
            switch result {
            case .success(let response):
                print(response)
                guard let data = response.data else { return }
                self.view.handleOutput(.showUrl(data))
            case .wrong(let response):
                print("wrong: ", response)
                self.view.handleOutput(.showMessage(response.message ?? ""))

            case .failure(let error):
                print("fail: ", error)
                self.view.handleOutput(.showNetworkError(.storeOrder))

            default: break
            }
        }
    }
}
