//
//  CheckoutMyBagCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

final class CheckoutMyBagCell: UICollectionViewCell {
    
    static let ID: String = "CheckoutMyBagCell"
    
    var data: OrderImage! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var photoView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubviews()
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.photoView.image = nil
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        photoView.anchor(.fillSuperview())
        
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(photoView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear

    }
    
    private func configure() {
        if let imageUrl = URL(string: data.image ?? "") {
            self.photoView.sd_setImage(with: imageUrl)
        }
    }
}
