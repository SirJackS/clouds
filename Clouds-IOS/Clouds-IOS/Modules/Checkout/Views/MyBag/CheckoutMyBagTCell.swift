//
//  CheckoutMyBagTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

//protocol OrderDetailMainTCellDelegate: class {
//    func addBagButtonClick()
//    func infoButtonClick()
//}


final class CheckoutMyBagTCell: UITableViewCell {

    static let ID: String = "CheckoutMyBagTCell"
    
//    weak var delegate: OrderDetailMainTCellDelegate?
    

    var data: [OrderImage]! {
        didSet {
            self.productCollection.reloadData()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var collectionBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "My Bag".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var productCollection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        layout.itemSize = .init(width: UIScreen.main.bounds.width*(80/375), height: UIScreen.main.bounds.width*(80/375))
        //layout.headerReferenceSize = CGSize(width: 0, height: 100);
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.clipsToBounds = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
        collectionView.register(CheckoutMyBagCell.self, forCellWithReuseIdentifier: CheckoutMyBagCell.ID)
        return collectionView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(24), .leading(16), .trailing(-16), .bottom(-24))
        
        titleLabel.anchor(.top(), .leading(),.trailing(-16))
        
        collectionBackView.anchor(
            .top(titleLabel.bottomAnchor, constant: 4),
            .leading(), .trailing(), .bottom())
        
        productCollection.anchor(
            .top(12), .bottom(-12),
            .leading(), .trailing(), .height(UIScreen.main.bounds.width*(80/375)))
        
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(collectionBackView)
        
        collectionBackView.addSubview(productCollection)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        
        
    }
    
    // MARK: - Action
    
//    @objc private func addBagButtonTouchUp() {
//        self.delegate?.addBagButtonClick()
//    }
//
//    @objc private func infoButtonTouchUp() {
//        self.delegate?.infoButtonClick()
//    }
}

// MARK: - UICollectionView datasource and delegate

extension CheckoutMyBagTCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case productCollection:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CheckoutMyBagCell.ID, for: indexPath) as? CheckoutMyBagCell else { return UICollectionViewCell() }
            cell.data = data[indexPath.row]
            return cell
        default:
            return UICollectionViewCell.init()
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        switch collectionView {
//        case categoryCollection:
//            self.delegate?.didSelectCategory(index: indexPath.row)
//
//        default:
//            break
//        }
//        collectionView.deselectItem(at: indexPath, animated: false)
//    }
}
