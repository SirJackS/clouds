//
//  CheckoutCommentTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 03.01.21.
//

import UIKit

protocol CheckoutCommentTCellDelegate: class {
    func payButtonClick(_ note: String)
}


final class CheckoutCommentTCell: UITableViewCell {

    static let ID: String = "CheckoutCommentTCell"
    
    weak var delegate: CheckoutCommentTCellDelegate?
    
    var data: String! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Leave Comment to Courier"
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var textBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        //view.text = termAndConditions
        textView.text = "Write here"
        textView.textColor = UIColor.black.withAlphaComponent(0.38)
        textView.textAlignment = .left
        textView.font = UIFont(weight: .regular, size: 16)
        textView.backgroundColor = .clear
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.sizeToFit()
        textView.isScrollEnabled = true
        textView.delegate = self
        return textView
    }()
    
    private lazy var payButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Pay".uppercased(), for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
//        button.contentHorizontalAlignment = .center
        
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        button.addTarget(self, action: #selector(payButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(), .leading(16), .trailing(-16), .bottom(-24))
        
        titleLabel.anchor(.top(), .leading(), .trailing())
        
        textBackView.anchor(
            .top(titleLabel.bottomAnchor, constant: 4), .leading(), .trailing(),
            .height(121))
        
        textView.anchor(.fillSuperviewPadding(8))
        
        payButton.anchor(
            .top(textBackView.bottomAnchor, constant: 32),
            .leading(), .trailing(), .bottom(-120))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(textBackView)
        bodyView.addSubview(payButton)
        
        textBackView.addSubview(textView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        
    }
    
    private func configure() {
        
    }
    
    // MARK: - Action

    @objc private func payButtonTouchUp() {
        self.delegate?.payButtonClick(self.textView.text)
    }
}

extension CheckoutCommentTCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.black.withAlphaComponent(0.38) {
            textView.text = nil
            textView.textColor = UIColor.black.withAlphaComponent(0.87)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write here"
            textView.textColor = UIColor.black.withAlphaComponent(0.38)
        }
    }
}
