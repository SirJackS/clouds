//
//  CheckoutRewardTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

protocol CheckoutRewardTCellDelegate: class {
    func addGiftcardClick()
    func giftcardPickerClick(_ giftcarts: [GiftCard])
    func rewardDidChange(_ num: Int)
}


final class CheckoutRewardTCell: UITableViewCell {

    static let ID: String = "CheckoutRewardTCell"
    
    weak var delegate: CheckoutRewardTCellDelegate?
    
    var data: [GiftCard]! {
        didSet {
            self.configure()
        }
    }
    
    var selectedData: GiftCard? {
        didSet {
            if self.rewardBool {
                self.mainLabel.text = String(format: "Rewards *".localized(), self.totalReward)
            } else {
                if let bonus = selectedData?.bonus {
                    self.mainLabel.text = String(format: "Gift Card * Azn".localized(), bonus)
                }
            }
        }
    }
    
    var totalReward: Int = 0 {
        didSet {
            self.bottomView.countLabel.text = "/\(totalReward)"
            self.bottomView.totalReward = totalReward
        }
    }
    
    var rewardBool: Bool = false {
        didSet {
            self.bottomView.isHidden = !rewardBool
            if self.rewardBool {
                self.mainLabel.text = String(format: "Rewards *".localized(), self.totalReward)
            } 
        }
    }
    
    private lazy var viewStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [topView,bottomView])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 16
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    //top view
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var addEditButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add/Edit".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        button.backgroundColor = UIColor.clear
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.contentHorizontalAlignment = .trailing
        button.anchor(.height(26))
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        button.addTarget(self, action: #selector(addGiftcardTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var downButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.contentHorizontalAlignment = .trailing
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        button.addTarget(self, action: #selector(giftcardPickerTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Rewards / Gift Card Number".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var mainLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    //bottom view
    private lazy var bottomView: CheckoutRewardView = {
        let view = CheckoutRewardView()
        view.clipsToBounds = true
        view.delegate = self
        view.backgroundColor = UIColor.clear
        view.isHidden = true
        return view
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(giftcardPickerTouchUp))
        tap.delegate = self
        return tap
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        viewStack.anchor(.top(), .leading(16), .trailing(-16), .bottom(-24))
        
        titleLabel.anchor(
            .top(), .leading(), .widthGreater(50),
            .trailing(addEditButton.leadingAnchor, constant: -8))
        
        addEditButton.anchor(
            .top(), .trailing(), .widthGreater(50))
        
        downButton.anchor(
            .top(titleLabel.bottomAnchor, constant: 3),
            .leading(), .trailing(), .bottom(), .height(56))
        
        mainLabel.anchor(
            .top(downButton.topAnchor), .bottom(downButton.bottomAnchor),
            .trailing(downButton.trailingAnchor, constant: -52), .leading(downButton.leadingAnchor, constant: 16))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(viewStack)
        
        topView.addSubview(titleLabel)
        topView.addSubview(addEditButton)
        topView.addSubview(downButton)
        topView.addSubview(mainLabel)
        
        self.mainLabel.addGestureRecognizer(tapGesture)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        
        
    }
    
    // MARK: - Action
    
    @objc private func addGiftcardTouchUp() {
        self.delegate?.addGiftcardClick()
    }
    
    @objc private func giftcardPickerTouchUp() {
        self.delegate?.giftcardPickerClick(data)
    }
}

extension CheckoutRewardTCell: CheckoutRewardViewDelegate {
    
    func rewardDidChange(_ num: Int) {
        self.delegate?.rewardDidChange(num)
    }
}
