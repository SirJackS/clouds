//
//  CheckoutRewardView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

protocol CheckoutRewardViewDelegate: class {
    func rewardDidChange(_ num: Int)
}

//extension CustomDateViewDelegate {
//    func downButtonClick(_ textView: CustomDateView) {}
//}

final class CheckoutRewardView: UIView {

    weak var delegate: CheckoutRewardViewDelegate?
    
    var totalReward: Int = 0
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var mainBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Rewards".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    lazy var mainText: UITextField = {
        let textField = UITextField()
        textField.tintColor = .clear
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .left
        textField.font = UIFont(weight: .medium, size: 16)
        textField.text = "0"
        textField.keyboardType = .numberPad
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        return textField
    }()
    
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .right
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var lineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()
    
    private lazy var infoBackView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "info-icon")
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 16, height: 16))
        return imageView
    }()
    
    private lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.text = "1 Reward = 1.00 Azn".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
    
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        bodyView.anchor(.fillSuperview())
        
        titleLabel.anchor(.leading(), .trailing(), .top(), .height(16))
        
        mainBackView.anchor(
            .top(titleLabel.bottomAnchor, constant: 4),
            .leading(), .trailing(), .height(56))
        
        lineView.anchor(
            .leading(), .trailing(), .height(0.3),
            .bottom(mainBackView.bottomAnchor))
        
        mainText.anchor(
            .centerY(), .leading(12), .height(20),
            .trailing(countLabel.leadingAnchor, constant: -8))
        
        countLabel.anchor(
            .centerY(), .height(26), .trailing(-16))
        
        infoBackView.anchor(
            .top(mainBackView.bottomAnchor, constant: 6),
            .leading(), .trailing(), .bottom(), .height(32))
        
        iconView.anchor(
            .centerY(), .leading(12), .trailing(infoLabel.leadingAnchor, constant: -8))
        
        infoLabel.anchor(.trailing(-12), .centerY(), .height(20))
        
        super.updateConstraints()
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(mainBackView)
        bodyView.addSubview(infoBackView)
        
        mainBackView.addSubview(countLabel)
        mainBackView.addSubview(mainText)
        mainBackView.addSubview(lineView)
        
        infoBackView.addSubview(iconView)
        infoBackView.addSubview(infoLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }

    // MARK: - Action
    
    @objc func textFieldDidChange() {
        if let number = Int(self.mainText.text ?? "") {
            if number <= totalReward {
                self.delegate?.rewardDidChange(number)
            }
        } else {
            self.mainText.text = ""
        }
    }
    
}
