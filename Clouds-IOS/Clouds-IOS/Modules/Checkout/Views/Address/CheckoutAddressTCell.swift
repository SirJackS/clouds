//
//  CheckoutAddressTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

protocol CheckoutAddressTCellDelegate: class {
    func newAddressButtonClick()
    func addressPickerClick(_ addresses: [AddressMain])
}


final class CheckoutAddressTCell: UITableViewCell {

    static let ID: String = "CheckoutAddressTCell"
    
    weak var delegate: CheckoutAddressTCellDelegate?
    
    var data: [AddressMain]! {
        didSet {
            self.configure()
        }
    }
    
    var selectedData: AddressMain? {
        didSet {
            self.mainLabel.text = "\(selectedData?.city?.name ?? ""), \(selectedData?.address ?? "")" 
        }
    }
    var indexPath: IndexPath!
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var addEditButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add/Edit".localized(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.6), for: .normal)
        button.backgroundColor = UIColor.clear
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.contentHorizontalAlignment = .trailing
        button.anchor(.height(26))
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        button.addTarget(self, action: #selector(newAddressButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var downButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.contentHorizontalAlignment = .trailing
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        button.addTarget(self, action: #selector(addressPickerTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Shipping Address".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var mainLabel: UILabel = {
        let label = UILabel()
        label.text = "Choose".localized()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        return label
    }()
    
//    lazy var mainText: UITextField = {
//        let textField = UITextField()
//        textField.text = "Choose"
//        textField.tintColor = .clear
//        textField.textColor = UIColor.black.withAlphaComponent(0.60)
//        textField.textAlignment = .left
//        textField.font = UIFont(weight: .regular, size: 16)
//        textField.placeholder = ""
//        return textField
//    }()
    
    
    private lazy var addButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Add New Address".localized().uppercased(), for: .normal)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.60), for: .normal)
        button.setImage(UIImage(named: "add-circle-icon"), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        //button.setupBorder(width: 1, color: UIColor.black.withAlphaComponent(0.87))
        button.addDashedBorder()
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(56))
//        button.contentHorizontalAlignment = .center

        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
//        button.addTarget(self, action: #selector(addButtonTouchUp), for: .touchUpInside)
        button.isHidden = true
        return button
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let tap = UITapGestureRecognizer(target: self, action: #selector(addressPickerTouchUp))
        tap.delegate = self
        return tap
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(.top(), .leading(16), .trailing(-16), .bottom(-24))
        
        titleLabel.anchor(
            .top(), .leading(), .widthGreater(50),
            .trailing(addEditButton.leadingAnchor, constant: -8))
        
        addEditButton.anchor(
            .top(), .trailing(), .widthGreater(50))
        
        downButton.anchor(
            .top(titleLabel.bottomAnchor, constant: 3),
            .leading(), .trailing(), .bottom(), .height(56))
        
        addButton.anchor(
            .top(titleLabel.bottomAnchor, constant: 3),
            .leading(), .trailing(), .bottom(), .height(56))
        
        mainLabel.anchor(
            .top(downButton.topAnchor), .bottom(downButton.bottomAnchor),
            .trailing(downButton.trailingAnchor, constant: -52), .leading(downButton.leadingAnchor, constant: 16))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(addEditButton)
        bodyView.addSubview(addButton)
        bodyView.addSubview(downButton)
        bodyView.addSubview(mainLabel)
        
        self.mainLabel.addGestureRecognizer(tapGesture)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        let count = data.count
        
        if count == 0 {
            addButton.isHidden = false
            downButton.isHidden = true
            mainLabel.isHidden = true
        } else if count > 0 {
            mainLabel.text = data[0].address
            selectedData = data[0]
            addButton.isHidden = true
            downButton.isHidden = false
            mainLabel.isHidden = false
        }
    }
    
    // MARK: - Action
    
    @objc private func addressPickerTouchUp() {
        self.delegate?.addressPickerClick(data)
    }

    @objc private func newAddressButtonTouchUp() {
        self.delegate?.newAddressButtonClick()
    }
}


