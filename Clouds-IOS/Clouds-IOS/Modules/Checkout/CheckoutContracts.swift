//
//  CheckoutContracts.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import Foundation

// MARK: - Presenter

protocol CheckoutPresenterProtocol: class {
    func load()
    func storeOrder(cartId: Int,addressId: Int,note: String,giftCard: Int?,total: Float,reward: Int?)
}

enum CheckoutPresenterOutput {
    case showMessage(String)
    case showOrder(Order)
    case showReward(Reward)
    case showGiftcards([GiftCard])
    case showAddressList([AddressMain])
    case showActivity(Bool)
    case showNetworkError(NetworkErrorType)
    case showVC
    case showUrl(RedirectUrl)
}


// MARK: - View

protocol CheckoutViewProtocol: class {
    func handleOutput(_ output: CheckoutPresenterOutput)
}
