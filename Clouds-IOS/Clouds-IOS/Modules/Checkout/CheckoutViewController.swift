//
//  CheckoutViewController.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit
import SDWebImage

final class CheckoutViewController: UIViewController, CheckoutViewProtocol {
 
    var presenter: CheckoutPresenterProtocol!
    
    private lazy var navBarView: BTNavbarView = {
        let view = BTNavbarView(title: "Checkout".localized())
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .white
        tableView.separatorStyle = .none
        tableView.register(CheckoutMyBagTCell.self, forCellReuseIdentifier: CheckoutMyBagTCell.ID)
        tableView.register(CheckoutAddressTCell.self, forCellReuseIdentifier: CheckoutAddressTCell.ID)
        tableView.register(CheckoutRewardTCell.self, forCellReuseIdentifier: CheckoutRewardTCell.ID)
        tableView.register(TotalTCell.self, forCellReuseIdentifier: TotalTCell.ID)
        tableView.register(CheckoutCommentTCell.self, forCellReuseIdentifier: CheckoutCommentTCell.ID)
//        tableView.register(OrderDetailTotalTCell.self, forCellReuseIdentifier: OrderDetailTotalTCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.isHidden = true
        return tableView
    }()
    
    
    // MARK: - Data
    private var order: Order?
    private var note: String = ""
    private var totalReward: Int = 0
    private var selectedReward: Int = 0
    private var rewardSelectBool: Bool = false
    private var selectedAddress: AddressMain?
    private var selectedGiftcard: GiftCard?
    private var items: [OrderImage] = []
    private var addresses: [AddressMain] = []
    private var giftcards: [GiftCard] = []
    private var orderTotal: [OrderTotal] = []
    private var popRecognizer: InteractivePopRecognizer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addSubviews()
        self.setupUI()
        self.setInteractiveRecognizer()
        
        self.presenter.load()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        tableView.anchor(.fillSuperview())
        
    }

    func handleOutput(_ output: CheckoutPresenterOutput) {
        switch output {
        case .showUrl(let redirectUrl):
            let viewController = PaymentBuilder.make(url: redirectUrl.redirectUrl)
            self.navigationController?.pushViewController(viewController, animated: true)
        case .showVC:
            self.tableView.isHidden = false
        case .showOrder(let data):
            self.order = data
            guard let orderItems = data.items else { return }
            self.items = orderItems
            self.orderTotal = [
                OrderTotal(name: "Sub-Total:".localized(), price: data.subTotal),
                OrderTotal(name: "Delivery:".localized(), price: data.delivery ?? 0.0),
                OrderTotal(name: "Total:".localized(), price: data.total),
            ]
            self.tableView.reloadData()
        case .showAddressList(let addresses):
            self.addresses = addresses
            self.selectedAddress = addresses[0]
            self.tableView.reloadData()
        case .showReward(let reward):
            self.totalReward = reward.total
        case .showGiftcards(let giftcards):
            self.giftcards = giftcards
        case .showMessage(let message):
            if message != "" {
                self.showMessage(message)
            }
        case .showActivity(let bool):
            self.showLoader(bool)
        case .showNetworkError(let type):
            self.showNetworkError { (result) in
                if result {
                    switch type {
                    case .getCheckout,.getAllAddresses,.getGiftcards,.getRewards:
                        self.presenter.load()
                    case .storeOrder:
                        self.payButtonClick()
                    default:
                        return
                    }
                }
            }
        }
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.view.addSubview(tableView)
        
        self.updateViewConstraints()
    }
    
    private func setupUI() {
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.view.backgroundColor = UIColor.white
        self.navigationItem.titleView = navBarView
    }
    
    private func setInteractiveRecognizer() {
        guard let controller = navigationController else { return }
        popRecognizer = InteractivePopRecognizer(controller: controller)
        controller.interactivePopGestureRecognizer?.delegate = popRecognizer
    }
    
    private func openAddressPicker(addresses: [AddressMain]) {
        let alert = UIAlertController(title: "Addresses".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedAddress: AddressMain?
        if addresses.count > 0 {
            selectedAddress = addresses[0]
        }
        let frameSizes: [CGFloat] = (0..<addresses.count).map { CGFloat($0) }
        let pickerViewValues: [[String]] = [addresses.map { "\($0.city?.name ?? ""), \($0.address ?? "")"}]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
                                                       
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if addresses.count > 0 {
                selectedAddress = addresses[index.row]
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                guard let cell = self.tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? CheckoutAddressTCell else { return }
                if let selected = selectedAddress {
                    cell.selectedData = selected
                    self.selectedAddress = selected
                } else {
                    cell.selectedData = addresses[0]
                    self.selectedAddress = addresses[0]
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func openGiftcardPicker(giftcards: [GiftCard]) {
        let alert = UIAlertController(title: "Giftcards".localized(), message: nil, preferredStyle: .actionSheet)
        var selectedIndex: Int = 0
        let frameSizes: [CGFloat] = (0...giftcards.count).map { CGFloat($0) }
        var data = giftcards.map { "Gift Card \($0.bonus)Azn"}
        data.append("Rewards \(self.totalReward)")
        let pickerViewValues: [[String]] = [data]
        let pickerViewSelectedValue: PickerViewViewController.Index = (column: 0, row: frameSizes.firstIndex(of: CGFloat(frameSizes.count)) ?? 0)
              
        guard let cell = self.tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? CheckoutRewardTCell else { return }
        
        alert.addPickerView(values: pickerViewValues, initialSelection: pickerViewSelectedValue) { vc, picker, index, values in
            if giftcards.count > 0 {
                selectedIndex = index.row
            }
        }
        alert.addAction(UIAlertAction(title: "done".localized().capitalized, style: .default, handler: { (action) in
            if action.style == .default {
                if selectedIndex == giftcards.count {
                    self.rewardSelectBool = true
                    cell.rewardBool = true
                } else {
                    self.rewardSelectBool = false
                    cell.rewardBool = false
                    self.selectedGiftcard = giftcards[selectedIndex]
                    cell.selectedData = giftcards[selectedIndex]
                }
                self.tableView.layoutIfNeeded()
                self.tableView.reloadData()
            }
        }))
        alert.addAction(UIAlertAction(title: "cancel".localized().capitalized, style: .cancel, handler: { (actionn) in
            if actionn.style == .cancel {
                alert.dismiss(animated: true, completion: nil)
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func payButtonClick() {
        guard let cartId = order?.id else { return }
        guard let total = order?.total else { return }
        guard let addressId = selectedAddress?.id else { return self.showMessage("Address not selected".localized())}
        if rewardSelectBool {
            self.presenter.storeOrder(cartId: cartId, addressId: addressId, note: self.note, giftCard: nil, total: total, reward: self.selectedReward)
        } else {
            guard let giftCard = selectedGiftcard?.id else { return self.showMessage("Gift card not selected".localized())}
            self.presenter.storeOrder(cartId: cartId, addressId: addressId, note: note, giftCard: giftCard, total: total, reward: nil)
        }
    }
}


extension CheckoutViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutMyBagTCell.ID, for: indexPath) as? CheckoutMyBagTCell else { return UITableViewCell() }
            cell.data = items
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutAddressTCell.ID, for: indexPath) as? CheckoutAddressTCell else { return UITableViewCell() }
            cell.data = addresses
            if let selected = selectedAddress {
                cell.selectedData = selected
            }
            cell.delegate = self
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutRewardTCell.ID, for: indexPath) as? CheckoutRewardTCell else { return UITableViewCell() }
            cell.data = giftcards
            cell.totalReward = totalReward
            if let selected = selectedGiftcard {
                cell.selectedData = selected
            }
            cell.delegate = self
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalTCell.ID, for: indexPath) as? TotalTCell else { return UITableViewCell() }
            cell.data = orderTotal
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutCommentTCell.ID, for: indexPath) as? CheckoutCommentTCell else { return UITableViewCell() }
            cell.delegate = self
            return cell
        default: break
        }
        return UITableViewCell()
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let viewController = ProductDetailBuilder.make()
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
}

extension CheckoutViewController: BTNavbarViewDelegate {
    func backButtonClick() {
        self.popVC()
    }
}

extension CheckoutViewController: CheckoutAddressTCellDelegate {
    func newAddressButtonClick() {
        let viewController = AddressDetailBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func addressPickerClick(_ addresses: [AddressMain]) {
        self.openAddressPicker(addresses: addresses)
    }
}

extension CheckoutViewController: CheckoutRewardTCellDelegate {
    func rewardDidChange(_ num: Int) {
        self.selectedReward = num
    }
    
    func giftcardPickerClick(_ giftcarts: [GiftCard]) {
        self.openGiftcardPicker(giftcards: giftcards)
    }
    
    func addGiftcardClick() {
        let viewController = GiftCardAddBuilder.make()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension CheckoutViewController: CheckoutCommentTCellDelegate {
    func payButtonClick(_ note: String) {
        self.note = note
        self.payButtonClick()
    }
}
