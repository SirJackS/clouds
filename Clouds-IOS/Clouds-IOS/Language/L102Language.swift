//
//  L102Language.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 12.04.21.
//

import Foundation

let APPLE_LANGUAGE_KEY = "AppleLanguages"

class L102Language {
    
    class func currentAppleLanguage() -> String{
        let userdef = UserDefaults.standard
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        let current = langArray.firstObject as! String
        return current
    }
    
    class func setAppleLAnguageTo(lang: String) {
        let userdef = UserDefaults.standard
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        userdef.synchronize()
    }
}
