//
//  DetailInfoButton.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 13.03.21.
//

import UIKit

final class DetailInfoButton: UIButton {

    var value: String? {
        didSet {
//            self.titleLabel.isHidden = value == nil
            self.nameLabel.text = value
        }
    }
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleToFill//.scaleAspectFill
        imageView.image = UIImage(named: "down-arrow-icon")
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.font = UIFont(weight: .regular, size: 16)
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        return view
    }()

    required init(name: String) {
        super.init(frame: .zero)
        
        self.nameLabel.text = name

        self.addSubviews()
        self.setupUI()
    }
    
    override func updateConstraints() {
        
        self.nameLabel.anchor(.leading(), .trailing(iconView.leadingAnchor, constant: -16), .centerY(), .height(20))
        
        self.iconView.anchor(.size(width: 24, height: 24), .trailing(-16), .centerY())
        
        self.lineView.anchor(.leading(), .trailing(), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            self.alpha = newValue ? 0.5 : 1.0
            super.isHighlighted = newValue
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.addSubview(self.iconView)
        self.addSubview(self.nameLabel)
        self.addSubview(self.lineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
}
