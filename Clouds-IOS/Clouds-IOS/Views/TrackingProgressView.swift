//
//  TrackingProgressView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit

final class TrackingProgressView: UIView {
    
    var progress = "Empty" {
        didSet {
            self.configureProgress()
        }
    }
    
    private lazy var topView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = .clear
        view.anchor(.height(18))
        return view
    }()
    
    private lazy var progressView: UIProgressView = {
        let progressView = UIProgressView(progressViewStyle: .bar)
        progressView.trackTintColor = UIColor.black.withAlphaComponent(0.38)
        progressView.tintColor = UIColor.secondaryColor
//        view.clipsToBounds = true
//        view.backgroundColor = .clear
//        view.anchor(.height(18))
        return progressView
    }()
    
    private lazy var checkStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [check1View, check2View, check3View, check4View])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        //stackView.spacing = 0
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var check1View: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "track-uncheck-icon")
        imageView.backgroundColor = .clear
        //imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 19, height: 18))
        return imageView
    }()
    
    private lazy var check2View: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "track-uncheck-icon")
        imageView.backgroundColor = .clear
        //imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 19, height: 18))
        return imageView
    }()
    
    private lazy var check3View: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "track-uncheck-icon")
        imageView.backgroundColor = .clear
        //imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 19, height: 18))
        return imageView
    }()
    
    private lazy var check4View: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "track-uncheck-icon")
        imageView.backgroundColor = .clear
        //imageView.tintColor = UIColor.black.withAlphaComponent(0.6)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.anchor(.size(width: 19, height: 18))
        return imageView
    }()
    
    
    private lazy var labelsStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [text1Label, text2Label, text3Label, text4Label])
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        stackView.clipsToBounds = true
        return stackView
    }()
    
    private lazy var text1Label: UILabel = {
        let label = UILabel()
        label.text = "Confirmed"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var text2Label: UILabel = {
        let label = UILabel()
        label.text = "Prepared"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var text3Label: UILabel = {
        let label = UILabel()
        label.text = "Picked"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var text4Label: UILabel = {
        let label = UILabel()
        label.text = "Delivered"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .right
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.anchor(.height(16))
        return label
    }()
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(42))
        
        labelsStack.anchor(.leading(), .trailing(), .bottom(-2))
        
        progressView.anchor(.leading(9), .trailing(-9), .centerY())
        
        checkStack.anchor(.centerY(), .leading(), .trailing())
        
        topView.anchor(
            .leading(11), .trailing(-11),
            .top(2), .bottom(labelsStack.topAnchor, constant: -4))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 56)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(topView)
        addSubview(labelsStack)
        
        topView.addSubview(progressView)
        topView.addSubview(checkStack)
        
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }
    
    private func configureProgress() {
        let progressWidth = (UIScreen.main.bounds.width - 72)
        let portionWidth = progressWidth/3
        
        switch progress {
        case "Confirmed":
            check1View.image = UIImage(named: "track-check-icon")
            check2View.image = UIImage(named: "track-uncheck-icon")
            check3View.image = UIImage(named: "track-uncheck-icon")
            check4View.image = UIImage(named: "track-uncheck-icon")
            progressView.setProgress(Float(0.0), animated: false)
        case "Prepared":
            check1View.image = UIImage(named: "track-check-icon")
            check2View.image = UIImage(named: "track-check-icon")
            check3View.image = UIImage(named: "track-uncheck-icon")
            check4View.image = UIImage(named: "track-uncheck-icon")
            progressView.setProgress(Float(portionWidth/progressWidth), animated: false)
        case "Picked":
            check1View.image = UIImage(named: "track-check-icon")
            check2View.image = UIImage(named: "track-check-icon")
            check3View.image = UIImage(named: "track-check-icon")
            check4View.image = UIImage(named: "track-uncheck-icon")
            progressView.setProgress(Float(portionWidth/progressWidth)*2.0, animated: false)
        case "Delivered":
            check1View.image = UIImage(named: "track-check-icon")
            check2View.image = UIImage(named: "track-check-icon")
            check3View.image = UIImage(named: "track-check-icon")
            check4View.image = UIImage(named: "track-check-icon")
            progressView.setProgress(Float(portionWidth/progressWidth)*3.0, animated: false)
        default:
            check1View.image = UIImage(named: "track-uncheck-icon")
            check2View.image = UIImage(named: "track-uncheck-icon")
            check3View.image = UIImage(named: "track-uncheck-icon")
            check4View.image = UIImage(named: "track-uncheck-icon")
            progressView.setProgress(Float(0.0), animated: false)
        }
    }

}
