//
//  BTBNavbarView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import UIKit

protocol BTBNavbarViewDelegate: class {
    func backButtonClick()
    func rightButtonClick()
}

final class BTBNavbarView: UIView {

    weak var delegate: BTBNavbarViewDelegate?
    
    private lazy var backButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
//        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 48, height: 24))
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(backButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    lazy var rightButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
//        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.anchor(.size(width: 24, height: 24))
        button.addTarget(self, action: #selector(rightButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 20)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var navbarLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.38))
    }()

    init(title: String, backIcon: String = "back-arrow-icon", rightIcon: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
        backButton.setImage(UIImage(named: backIcon), for: .normal)
        rightButton.setImage(UIImage(named: rightIcon), for: .normal)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        anchor(.height(44))
        
        backButton.anchor(.leading(8), .centerY())
        
        rightButton.anchor(.trailing(-8), .centerY())
        
        titleLabel.anchor(.centerX(), .centerY())
        
        navbarLineView.anchor( .leading(), .trailing(), .bottom(), .height(0.3))
        
        super.updateConstraints()
    }
    
    override var intrinsicContentSize: CGSize {
        let width = UIScreen.main.bounds.width
        return .init(width: width, height: 44)
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(backButton)
        addSubview(titleLabel)
        addSubview(rightButton)
        addSubview(navbarLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func backButtonTouchUp() {
        self.delegate?.backButtonClick()
    }
    
    @objc private func rightButtonTouchUp() {
        self.delegate?.rightButtonClick()
    }
}

