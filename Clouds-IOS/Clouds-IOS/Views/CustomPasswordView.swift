//
//  CustomPasswordView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.01.21.
//

import UIKit

final class CustomPasswordView: UIView {
    
    var errorText: String! {
        didSet {
            self.errorLabel.text = errorText
        }
    }
    
    var isError: Bool = false {
        didSet {
            lineView.backgroundColor = isError ? UIColor(hexString: "E52836") : UIColor(hexString: "000000").withAlphaComponent(0.25)
            errorlabelView.isHidden = !isError
            emptyView.isHidden = isError
            if !isError {
                errorLabel.text = ""
            }
        }
    }
    
    lazy var showButton: UIButton = {
        let button = UIButton(type: .custom)
        button.backgroundColor = .clear
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.setImage(UIImage(named: "hidePass-icon"), for: .normal)
        button.setImage(UIImage(named: "showPass-icon"), for: .selected)
        button.anchor(.size(width: 24, height: 24))
        button.contentHorizontalAlignment = .center
        button.adjustsImageWhenHighlighted = false
        button.addTarget(self, action: #selector(showButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var errorStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [lineView,spaceView,emptyView,errorlabelView])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 0
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    private lazy var spaceView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.clear
        view.anchor(.height(4))
        return view
    }()
    
    private lazy var emptyView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.clear
        view.anchor(.height(8))
        return view
    }()
    
    private lazy var errorlabelView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.clear
        view.anchor(.height(16))
        view.isHidden = true
        view.backgroundColor = .clear
        return view
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "EE0005")
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
//        label.isHidden = true
        label.anchor(.height(16))
        return label
    }()
    
    lazy var mainText: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .left
        textField.delegate = self
        textField.font = UIFont(weight: .regular, size: 16)
        textField.placeholder = ""
        textField.isSecureTextEntry = true
        return textField
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.black.withAlphaComponent(0.25)
        view.anchor(.height(0.5))
        return view
    }()

    init(title: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        titleLabel.anchor(.leading(), .trailing(), .top(), .height(16))
        
        mainText.anchor(
            .leading(), .trailing(),
            .top(titleLabel.bottomAnchor, constant: 4), .height(32))
        
        errorStack.anchor(
            .top(mainText.bottomAnchor),
            .leading(), .trailing(), .bottom())
        
//        lineView.anchor( .leading(), .trailing(), .height(0.3))
//
        errorLabel.anchor(.leading(4), .trailing(-4), .top() ,.bottom())
        
        showButton.anchor(
            .centerY(mainText.centerYAnchor),
            .trailing(mainText.trailingAnchor, constant: -24))
        
        super.updateConstraints()
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(mainText)
        addSubview(errorStack)
//        addSubview(lineView)
        addSubview(showButton)
        
        errorlabelView.addSubview(errorLabel)
//        errorView.addSubview(errorLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    // MARK: - Action
    
    @objc private func showButtonTouchUp() {
        self.showButton.isSelected = !self.showButton.isSelected
        self.mainText.isSecureTextEntry = !self.showButton.isSelected
    }
    
}

extension CustomPasswordView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor(hexString: "2F69FF")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
    }
}
