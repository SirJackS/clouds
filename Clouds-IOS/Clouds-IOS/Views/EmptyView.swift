//
//  EmptyView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import UIKit

protocol EmptyViewDelegate: class {
    func startButtonClick()
}

final class EmptyView: UIView {

    weak var delegate: EmptyViewDelegate?
    
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.tintColor = UIColor.black.withAlphaComponent(0.60)
        imageView.contentMode = .scaleAspectFill
        imageView.anchor(.size(width: 32, height: 32))
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .medium, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .center
        label.font = UIFont(weight: .regular, size: 14)
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var startButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.87)
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        return button
    }()
    
    init(icon: String, title: String, desc: String,forButton name: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
        descLabel.text = desc
        iconView.image = UIImage(named: icon)
        startButton.setTitle(name.uppercased(), for: .normal)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        iconView.anchor(.top(40), .centerX())
        
        titleLabel.anchor(
            .top(iconView.bottomAnchor, constant: 16),
            .leading(24), .trailing(-24))
        
        descLabel.anchor(
            .top(titleLabel.bottomAnchor, constant: 8),
            .leading(24), .trailing(-24))
        
        startButton.anchor(
            .top(titleLabel.bottomAnchor, constant: 132),
            .leading(16), .trailing(-16), .bottom(-24))
        
        super.updateConstraints()
    }
    
//    override var intrinsicContentSize: CGSize {
//        let width = UIScreen.main.bounds.width
//        return .init(width: width, height: 56)
//    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(iconView)
        addSubview(titleLabel)
        addSubview(descLabel)
        addSubview(startButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func backButtonTouchUp() {
        self.delegate?.startButtonClick()
    }
}
