//
//  KitButtonView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.12.20.
//

import UIKit

protocol KitButtonViewDelegate: class {
    func kitButtonClick()
}

final class KitButtonView: UIView {

    weak var delegate: KitButtonViewDelegate?
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.anchor(.size(width: 24, height: 24))
        return imageView
    }()
    
    lazy var kitButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(UIColor.black.withAlphaComponent(0.87), for: .normal)
        button.tintColor = UIColor.black.withAlphaComponent(0.87)
        button.backgroundColor = UIColor.clear
        button.setupBorder(width: 1, color: UIColor.black.withAlphaComponent(0.87))
        button.titleLabel?.font = UIFont(weight: .medium, size: 14)
        button.anchor(.height(46))
        button.contentHorizontalAlignment = .center
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //change image and text place
//        button.semanticContentAttribute = UIApplication.shared
//            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0)
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
//        button.addTarget(self, action: #selector(pieceButtonTouchUp), for: .touchUpInside)
        return button
    }()

    init(title: String, icon: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        kitButton.setTitle(title.uppercased(), for: .normal)
        imageView.image = UIImage(named: icon)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        imageView.anchor(.leading(24), .centerY())
        
        kitButton.anchor(.fillSuperview())
        
        super.updateConstraints()
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(imageView)
        addSubview(kitButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }

    
    // MARK: - Action
    
    @objc private func kitButtonTouchUp() {
        self.delegate?.kitButtonClick()
    }
}
