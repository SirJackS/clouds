//
//  CustomPickerView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import UIKit

class CustomPickerView: UIPickerView {

    var customBackgroundColor = UIColor.white

    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)

        if newWindow != nil {
            inputView?.backgroundColor = customBackgroundColor
        }
    }
}

class CustomDatePickerView: UIDatePicker {

    var customBackgroundColor = UIColor.white

    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)

        if newWindow != nil {
            inputView?.backgroundColor = customBackgroundColor
        }
    }
}
