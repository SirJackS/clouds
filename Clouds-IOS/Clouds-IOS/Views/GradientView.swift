//
//  GradientView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 16.01.21.
//

import UIKit

class GradientView: UIView {

    var topColor: UIColor = UIColor.black.withAlphaComponent(0.0) {
        didSet {
            self.setNeedsLayout()
        }
    }
    var bottomColor: UIColor = UIColor.black.withAlphaComponent(0.7)   {
        didSet {
            self.setNeedsLayout()
        }
    }

    override func layoutSubviews() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        gradientLayer.locations =  [0.0, 1.0]
//        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
