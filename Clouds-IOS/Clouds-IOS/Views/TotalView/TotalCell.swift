//
//  OrderDetailTotalCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import UIKit

final class TotalCell: UITableViewCell {

    static let ID: String = "TotalCell"

    var data: OrderTotal! {
        didSet {
            self.configure()
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var descLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .right
        label.font = UIFont(weight: .regular, size: 16)
        label.numberOfLines = 1
        label.anchor(.height(22))
        return label
    }()
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        bodyView.anchor(
            .top(2), .leading(), .trailing(),
            .bottom(-2))
        
        titleLabel.anchor(
            .leading(), .trailingGreater(descLabel.leadingAnchor, constant: -16),
            .centerY(), .widthGreater(50))
        
        descLabel.anchor(.trailing(), .centerY(), .widthGreater(50))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.titleLabel.text = ""
        self.descLabel.text = ""
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(descLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
    }
    
    private func configure() {
        self.titleLabel.text = data.name
        self.descLabel.text = "\(data.price) AZN"   
    }
}
