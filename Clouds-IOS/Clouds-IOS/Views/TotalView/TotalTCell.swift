//
//  OrderDetailTotalTCell.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import UIKit

final class TotalTCell: UITableViewCell {

    static let ID: String = "TotalTCell"

    var data: [OrderTotal]! = [] {
        didSet {
            self.configure()
            tableView.reloadData()
        }
    }
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor(hexString: "EFEFEF").withAlphaComponent(0.5)
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Order total"
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var totalTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Total:"
        label.textColor = UIColor.black.withAlphaComponent(0.87)
        label.textAlignment = .left
        label.font = .systemFont(ofSize: 16, weight: .medium)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var totalDescLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .right
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.numberOfLines = 1
        label.anchor(.height(26))
        return label
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(TotalCell.self, forCellReuseIdentifier: TotalCell.ID)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.rowHeight = 26
        tableView.estimatedRowHeight = 26
        tableView.sectionHeaderHeight = 0
        tableView.sectionFooterHeight = 0
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return tableView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubviews()
        self.setupUI()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        titleLabel.anchor(.top(), .leading(16), .trailing(-16))
        
        bodyView.anchor(
            .top(titleLabel.bottomAnchor, constant: 5),
            .leading(16), .trailing(-16), .bottom(-24))
        
        tableView.anchor(
            .top(10), .leading(12), .trailing(-12),
            .bottom(totalTitleLabel.topAnchor, constant: -7),
            .height(26*CGFloat(3)))
        
        totalTitleLabel.anchor(
            .leading(12), .trailingGreater(totalDescLabel.leadingAnchor, constant: -16),
            .bottom(-12), .widthGreater(50))
        
        totalDescLabel.anchor(
            .trailing(-12), .centerY(totalTitleLabel.centerYAnchor),
            .widthGreater(50))
    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(bodyView)
        
        bodyView.addSubview(tableView)
        bodyView.addSubview(totalTitleLabel)
        bodyView.addSubview(totalDescLabel)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
    }
    
    private func configure() {
        if data.count > 0 {
            totalDescLabel.text = "\(data[2].price) AZN"
        }
//        "\(String(format: "%.2f", data.sum(\.price))) AZN"
    }
    
    // MARK: - Action
    
//    @objc private func addBagButtonTouchUp() {
//        self.delegate?.addBagButtonClick()
//    }
//
//    @objc private func infoButtonTouchUp() {
//        self.delegate?.infoButtonClick()
//    }
}

extension TotalTCell: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count-1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TotalCell.ID, for: indexPath) as? TotalCell else { return UITableViewCell() }
        cell.data = data[indexPath.row]
        return cell
    }

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let viewController = ProductDetailBuilder.make()
//        self.navigationController?.pushViewController(viewController, animated: true)
//    }
}
