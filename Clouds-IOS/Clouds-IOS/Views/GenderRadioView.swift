//
//  GenderRadioButtonsView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import UIKit

final class GenderRadioView: UIView {

    var isSelected: Bool = false {
        didSet {
            self.configure()
        }
    }
    
    private lazy var radioBackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.38)
        view.anchor(.size(width: 16, height: 16))
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var radioCenterView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.anchor(.size(width: 12, height: 12))
        view.layer.cornerRadius = 6
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var radioCheckView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.anchor(.size(width: 8, height: 8))
        view.layer.cornerRadius = 4
        view.clipsToBounds = true
        return view
    }()
    
    
    init() {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        
        radioBackView.anchor(.fillSuperview())
        
        radioCenterView.anchor(.centerX(), .centerY())
        
        radioCheckView.anchor(.centerX(), .centerY())
        
        super.updateConstraints()
    }
    
//    override var intrinsicContentSize: CGSize {
//        let width = UIScreen.main.bounds.width
//        return .init(width: width, height: 56)
//    }
    
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(radioBackView)
        radioBackView.addSubview(radioCenterView)
        radioCenterView.addSubview(radioCheckView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }
    
    private func configure() {
        if isSelected {
            self.radioBackView.backgroundColor = .secondaryColor
            self.radioCheckView.backgroundColor = .secondaryColor
        } else {
            self.radioBackView.backgroundColor = UIColor.black.withAlphaComponent(0.38)
            self.radioCheckView.backgroundColor = UIColor.white
        }
    }
}
