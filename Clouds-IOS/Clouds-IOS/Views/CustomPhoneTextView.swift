//
//  CustomPhoneTextView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 26.12.20.
//

import UIKit

final class CustomPhoneTextView: UIView {
    
    var errorText: String! {
        didSet {
            self.errorLabel.text = errorText
        }
    }
    
    var isError: Bool = false {
        didSet {
            lineView.backgroundColor = isError ? UIColor(hexString: "E52836") : UIColor(hexString: "000000").withAlphaComponent(0.25)
            errorLabel.isHidden = !isError
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var labelsView: UIView = {
        let view = UIView()
        view.backgroundColor =  .clear
        return view
    }()
    
    lazy var phoneText: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .left
        textField.delegate = self
        textField.font = UIFont(weight: .regular, size: 16)
        textField.placeholder = ""
        return textField
    }()
    
    lazy var codeText: UITextField = {
        let textField = UITextField()
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .center
        textField.delegate = self
        textField.font = UIFont(weight: .regular, size: 16)
        textField.placeholder = ""
        return textField
    }()
    
    private lazy var errorStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [lineView,spaceView,errorLabel])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 4
        stackView.backgroundColor = .clear
        return stackView
    }()
    
    private lazy var spaceView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.clear
        view.anchor(.height(4))
        return view
    }()
    
    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor(hexString: "EE0005")
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        label.isHidden = true
        label.anchor(.height(16))
        return label
    }()
    
    private lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor =  UIColor.black.withAlphaComponent(0.25)
        view.anchor(.height(0.5))
        return view
    }()
    
    private lazy var horizontalLineView: UIView = {
        return UIView(backgroundColor: UIColor.black.withAlphaComponent(0.25))
    }()

    init(title: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        titleLabel.anchor(.leading(), .trailing(), .top(), .height(16))
        
        labelsView.anchor(
            .leading(), .trailing(),
            .top(titleLabel.bottomAnchor, constant: 4), .height(32))
        
        phoneText.anchor(
            .leading(horizontalLineView.trailingAnchor, constant: 8),
            .trailing(),.top(), .bottom())
        
        codeText.anchor(
            .trailing(horizontalLineView.leadingAnchor, constant: -8),
            .leading(),.top(), .bottom(), .width(60))
        
        horizontalLineView.anchor(
            .top(4),.width(0.3),.bottom(-4))
        
        errorStack.anchor(
            .top(labelsView.bottomAnchor),
            .leading(), .trailing(), .bottom())
        
        super.updateConstraints()
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(labelsView)
        addSubview(errorStack)
        
        labelsView.addSubview(phoneText)
        labelsView.addSubview(codeText)
        labelsView.addSubview(horizontalLineView)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
        
        //muveqqeti
        //self.titleLabel.text = "Skirts"
    }
}

extension CustomPhoneTextView: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor(hexString: "2F69FF")
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.2) {
            self.lineView.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        }
    }
}
