//
//  CustomDateView.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 30.12.20.
//

import UIKit

protocol CustomDateViewDelegate: class {
    func downButtonClick()
}

//extension CustomDateViewDelegate {
//    func downButtonClick(_ textView: CustomDateView) {}
//}

final class CustomDateView: UIView {

    weak var delegate: CustomDateViewDelegate?
    
    private lazy var bodyView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    private lazy var downButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .clear
        button.tintColor = UIColor.black.withAlphaComponent(0.6)
        button.setupBorder(width: 1, color: UIColor(hexString: "EFEFEF"))
        button.contentHorizontalAlignment = .trailing
        button.setImage(UIImage(named: "down-arrow-icon"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 24)
        button.addTarget(self, action: #selector(downButtonTouchUp), for: .touchUpInside)
        return button
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.60)
        label.textAlignment = .left
        label.font = UIFont(weight: .regular, size: 12)
        label.numberOfLines = 1
        return label
    }()
    
    lazy var mainText: UITextField = {
        let textField = UITextField()
        textField.text = "MM / DD / YYYY"
        textField.tintColor = .clear
        textField.textColor = UIColor.black.withAlphaComponent(0.60)
        textField.textAlignment = .center
        textField.font = UIFont(weight: .medium, size: 16)
        textField.placeholder = ""
        return textField
    }()
    
    init(title: String) {
        super.init(frame: .zero)
        
        addSubviews()
        setupUI()
        
        titleLabel.text = title
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        bodyView.anchor(.fillSuperview())
        
        titleLabel.anchor(.leading(), .trailing(), .top(), .height(16))
        
        downButton.anchor(
            .top(titleLabel.bottomAnchor, constant: 4),
            .trailing(), .leading(), .bottom(), .height(52))
        
        mainText.anchor(
            .top(downButton.topAnchor), .bottom(downButton.bottomAnchor),
            .trailing(downButton.trailingAnchor, constant: -88), .leading(downButton.leadingAnchor, constant: 40))
        
        
        super.updateConstraints()
    }
    
    // MARK: - Private
    
    private func addSubviews() {
        addSubview(bodyView)
        
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(mainText)
        bodyView.addSubview(downButton)
        
        self.updateConstraints()
    }
    
    private func setupUI() {
        self.backgroundColor = .clear
    }

    // MARK: - Action
    
    @objc private func downButtonTouchUp() {
        self.delegate?.downButtonClick()
    }
    
}
