//
//  AppRouter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import UIKit

final class AppRouter {
    
    let window: UIWindow
    
    init() {
        window = UIWindow(frame: UIScreen.main.bounds)
    }
    
    func start() {
        self.tabbar()
    }
    
    func login() {
//        let storyboard = UIStoryboard(name: "Accounts", bundle: nil)
//        let viewController = storyboard.instantiateViewController(withIdentifier: LOGIN_VC) as! LoginVC
        
//        let viewController = LoginNavigation(rootViewController: LoginBuilder.make())
//        self.window.rootViewController = viewController
//        self.window.makeKeyAndVisible()
    }
    
    func main() {
        let viewController = MainNavigation(rootViewController: ProfileBuilder.make())
        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
    
    func tabbar() {
        let viewController = TabBarBuilder.make()
        self.window.rootViewController = viewController
        self.window.makeKeyAndVisible()
    }
}
