//
//  AppDelegate.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn
import FBSDKCoreKit
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
//        Facebook Login
        FBSDKCoreKit.ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
//        Google login
        GIDSignIn.sharedInstance().clientID = "277298623702-vgrkq92642krh0fmms6pjggkh6f3fnmp.apps.googleusercontent.com"
//      reversed  com.googleusercontent.apps.1067127000230-o3cspf95u7r14apstvuoqb3g59cop6hg
        

        //Thread.sleep(forTimeInterval: 0.3)
        //Keyboard Manager
        IQKeyboardManager.shared.enable = true
        //IQKeyboardManager.shared.enableDebugging = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        App.router.start()
        
        // Remove this method to stop OneSignal Debugging
        OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)

        // OneSignal initialization
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId("a8427184-c7bd-42a9-b463-06ef9849de7e")

        // promptForPushNotifications will show the native iOS notification permission prompt.
        // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    
        // Language
        let language = Defaults.instance.getLang()
        L102Language.setAppleLAnguageTo(lang: language)
        L012Localizer.Swizzling()
//        FlagFetcher.fetchFlags { result in
//            if case let .success(flags) = result,
//              flags.contains("use_facebook") {
//                // Initialize the SDK
//                ApplicationDelegate.shared.application(
//                    application,
//                    didFinishLaunchingWithOptions: launchOptions
//                )
//            }
//        }
        
        return true
    }
    
    //Facebook and Google Login
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        let handledFB = FBSDKCoreKit.ApplicationDelegate.shared.application(app, open: url, options: options)
        let handledGoogle = GIDSignIn.sharedInstance().handle(url)
        return handledFB || handledGoogle
    }
}

