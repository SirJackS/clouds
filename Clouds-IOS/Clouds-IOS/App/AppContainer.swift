//
//  AppContainer.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

let App = AppContainer()

final class AppContainer {
    
    let router = AppRouter()
    
    let service = Service()
}
