//
//  Service.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation
import Alamofire
import Moya
import Moya_ModelMapper
import Mapper

fileprivate final class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}

protocol ServiceProtocol {
    func login(request: LoginRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ())
    func register(request: RegisterRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ())
    func logout(completion: @escaping (Result< Empty, WrongResponse>) -> ())
    func resetPassBy(email: String, completion: @escaping (Result<Response<ForgotPassword>, WrongResponse>) -> ())
    func recoveryPassBy(request: RecoveryPasswordRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func otpVerifyBy(request: OTPRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func resendOtpBy(request: ResendOTPRequest, completion: @escaping (Result<Response<ForgotPassword>, WrongResponse>) -> ())
    func changePasswordBy(request: ChangePasswordRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func me(completion: @escaping (Result<Response<Me>, WrongResponse>) -> ())
    func updateMe(request: MeRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func facebookBy(request: SocialRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ())
    func googleBy(request: SocialRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ())
    //Home
    func getHome(completion: @escaping (Result<Response<Home>, WrongResponse>) -> ())
    //Address
    func getCountries(completion: @escaping (Result<DataResponse<Country>, WrongResponse>) -> ())
    func getCitiesByCountry(_ countryId: Int, completion: @escaping (Result<DataResponse<City>, WrongResponse>) -> ())
    func storeAddress(request: AddressDetailRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func updateAddress(_ addressId: Int, request: AddressDetailRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func getAllAddresses(completion: @escaping (Result<DataResponse<AddressMain>, WrongResponse>) -> ())
    func getAddressDetail(_ id: Int, completion: @escaping (Result<Response<AddressDetail>, WrongResponse>) -> ())
    func deleteAddress(_ id: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    //Bag
    func getCart(completion: @escaping (Result<Response<Bag>, WrongResponse>) -> ())
    func addBagItem(_ request: AddBagItemRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func updateQuantityInBag(_ itemId: Int, request: UpdateQuantityInBagRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func updateOptionsInBag(_ itemId: Int, request: UpdateOptionsInBagRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func deletBagItem(_ itemId: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    //Wishlist
    func getWishlist(completion: @escaping (Result<DataResponse<Wishlist>, WrongResponse>) -> ())
    func addToWishlist(_ request: AddToWishlistRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func updateOptionsInWishlist(_ wishlistId: Int, request: UpdateOptionsInWishlistRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func removeFromWishlist(_ productId: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    func getSizesWithColor(_ productId: Int, _ color: String, completion: @escaping (Result<DataResponse<Size>, WrongResponse>) -> ())
    func getColorsWithSize(_ productId: Int, _ size: String ,completion: @escaping (Result<DataResponse<Color>, WrongResponse>) -> ())
    func getSizesAndColorsBy(_ productId: Int ,completion: @escaping (Result<Response<ColorSize>, WrongResponse>) -> ())
    //Banners
    func getAllCampaigns(completion: @escaping (Result<DataResponse<BannerCampaign>, WrongResponse>) -> ())
    func getCampaignForSignUp(completion: @escaping (Result<Response<BannerCampaign>, WrongResponse>) -> ())
    //Categories
    func getCategoriesByType(request: CategoryTypeRequest, completion: @escaping (Result<DataResponse<Category>, WrongResponse>) -> ())
    func getCategoryAges(completion: @escaping (Result<DataResponse<CategoryType>, WrongResponse>) -> ())
    func getCategoryGenders(completion: @escaping (Result<DataResponse<CategoryType>, WrongResponse>) -> ())
    func getAllCategories(completion: @escaping (Result<Empty, WrongResponse>) -> ())
    func getMainCategories(completion: @escaping (Result<Empty, WrongResponse>) -> ())
    func getSubCategory(_ categoryId: Int, completion: @escaping (Result<DataResponse<Category>, WrongResponse>) -> ())
    //Filter
    func getFilterCategories(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ())
    func getFilterProductTypes(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ())
    func getFilterBrands(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ())
    func getFilterColors(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ())
    func getFilterSizes(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ())
    func getFilterProducts(request: FilterRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ())
    //Product
    func getSubcategoryProductsBy(request: ProductSubcategoryRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ())
    func getAllProducts(completion: @escaping (Result<DataResponse<Product>, WrongResponse>) -> ())
    func getProductBy(_ id: Int, completion: @escaping (Result<Response<ProductDetail>, WrongResponse>) -> ())
    //Search
    func searchProductsBy(request: SearchRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ())
    //Profile -> Orders
    func getCustomerOrders(completion: @escaping (Result<DataResponse<OrderList>, WrongResponse>) -> ())
    func getCheckout(completion: @escaping (Result<Response<Order>, WrongResponse>) -> ())
    func storeOrder(request: OrderRequest, completion: @escaping (Result<Response<RedirectUrl>, WrongResponse>) -> ())
    func getOrderDetailBy(_ id: Int, completion: @escaping (Result<Response<OrderDetail>, WrongResponse>) -> ())
    //Profile -> Rewards
    func getRewards(completion: @escaping (Result<Response<Reward>, WrongResponse>) -> ())
    func getGiftcards(completion: @escaping (Result<DataResponse<GiftCard>, WrongResponse>) -> ())
    func addGiftcard(request: AddGiftCardRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ())
    //Notifications
    func getNotifications(completion: @escaping (Result<DataResponse<Notification>, WrongResponse>) -> ())
}

final class Service: ServiceProtocol {
    
    private var provider: MoyaProvider<API>!
    
    enum Error: Swift.Error {
        case networkError
        case serializationError(internal: Swift.Error)
    }
    
    init() {
        let networkLoggerPlugin = NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: .verbose))
        self.provider = MoyaProvider<API>(plugins: [networkLoggerPlugin])
    }
    
    private func request<SuccessT: Decodable, WrongT: Decodable>(_ target: API, successCode: Int = 200, completion: @escaping (Result<SuccessT, WrongT>) -> ()) {
        guard Connectivity.isConnectedToInternet() else {
            return completion(.failure(Error.networkError))
        }
        
        provider.request(target) { (result) in
            switch result {
            case let .success(response):
                do {
                    if response.statusCode == successCode {
                        if successCode == 204 {
                            completion(.successNoContent)
                        }
                        else {
                            let decoder = JSONDecoder()
                            decoder.keyDecodingStrategy = .convertFromSnakeCase
                            let result = try decoder.decode(SuccessT.self, from: response.data)
                            completion(.success(result))
                        }
                    }
                    else {
                        if response.statusCode == 401 {
                            UserDefaults.standard.removeObject(forKey: TOKEN_KEY)
                            UserDefaults.standard.removeObject(forKey: LOGGED_IN_KEY)
                            App.router.start()
                            completion(.unauthenticated)
                        }
                        let result = try response.map(WrongT.self)
                        completion(.wrong(result))
                    }
                }
                catch {
                    print("log: ", error)
                    completion(.failure(Error.serializationError(internal: error)))
                }
                
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
 
    // MARK: - Auth
    
    func login(request: LoginRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ()) {
        self.request(.login(request)) { (result) in
            completion(result)
        }
//        self.request(.login(request), successCode: 201) { (result) in
    }
    
    func register(request: RegisterRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ()) {
        self.request(.register(request), successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func logout(completion: @escaping (Result<Empty, WrongResponse>) -> ()) {
        self.request(.logout) { (result) in
            completion(result)
        }
    }

    func recoveryPassBy(request: RecoveryPasswordRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.recoveryPassBy(request), successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func resetPassBy(email: String, completion: @escaping (Result<Response<ForgotPassword>, WrongResponse>) -> ()) {
        self.request(.resetPassBy(email)) { (result) in
            completion(result)
        }
    }
    
    func otpVerifyBy(request: OTPRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.otpVerifyBy(request)) { (result) in
            completion(result)
        }
    }
    
    func resendOtpBy(request: ResendOTPRequest, completion: @escaping (Result<Response<ForgotPassword>, WrongResponse>) -> ()) {
        self.request(.resendOtpBy(request)) { (result) in
            completion(result)
        }
    }
    
    func changePasswordBy(request: ChangePasswordRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.changePasswordBy(request)) { (result) in
            completion(result)
        }
    }
    
    func me(completion: @escaping (Result<Response<Me>, WrongResponse>) -> ()) {
        self.request(.me) { (result) in
            completion(result)
        }
    }
    
    func updateMe(request: MeRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.updateMe(request)) { (result) in
            completion(result)
        }
    }
    
    
    func facebookBy(request: SocialRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ()) {
        self.request(.facebookBy(request)) { (result) in
            completion(result)
        }
    }
    
    func googleBy(request: SocialRequest, completion: @escaping (Result<Response<Login>, WrongResponse>) -> ()) {
        self.request(.googleBy(request)) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Home
    
    func getHome(completion: @escaping (Result<Response<Home>, WrongResponse>) -> ()) {
        self.request(.home) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Address
    
    func getCountries(completion: @escaping (Result<DataResponse<Country>, WrongResponse>) -> ()) {
        self.request(.getCountries) { (result) in
            completion(result)
        }
    }
    
    func getCitiesByCountry(_ countryId: Int, completion: @escaping (Result<DataResponse<City>, WrongResponse>) -> ()) {
        self.request(.getCitiesBy(countryId)) { (result) in
            completion(result)
        }
    }
    
    func storeAddress(request: AddressDetailRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.storeAddress(request),successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func updateAddress(_ addressId: Int, request: AddressDetailRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.updateAddress(addressId, request),successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func getAllAddresses(completion: @escaping (Result<DataResponse<AddressMain>, WrongResponse>) -> ()) {
        self.request(.getAllAddresses) { (result) in
            completion(result)
        }
    }
    
    func getAddressDetail(_ id: Int, completion: @escaping (Result<Response<AddressDetail>, WrongResponse>) -> ()) {
        self.request(.getAddressDetail(id)) { (result) in
            completion(result)
        }
    }
    
    func deleteAddress(_ id: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.deleteAddress(id)) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Cart
    
    func getCart(completion: @escaping (Result<Response<Bag>, WrongResponse>) -> ()) {
        self.request(.getCartItems) { (result) in
            completion(result)
        }
    }
    
    func addBagItem(_ request: AddBagItemRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.addBagItem(request), successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func updateQuantityInBag(_ itemId: Int, request: UpdateQuantityInBagRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.updateQuantityInBag(itemId, request), successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func updateOptionsInBag(_ itemId: Int, request: UpdateOptionsInBagRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.updateOptionsInBag(itemId, request)) { (result) in
            completion(result)
        }
    }
    
    func deletBagItem(_ itemId: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.deletBagItem(itemId)) { (result) in
            completion(result)
        }
    }
    
    
    // MARK: - Wishlist
    
    func getWishlist(completion: @escaping (Result<DataResponse<Wishlist>, WrongResponse>) -> ()) {
        self.request(.getWishlist) { (result) in
            completion(result)
        }
    }
    
    func removeFromWishlist(_ productId: Int, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.removeFromWishlist(productId)) { (result) in
            completion(result)
        }
    }
    
    func addToWishlist(_ request: AddToWishlistRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.addToWishlist(request)) { (result) in
            completion(result)
        }
    }
    
    func updateOptionsInWishlist(_ wishlistId: Int, request: UpdateOptionsInWishlistRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.updateOptionsInWishlist(wishlistId, request)) { (result) in
            completion(result)
        }
    }
    
    func getSizesWithColor(_ productId: Int, _ color: String, completion: @escaping (Result<DataResponse<Size>, WrongResponse>) -> ()) {
        self.request(.getSizesWithColor(productId, color)) { (result) in
            completion(result)
        }
    }
    
    func getColorsWithSize(_ productId: Int, _ size: String, completion: @escaping (Result<DataResponse<Color>, WrongResponse>) -> ()) {
        self.request(.getColorsWithSize(productId, size)) { (result) in
            completion(result)
        }
    }
    
    func getSizesAndColorsBy(_ productId: Int, completion: @escaping (Result<Response<ColorSize>, WrongResponse>) -> ()) {
        self.request(.getSizesAndColorsBy(productId)) { (result) in
            completion(result)
        }
    }
    
    //MARK: - Banners
    
    func getAllCampaigns(completion: @escaping (Result<DataResponse<BannerCampaign>, WrongResponse>) -> ()) {
        self.request(.getAllCampaigns) { (result) in
            completion(result)
        }
    }
    
    func getCampaignForSignUp(completion: @escaping (Result<Response<BannerCampaign>, WrongResponse>) -> ()) {
        self.request(.getCampaignForSignUp) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Categories
    
    func getCategoriesByType(request: CategoryTypeRequest, completion: @escaping (Result<DataResponse<Category>, WrongResponse>) -> ()) {
        self.request(.getCategoriesByType(request)) { (result) in
            completion(result)
        }
    }
    
    func getCategoryAges(completion: @escaping (Result<DataResponse<CategoryType>, WrongResponse>) -> ()) {
        self.request(.getCategoryAges) { (result) in
            completion(result)
        }
    }
    
    func getCategoryGenders(completion: @escaping (Result<DataResponse<CategoryType>, WrongResponse>) -> ()) {
        self.request(.getCategoryGenders) { (result) in
            completion(result)
        }
    }
    
    func getAllCategories(completion: @escaping (Result<Empty, WrongResponse>) -> ()) {
        self.request(.getAllCategories) { (result) in
            completion(result)
        }
    }
    
    func getMainCategories(completion: @escaping (Result<Empty, WrongResponse>) -> ()) {
        self.request(.getMainCategories) { (result) in
            completion(result)
        }
    }
    
    func getSubCategory(_ categoryId: Int, completion: @escaping (Result<DataResponse<Category>, WrongResponse>) -> ()) {
        self.request(.getSubCategory(categoryId)) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Filter
    
    func getFilterCategories(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ()) {
        self.request(.getFilterCategories) { (result) in
            completion(result)
        }
    }
    
    func getFilterProductTypes(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ()) {
        self.request(.getFilterProductTypes) { (result) in
            completion(result)
        }
    }
    
    func getFilterBrands(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ()) {
        self.request(.getFilterBrands) { (result) in
            completion(result)
        }
    }
    
    func getFilterColors(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ()) {
        self.request(.getFilterColors) { (result) in
            completion(result)
        }
    }
    
    func getFilterSizes(completion: @escaping (Result<DataResponse<DetailFilter>, WrongResponse>) -> ()) {
        self.request(.getFilterSizes) { (result) in
            completion(result)
        }
    }
    
    func getFilterProducts(request: FilterRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ()) {
        self.request(.getFilterProducts(request)) { (result) in
            completion(result)
        }
    }
    
    
    // MARK: - Search
    
    func searchProductsBy(request: SearchRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ()) {
        self.request(.searchProductsBy(request)) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Products
    func getSubcategoryProductsBy(request: ProductSubcategoryRequest, completion: @escaping (Result<DataResponse<ProductList>, WrongResponse>) -> ()) {
        self.request(.getSubcategoryProductsBy(request)) { (result) in
            completion(result)
        }
    }
    
    func getAllProducts(completion: @escaping (Result<DataResponse<Product>, WrongResponse>) -> ()) {
        self.request(.getAllProducts) { (result) in
            completion(result)
        }
    }
    
    func getProductBy(_ id: Int, completion: @escaping (Result<Response<ProductDetail>, WrongResponse>) -> ()) {
        self.request(.getProductBy(id)) { (result) in
            completion(result)
        }
    }
    // MARK: - Profile -> Orders
    func getCustomerOrders(completion: @escaping (Result<DataResponse<OrderList>, WrongResponse>) -> ()) {
        self.request(.getCustomerOrders) { (result) in
            completion(result)
        }
    }
    
    func getCheckout(completion: @escaping (Result<Response<Order>, WrongResponse>) -> ()) {
        self.request(.getCheckout) { (result) in
            completion(result)
        }
    }
    
    func storeOrder(request: OrderRequest, completion: @escaping (Result<Response<RedirectUrl>, WrongResponse>) -> ()) {
        self.request(.storeOrder(request), successCode: 201) { (result) in
            completion(result)
        }
    }
    
    func getOrderDetailBy(_ id: Int, completion: @escaping (Result<Response<OrderDetail>, WrongResponse>) -> ()) {
        self.request(.getOrderDetailBy(id)) { (result) in
            completion(result)
        }
    }
    
    
    // MARK: - Profile -> Rewards
    func getRewards(completion: @escaping (Result<Response<Reward>, WrongResponse>) -> ()) {
        self.request(.getRewards) { (result) in
            completion(result)
        }
    }
    
    func addGiftcard(request: AddGiftCardRequest, completion: @escaping (Result<Response<Empty>, WrongResponse>) -> ()) {
        self.request(.addGiftcard(request)) { (result) in
            completion(result)
        }
    }
    
    func getGiftcards(completion: @escaping (Result<DataResponse<GiftCard>, WrongResponse>) -> ()) {
        self.request(.getGiftcards) { (result) in
            completion(result)
        }
    }
    
    // MARK: - Notifications
    func getNotifications(completion: @escaping (Result<DataResponse<Notification>, WrongResponse>) -> ()) {
        self.request(.getNotifications) { (result) in
            completion(result)
        }
    }
}
