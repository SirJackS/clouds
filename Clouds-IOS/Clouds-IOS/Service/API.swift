//
//  API.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation
import Moya

enum API {
    case login(_ request: Encodable)
    case register(_ request: Encodable)
    case resetPassBy(_ email: String)
    case recoveryPassBy(_ request: Encodable)
    case otpVerifyBy(_ request: Encodable)
    case resendOtpBy(_ request: Encodable)
    case changePasswordBy(_ request: Encodable)
    case logout
    case me
    case updateMe(_ request: Encodable)
    case facebookBy(_ request: Encodable)
    case googleBy(_ request: Encodable)
    //Home
    case home
    //Address
    case getCountries
    case getCitiesBy(_ countryId: Int)
    case storeAddress(_ request: Encodable)
    case updateAddress(_ addressId: Int,_ request: Encodable)
    case getAllAddresses
    case getAddressDetail(_ id: Int)
    case deleteAddress(_ id: Int)
    //Bag
    case getCartItems
    case addBagItem(_ request: Encodable)
    case deletBagItem(_ id: Int)
    case updateQuantityInBag(_ id: Int,_ request: Encodable)
    case updateOptionsInBag(_ id: Int,_ request: Encodable)
    //Wishlist
    case getWishlist
    case addToWishlist(_ request: Encodable)
    case updateOptionsInWishlist(_ wishlistId: Int,_ request: Encodable)
    case removeFromWishlist(_ productId: Int)
    case getSizesWithColor(_ productId: Int, _ color: String)
    case getColorsWithSize(_ productId: Int, _ size: String)
    case getSizesAndColorsBy(_ productId: Int)
    //Banners
    case getAllCampaigns
    case getCampaignForSignUp
    //Categories
    case getCategoryAges
    case getCategoryGenders
    case getCategoriesByType(_ request: Encodable)
    case getAllCategories
    case getMainCategories
    case getSubCategory(_ categoryId: Int)
    //Filter
    case getFilterCategories
    case getFilterProductTypes
    case getFilterBrands
    case getFilterColors
    case getFilterSizes
    case getFilterProducts(_ request: Encodable)
    //Product
    case getSubcategoryProductsBy(_ request: Encodable)
    case getAllProducts
    case getProductBy(_ id: Int)
    //Search
    case searchProductsBy(_ request: Encodable)
    //ProfileOrders
    case getCustomerOrders
    case getCheckout
    case storeOrder(_ request: Encodable)
    case getOrderDetailBy(_ id: Int)
    //ProfileRewards
    case getRewards
    case getGiftcards
    case addGiftcard(_ request: Encodable)
    //Notifications
    case getNotifications
}

extension API: TargetType {
    
    private func getHeader(token: String = "") -> [String:String] {
        if token != "" {
            return [
                "Content-type": "application/json",
                "Accept": "application/json",
                "Authorization": "Bearer \(token)",
                "Accept-Language": L102Language.currentAppleLanguage()]
        } else {
            return [
                "Content-type": "application/json",
                "Accept": "application/json",
                "Accept-Language": L102Language.currentAppleLanguage()]
        }
    }
    
    var headers: [String : String]? {
//        let dict = ["Content-type": "application/json",
//                    "Accept": "application/json; charset=utf-8"]
        if Defaults.instance.isLoggedIn {
            return getHeader(token: Defaults.instance.authToken)
        }
        return getHeader()
    }
    
    var baseURL: URL {
        return URL(string: "https://clouds-api.net-tech.az/api/v1")!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        case .register:
            return "/register"
        case .resetPassBy:
            return "/password/reset"
        case .recoveryPassBy:
            return "/password/recover"
        case .otpVerifyBy:
            return "/password/otp/verify"
        case .resendOtpBy:
            return "/password/otp/resend"
        case .changePasswordBy:
            return "/password/update"
        case .logout:
            return "/logout"
        case .me:
            return "/me"
        case .updateMe:
            return "/me/update"
        case .facebookBy:
            return "/facebook"
        case .googleBy:
            return "/google"
        //Home
        case .home:
            return "/pages/home"
        //Address
        case .getCountries:
            return "/addresses/countries"
        case let .getCitiesBy(countryId):
            return "/addresses/\(countryId)/cities"
        case .storeAddress:
            return "/addresses/store"
        case let .updateAddress(addressId,_):
            return "/addresses/\(addressId)/update"
        case .getAllAddresses:
            return "/addresses"
        case let .getAddressDetail(id):
            return "/addresses/\(id)"
        case let .deleteAddress(id):
            return "/addresses/\(id)/delete"
        //Bag
        case .getCartItems:
            return "/cart"
        case .addBagItem:
            return "/cart/add"
        case let .deletBagItem(id):
            return "/cart/\(id)/delete"
        case let .updateQuantityInBag(id,_):
            return "/cart/\(id)/quantity/update"
        case let .updateOptionsInBag(id,_):
            return "/cart/\(id)/option/update"
        //Wishlist
        case .getWishlist:
            return "/wishlist"
        case .addToWishlist:
            return "/wishlist/store"
        case let .updateOptionsInWishlist(wishlistId,_):
            return "/wishlist/\(wishlistId)/option/update"
        case let .removeFromWishlist(productId):
            return "/wishlist/\(productId)/delete"
        case let .getSizesWithColor(productId,color):
            return "/products/\(productId)/sizes/\(color)"
        case let .getColorsWithSize(productId,size):
            return "/products/\(productId)/colors/\(size)"
        case let .getSizesAndColorsBy(productId):
            return "/products/\(productId)/options"
        //Banners
        case .getAllCampaigns:
            return "/campaigns"
        case .getCampaignForSignUp:
            return "/campaigns/signup"
        //Categories
        case .getCategoryAges:
            return "/categories/ages"
        case .getCategoryGenders:
            return "/categories/genders"
        case .getAllCategories:
            return "/categories"
        case .getCategoriesByType:
            return "/categories/types/"
        case .getMainCategories:
            return "/categories/main"
        case let .getSubCategory(categoryId):
            return "/categories/\(categoryId)/subs"
        //Filter
        case .getFilterCategories:
            return "/products/categories"
        case .getFilterProductTypes:
            return "/products/productTypes"
        case .getFilterBrands:
            return "/brands"
        case .getFilterColors:
            return "/products/colors"
        case .getFilterSizes:
            return "/products/sizes"
        case .getFilterProducts:
            return "/products/filter"
        //Product
        case .getSubcategoryProductsBy:
            return "/products/subcategory"
        case .getAllProducts:
            return "/products/all"
        case let .getProductBy(id):
            return "/products/\(id)"
        //Search
        case .searchProductsBy:
            return "/search"
        //ProfileOrders
        case .getCustomerOrders:
            return "/orders"
        case .getCheckout:
            return "/orders/checkout"
        case .storeOrder:
            return "/orders/store"
        case let .getOrderDetailBy(id):
            return "/orders/\(id)"
        //ProfileRewards
        case .getRewards:
            return "/reward"
        case .getGiftcards:
            return "/giftcards"
        case .addGiftcard:
            return "/giftcards/add"
        //ProfileRewards
        case .getNotifications:
            return "/notifications"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login, .logout,.resetPassBy,.recoveryPassBy,.storeAddress,.register,
             .facebookBy,.googleBy,.addToWishlist,.updateMe,.otpVerifyBy,.resendOtpBy,
             .changePasswordBy,.updateOptionsInWishlist,.addBagItem,.storeOrder,
             .addGiftcard:
            return .post
        case .updateAddress,.updateQuantityInBag,.updateOptionsInBag:
            return .put
        case .deleteAddress,.removeFromWishlist,.deletBagItem:
            return .delete
        default:
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case let .login(request), let .register(request), let .storeAddress(request),
             let .updateAddress(_, request), let .updateMe(request), let .recoveryPassBy(request),
             let .otpVerifyBy(request),let .resendOtpBy(request), let .changePasswordBy(request),
             let .addGiftcard(request),let .googleBy(request),let .facebookBy(request):
            return .requestParameters(parameters: request.dictionary, encoding: JSONEncoding.default)
        case let .resetPassBy(email):
            return .requestParameters(parameters: ["email": email], encoding: JSONEncoding.default)
        case let .getCategoriesByType(request), let .getSubcategoryProductsBy(request),
             let .searchProductsBy(request), let .getFilterProducts(request),
             let .updateOptionsInWishlist(_, request),let .addToWishlist(request),
             let .addBagItem(request),let .updateQuantityInBag(_,request),
             let .updateOptionsInBag(_,request),let .storeOrder(request):
            return .requestParameters(parameters: request.dictionary, encoding: URLEncoding.queryString)
        default:
            return .requestPlain
        }
    }
    
    var sampleData: Data {
        switch self {
//        case .login(let email, let password):
//            return "{'email': \(email),'password': \(password) }".utf8Encoded
        default:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        }
    }
    
    var validate: Bool {
        return true
    }
}

fileprivate extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }

    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

enum NetworkErrorType {
    case login
    case register
    case resetPassBy
    case recoveryPassBy
    case otpVerifyBy
    case resendOtpBy
    case changePasswordBy
    case logout
    case me
    case updateMe
    case facebookBy
    case googleBy
    //Home
    case home
    //Address
    case getCountries
    case getCitiesBy
    case storeAddress
    case updateAddress
    case getAllAddresses
    case getAddressDetail
    case deleteAddress
    //Bag
    case getCartItems
    case addBagItem
    case deletBagItem
    case updateQuantityInBag
    case updateOptionsInBag
    //Wishlist
    case getWishlist
    case addToWishlist
    case updateOptionsInWishlist
    case removeFromWishlist
    case getSizesWithColor
    case getColorsWithSize
    case getSizesAndColorsBy
    //Banners
    case getAllCampaigns
    case getCampaignForSignUp
    //Categories
    case getCategoryAges
    case getCategoryGenders
    case getCategoriesByType
    case getAllCategories
    case getMainCategories
    case getSubCategory
    //Filter
    case getFilterCategories
    case getFilterProductTypes
    case getFilterBrands
    case getFilterColors
    case getFilterSizes
    case getFilterProducts
    //Product
    case getSubcategoryProductsBy
    case getAllProducts
    case getProductBy
    //Search
    case searchProductsBy
    //ProfileOrders
    case getCustomerOrders
    case getCheckout
    case storeOrder
    case getOrderDetailBy
    //ProfileRewards
    case getRewards
    case getGiftcards
    case addGiftcard
    //Notifications
    case getNotifications
}
