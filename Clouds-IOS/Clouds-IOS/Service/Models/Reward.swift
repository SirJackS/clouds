//
//  Reward.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

struct Reward: Decodable {
    let id: Int
    let total: Int
    let logs: [RewardItem]?
}

struct RewardItem: Decodable {
    let id: Int
    let name: String
    let amount: Int
    let type: String
}
