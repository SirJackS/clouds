//
//  Notification.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

struct Notification: Decodable {
    let title: String?
    let description: String?
    let date: String?
//    var image: String {
//        switch (type) {
//            case "shipping" :
//                return "tabbar3"
//            case "reward" :
//                return "reward-icon"
//            default :
//                return ""
//        }
//    }
}
