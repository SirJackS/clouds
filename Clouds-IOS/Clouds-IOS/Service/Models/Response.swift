//
//  Response.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

struct WrongResponse: Decodable {
    var statusCode: Int?
    var message: String?
    var status: String?
    var errors: [String: [String]]?
}

//struct Errors: Decodable {
//    var errors: [String: [String]]
//}

struct Response<T: Decodable>: Decodable {
    var statusCode: Int?
    var message: String?
    var status: String?
    var data: T?
}
