//
//  Order.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

struct RedirectUrl: Decodable {
    let redirectUrl: String
}

struct OrderRequest: Encodable {
    var cartId: Int?
    var addressId: Int?
    var note: String?
    var subtotal: Float?
    var delivery: Float?
    var giftCard: Int?
    var debitCard: Float?
    var total: Float?
    var reward: Int?
}

struct Order: Decodable {
    let id: Int
    let items: [OrderImage]?
    let subTotal: Float
    let delivery: Float?
    let total: Float
}

struct OrderTotal: Decodable {
    let name: String
    let price: Float
}

struct OrderImage: Decodable {
    let id: Int
    let image: String?
}

struct OrderDetail: Decodable {
    let id: Int
    let orderNo: String
    let address: String
    let items: [OrderDetailItem]?
    let debetcard: Bool
    let giftcard: Bool
    let reward: Bool
    let subTotal: Float
    let delivery: Float
    let total: Float
    let date: String?
}

struct OrderDetailItem: Decodable {
    let id: Int
    let title: String?
    let image: String?
    let price: Float
    let color: String?
    let size: String?
    let qty: Int
}
