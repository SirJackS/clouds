//
//  Search.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 04.03.21.
//

import Foundation

struct SearchRequest: Encodable {
    let term: String
}
