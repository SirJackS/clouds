//
//  Home.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 29.03.21.
//

import Foundation

struct Home: Decodable {
    var categories: [Category]?
    var campaigns: [BannerCampaign]?
    var banners: [BannerCampaign]?
}

struct BannerCampaign: Decodable {
    var id: Int
    var name: String?
    var description: String?
    var cover: String?
    var rateType: Int
    var rate: String?
    var campaignType: Int
}
