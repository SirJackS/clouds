//
//  WishList.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 18.12.20.
//

import Foundation

struct Wishlist: Decodable {
    var id: Int
    var name: String?
    var productId: Int
    var price: Float?
    var salePrice: Float?
    var color: String?
    var size: String?
    var image: String?
}

struct AddToWishlistRequest: Encodable {
    var productId: Int
}

struct UpdateOptionsInWishlistRequest: Encodable {
    var color: String?
    var size: String?
}
