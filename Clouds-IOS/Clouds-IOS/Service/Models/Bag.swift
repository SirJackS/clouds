//
//  BagList.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation
//021-01-31T17:18:38.000000Z
struct AddBagItemRequest: Encodable {
    var productId: Int
    var quantity: Int
    var size: String
    var color: String
}

struct UpdateQuantityInBagRequest: Encodable {
    var quantity: Int
}

struct UpdateOptionsInBagRequest: Encodable {
    var productId: Int
    var size: String
    var color: String
}

struct Bag: Decodable {
    var id: Int
    var total: Float
    var items: [BagItem]?
}

struct BagItem: Decodable {
    var id: Int
    var productId: Int
    var variantId: Int?
    var price: Float?
    var size: String?
    var color: String?
    var colorCode: String?
    var variantPrice: Float?
    var salePrice: Float?
    var name: String?
    var image: String?
    var quantity: Int
}
