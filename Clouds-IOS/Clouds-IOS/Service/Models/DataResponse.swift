//
//  DataResponse.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

struct DataResponse<T: Decodable>: Decodable {
    var statusCode: Int?
    var message: String?
    var status: String?
    var data: [T]?
}
