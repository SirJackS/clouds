//
//  Category.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 15.12.20.
//

import Foundation

struct Category: Decodable {
    var id: Int
    var slug: String?
    var name: String?
    var image: String?
    var isGrid: Bool?
}

struct CategoryMain: Decodable {
    var id: Int?
    var name: String?
    var desc: String?
    var image: String?
}

struct CategoryType: Decodable {
    var id: Int
    var name: String
}

struct CategoryTypeRequest: Encodable {
    var genderType: Int
    var ageType: Int
}
