//
//  OTP.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

struct OTPRequest: Encodable {
    var otpCode: String
    var token: String
}

struct ResendOTPRequest: Encodable {
    var token: String
}
