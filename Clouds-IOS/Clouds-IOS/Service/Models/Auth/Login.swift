//
//  Login.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

struct Login: Decodable {
    var accessToken: String
    var type: String
    var personalDetails: Me
}

struct LoginRequest: Encodable {
    var email: String
    var password: String
    
//    mutating func checkPassword() -> Bool {
//        if email == password {
//            return true
//        } else {
//            return false
//        }
//    }
}
