//
//  Social.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 20.01.21.
//

import Foundation

struct SocialRequest: Encodable {
    var accessToken: String
}
