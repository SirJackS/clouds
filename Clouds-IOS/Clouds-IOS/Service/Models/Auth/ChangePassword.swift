//
//  ChangePassword.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

struct ChangePasswordRequest: Encodable {
    var oldPassword: String
    var password: String
    var passwordConfirmation: String
}
