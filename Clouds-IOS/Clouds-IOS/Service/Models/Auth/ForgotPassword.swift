//
//  ForgotPassword.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 27.01.21.
//

import Foundation

struct ForgotPassword: Decodable {
    var token: String
    var otpCode: Int
}
