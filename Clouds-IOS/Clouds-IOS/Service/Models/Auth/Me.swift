//
//  Me.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

struct Me: Decodable {
    var id: Int
    var firstname: String?
    var lastname: String?
    var email: String
    var birthdate: String?
    var interestedIn: String?
}

struct MeRequest: Encodable {
    var firstname: String?
    var lastname: String?
    var email: String?
    var birthdate: String?
    var interestedIn: String?
}
