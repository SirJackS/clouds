//
//  RecoveryPassword.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 20.01.21.
//

import Foundation

struct RecoveryPasswordRequest: Encodable {
    var password: String
    var passwordConfirmation: String
    var token: String
}
