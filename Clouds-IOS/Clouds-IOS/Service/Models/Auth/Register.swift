//
//  Register.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 20.01.21.
//

import Foundation

struct Register: Decodable {
    var token: String
}

struct RegisterRequest: Encodable {
    var firstname: String
    var lastname: String
    var birthdate: String
    var interestedIn: String
    var email: String
    var password: String
    var passwordConfirmation: String
}
