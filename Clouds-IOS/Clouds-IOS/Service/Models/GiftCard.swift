//
//  GiftCard.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

struct AddGiftCardRequest: Encodable {
    let number: String?
}

struct GiftCard: Decodable {
    let id: Int
    let number: String
    let bonus: Int
}
