//
//  Product.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.12.20.
//

import Foundation

struct ProductList: Decodable {
    var id: Int
    var price: String?
    var salePrice: String?
    var name: String?
    var image: String?
    var wishlist: Bool
}

struct ProductSubcategoryRequest: Encodable {
    var categoryId: Int
    var genderTypeId: Int
    var ageTypeId: Int
}

struct Product: Decodable {
    var id: Int
    var sku: String?
    var quantity: Int
    var price: String?
    var salePrice: String?
    var title: String?
    var description: String?
    var brand: BrandProductType?
    var productType: BrandProductType?
    var images: [ProductImage]
    var mainImage: ProductImage?
    var variants: [ProductVariant]?
    var wishlist: Bool
}

struct BrandProductType: Decodable {
    var id: Int
    var name: String
}

struct ProductImage: Decodable {
    var id: Int
    var path: String?
}

struct ProductVariant: Decodable {
    var id: Int
    var size: String?
    var color: String?
    var price: String?
    var quantity: Int
    var sku: String?
}

struct ColorSize: Decodable {
    var colors: [Color]?
    var sizes: [Size]?
}

struct Color: Decodable {
    var name: String?
    var code: String?
}

struct Size: Decodable {
    var name: String?
}
