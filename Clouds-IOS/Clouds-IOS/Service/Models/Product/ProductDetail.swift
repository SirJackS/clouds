//
//  ProductDetail.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 19.02.21.
//

import Foundation

struct ProductDetail: Decodable {
    var product: Product
    var colors: [Color]?
    var sizes: [Size]?
    var similarProducts: [ProductList]?
}
