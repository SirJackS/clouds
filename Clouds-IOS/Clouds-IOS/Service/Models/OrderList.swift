//
//  Order.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 24.12.20.
//

import Foundation

struct OrderList: Decodable {
    var id: Int
    var orderNo: Int
    var title: String?
    var pieces: Int
    var image: String?
    var date: String?
}
