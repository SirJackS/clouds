//
//  Address.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 20.01.21.
//

import Foundation

struct Country: Decodable {
    var id: Int
    var name: String
    var phoneCode: String
}
