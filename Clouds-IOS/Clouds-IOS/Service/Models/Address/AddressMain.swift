//
//  AddressMain.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 25.12.20.
//

import Foundation

struct AddressMain: Decodable {
    var id: Int
    var address: String?
    var floor: String?
    var countryId: Int?
    var cityId: Int?
    var postCode: String?
    var phone: String?
    var country: Country?
    var city: City?
}
