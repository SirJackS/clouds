//
//  AddressDetail.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 20.01.21.
//

import Foundation

struct AddressDetail: Decodable {
    var id: Int
    var address: String?
    var floor: String?
    var country: Country?
    var city: City?
    var phone: String?
}

struct AddressDetailRequest: Encodable {
    var address: String?
    var floor: String?
    var countryId: String?
    var cityId: String?
    var phone: String?
    
//    init(address: String? = nil,
//         floor: String? = nil,
//         countryId: String? = nil,
//         cityId: String? = nil,
//         postCode: String? = nil,
//         phone: String? = nil) {
//        self.address = address
//        self.floor = floor
//        self.countryId = countryId
//        self.cityId = cityId
//        self.postCode = postCode
//        self.phone = phone
//    }
}
