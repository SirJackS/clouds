//
//  Filter.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 23.12.20.
//

import Foundation

struct FilterRequest: Encodable {
    public var search: String?
    public var categoryId: [Int]?
    public var productTypeId: [Int]?
    public var brandId: [Int]?
    public var color: [String]?
    public var size: [String]?
    public var priceMin: Int?
    public var priceMax: Int?
    public var sortBy: Int?
    public var bannerId: Int?
    
    public init(search: String? = nil,
                categoryId: [Int]? = nil,
                productTypeId: [Int]? = nil,
                brandId: [Int]? = nil,
                color: [String]? = nil,
                size: [String]? = nil,
                priceMin: Int? = nil,
                priceMax: Int? = nil,
                sortBy: Int? = nil,
                bannerId: Int? = nil) {
        self.search = search
        self.categoryId = categoryId
        self.productTypeId = productTypeId
        self.brandId = brandId
        self.color = color
        self.size = size
        self.priceMin = priceMin
        self.priceMax = priceMax
        self.sortBy = sortBy
        self.bannerId = bannerId
    }
}

struct MainFilter: Decodable {
    let title: String
    let desc: String
}

struct DetailFilter: Decodable {
    let id: Int?
    let name: String
    let slug: String?
    let cnt: Int?
}
