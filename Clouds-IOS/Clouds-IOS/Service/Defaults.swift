//
//  Defaults.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

enum AgeType: Int {
    case toddler = 1
    case junior = 2
    case baby = 3
}

enum GenderType: Int {
    case boy = 1
    case girl = 2
}

class Defaults {
    static let instance = Defaults()
    
    let defaults = UserDefaults.standard
    
    var isLoggedIn : Bool {
        get  {
            return defaults.bool(forKey: LOGGED_IN_KEY)
        }
        set {
            defaults.set(newValue, forKey: LOGGED_IN_KEY)
        }
    }
    var authToken: String {
        get {
            return defaults.value(forKey: TOKEN_KEY) as? String ?? ""
        }
        set {
            defaults.set(newValue, forKey: TOKEN_KEY)
        }
    }
    
    var otpToken: String {
        get {
            return defaults.value(forKey: OTP_TOKEN_KEY) as? String ?? ""
        }
        set {
            defaults.set(newValue, forKey: OTP_TOKEN_KEY)
        }
    }
    
    var fullname: String {
        get {
            return defaults.value(forKey: FULLNAME) as? String ?? ""
        }
        set {
            defaults.set(newValue, forKey: FULLNAME)
        }
    }
    
    var searchKeywords: [String] {
        get {
            return defaults.value(forKey: KEYWORDS) as? [String] ?? []
        }
        set {
            defaults.set(newValue, forKey: KEYWORDS)
            defaults.synchronize()
        }
    }
    
    var ageType: Int {
        get {
            return defaults.value(forKey: AGE_KEY) as? Int ?? 1
        }
        set {
            defaults.set(newValue, forKey: AGE_KEY)
        }
    }
    
    var genderType: Int {
        get {
            return defaults.value(forKey: GENDER_KEY) as? Int ?? 2
        }
        set {
            defaults.set(newValue, forKey: GENDER_KEY)
        }
    }
    var language: String {
        get {
            return defaults.value(forKey: UD_KEY_LANG) as? String ?? "az"
        }
        set {
            defaults.set(newValue, forKey: UD_KEY_LANG)
            defaults.synchronize()
        }
    }
    
    func getGender() -> Int {
        return self.genderType
    }
    
    func getAge() -> Int {
        return self.ageType
    }
    
    func getLang() -> String {
        return self.language
    }
    
}
