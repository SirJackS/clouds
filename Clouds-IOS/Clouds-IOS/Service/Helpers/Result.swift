//
//  Result.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 01.12.20.
//

import Foundation

enum Result<SuccessValue, WrongValue> {
    case successNoContent
    case unauthenticated
    case success(SuccessValue)
    case wrong(WrongValue)
    case failure(Error)
}
