//
//  OrderResponse.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 28.12.20.
//

import Foundation

struct OrderResponse: Decodable {
    var icon: String
    var title: String
    var desc: String
}
