//
//  PickerModel.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 21.12.20.
//

import Foundation

struct PickerModel: Decodable {
    var name: String
    var inStock: Bool
}
