//
//  ProfileModel.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import Foundation

struct ProfileModel: Decodable {
    var icon: String
    var title: String
}
