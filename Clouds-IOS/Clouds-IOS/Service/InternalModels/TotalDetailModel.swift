//
//  TotalDetailModel.swift
//  Clouds-IOS
//
//  Created by  Cavidan Memmedli on 22.12.20.
//

import Foundation

struct TotalDetailModel: Decodable {
    var title: String
    var total: Float
}
